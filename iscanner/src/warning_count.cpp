/*
    File:    warning_count.cpp
    Created: 11 April 2021 at 17:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/warning_count.hpp"

void Warning_count::increment_number_of_warnings()
{
    number_of_warnings_++;
}

size_t Warning_count::get_number_of_warnings() const
{
    return number_of_warnings_;
}

void Warning_count::print() const
{
    printf("\nTotal warnings: %zu\n", number_of_warnings_);
}