/*
    File:    position.cpp
    Created: 11 April 2021 at 17:39 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/position.hpp"
namespace iscaner{
    bool operator==(const Position& lhs, const Position& rhs)
    {
        return (lhs.line_no_ == rhs.line_no_) && (lhs.line_pos_ == rhs.line_pos_);
    }

    bool operator==(const Position_range& lhs, const Position_range& rhs)
    {
        bool t = (lhs.begin_pos_ == rhs.begin_pos_) && (lhs.end_pos_ == rhs.end_pos_);
        if(!t){
            puts("Positions of two tokens are not equal.");
            printf("First (actual) position is     %zu:%zu--%zu:%zu\n",
                   lhs.begin_pos_.line_no_, lhs.begin_pos_.line_pos_,
                   lhs.end_pos_.line_no_,   lhs.end_pos_.line_pos_);
            printf("Second (reference) position is %zu:%zu--%zu:%zu\n",
                   rhs.begin_pos_.line_no_, rhs.begin_pos_.line_pos_,
                   rhs.end_pos_.line_no_,   rhs.end_pos_.line_pos_);
        }
        return (lhs.begin_pos_ == rhs.begin_pos_) && (lhs.end_pos_ == rhs.end_pos_);
    }
}