/*
    File:    error_count.cpp
    Created: 11 April 2021 at 17:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/error_count.hpp"

void Error_count::increment_number_of_errors()
{
    number_of_errors_++;
}

size_t Error_count::get_number_of_errors() const
{
    return number_of_errors_;
}

void Error_count::print() const
{
    printf("\nTotal errors: %zu.\n", number_of_errors_);
}