/*
    File:    position.hpp
    Created: 11 April 2021 at 17:35 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
namespace iscaner{
    struct Position{
        Position()                           = default;
        Position(const Position&)            = default;
        ~Position()                          = default;
        Position& operator=(const Position&) = default;

        Position(size_t line_no, size_t line_pos) :
            line_no_(line_no), line_pos_(line_pos) {}

        size_t line_no_  = 1;
        size_t line_pos_ = 1;
    };

    struct Position_range{
        Position begin_pos_;
        Position end_pos_;
    };

    bool operator==(const Position& lhs, const Position& rhs);
    bool operator==(const Position_range& lhs, const Position_range& rhs);
}