/*
    File:    error_count.hpp
    Created: 11 April 2021 at 17:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>

/* A class for calculating the number of errors. */
class Error_count{
public:
    Error_count() : number_of_errors_(0) {};
    void   increment_number_of_errors();
    void   print() const;
    size_t get_number_of_errors() const;
private:
    size_t number_of_errors_;
};