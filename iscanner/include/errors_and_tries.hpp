/*
    File:    errors_and_tries.hpp
    Created: 11 April 2021 at 17:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>
#include "../../strings-utils/include/char_trie.hpp"
#include "../include/error_count.hpp"
#include "../include/warning_count.hpp"

struct Errors_and_tries{
    Errors_and_tries()                                    = default;
    ~Errors_and_tries()                                   = default;
    Errors_and_tries(const Errors_and_tries&)             = default;
    Errors_and_tries& operator=(const Errors_and_tries&)  = default;
    Errors_and_tries& operator=(Errors_and_tries&&)       = default;

    void print() const;

    std::shared_ptr<Error_count>              ec_;
    std::shared_ptr<Warning_count>            wc_;
    std::shared_ptr<strings::trie::Char_trie> ids_trie_;
    std::shared_ptr<strings::trie::Char_trie> strs_trie_;
};