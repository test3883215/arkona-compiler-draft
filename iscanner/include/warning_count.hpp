/*
    File:    warning_count.hpp
    Created: 11 April 2021 at 17:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>

/* A class for calculating the number of warnings. */
class Warning_count{
public:
    Warning_count() : number_of_warnings_(0) {};
    void   increment_number_of_warnings();
    void   print() const;
    size_t get_number_of_warnings() const;
private:
    size_t number_of_warnings_;
};