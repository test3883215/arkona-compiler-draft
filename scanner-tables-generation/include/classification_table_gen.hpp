/*
    File:    classification_table_gen.hpp
    Created: 12 April 2023 at 20:08 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>
struct Classification_table_info
{
    std::string category_header_;
    std::string table_;
};

Classification_table_info build_classification_table(const std::u32string& delimiter_begin_chars,
                                                     const std::u32string& keyword_begin_chars);