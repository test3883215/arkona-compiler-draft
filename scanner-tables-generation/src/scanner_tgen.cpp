/*
    File:    scanner_tgen.cpp
    Created: 12 April 2023 at 20:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../../file-utils/include/get_processed_text.hpp"
#include "../../kwdelim-table-gen-lib/include/kw_delim_table_gen.hpp"
#include "../../strings-utils/include/config_from_text.hpp"
#include "../include/classification_table_gen.hpp"
#include <iostream>
#include <fstream>
#include <optional>
#include <string>

static const std::string scanner_include_dir = "./scanner/include/";
static const std::string scanner_src_dir     = "./scanner/src/";

enum Return_codes
{
    Success, Not_enough_arguments, Error_with_configs
};

namespace{
    const char* usage_str = R"~(scanner-tgen
Copyright (c) Gavrilov V.S., 2023
test-parserlib is a program for a testing of parsing of the programming language Arkona.

This program is free sofwtware, and it is licensed under the GPLv3 license.
There is NO warranty, not even MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Usage: )~";
    void usage(char* name)
    {
        std::cout << usage_str << name << " keywords-file delimiters-file\n";
    }
    
    std::optional<Text_configs> get_text_config_from_files(const char* keywords_file, const char* delimiters_file)
    {
        std::cout << "Getting data from keywords config file " << keywords_file << "\n";
        const auto keywords_text = file_utils::get_processed_text(keywords_file);
        if(keywords_text.empty())
        {
            return {};
        }

        std::cout << "Getting data from delimiters config file " << delimiters_file << "\n";
        const auto delimiters_text = file_utils::get_processed_text(delimiters_file);
        if(delimiters_text.empty())
        {
            return {};
        }
        
        const auto keywords_config   = strings::config::get_config_from_text(keywords_text);
        const auto delimiters_config = strings::config::get_config_from_text(delimiters_text);
        
        return Text_configs{keywords_config, delimiters_config};
    }
};

int main(int argc, char* argv[])
{
    if(argc < 3)
    {
        usage(argv[0]);
        return Not_enough_arguments;
    }
    
    const auto configs = get_text_config_from_files(argv[1], argv[2]);

    if(!configs)
    {
        return Error_with_configs;
    }

    const auto kw_delim_tables = build_kw_delim_tables_from_text_config(*configs);

    for(auto&& [name, content] : kw_delim_tables.includes_)
    {
        const auto file_name = scanner_include_dir + name;
        std::ofstream fout{file_name};
        fout << content;
    }

    for(auto&& [name, content] : kw_delim_tables.srcs_)
    {
        const auto file_name = scanner_src_dir + name;
        std::ofstream fout{file_name};
        fout << content;
    }

    const auto classification_table = build_classification_table(kw_delim_tables.delimiter_begin_chars_,
                                                                 kw_delim_tables.keyword_begin_chars_);
    {
        const auto file_name = scanner_include_dir + "category.hpp";
        std::ofstream fout{file_name};
        fout << classification_table.category_header_;
    }
    {
        const std::string file_name = scanner_include_dir + "table.hpp";
        std::ofstream fout{file_name};
        fout << classification_table.table_;
    }
    return Success;
}