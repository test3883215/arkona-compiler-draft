/*
    File:    classification_table_gen.cpp
    Created: 12 April 2023 at 20:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/classification_table_gen.hpp"
#include "../../classification-table-gen-lib/include/table_gen.hpp"
#include <array>
#include <cstdint>
#include <map>

static std::u32string spaces_str()
{
    std::u32string s;
    for(char32_t c = 1; c <= U' '; c++){
        s += c;
    }
    return s;
}

static const char32_t* id_begin_chars          =
    U"_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    U"ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуф"
    U"хцчшщъыьэюяѐёђѓєѕіїјљњћќўџѠѡѢѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿҀҁҊҋҌҍҎҏҐґ"
    U"ҒғҔҕҖҗҘҙҚқҜҝҞҟҠҡҢңҤҥҦҧҨҩҪҫҬҭҮүҰұҲҳҴҵҶҷҸҹҺһҼҽҾҿӀӁӂӀӃӄӅӆӇӈӉӊӋӌӍӎӏӐӑӒӓӔӕ"
    U"ӖӗӘәӚӛӜӝӞӟӠӡӢӣӤӥӦӧӨөӪӫӬӭӮӯӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ";
static const char32_t* id_body_chars           =
    U"_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    U"ЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуф"
    U"хцчшщъыьэюяѐёђѓєѕіїјљњћќўџѠѡѢѣѤѥѦѧѨѩѪѫѬѭѮѯѰѱѲѳѴѵѶѷѸѹѺѻѼѽѾѿҀҁҊҋҌҍҎҏҐґ"
    U"ҒғҔҕҖҗҘҙҚқҜҝҞҟҠҡҢңҤҥҦҧҨҩҪҫҬҭҮүҰұҲҳҴҵҶҷҸҹҺһҼҽҾҿӀӁӂӀӃӄӅӆӇӈӉӊӋӌӍӎӏӐӑӒӓӔӕ"
    U"ӖӗӘәӚӛӜӝӞӟӠӡӢӣӤӥӦӧӨөӪӫӬӭӮӯӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ";
static const char32_t* double_quote_chars      =
    U"\"";
static const char32_t* single_quote_chars      =
    U"\'";
static const char32_t* letters_Xx_chars        =
    U"Xx";
static const char32_t* letters_Bb_chars        =
    U"Bb";
static const char32_t* letters_Oo_chars        =
    U"Oo";
static const char32_t* dollar_chars            =
    U"$";
static const char32_t* hex_digits_chars        =
    U"0123456789ABCDEFabcdef";
static const char32_t* oct_digits_chars        =
    U"01234567";
static const char32_t* bin_digits_chars        =
    U"01";
static const char32_t* dec_digits_chars        =
    U"0123456789";
static const char32_t* zero_chars              =
    U"0";
static const char32_t* letters_Ee_chars        =
    U"Ee";
static const char32_t* plus_minus_chars        =
    U"+-";
static const char32_t* precision_letters_chars =
    U"fdxq";
static const char32_t* digits_1_to_9_chars     =
    U"123456789";
static const char32_t* point_chars             =
    U".";
static const char32_t* quat_suffix_chars       =
    U"ijk";

enum class MyCategory : uint64_t{
    Spaces,           Id_begin,        Id_body,
    Keyword_begin,    Delimiter_begin, Double_quote,
    Letters_Xx,       Letters_Bb,      Letters_Oo,
    Single_quote,     Dollar,          Hex_digit,
    Oct_digit,        Bin_digit,       Dec_digit,
    Zero,             Letters_Ee,      Plus_minus,
    Precision_letter, Digits_1_to_9,   Point,
    Quat_suffix,      Other
};

static const Info_for_category_header header_info = {
    U"scanner",
    U"Гаврилов Владимир Сергеевич",
    {
        U"vladimir.s.gavrilov@gmail.com",
        U"gavrilov.vladimir.s@mail.ru"
    },
    U"GPLv3"
};

static constexpr uint32_t start_of_egyptian_hierogliphs_block = 0x1'3000ULL;
static constexpr uint32_t end_of_egyptian_hierogliphs_block   = 0x1'342EULL;
static constexpr uint32_t number_of_hierogliphs               = end_of_egyptian_hierogliphs_block - start_of_egyptian_hierogliphs_block + 1;

static char32_t egyptian_hierogliphs[number_of_hierogliphs + 1];

static void fill_egyptian_hierogliphs_array()
{
    for(uint32_t h = start_of_egyptian_hierogliphs_block; h <= end_of_egyptian_hierogliphs_block; ++h)
    {
        egyptian_hierogliphs[h - start_of_egyptian_hierogliphs_block] = h;
    }
    egyptian_hierogliphs[number_of_hierogliphs] = 0;
}

static std::u32string get_egyptian_hierogliphs()
{
    fill_egyptian_hierogliphs_array();
    return std::u32string{egyptian_hierogliphs};
}

static constexpr uint32_t start_of_cuineform_block1      = 0x1'2000ULL;
static constexpr uint32_t end_of_cuineform_block1        = 0x1'2399ULL;
static constexpr uint32_t number_of_cuineforms_in_block1 = end_of_cuineform_block1 - start_of_cuineform_block1 + 1;

static char32_t cuineforms_of_block1[number_of_cuineforms_in_block1 + 1];

static constexpr uint32_t start_of_cuineform_block2      = 0x1'2400ULL;
static constexpr uint32_t end_of_cuineform_block2        = 0x1'2474ULL;
static constexpr uint32_t number_of_cuineforms_in_block2 = end_of_cuineform_block2 - start_of_cuineform_block2 + 1;

static char32_t cuineforms_of_block2[number_of_cuineforms_in_block2 + 1];

static constexpr uint32_t start_of_cuineform_block3      = 0x1'2480ULL;
static constexpr uint32_t end_of_cuineform_block3        = 0x1'2543ULL;
static constexpr uint32_t number_of_cuineforms_in_block3 = end_of_cuineform_block3 - start_of_cuineform_block3 + 1;

static char32_t cuineforms_of_block3[number_of_cuineforms_in_block3 + 1];

static void fill_cuineforms_arrays()
{
    for(uint32_t c = start_of_cuineform_block1; c <= end_of_cuineform_block1; ++c)
    {
        cuineforms_of_block1[c - start_of_cuineform_block1] = c;
    }
    cuineforms_of_block1[number_of_cuineforms_in_block1] = 0;

    for(uint32_t c = start_of_cuineform_block2; c <= end_of_cuineform_block2; ++c)
    {
        cuineforms_of_block2[c - start_of_cuineform_block2] = c;
    }
    cuineforms_of_block2[number_of_cuineforms_in_block2] = 0;

    for(uint32_t c = start_of_cuineform_block3; c <= end_of_cuineform_block3; ++c)
    {
        cuineforms_of_block3[c - start_of_cuineform_block3] = c;
    }
    cuineforms_of_block3[number_of_cuineforms_in_block3] = 0;
}

static std::u32string get_cuineforms()
{
    fill_cuineforms_arrays();

    const auto s1 = std::u32string{cuineforms_of_block1};
    const auto s2 = std::u32string{cuineforms_of_block2};
    const auto s3 = std::u32string{cuineforms_of_block3};

    return s1 + s2 + s3;
}

static const char32_t* latin_extension = U"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ"
                                         U"ĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĸĹĺĻļĽ"
                                         U"ľĿŀŁłŃńŅņŇňŉŊŋŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻ"
                                         U"żŽžſƀƁƂƃƄƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯưƱƲƳƴƵƶƷƸƹ"
                                         U"ƺƻƼƽƾƿǀǁǂǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹ"
                                         U"ǺǻǼǽǾǿȀȁȂȃȄȅȆȇȈȉȊȋȌȍȎȏȐȑȒȓȔȕȖȗȘșȚțȜȝȞȟȠȡȢȣȤȥȦȧȨȩȪȫȬȭȮȯȰȱȲȳȴȵȶȷȸ"
                                         U"ȹȺȻȼȽȾȿɀɁɂɃɄɅɆɇɈɉɊɋɌɍɎɏ"
                                         U"ⱠⱡⱢⱣⱤⱥⱦⱧⱨⱩⱪⱫⱬⱭⱮⱯⱰⱱⱲⱳⱴⱵⱶⱷⱸⱹⱺⱻⱼⱽⱾⱿ"
                                         U"ꬰꬱꬲꬳꬴꬵꬶꬷꬸꬹꬺꬻꬼꬽꬾꬿꭀꭁꭂꭃꭗꭖꭕꭔꭓꭒꭑꭐꭏꭎꭍꭌꭋꭊꭉꭈꭇꭆꭅꭄꭘꭙꭚ꭛ꭜꭝꭞꭟꭠꭡꭢꭣꭤꭥꭦꭧꭨꭩ꜠꜡ꜢꜣꜤꜥꜦꜧꜨꜩꜪꜫꜬꜭꜮ"
                                         U"ꜯꜰꜱꜲꜳꝇꝆꝅꝄꝃꝂꝁꝀꜿꜾꜽꜼꜻꜺꜹꜸꜷꜶꜵꜴꝈꝉꝊꝋꝌꝍꝎꝏꝐꝑꝒꝓꝔꝕꝖꝗꝘꝙꝚꝛꝯꝮꝭꝬꝿꝫꞀꞁꞂꞃꝾꝪꝩꝽꝼꝨ"
                                         U"ꝻꝧꝦꝺꝹꝥꝤꝸꝷꝣꝶꝢꝡꝵꝴꝠꝟꝳꝲꝞꝱꝝꝜꝰꞄꞅꞆꞇꞋꞌꞍꞎꞏꞐꞑꞒꞓꞔꞕꞖꞗꞫꞪꞩꞨꞧꞦꞥꞤꞣꞢꞡꞠꞟꞞꞝꞜꞛꞚꞙꞘꞬꞭꞮꞯꞰꞱꞲ"
                                         U"ꞳꞴꞵꞶꞷꞸꞹꞺꞻꞼꞽꞾꞿꟂꟃꟄꟅꟆꟇꟈꟉꟊꟵꟶꟷꟸꟹꟺꟻꟼꟽꟾꟿ"
                                         U"ḀḁḂḃḄḅḆḇḈḉḊḋḌḍḎḏḐḑḒḓḔḕḖḗḘḙḚḛḜḝḞḟḠḡḢḣḤḥḦḧḨḩḪḫḬḭḮḯḰḱḲḳḴḵḶḷḸḹḺḻḼḽḾḿ"
                                         U"ṀṁṂṃṄṅṆṇṈṉṊṋṌṍṎṏṐṑṒṓṔṕṖṗṘṙṚṛṜṝṞṟṠṡṢṣṤṥṦṧṨṩṪṫṬṭṮṯṰṱṲṳṴṵṶṷṸṹṺṻṼṽṾṿ"
                                         U"ẀẁẂẃẄẅẆẇẈẉẊẋẌẍẎẏẐẑẒẓẔẕẖẗẘẙẚẛẜẝẞẟẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀề"
                                         U"ỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹỺỻỼỽỾỿ";

static const char32_t* greek = U"ͰͱͲͳͶͷͻͼͽͿΆΈΉΊΌΎΏΐΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΪΫάέήίΰαβγδεζηθικλμνξοπρςστυφχψω"
                               U"ϊϋόύώϏϐϑϒϓϔϕϖϗϘϙϚϛϜϝϞϟϠϡϢϣϤϥϦϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ϶ϷϸϹϺϻϼϽϾϿ"
                               U"ἀἁἂἃἄἅἆἇἈἉἊἋἌἍἎἏἐἑἒἓἔἕἘἙἚἛἜἝἠἡἢἣἤἥἦἧἻἺἹἸἷἶἵἴἳἲἱἰἯἮἭἬἫἪἩἨἼὐὤὸᾌᾠᾴῈἽἾἿὀὁὑὒὓὔὕ"
                               U"ὥὦὧὨὩόὺύὼώᾍᾎᾏᾐᾑᾡᾢᾣᾤᾥᾶᾷᾸᾹΈῊΉῌῠῡῴῳῲὂὖὪᾒᾦᾺῢῶῷΰΆᾧᾓὫὗὬᾀᾔᾨᾼῐῤῸΌῥῑᾩᾕᾁὭὙὮᾂᾖᾪῒῦῺὛὯᾃ"
                               U"ᾗᾫΐῧΏὰᾄᾘᾬῨῼὝάᾅᾙᾭῩὲᾆᾚᾮῂῖῪὟέᾇᾛᾯῃῗΎὠὴᾈᾜᾰῄῘῬῙᾱᾝᾉήὡὢὶᾊᾞᾲῆῚὣίᾋᾟᾳῇΊ";

static const char32_t* glagolitic = U"ⰀⰁⰂⰃⰄⰅⰆⰇⰈⰉⰊⰋⰌⰍⰎⰏⰐⰑⰒⰓⰔⰕⰖⰗⰘⰙⰚⰛⰜⰝⰞⰟⰠ"
                                    U"ⰡⰢⰣⰤⰥⰦⰧⰨⰩⰪⰫⰬⰭⰮⰰⰱⰲⰳⰴⰵⰶⰷⰸⰹⰺⰻⰼⰽⰾⰿⱀⱁⱂⱃⱄⱅⱆⱇⱈⱉⱊⱋⱌⱍⱎⱏⱐⱑⱒⱓⱔⱕⱖⱗⱘⱙⱚⱛⱜⱝⱞ";

Classification_table_info build_classification_table(const std::u32string& delimiter_begin_chars,
                                                     const std::u32string& keyword_begin_chars)
{
    Classification_table_info result;

    const auto egyptian_hierogliphs_chars = get_egyptian_hierogliphs();
    const auto cuineforms                 = get_cuineforms();
    const auto l_ext                      = std::u32string{latin_extension};
    const auto greek_and_coptic_chars     = std::u32string{greek};
    const auto glagolitic_chars           = std::u32string{glagolitic};

    std::map<std::u32string, Category_info> categories = {
        {U"Spaces",           {static_cast<uint64_t>(MyCategory::Spaces),           spaces_str()                    }},
        {U"Id_begin",         {static_cast<uint64_t>(MyCategory::Id_begin),         std::u32string{id_begin_chars} + 
                                                                                    egyptian_hierogliphs_chars     + 
                                                                                    cuineforms                     +
                                                                                    l_ext                          +
                                                                                    greek_and_coptic_chars         + 
                                                                                    glagolitic_chars}},
        {U"Id_body",          {static_cast<uint64_t>(MyCategory::Id_body),          std::u32string{id_body_chars} +
                                                                                    egyptian_hierogliphs_chars    +
                                                                                    cuineforms                    +
                                                                                    l_ext                         +
                                                                                    greek_and_coptic_chars        + 
                                                                                    glagolitic_chars}},
        {U"Keyword_begin",    {static_cast<uint64_t>(MyCategory::Keyword_begin),    keyword_begin_chars             }},
        {U"Delimiter_begin",  {static_cast<uint64_t>(MyCategory::Delimiter_begin),  delimiter_begin_chars           }},
        {U"Double_quote",     {static_cast<uint64_t>(MyCategory::Double_quote),     double_quote_chars              }},
        {U"Letters_Xx",       {static_cast<uint64_t>(MyCategory::Letters_Xx),       letters_Xx_chars                }},
        {U"Letters_Bb",       {static_cast<uint64_t>(MyCategory::Letters_Bb),       letters_Bb_chars                }},
        {U"Letters_Oo",       {static_cast<uint64_t>(MyCategory::Letters_Oo),       letters_Oo_chars                }},
        {U"Single_quote",     {static_cast<uint64_t>(MyCategory::Single_quote),     single_quote_chars              }},
        {U"Dollar",           {static_cast<uint64_t>(MyCategory::Dollar),           dollar_chars                    }},
        {U"Hex_digit",        {static_cast<uint64_t>(MyCategory::Hex_digit),        hex_digits_chars                }},
        {U"Oct_digit",        {static_cast<uint64_t>(MyCategory::Oct_digit),        oct_digits_chars                }},
        {U"Bin_digit",        {static_cast<uint64_t>(MyCategory::Bin_digit),        bin_digits_chars                }},
        {U"Dec_digit",        {static_cast<uint64_t>(MyCategory::Dec_digit),        dec_digits_chars                }},
        {U"Zero",             {static_cast<uint64_t>(MyCategory::Zero),             zero_chars                      }},
        {U"Letters_Ee",       {static_cast<uint64_t>(MyCategory::Letters_Ee),       letters_Ee_chars                }},
        {U"Plus_minus",       {static_cast<uint64_t>(MyCategory::Plus_minus),       plus_minus_chars                }},
        {U"Precision_letter", {static_cast<uint64_t>(MyCategory::Precision_letter), precision_letters_chars         }},
        {U"Digits_1_to_9",    {static_cast<uint64_t>(MyCategory::Digits_1_to_9),    digits_1_to_9_chars             }},
        {U"Point",            {static_cast<uint64_t>(MyCategory::Point),            point_chars                     }},
        {U"Quat_suffix",      {static_cast<uint64_t>(MyCategory::Quat_suffix),      quat_suffix_chars               }},
    };

    result.category_header_ = build_category_include(categories, header_info);
    result.table_           = build_category_table(categories);

    return result;
}