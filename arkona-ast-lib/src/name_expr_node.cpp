/*
    File:    name_expr_node.cpp
    Created: 23 August 2023 at 19:27 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/name_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* name_expr_fmt = R"~({0}NAME_EXPR
{1}BASE {2}
{1}SUFFIXES
{3}
{1}END_SUFFIXES
{1}FIELD_VALUES
{4}
{1}END_FIELD_VALUES
{0}END_NAME_EXPR)~";

    const char* suffix_fmt = R"~({0}SUFFIX
{1}
{0}END_SUFFIX)~";
}

namespace ast{
    NodeInfo Name_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Name_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Name_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                  const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                  const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto base_part_str = ast::to_string(base_, ids_trie);

        const auto suffixes_str = strings::join::join([&ids_trie, &strs_trie, &nested_indent3, &indent1_str, &indent2_str](const auto& s) -> std::string
                                                      {
                                                          const auto suffix_str   = s->to_string(strs_trie, ids_trie, nested_indent3);
                                                          
                                                          return fmt::format(suffix_fmt, 
                                                                             indent2_str, 
                                                                             s->to_string(strs_trie, ids_trie, nested_indent3));
                                                      }, 
                                                      suffixes_.begin(), 
                                                      suffixes_.end(), 
                                                      std::string{"\n"});

        const auto field_values_str = field_values_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(name_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           base_part_str,
                           suffixes_str, 
                           field_values_str);
    }

    std::shared_ptr<Node> Name_expr_node::clone() const
    {
        return std::make_shared<Name_expr_node>(base_, suffixes_, field_values_);
    }    
}