/*
    File:    literal_expr_node.cpp
    Created: 22 August 2023 at 19:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/literal_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings-utils/include/idx_to_string.hpp"
#include "../../numbers/include/float128_to_string.hpp"
#include "../../numbers/include/int128_to_str.hpp"
#include "../../char-conv/include/char_conv.hpp"
#include "../../char-conv/include/print_char32.hpp"

namespace{
    const char* literal_expr_fmt = R"~({0}LITERAL_EXPR
{1}LITERAL_KIND {2}
{1}LITERAL_SUBKIND: {3}
{1}LITERAL_VALUE
{4}
{1}END_LITERAL_VALUE
{1}LITERAL_POSITION {5}
{0}END_LITERAL_EXPR)~";

    const char* pos_fmt = R"~({{POSITION: [line: [0], pos: {1}]--[line: {2}, pos: {3}]}})~";

    const char* literal_kinds[] = {
        "Char",  "String",  "Integer",
        "Float", "Complex", "Quat",
        "Bool",  "Ord",     "Void"
    };

    const char* char_kinds[] = {
        "Char8", "Char16", "Char32"
    };

    const char* string_kinds[] = {
        "String8", "String16", "String32"
    };

    const char* integer_kinds[] = {
        ""
    };

    const char* float_kinds[] = {
        "Float32", "Float64", "Float80", "Float128"
    };

    const char* complex_kinds[] = {
        "Complex32", "Complex64", "Complex80", "Complex128"
    };

    const char* quat_kinds[] = {
        "Quat32", "Quat64", "Quat80", "Quat128"
    };

    const char* bool_kinds[] = {
        ""
    };

    const char* ord_kinds[] = {
        ""
    };

    const char* void_kinds[] = {
        ""
    };

    const char** subkinds[] = {
        char_kinds,  string_kinds,  integer_kinds,
        float_kinds, complex_kinds, quat_kinds,
        bool_kinds,  ord_kinds,     void_kinds
    };

    const char* orderings[] = {
        "Less", "Equal", "Greater"
    };
}

namespace ast{
    NodeInfo Literal_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Literal_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Literal_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                             const std::shared_ptr<strings::trie::Char_trie>&,
                                             const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto& p       = pos_;
        const auto& b       = p.begin_pos_;
        const auto& e       = p.end_pos_;
        const auto  pos_str = fmt::format(pos_fmt, b.line_no_, b.line_pos_, e.line_no_, e.line_pos_);

        const auto  lc      = value_.code_;
        const auto  lk      = lc.kind_;
        const auto  lsk     = lc.subkind_;
        
        const auto  lk_str  = literal_kinds[static_cast<unsigned>(lk)];
        const auto  lsk_str = subkinds[static_cast<unsigned>(lk)][static_cast<unsigned>(lsk)];

        std::string value_str;

        switch(lk){
            case Literal_kind::Char:
                value_str = show_char32(value_.char_val_);
                break;
            case Literal_kind::String:
                value_str = idx_to_string(strs_trie, value_.str_index_);
                break;
            case Literal_kind::Integer:
                value_str = ::to_string(value_.int_val_);
                break;
            case Literal_kind::Float:
                value_str = ::to_string(value_.float_val_);
                break;
            case Literal_kind::Complex:
                value_str = ::to_string(crealq(value_.complex_val_)) +
                            " + "                                +
                            ::to_string(cimagq(value_.complex_val_)) +
                            "i";
                break;
            case Literal_kind::Quat:
                value_str = ::to_string(value_.quat_val_[0]) +
                            ::to_string(value_.quat_val_[1]) + "i" +
                            ::to_string(value_.quat_val_[2]) + "j" +
                            ::to_string(value_.quat_val_[3]) + "k";
                break;
            case Literal_kind::Bool:
                value_str = value_.bool_value_ ? "TRUE" : "FALSE";
                break;
            case Literal_kind::Ord:
                value_str = orderings[static_cast<unsigned>(value_.ord_value_)];
            default:
                ;
        }

        return fmt::format(literal_expr_fmt,
                           indent_str, 
                           indent1_str,
                           lk_str,
                           lsk_str,
                           value_str,
                           pos_str);
    }

    std::shared_ptr<Node> Literal_expr_node::clone() const
    {
        return std::make_shared<Literal_expr_node>(value_, pos_);
    }    
}