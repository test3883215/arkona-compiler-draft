/*
    File:    assignment_expr_node.cpp
    Created: 06 August 2023 at 17:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/assignment_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* assignment_expr_fmt = R"~({0}ASSIGNMENT_EXPR
{1}ASSIGNMENT_KIND {2}
{1}LHS
{3}
{1}END_LHS
{1}RHS
{4}
{1}END_RHS
{0}END_ASSIGNMENT_EXPR)~";

    const char* assignment_kinds[] = {
        "Assignment",             "Copy",                        "Logical_and_assign",       "Logical_and_full_assign", 
        "Logical_and_not_assign", "Logical_and_not_full_assign", "Logical_or_assign",        "Logical_or_full_assign",
        "Logical_or_not_assign",  "Logical_or_not_full_assign",  "Logical_xor_assign",       "Bitwise_and_assign",
        "Bitwise_and_not_assign", "Bitwise_or_assign",           "Bitwise_or_not_assign",    "Bitwise_xor_assign",
        "Left_shift_assign",      "Right_shift_assign",          "Cyclic_left_shift_assign", "Cyclic_right_shift_assign",
        "Plus_assign",            "Minus_assign",                "Type_add_assign",          "Algebraic_separator_assign",
        "Mul_assign",             "Div_assign",                  "Set_difference_assign",    "Symmetric_difference_assign",
        "Remainder_assign",       "Float_remainder_assign",      "Power_assign",             "Float_power_assign",
    };
}

namespace ast{
    NodeInfo Assignment_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Assignment_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Assignment_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(assignment_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           assignment_kinds[static_cast<unsigned>(a_kind_)],
                           lhs_str,
                           rhs_str);
    }

    std::shared_ptr<Node> Assignment_expr_node::clone() const
    {
        return std::make_shared<Assignment_expr_node>(lhs_, rhs_, a_kind_);
    }
}