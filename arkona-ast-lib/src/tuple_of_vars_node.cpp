/*
    File:    tuple_of_vars_node.cpp
    Created: 17 July 2023 at 22:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/tuple_of_vars_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* tuple_of_vars_fmt = R"~({0}TUPLE_OF_VARS
{1}VARS_OF_TUPLE
{2}
{1}END_VARS_OF_TUPLE
{1}TUPLE_OF_VARS_TYPE
{3}
{1}END_TUPLE_OF_VARS_TYPE
{1}TUPLE_OF_VARS_VALUE
{4}
{1}END_TUPLE_OF_VARS_VALUE
{0}END_TUPLE_OF_VARS)~";
}

namespace ast{
    NodeInfo Tuple_of_vars_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Tuple_of_vars;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Tuple_of_vars_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto vars_str = strings::join::join([&indent2_str, &ids_trie](const auto& info) -> std::string
                                                  {
                                                      return indent2_str + ast::to_string(info, ids_trie);
                                                  }, 
                                                  var_infos_.begin(), 
                                                  var_infos_.end(), 
                                                  std::string{"\n"});

        const auto tuple_type_str  = tuple_type_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto tuple_value_str = tuple_value_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(tuple_of_vars_fmt, 
                           indent_str, 
                           indent1_str, 
                           vars_str, 
                           tuple_type_str, 
                           tuple_value_str);;
    }

    std::shared_ptr<Node> Tuple_of_vars_node::clone() const
    {
        return std::make_shared<Tuple_of_vars_node>(var_infos_, tuple_type_, tuple_value_);
    }
}