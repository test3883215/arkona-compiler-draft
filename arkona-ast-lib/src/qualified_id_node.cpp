/*
    File:    qualified_id_node.cpp
    Created: 24 June 2023 at 21:11 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <vector>
#include "../include/qualified_id_node.hpp"
#include "../../strings-utils/include/idx_to_string.hpp"
#include "../../strings-utils/include/join.hpp"

namespace ast{
    NodeInfo Qualified_id_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Qualified_id;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Qualified_id_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                             const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                             const Indents&                                   indents) const
    {
        std::string result;

        const auto q_id_str = strings::join::join([&ids_trie](const auto& c) -> std::string
                                                  {
                                                      return strings::trie::idx_to_string(ids_trie, c.identifier_id_);
                                                  },
                                                  components_.begin(),
                                                  components_.end(),
                                                  std::string{"::"});

        result = std::string(indents.indent_, ' ') + "QUALIFIED_ID " + q_id_str;

        return result;
    }
    
    std::shared_ptr<Node> Qualified_id_node::clone() const
    {
        return std::make_shared<Qualified_id_node>(components_);
    }
}