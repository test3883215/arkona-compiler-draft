/*
    File:    range_expr_node.cpp
    Created: 10 August 2023 at 20:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/range_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* range_expr_fmt = R"~({0}RANGE_EXPR
{1}RANGE_KIND {2}
{1}START_VALUE
{3}
{1}END_START_VALUE
{1}END_VALUE
{4}
{1}END_END_VALUE
{1}STEP_VALUE
{5}
{1}END_STEP_VALUE
{0}END_RANGE_EXPR)~";

    const char* range_kinds[] = {
        "Including_end", "Excluding_end"
    };
}

namespace ast{
    NodeInfo Range_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Range_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Range_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                           const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                           const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto start_value_str  = start_value_ ->to_string(strs_trie, ids_trie, nested_indent2);
        const auto end_value_str    = end_value_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto step_value_str   = step_value_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(range_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           start_value_str,
                           end_value_str,
                           step_value_str,
                           range_kinds[static_cast<unsigned>(r_kind_)]);
    }

    std::shared_ptr<Node> Range_expr_node::clone() const
    {
        return std::make_shared<Range_expr_node>(start_value_, end_value_, step_value_, r_kind_);
    }
}