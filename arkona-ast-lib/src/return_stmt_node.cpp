/*
    File:    return_stmt_node.cpp
    Created: 10 July 2023 at 20:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/return_stmt_node.hpp"
#include "../../strings-utils/include/idx_to_string.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* return_stmt_fmt = R"~({0}RETURN_STMT
{1}RETURNED_EXPR
{2}
{1}END_RETURNED_EXPR
{0}END_RETURN_STMT)~";
}

namespace ast{
    NodeInfo Return_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Return_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Return_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                            const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                            const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        std::string expr_str;
        if(returned_expr_)
        {
            expr_str = returned_expr_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        return fmt::format(return_stmt_fmt, indent1_str, indent1_str, expr_str);
    }
    
    std::shared_ptr<Node> Return_stmt_node::clone() const
    {
        return std::make_shared<Return_stmt_node>(returned_expr_);
    }
}