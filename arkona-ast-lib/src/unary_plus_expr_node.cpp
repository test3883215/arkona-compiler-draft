/*
    File:    unary_plus_expr_node.cpp
    Created: 12 August 2023 at 17:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/unary_plus_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* unary_plus_expr_fmt = R"~({0}UNARY_PLUS_EXPR
{1}UNARY_PLUSES {2}
{1}OP
{3}
{1}END_OP
{0}END_UNARY_PLUS_EXPR)~";

    const char* unary_plus_kinds[] = {
        "Unary_plus",                  "Unary_minus",           "Get_expr_type",         "Minimal_value_in_collection",
        "Maximal_value_in_collection", "Minimal_value_of_type", "Maximal_value_of_type",
    };
}

namespace ast{
    NodeInfo Unary_plus_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Unary_plus_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Unary_plus_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto op_str = op_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto unary_pluses_str = strings::join::join([](const auto& i) -> std::string
                                                          {
                                                              return unary_plus_kinds[static_cast<unsigned>(i)];
                                                          }, 
                                                          unary_pluses_.begin(), 
                                                          unary_pluses_.end(), 
                                                          std::string{" "});

        return fmt::format(unary_plus_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           unary_pluses_str,
                           op_str);
    }

    std::shared_ptr<Node> Unary_plus_expr_node::clone() const
    {
        return std::make_shared<Unary_plus_expr_node>(op_, unary_pluses_);
    }
}