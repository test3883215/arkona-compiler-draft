/*
    File:    if_stmt_node.cpp
    Created: 05 July 2023 at 21:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/if_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings-utils/include/join.hpp"

namespace{
    static const char* if_stmt_fmt = R"~({0}IF_STMT
{1}
{0}END_IF)~";

    static const char* if_branch_fmt = R"~({0}IF
{1}
{0}THEN
{2})~";

    static const char* else_branch_fmt = R"~({0}ELSE
{1})~";

    static const char* elif_branch_fmt = R"~({0}ELIF
{1}
{0}THEN
{2})~";
}

namespace ast{
    NodeInfo If_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::If_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string If_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                        const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                        const Indents&                                   indents) const
    {
        const auto if_branch_to_str = [strs_trie, ids_trie, indents](const Branch& if_branch) -> std::string
                                      {
                                          Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
                                          Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

                                          const auto  nested_indent1_str = std::string(nested_indent1.indent_, ' ');
                                          const auto& [condition, block] = if_branch;

                                          std::string condition_str;
                                          std::string block_str;

                                          if(condition)
                                          {
                                              condition_str = condition->to_string(strs_trie, ids_trie, nested_indent2);
                                          }

                                          if(block)
                                          {
                                              block_str = block->to_string(strs_trie, ids_trie, nested_indent2);
                                          }

                                          return fmt::format(if_branch_fmt, nested_indent1_str, condition_str, block_str);
                                      };

        const auto if_branch_str = if_branch_to_str(if_branch_);

        const auto else_branch_to_str = [strs_trie, ids_trie, indents](const std::shared_ptr<Block_node>& else_branch) -> std::string
                                        {
                                            Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
                                            Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

                                            const auto  nested_indent1_str = std::string(nested_indent1.indent_, ' ');
                                            
                                            std::string block_str;
                                            if(else_branch)
                                            {
                                                block_str = else_branch->to_string(strs_trie, ids_trie, nested_indent2);
                                            }
                                            
                                            return fmt::format(else_branch_fmt, nested_indent1_str, block_str);
                                        };

        const auto else_branch_str = else_branch_to_str(else_branch_);

        const auto elif_branch_to_str = [strs_trie, ids_trie, indents](const Branch& elif_branch) -> std::string
                                        {
                                            Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
                                            Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

                                            const auto  nested_indent1_str = std::string(nested_indent1.indent_, ' ');

                                            const auto& [condition, block] = elif_branch;

                                            std::string condition_str;
                                            std::string block_str;

                                            if(condition)
                                            {
                                                condition_str = condition->to_string(strs_trie, ids_trie, nested_indent2);
                                            }

                                            if(block)
                                            {
                                                block_str = block->to_string(strs_trie, ids_trie, nested_indent2);
                                            }

                                            return fmt::format(elif_branch_fmt, nested_indent1_str, condition_str, block_str);
                                        };

        const auto elif_branches_str = strings::join::join(elif_branch_to_str, elif_branches_.begin(), elif_branches_.end(), std::string{"\n"});

        const auto indent_str = std::string(indents.indent_, ' ');

        std::string if_stmt_body;
        
        std::vector<std::string> strs;
        strs.push_back(if_branch_str);
        if(!elif_branches_str.empty())
        {
            strs.push_back(elif_branches_str);
        }
        strs.push_back(else_branch_str);

        return fmt::format(if_stmt_fmt, 
                           indent_str, 
                           strings::join::join(strs.begin(), strs.end(), std::string{"\n"}));
    }

    std::shared_ptr<Node> If_stmt_node::clone() const
    {
        return std::make_shared<If_stmt_node>(if_branch_, elif_branches_, else_branch_);
    }
}