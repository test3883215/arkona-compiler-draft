/*
    File:    block_stmt_node.cpp
    Created: 04 July 2023 at 20:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/block_stmt_node.hpp"

namespace ast{
    NodeInfo Block_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Block_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Block_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                           const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                           const Indents&                                   indents) const
    {
        return block_->to_string(strs_trie, ids_trie, indents);
    }

    std::shared_ptr<Node> Block_stmt_node::clone() const
    {
        return std::make_shared<Block_stmt_node>(block_);
    }
}