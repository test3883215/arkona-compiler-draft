/*
    File:    add_like_expr_node.cpp
    Created: 11 August 2023 at 19:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/add_like_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* add_like_expr_fmt = R"~({0}ADD_LIKE_EXPR
{1}ADD_LIKE_KIND {2}
{1}LHS
{3}
{1}END_LHS
{1}RHS
{4}
{1}END_RHS
{0}END_ADD_LIKE_EXPR)~";

    const char* add_like_kinds[] = {
        "Add", "Sub", "Algebraic_separator", "Type_add"
    };
}

namespace ast{
    NodeInfo Add_like_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Add_like_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Add_like_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(add_like_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           add_like_kinds[static_cast<unsigned>(a_kind_)],
                           lhs_str,
                           rhs_str);
    }

    std::shared_ptr<Node> Add_like_expr_node::clone() const
    {
        return std::make_shared<Add_like_expr_node>(lhs_, rhs_, a_kind_);
    }
}