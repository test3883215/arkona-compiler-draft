/*
    File:    indexing_args_node.cpp
    Created: 23 August 2023 at 18:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/indexing_args_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* indexing_args_fmt = R"~({0}INDEXING_ARGS
{1}ARGS
{2}
{1}END_ARGS
{0}END_INDEXING_ARGS)~";

    const char* arg_fmt = R"~({0}INDEXING_ARG
{1}
{0}END_INDEXING_ARG)~";
}

namespace ast{
    NodeInfo Indexing_args_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Indexing_args;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Indexing_args_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto args_str = strings::join::join([&ids_trie, &strs_trie, &nested_indent3, &indent2_str](const auto& a) -> std::string
                                                  {
                                                      return fmt::format(arg_fmt, 
                                                                         indent2_str, 
                                                                         a->to_string(strs_trie, ids_trie, nested_indent3));
                                                  }, 
                                                  args_.begin(), 
                                                  args_.end(), 
                                                  std::string{"\n"});

        return fmt::format(indexing_args_fmt, 
                           indent_str, 
                           indent1_str, 
                           args_str);
    }

    std::shared_ptr<Node> Indexing_args_node::clone() const
    {
        return std::make_shared<Indexing_args_node>(args_);
    }    
}