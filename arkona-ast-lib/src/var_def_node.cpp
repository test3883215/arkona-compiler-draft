/*
    File:    var_def_node.cpp
    Created: 18 July 2023 at 21:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/var_def_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* var_def_fmt = R"~({0}VARIABLE_DEFINITION_SECTION
{1}EXPORT_STATUS {2}
{1}VARIABLES
{3}
{1}END_VARIABLES
{0}END_VARIABLE_DEFINITION_SECTION)~";
}

namespace ast{
    NodeInfo Var_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Var_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Var_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                        const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                        const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto exported_kind_str = ast::to_string(exported_kind_);

        const auto vars_str = strings::join::join([&nested_indent2, &ids_trie, &strs_trie](const auto& g) -> std::string
                                                  {
                                                      return g->to_string(ids_trie, strs_trie, nested_indent2);
                                                  }, 
                                                  var_groups_.begin(), 
                                                  var_groups_.end(), 
                                                  std::string{"\n"});

        return fmt::format(var_def_fmt, 
                           indent_str, 
                           indent1_str, 
                           exported_kind_str, 
                           vars_str);
    }

    std::shared_ptr<Node> Var_def_node::clone() const
    {
        return std::make_shared<Var_def_node>(exported_kind_, var_groups_);
    }    
}