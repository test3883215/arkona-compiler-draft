/*
    File:    meta_select_stmt_node.cpp
    Created: 09 July 2023 at 14:55 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/meta_select_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings-utils/include/join.hpp"

namespace{
    static const char* select_stmt_fmt = R"~({0}META_SELECT_STMT
{1}VALUE_TO_SELECT
{2}
{1}END_VALUE_TO_SELECT
{1}BRANCHES
{3}
{1}END_META_BRANCHES
{1}ELSE_META_BRANCH
{4}
{1}END_META_ELSE_BRANCH
{0}END_META_SELECT)~";
}

namespace ast{
    NodeInfo Meta_select_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Meta_select;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Meta_select_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                 const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                 const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        
        const auto value_str    = value_to_select_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto branches_str = strings::join::join([strs_trie, ids_trie, nested_indent2](const auto& b) -> std::string
                                                      {
                                                          return b->to_string(strs_trie, ids_trie, nested_indent2);
                                                      }, 
                                                      branches_.begin(), 
                                                      branches_.end(), 
                                                      std::string{"\n"});

        std::string else_branch_str;
        if(else_branch_)
        {
            else_branch_str = else_branch_->to_string(strs_trie, ids_trie, nested_indent2);
        }
        
        return fmt::format(select_stmt_fmt, 
                           indent_str, 
                           indent1_str, 
                           value_str, 
                           branches_str, 
                           else_branch_str);
    }
    
    std::shared_ptr<Node> Meta_select_stmt_node::clone() const
    {
        return std::make_shared<Meta_select_stmt_node>(value_to_select_, branches_, else_branch_);
    }
}