/*
    File:    operation_def_node.cpp
    Created: 30 July 2023 at 20:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/operation_def_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* operation_def_fmt = R"~({0}OPERATION_DEFINITION
{1}EXPORT_STATUS {2}
{1}OPERATION_HEADER
{3}
{1}END_OPERATION_HEADER
{1}OPERATION_BODY
{4}
{1}END_OPERATION_BODY
{0}END_OPERATION_DEFINITION)~";
}

namespace ast{
    NodeInfo Operation_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Operation_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Operation_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto exported_kind_str    = ast::to_string(exported_kind_);
        const auto operation_header_str = operation_header_->to_string(ids_trie, strs_trie, nested_indent2);

        std::string func_body_str;
        if(func_body_)
        {
            func_body_str = func_body_->to_string(ids_trie, strs_trie, nested_indent2);
        }

        return fmt::format(operation_def_fmt, 
                           indent_str,
                           indent1_str,
                           exported_kind_str, 
                           operation_header_str, 
                           func_body_str);
    }

    std::shared_ptr<Node> Operation_def_node::clone() const
    {
        return std::make_shared<Operation_def_node>(exported_kind_, operation_header_, func_body_);
    }    
}