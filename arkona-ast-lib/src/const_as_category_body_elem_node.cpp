/*
    File:    const_as_category_body_elem_node.cpp
    Created: 02 August 2023 at 20:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/const_as_category_body_elem_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* const_as_category_body_elem_fmt = R"~({0}CONST_AS_CATEGORY_BODY_ELEM
{1}CONST_NAME {2}
{1}CONST_TYPE
{3}
{1}END_CONST_TYPE
{0}END_CONST_AS_CATEGORY_BODY_ELEM)~";
}

namespace ast{
    NodeInfo Const_as_category_body_elem_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Const_as_category_body;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Const_as_category_body_elem_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                            const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                            const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto const_type_str = const_type_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto const_name_str = ast::to_string(const_name_, ids_trie);

        return fmt::format(const_as_category_body_elem_fmt, 
                           indent_str, 
                           indent1_str, 
                           const_name_str, 
                           const_type_str);
    }

    std::shared_ptr<Node> Const_as_category_body_elem_node::clone() const
    {
        return std::make_shared<Const_as_category_body_elem_node>(const_name_, const_type_);
    }
}