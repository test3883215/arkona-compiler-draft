/*
    File:    pure_kind.cpp
    Created: 23 July 2023 at 15:03 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/pure_kind.hpp"

namespace{
    const char* pure_kind_strs[] = {
        "Pure", "Non_pure"
    };
}

namespace ast{
    std::string to_string(Pure_kind k)
    {
        return pure_kind_strs[static_cast<unsigned>(k)];
    }
}