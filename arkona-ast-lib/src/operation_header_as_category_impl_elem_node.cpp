/*
    File:    operation_header_as_category_impl_elem_node.cpp
    Created: 05 August 2023 at 17:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/operation_header_as_category_impl_elem_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* operation_header_as_category_impl_elem_fmt = R"~({0}OPERATION_HEADER_AS_CATEGORY_IMPL_ELEM
{1}OPERATION_HEADER
{2}
{1}END_OPERATION_HEADER
{1}OPERATION_HEADER_IMPL
{3}
{1}END_OPERATION_HEADER_IMPL
{0}END_OPERATION_HEADER_AS_CATEGORY_IMPL_ELEM)~";
}

namespace ast{
    NodeInfo Operation_header_as_category_impl_elem_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Operation_header_as_category_impl_elem;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Operation_header_as_category_impl_elem_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                                       const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                       const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto func_header_str      = func_header_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto func_header_impl_str = func_header_impl_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(operation_header_as_category_impl_elem_fmt, 
                           indent_str, 
                           indent1_str, 
                           func_header_str, 
                           func_header_impl_str);
    }

    std::shared_ptr<Node> Operation_header_as_category_impl_elem_node::clone() const
    {
        return std::make_shared<Operation_header_as_category_impl_elem_node>(func_header_, func_header_impl_);
    }
}