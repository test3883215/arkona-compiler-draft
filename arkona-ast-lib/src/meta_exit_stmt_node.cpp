/*
    File:    meta_exit_stmt_node.cpp
    Created: 09 July 2023 at 19:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/meta_exit_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* exit_stmt_fmt = R"~({0}META_EXIT {1})~";
}

namespace ast{
    NodeInfo Meta_exit_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Meta_exit_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Meta_exit_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                               const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                               const Indents&                                   indents) const
    {
        const auto indent_str = std::string(indents.indent_, ' ');
        const auto label_name = ast::to_string(label_id_, ids_trie);

        return fmt::format(exit_stmt_fmt, indent_str, label_name);
    }
    
    std::shared_ptr<Node> Meta_exit_stmt_node::clone() const
    {
        return std::make_shared<Meta_exit_stmt_node>(label_id_);
    }
}