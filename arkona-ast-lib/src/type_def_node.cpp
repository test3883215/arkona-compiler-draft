/*
    File:    type_def_node.cpp
    Created: 22 July 2023 at 18:31 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/type_def_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* type_def_fmt = R"~({0}TYPE_DEFINITION_SECTION
{1}EXPORT_STATUS {2}
{1}TYPES
{3}
{1}END_TYPES
{0}END_TYPE_DEFINITION_SECTION)~";

    const char* type_info_fmt = R"~({0}TYPE_INFO
{1}TYPE_NAME {2}
{1}TYPE_DEFINITION
{3}
{1}END_TYPE_DEFINITION
{0}END_TYPE_INFO)~";
}

namespace ast{
    NodeInfo Type_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Type_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Type_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                         const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                         const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto exported_kind_str = ast::to_string(exported_kind_);

        const auto type_info_to_str = [&ids_trie, &strs_trie, &nested_indent2, &indents](const Type_info& type_info) -> std::string
                                      {
                                          Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};
                                          Indents nested_indent4{indents.indent_ + 4 * indents.indent_inc_, indents.indent_inc_};

                                          const auto indent2_str    = std::string(nested_indent2.indent_, ' ');
                                          const auto indent3_str    = std::string(nested_indent3.indent_, ' ');

                                          const auto type_name_str = ast::to_string(type_info.type_name_, ids_trie);
                                          const auto type_def_str  = type_info.type_def_->to_string(strs_trie, ids_trie, nested_indent4);

                                          return fmt::format(type_info_fmt, 
                                                             indent2_str, 
                                                             indent3_str, 
                                                             type_name_str, 
                                                             type_def_str);
                                      };

        const auto types_str = strings::join::join(type_info_to_str, 
                                                   type_infos_.begin(), 
                                                   type_infos_.end(), 
                                                   std::string{"\n"});

        return fmt::format(type_def_fmt, 
                           indent_str, 
                           indent1_str, 
                           exported_kind_str, 
                           types_str);
    }

    std::shared_ptr<Node> Type_def_node::clone() const
    {
        return std::make_shared<Type_def_node>(exported_kind_, type_infos_);
    }    
}