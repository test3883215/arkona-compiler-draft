/*
    File:    array_type_expr_node.cpp
    Created: 19 August 2023 at 20:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/array_type_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* array_type_expr_fmt = R"~({0}ARRAY_TYPE_EXPR
{1}ARRAY_TYPE_INDICIS
{2}
{1}END_ARRAY_TYPE_INDICIS
{1}ARRAY_ELEM_TYPE
{3}
{1}END_ARRAY_ELEM_TYPE
{0}END_ARRAY_TYPE_EXPR)~";

    const char* array_type_elem_fmt = R"~({0}ARRAY_TYPE_INDEX
{1}
{0}END_ARRAY_TYPE_INDEX)~";
}

namespace ast{
    NodeInfo Array_type_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Array_type_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Array_type_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto indices_str = strings::join::join([&ids_trie, &strs_trie, &nested_indent3, &indent2_str](const auto& e) -> std::string
                                                     {
                                                         const std::string e_str = e ? e->to_string(strs_trie, ids_trie, nested_indent3) : "NULL";

                                                         return fmt::format(array_type_elem_fmt, 
                                                                            indent2_str, 
                                                                            e_str);
                                                     }, 
                                                     indices_.begin(), 
                                                     indices_.end(), 
                                                     std::string{"\n"});

        const auto elem_type_str = elem_type_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(array_type_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           indices_str, 
                           elem_type_str);
    }

    std::shared_ptr<Node> Array_type_expr_node::clone() const
    {
        return std::make_shared<Array_type_expr_node>(indices_, elem_type_);
    }    
}