/*
    File:    operation_header_node.cpp
    Created: 30 July 2023 at 19:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/operation_header_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* package_func_def_fmt = R"~({0}OPERATION_HEADER
{1}PURE_KIND {2}
{1}OPERATION_INFO {3}
{1}OPERATION_SIGNATURE
{4}
{1}END_OPERATION_SIGNATURE
{0}END_OPERATION_HEADER)~";
}

namespace ast{
    NodeInfo Operation_header_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Operation_header;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Operation_header_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                 const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                 const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto pure_kind_str      = ast::to_string(pure_kind_);
        const auto op_info_str        = ast::to_string(op_info_);
        const auto func_signature_str = func_signature_->to_string(ids_trie, strs_trie, nested_indent2);

        return fmt::format(package_func_def_fmt, 
                           indent_str,
                           indent1_str, 
                           pure_kind_str, 
                           op_info_str,
                           func_signature_str);
    }

    std::shared_ptr<Node> Operation_header_node::clone() const
    {
        return std::make_shared<Operation_header_node>(pure_kind_, op_info_, func_signature_);
    }    
}