/*
    File:    fixed_size_base_type_expr_node.cpp
    Created: 21 August 2023 at 16:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/fixed_size_base_type_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* fixed_size_base_type_expr_fmt = R"~({0}FIXED_SIZE_BASE_TYPE_EXPR
{1}FIXED_SIZE_BASE_TYPE_EXPR_KIND {2}
{0}END_FIXED_SIZE_BASE_TYPE_EXPR)~";

    const char* fixed_size_base_type_kinds[] = {
        "Float32",     "Float64",    "Float80",    "Float128",
        "Complex32",   "Complex64",  "Complex80",  "Complex128",
        "Quat32",      "Quat64",     "Quat80",     "Quat128",
        "Unsigned8",   "Unsigned16", "Unsigned32", "Unsigned64",
        "Unsigned128", "Signed8",    "Signed16",   "Signed32",
        "Signed64",    "Signed128",  "String8",    "String16",
        "String32",    "Char8",      "Char16",     "Char32",
        "Ord8",        "Ord16",      "Ord32",      "Ord64",
        "Bool8",       "Bool16",     "Bool32",     "Bool64",
        "Void"
    };
}

namespace ast{
    NodeInfo Fixed_size_base_type_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Fixed_size_base_type_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Fixed_size_base_type_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                                          const std::shared_ptr<strings::trie::Char_trie>&,
                                                          const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        return fmt::format(fixed_size_base_type_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           fixed_size_base_type_kinds[static_cast<unsigned>(kind_)]);
    }

    std::shared_ptr<Node> Fixed_size_base_type_expr_node::clone() const
    {
        return std::make_shared<Fixed_size_base_type_expr_node>(kind_);
    }    
}