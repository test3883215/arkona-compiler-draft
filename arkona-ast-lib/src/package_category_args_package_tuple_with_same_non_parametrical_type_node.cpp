/*
    File:    package_category_args_package_tuple_with_same_non_parametrical_type_node.cpp
    Created: 03 August 2023 at 20:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/package_category_args_package_tuple_with_same_non_parametrical_type_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* package_category_args_package_tuple_with_same_non_parametrical_type_fmt = R"~({0}PACKAGE_CATEGORY_ARGS_TUPLE_PACKAGE_WITH_SAME_NON_PARAMETRIC_TYPE
{1}ARGS
{2}
{1}END_ARGS
{1}ARGS_TYPE
{3}
{1}END_ARGS_TYPE
{0}END_PACKAGE_CATEGORY_ARGS_TUPLE_PACKAGE_WITH_SAME_NON_PARAMETRIC_TYPE)~";
}

namespace ast{
    NodeInfo Package_category_args_package_tuple_with_same_non_parametrical_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Package_category_args_tuple_package_with_same_non_parametric_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Package_category_args_package_tuple_with_same_non_parametrical_type_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                                                                    const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                                                    const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto args_type_str = args_type_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto args_str = strings::join::join([&indent2_str, &ids_trie](const auto& info) -> std::string
                                                  {
                                                      return indent2_str + ast::to_string(info, ids_trie);
                                                  }, 
                                                  arg_infos_.begin(), 
                                                  arg_infos_.end(), 
                                                  std::string{"\n"});

        return fmt::format(package_category_args_package_tuple_with_same_non_parametrical_type_fmt, 
                           indent_str, 
                           indent1_str, 
                           args_str, 
                           args_type_str);
    }

    std::shared_ptr<Node> Package_category_args_package_tuple_with_same_non_parametrical_type_node::clone() const
    {
        return std::make_shared<Package_category_args_package_tuple_with_same_non_parametrical_type_node>(arg_infos_, args_type_);
    }
}