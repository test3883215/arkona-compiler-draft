/*
    File:    abstract_generic_package_func_def_node.cpp
    Created: 29 July 2023 at 18:51 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/abstract_generic_package_func_def_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* abstract_generic_package_func_def_fmt = R"~({0}ABSTRACT_GENERIC_PACKAGE_FUNC_DEFINITION
{1}EXPORT_STATUS {2}
{1}PURE_KIND {3}
{1}PACKAGE_FUNC_NAME {4}
{1}PACKAGE_FUNC_SIGNATURE
{5}
{1}END_PACKAGE_FUNC_SIGNATURE
{0}END_ABSTRACT_GENERIC_PACKAGE_FUNC_DEFINITION)~";
}

namespace ast{
    NodeInfo Abstract_generic_package_func_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Abstract_generic_package_func_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Abstract_generic_package_func_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                                  const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                  const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto exported_kind_str  = ast::to_string(exported_kind_);
        const auto pure_kind_str      = ast::to_string(pure_kind_);
        const auto func_name_info_str = ast::to_string(func_name_info_, ids_trie);
        const auto func_signature_str = func_signature_->to_string(ids_trie, strs_trie, nested_indent2);

        return fmt::format(abstract_generic_package_func_def_fmt, 
                           indent_str,
                           indent1_str,
                           exported_kind_str, 
                           pure_kind_str, 
                           func_name_info_str,
                           func_signature_str);
    }

    std::shared_ptr<Node> Abstract_generic_package_func_def_node::clone() const
    {
        return std::make_shared<Abstract_generic_package_func_def_node>(exported_kind_, pure_kind_, func_name_info_, func_signature_);
    }    
}