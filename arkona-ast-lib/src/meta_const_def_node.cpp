/*
    File:    meta_const_def_node.cpp
    Created: 20 July 2023 at 21:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/meta_const_def_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* const_def_fmt = R"~({0}META_CONSTANT_DEFINITION_SECTION
{1}EXPORT_STATUS {2}
{1}META_CONSTANTS
{3}
{1}END_META_CONSTANTS
{0}END_META_CONSTANT_DEFINITION_SECTION)~";

    const char* const_info_fmt = R"~({0}META_CONST_INFO
{1}META_CONST_NAME {2}
{1}META_CONST_TYPE
{3}
{1}END_META_CONST_TYPE
{1}META_CONST_VALUE
{4}
{1}END_META_CONST_VALUE
{0}END_META_CONST_INFO)~";
}

namespace ast{
    NodeInfo Meta_const_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Meta_const_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Meta_const_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                               const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                               const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto exported_kind_str = ast::to_string(exported_kind_);

        const auto const_info_to_str = [&ids_trie, &strs_trie, &nested_indent2, &indents](const Const_info& const_info) -> std::string
                                       {
                                           Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};
                                           Indents nested_indent4{indents.indent_ + 4 * indents.indent_inc_, indents.indent_inc_};

                                           const auto indent2_str     = std::string(nested_indent2.indent_, ' ');
                                           const auto indent3_str     = std::string(nested_indent3.indent_, ' ');

                                           const auto const_name_str  = ast::to_string(const_info.const_name_, ids_trie);
                                           const auto const_type_str  = const_info.const_type_->to_string(strs_trie, ids_trie, nested_indent4);
                                           const auto const_value_str = const_info.const_value_->to_string(strs_trie, ids_trie, nested_indent4);

                                           return fmt::format(const_info_fmt, 
                                                              indent2_str, 
                                                              indent3_str, 
                                                              const_name_str, 
                                                              const_type_str, 
                                                              const_value_str);
                                       };

        const auto constants_str = strings::join::join(const_info_to_str, 
                                                       const_infos_.begin(), 
                                                       const_infos_.end(), 
                                                       std::string{"\n"});

        return fmt::format(const_def_fmt, 
                           indent_str, 
                           indent1_str, 
                           exported_kind_str, 
                           constants_str);
    }

    std::shared_ptr<Node> Meta_const_def_node::clone() const
    {
        return std::make_shared<Meta_const_def_node>(exported_kind_, const_infos_);
    }    
}