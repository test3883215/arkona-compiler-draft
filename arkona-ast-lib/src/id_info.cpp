/*
    File:    id_info.cpp
    Created: 16 July 2023 at 20:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/id_info.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* pos_fmt = R"~({{POSITION: [line: [0], pos: {1}]--[line: {2}, pos: {3}]}})~";
    
    const char* id_info_fmt = R"~(ID_INFO{{id: {0}, pos: {1}}})~";
}

namespace ast{    
    std::string to_string(const Id_info& id_info, const std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const auto& p       = id_info.pos_;
        const auto& b       = p.begin_pos_;
        const auto& e       = p.end_pos_;
        const auto  pos_str = fmt::format(pos_fmt, b.line_no_, b.line_pos_, e.line_no_, e.line_pos_);
        
        return fmt::format(id_info_fmt, strings::trie::idx_to_string(ids_trie, id_info.identifier_id_), pos_str);
    }

    bool equals(const Id_info& lhs, const Id_info& rhs)
    {
        return lhs.identifier_id_ == rhs.identifier_id_;
    }

    bool operator==(const Id_info& lhs, const Id_info& rhs)
    {
        return lhs.identifier_id_ == rhs.identifier_id_;
    }
}
