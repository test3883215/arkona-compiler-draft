/*
    File:    used_modules_node.cpp
    Created: 21 June 2023 at 21:09 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <vector>
#include "../include/used_modules_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* used_module_format = R"~({0}USED_MODULES {{
{1}
{0}}})~";
};

namespace ast{
    NodeInfo Used_modules_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Used_modules;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Used_modules_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                             const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                             const Indents&                                   indents) const
    {
        std::string result;

        const auto indent_str = std::string(indents.indent_, ' ');

        Indents nested_indent{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        std::vector<std::string> q_id_strs;
        for(const auto& q_id : names_){
            q_id_strs.push_back(q_id->to_string(strs_trie, ids_trie, nested_indent));
        }

        const auto used_q_ids_str = strings::join::join(q_id_strs.begin(), q_id_strs.end(), std::string{"\n"});

        result = fmt::format(used_module_format, indent_str, used_q_ids_str);

        return result;
    }
    
    std::shared_ptr<Node> Used_modules_node::clone() const
    {
        return std::make_shared<Used_modules_node>(names_);
    }
}