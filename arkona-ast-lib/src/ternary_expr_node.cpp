/*
    File:    ternary_expr_node.cpp
    Created: 07 August 2023 at 21:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/ternary_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* ternary_expr_fmt = R"~({0}TERNARY_EXPR
{1}TERNARY_KIND {2}
{1}CONDITION
{3}
{1}END_CONDITION
{1}TRUE_BRANCH
{4}
{1}END_TRUE_BRANCH
{1}FALSE_BRANCH
{5}
{1}END_FALSE_BRANCH
{0}END_TERNARY_EXPR)~";

    const char* ternary_kinds[] = {
        "Short_circuit", "Full"
    };
}

namespace ast{
    NodeInfo Ternary_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Ternary_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Ternary_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                             const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                             const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto condition_str    = condition_ ->to_string(strs_trie, ids_trie, nested_indent2);
        const auto true_branch_str  = true_branch_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto false_branch_str = false_branch_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(ternary_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           condition_str,
                           true_branch_str,
                           false_branch_str,
                           ternary_kinds[static_cast<unsigned>(t_kind_)]);
    }

    std::shared_ptr<Node> Ternary_expr_node::clone() const
    {
        return std::make_shared<Ternary_expr_node>(condition_, true_branch_, false_branch_, t_kind_);
    }
}