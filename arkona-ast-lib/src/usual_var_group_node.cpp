/*
    File:    usual_var_group_node.cpp
    Created: 16 July 2023 at 19:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/usual_var_group_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* usual_var_group_fmt = R"~({0}USUAL_VAR_GROUP
{1}VARS
{2}
{1}END_VARS
{1}VARS_TYPE
{3}
{1}END_VARS_TYPE
{0}END_USUAL_VAR_GROUP)~";
}

namespace ast{
    NodeInfo Usual_var_group_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Usual_var_group;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Usual_var_group_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto vars_type_str = vars_type_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto vars_str = strings::join::join([&indent2_str, &ids_trie](const auto& info) -> std::string
                                                  {
                                                      return indent2_str + ast::to_string(info, ids_trie);
                                                  }, 
                                                  var_infos_.begin(), 
                                                  var_infos_.end(), 
                                                  std::string{"\n"});

        return fmt::format(usual_var_group_fmt, 
                           indent_str, 
                           indent1_str, 
                           vars_str, 
                           vars_type_str);
    }

    std::shared_ptr<Node> Usual_var_group_node::clone() const
    {
        return std::make_shared<Usual_var_group_node>(var_infos_, vars_type_);
    }
}