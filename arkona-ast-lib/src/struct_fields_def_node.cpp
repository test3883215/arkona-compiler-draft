/*
    File:    struct_fields_def_node.cpp
    Created: 20 August 2023 at 12:25 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/struct_fields_def_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* fields_group_def_fmt = R"~({0}STRUCT_FIELDS
{1}FIELDS_GROUPS
{2}
{1}END_FIELDS_GROUPS
{0}END_STRUCT_FIELDS)~";
}

namespace ast{
    NodeInfo Struct_fields_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Struct_fields_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Struct_fields_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                  const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                  const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto groups_str = strings::join::join([&nested_indent2, &ids_trie, &strs_trie](const auto& g) -> std::string
                                                    {
                                                        return g->to_string(strs_trie, ids_trie, nested_indent2);
                                                    }, 
                                                    fields_groups_.begin(), 
                                                    fields_groups_.end(), 
                                                    std::string{"\n"});

        return fmt::format(fields_group_def_fmt, 
                           indent_str, 
                           indent1_str, 
                           groups_str);
    }

    std::shared_ptr<Node> Struct_fields_def_node::clone() const
    {
        return std::make_shared<Struct_fields_def_node>(fields_groups_);
    }
}