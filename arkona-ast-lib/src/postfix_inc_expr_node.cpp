/*
    File:    postfix_inc_expr_node.cpp
    Created: 12 August 2023 at 17:11 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/postfix_inc_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* postfix_inc_expr_fmt = R"~({0}POSTFIX_INC_EXPR
{1}POSTFIX_INCS {2}
{1}OP
{3}
{1}END_OP
{0}END_POSTFIX_INC_EXPR)~";

    const char* postfix_inc_kinds[] = {
        "Inc", "Dec", "Wrapping_inc", "Wrapping_dec"
    };
}

namespace ast{
    NodeInfo Postfix_inc_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Postfix_inc_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Postfix_inc_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                 const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                 const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto op_str = op_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto postfix_incs_str = strings::join::join([](const auto& i) -> std::string
                                                         {
                                                             return postfix_inc_kinds[static_cast<unsigned>(i)];
                                                         }, 
                                                         postfix_incs_.begin(), 
                                                         postfix_incs_.end(), 
                                                         std::string{" "});

        return fmt::format(postfix_inc_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           postfix_incs_str,
                           op_str);
    }

    std::shared_ptr<Node> Postfix_inc_expr_node::clone() const
    {
        return std::make_shared<Postfix_inc_expr_node>(op_, postfix_incs_);
    }
}