/*
    File:    type_type_expr_node.cpp
    Created: 20 August 2023 at 15:12 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/type_type_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* type_type_expr_fmt = R"~({0}TYPE_TYPE_EXPR
{0}END_TYPE_TYPE_EXPR)~";
}

namespace ast{
    NodeInfo Type_type_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Type_type_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Type_type_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&, 
                                               const std::shared_ptr<strings::trie::Char_trie>&,
                                               const Indents&                                   indents) const
    {
        const auto indent_str  = std::string(indents.indent_, ' ');
        
        return fmt::format(type_type_expr_fmt, 
                           indent_str);
    }

    std::shared_ptr<Node> Type_type_expr_node::clone() const
    {
        return std::make_shared<Type_type_expr_node>();
    }    
}