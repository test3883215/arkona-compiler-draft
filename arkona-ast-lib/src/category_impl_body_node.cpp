/*
    File:    category_impl_body_node.cpp
    Created: 05 August 2023 at 18:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/category_impl_body_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* category_impl_body_fmt = R"~({0}CATEGORY_IMPL_BODY
{1}CATEGORY_IMPL_BODY_ELEMS
{2}
{1}END_CATEGORY_IMPL_BODY_ELEMS
{0}END_CATEGORY_IMPL_BODY)~";

    const char* category_body_elem_fmt = R"~({0}CATEGORY_IMPL_BODY_ELEM
{1}
{0END_CATEGORY_IMPL_BODY_ELEM)~";
}

namespace ast{
    NodeInfo Category_impl_body_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Category_impl_body;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Category_impl_body_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                   const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                   const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');
        
        const auto elems_str = strings::join::join([&nested_indent3, &indent2_str, &ids_trie, &strs_trie](const auto& e) -> std::string
                                                   {
                                                       return fmt::format(category_body_elem_fmt, 
                                                                          indent2_str,
                                                                          e->to_string(ids_trie, strs_trie, nested_indent3));
                                                   }, 
                                                   elems_.begin(), 
                                                   elems_.end(), 
                                                   std::string{"\n"});

        return fmt::format(category_impl_body_fmt, 
                           indent_str, 
                           indent1_str, 
                           elems_str);
    }

    std::shared_ptr<Node> Category_impl_body_node::clone() const
    {
        return std::make_shared<Category_impl_body_node>(elems_);
    }
}