/*
    File:    exported_kind.cpp
    Created: 17 July 2023 at 23:14 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/exported_kind.hpp"

namespace{
    const char* exported_kind_strs[] = {
        "Exported", "Not_exported"
    };
}

namespace ast{    
    std::string to_string(Exported_kind k)
    {
        return exported_kind_strs[static_cast<unsigned>(k)];
    }
}