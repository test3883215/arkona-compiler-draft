/*
    File:    used_category_node.cpp
    Created: 31 July 2023 at 19:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/used_category_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* used_catefory_fmt = R"~({0}USED_CATEGORY
{1}CATEGORY_NAME
{2}
{1}END_CATEGORY_NAME
{1}CATEGORY_ARGS
{3}
{1}END_CATEGORY_ARGS
{0}END_USED_CATEGORY)~";

    const char* category_arg_fmt = R"~({0}USED_CATEGORY_ARG
{1}
{0}END_USED_CATEGORY_ARG)~";
}

namespace ast{
    NodeInfo Used_category_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Used_category;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Used_category_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{nested_indent2.indent_ + nested_indent2.indent_inc_, nested_indent2.indent_inc_};

        const auto indent_str          = std::string(indents.indent_, ' ');
        const auto indent1_str         = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str         = std::string(nested_indent2.indent_, ' ');

        const auto category_name_str   = category_name_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto category_arg_to_str = [&strs_trie, &ids_trie, &nested_indent2, &indent2_str, &nested_indent3](const std::shared_ptr<Expr_node>& arg) -> std::string
                                         {
                                             return fmt::format(category_arg_fmt, 
                                                                indent2_str, 
                                                                arg->to_string(strs_trie, ids_trie, nested_indent3));
                                         };

        const auto category_args_str   = strings::join::join(category_arg_to_str, 
                                                             category_args_.begin(), 
                                                             category_args_.end(), 
                                                             std::string{"\n"});

        return fmt::format(used_catefory_fmt, 
                           indent_str, 
                           indent1_str, 
                           category_name_str, 
                           category_args_str);
    }

    std::shared_ptr<Node> Used_category_node::clone() const
    {
        return std::make_shared<Used_category_node>(category_name_, category_args_);
    }
}