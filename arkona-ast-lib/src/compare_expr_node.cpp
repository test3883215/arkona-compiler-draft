/*
    File:    compare_expr_node.cpp
    Created: 09 August 2023 at 20:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/compare_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* compare_expr_fmt = R"~({0}COMPARE_EXPR
{1}LHS
{2}
{1}END_LHS
{1}RHS
{3}
{1}END_RHS
{0}END_COMPARE_EXPR)~";
}

namespace ast{
    NodeInfo Compare_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Compare_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Compare_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(compare_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           lhs_str,
                           rhs_str);
    }

    std::shared_ptr<Node> Compare_expr_node::clone() const
    {
        return std::make_shared<Compare_expr_node>(lhs_, rhs_);
    }
}