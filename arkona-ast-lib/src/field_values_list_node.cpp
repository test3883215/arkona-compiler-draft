/*
    File:    field_values_list_node.cpp
    Created: 23 August 2023 at 15:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/field_values_list_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* field_values_fmt = R"~({0}FIELD_VALUES_LIST
{1}
{0}END_FIELD_VALUES_LIST)~";

    const char* field_value_fmt = R"~({0}FIELD_VALUE
{1}FIELD_NAME {2}
{1}VALUE
{3}
{1}END_VALUE
{0}END_FIELD_VALUE)~";
}

namespace ast{
    NodeInfo Field_values_list_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Field_values_list;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Field_values_list_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                  const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                  const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto values_str = strings::join::join([&ids_trie, &strs_trie, &nested_indent3, &indent1_str, &indent2_str](const auto& e) -> std::string
                                                    {
                                                        const auto& [id, value] = e;
                                                        const auto& name_str    = ast::to_string(id, ids_trie);
                                                        const auto& value_str   = value->to_string(strs_trie, ids_trie, nested_indent3);
                                                        
                                                        return fmt::format(field_value_fmt, 
                                                                           indent1_str,
                                                                           indent2_str, 
                                                                           name_str, 
                                                                           value_str);
                                                    }, 
                                                    values_.begin(), 
                                                    values_.end(), 
                                                    std::string{"\n"});

        return fmt::format(field_values_fmt, 
                           indent_str, 
                           values_str);
    }

    std::shared_ptr<Node> Field_values_list_node::clone() const
    {
        return std::make_shared<Field_values_list_node>(values_);
    }    
}