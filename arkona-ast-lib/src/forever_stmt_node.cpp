/*
    File:    forever_stmt_node.cpp
    Created: 11 July 2023 at 20:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/forever_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* forever_stmt_fmt = R"~({0}FOREVER_STMT
{1}FOREVER_LABEL {2}
{1}FOREVER_BODY
{3}
{1}END_FOREVER_BODY
{0}END_FOREVER)~";
}

namespace ast{
    NodeInfo Forever_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Forever_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Forever_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                           const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                           const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto label_str = ast::to_string(loop_label_id_, ids_trie);
        
        std::string loop_body_str;
        if(loop_body_)
        {
            loop_body_str = loop_body_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        return fmt::format(forever_stmt_fmt, 
                           indent_str, 
                           indent1_str, 
                           label_str, 
                           loop_body_str);
    }

    std::shared_ptr<Node> Forever_stmt_node::clone() const
    {
        return std::make_shared<Forever_stmt_node>(loop_label_id_, loop_body_);
    }
}