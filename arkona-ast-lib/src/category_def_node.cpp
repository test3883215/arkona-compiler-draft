/*
    File:    category_def_node.cpp
    Created: 03 August 2023 at 19:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/category_def_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* category_def_fmt = R"~({0}CATEGORY_DEF
{1}EXPORT_STATUS {2}
{1}META_FUNC_NAME {3}
{1}CATEGORY_ARG_GROUPS
{4}
{1}END_CATEGORY_ARG_GROUPS
{1}USED_CATEGORIES_FOR_THIS_CATEGORY
{5}
{1}END_USED_CATEGORIES_FOR_THIS_CATEGORY
{1}CATEGORY_BODY
{6}
{1}END_CATEGORY_BODY
{0}END_CATEGORY_DEF)~";
}

namespace ast{
    NodeInfo Category_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Category_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Category_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                             const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                             const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto exported_kind_str      = ast::to_string(exported_kind_);
        const auto category_name_info_str = ast::to_string(category_name_info_, ids_trie);
        const auto category_body_str      = category_body_->to_string(ids_trie, strs_trie, nested_indent2);

        const auto args_str = strings::join::join([&nested_indent2, &ids_trie, &strs_trie](const auto& g) -> std::string
                                                  {
                                                      return g->to_string(ids_trie, strs_trie, nested_indent2);
                                                  }, 
                                                  arg_groups_.begin(), 
                                                  arg_groups_.end(), 
                                                  std::string{"\n"});

        std::string used_categories_str;
        if(used_categories_)
        {
            used_categories_str = used_categories_->to_string(ids_trie, strs_trie, nested_indent2);
        }

        return fmt::format(category_def_fmt, 
                           indent_str, 
                           indent1_str, 
                           exported_kind_str, 
                           category_name_info_str, 
                           args_str,
                           used_categories_str, 
                           category_body_str);
    }

    std::shared_ptr<Node> Category_def_node::clone() const
    {
        return std::make_shared<Category_def_node>(exported_kind_, category_name_info_, arg_groups_, used_categories_, category_body_);
    }    
}