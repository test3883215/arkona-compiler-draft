/*
    File:    lambda_expr_node.cpp
    Created: 17 August 2023 at 19:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/lambda_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* lambda_func_fmt = R"~({0}LAMBDA_FUNC_DEFINITION
{1}LAMBDA_FUNC_SIGNATURE
{2}
{1}END_LAMBDA_FUNC_SIGNATURE
{1}LAMBDA_FUNC_BODY
{3}
{1}LAMBDA_END_FUNC_BODY
{0}END_LAMBDA_FUNC_DEFINITION)~";
}

namespace ast{
    NodeInfo Lambda_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Lambda_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Lambda_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                            const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                            const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto func_signature_str = func_signature_->to_string(ids_trie, strs_trie, nested_indent2);
        const auto func_body_str      = func_body_->to_string(ids_trie, strs_trie, nested_indent2);

        return fmt::format(lambda_func_fmt, 
                           indent_str, 
                           indent1_str, 
                           func_signature_str, 
                           func_body_str);
    }

    std::shared_ptr<Node> Lambda_expr_node::clone() const
    {
        return std::make_shared<Lambda_expr_node>(func_signature_, func_body_);
    }    
}