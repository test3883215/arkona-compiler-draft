/*
    File:    non_fixed_size_base_type_expr_node.cpp
    Created: 21 August 2023 at 16:26 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/non_fixed_size_base_type_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* non_fixed_size_base_type_expr_fmt = R"~({0}NON_FIXED_SIZE_BASE_TYPE_EXPR
{1}NON_FIXED_SIZE_BASE_TYPE_EXPR_MODIFIERS {2}
{1}NON_FIXED_SIZE_BASE_TYPE_EXPR_KIND {3}
{0}END_NON_FIXED_SIZE_BASE_TYPE_EXPR)~";

    const char* non_fixed_size_base_type_kinds[] = {
        "Bool",   "Ord",      "Char",
        "String", "Unsigned", "Signed",
        "Float",  "Complex",  "Quat"
    };

    const char* modifiers[] = {
        "Long", "Short"
    };
}

namespace ast{
    NodeInfo Non_fixed_size_base_type_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Non_fixed_size_base_type_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Non_fixed_size_base_type_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                                              const std::shared_ptr<strings::trie::Char_trie>&,
                                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        
        const auto modifiers_str = strings::join::join([](const auto& m) -> std::string
                                                       {
                                                            return modifiers[static_cast<unsigned>(m)];
                                                       }, 
                                                       modifiers_.begin(), 
                                                       modifiers_.end(), 
                                                       std::string{" "});

        return fmt::format(non_fixed_size_base_type_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           modifiers_str,
                           non_fixed_size_base_type_kinds[static_cast<unsigned>(kind_)]);
    }

    std::shared_ptr<Node> Non_fixed_size_base_type_expr_node::clone() const
    {
        return std::make_shared<Non_fixed_size_base_type_expr_node>(modifiers_, kind_);
    }    
}