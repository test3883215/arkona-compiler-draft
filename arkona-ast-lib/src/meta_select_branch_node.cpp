/*
    File:    meta_select_branch_node.cpp
    Created: 09 July 2023 at 13:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/meta_select_branch_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings-utils/include/join.hpp"

namespace{
    static const char* select_branch_fmt = R"~({0}META_SELECT_BRANCH
{1}META_EXPRESSIONS_TO_COMPARE
{2}
{1}END_META_EXPRESSIONS_TO_COMPARE
{1}BLOCK_FOR_META_EXPRESSIONS
{3}
{1}END_BLOCK_FOR_META_EXPRESSIONS
{0}END_META_SELECT_BRANCH)~";
}

namespace ast{
    NodeInfo Meta_select_branch_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Meta_select_branch;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Meta_select_branch_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                   const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                   const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        
        const auto expressions_str = strings::join::join([strs_trie, ids_trie, nested_indent2](const std::shared_ptr<Expr_node>& e) -> std::string
                                                         {
                                                             return e->to_string(strs_trie, ids_trie, nested_indent2);
                                                         }, 
                                                         branch_expressions_.begin(), 
                                                         branch_expressions_.end(), 
                                                         std::string{"\n"});

        return fmt::format(select_branch_fmt, 
                           indent_str, 
                           indent1_str, 
                           expressions_str, 
                           block_->to_string(strs_trie, ids_trie, nested_indent2));
    }

    std::shared_ptr<Node> Meta_select_branch_node::clone() const
    {
        return std::make_shared<Meta_select_branch_node>(branch_expressions_, block_);
    }    
}