/*
    File:    func_signature_node.cpp
    Created: 22 July 2023 at 20:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/func_signature_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* func_signature_fmt = R"~({0}FUNC_SIGNATURE
{1}GROUP_OF_FUNC_ARGS
{2}
{1}END_GROUP_OF_FUNC_ARGS
{1}FUNC_VALUE_TYPE
{3}
{1}END_FUNC_VALUE_TYPE
{0}END_FUNC_SIGNATURE)~";
}

namespace ast{
    NodeInfo Func_signature_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Func_signature;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Func_signature_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                               const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                               const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto func_value_type_str = func_value_type_->to_string(ids_trie, strs_trie, nested_indent2);
        
        const auto groups_of_args_str = strings::join::join([&nested_indent2, &ids_trie, &strs_trie](const auto& g) -> std::string
                                                            {
                                                                return g->to_string(ids_trie, strs_trie, nested_indent2);
                                                            }, 
                                                            groups_of_args_.begin(), 
                                                            groups_of_args_.end(), 
                                                            std::string{"\n"});

        return fmt::format(func_signature_fmt, 
                           indent_str, indent1_str, 
                           groups_of_args_str, 
                           func_value_type_str);
    }

    std::shared_ptr<Node> Func_signature_node::clone() const
    {
        return std::make_shared<Func_signature_node>(groups_of_args_, func_value_type_);
    }    
}