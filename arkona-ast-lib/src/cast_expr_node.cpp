/*
    File:    cast_expr_node.cpp
    Created: 15 August 2023 at 20:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/cast_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* cast_expr_fmt = R"~({0}CAST_EXPR
{1}CAST_KIND {2}
{1}LHS
{3}
{1}END_LHS
{1}RHS
{4}
{1}END_RHS
{0}END_CAST_EXPR)~";

    const char* cast_kinds[] = {
        "Reinterpret", "Convert"
    };
}

namespace ast{
    NodeInfo Cast_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Cast_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Cast_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                          const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                          const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto from_str = from_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto to_str = to_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(cast_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           cast_kinds[static_cast<unsigned>(c_kind_)],
                           from_str,
                           to_str);
    }

    std::shared_ptr<Node> Cast_expr_node::clone() const
    {
        return std::make_shared<Cast_expr_node>(from_, to_, c_kind_);
    }
}