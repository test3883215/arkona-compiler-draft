/*
    File:    set_expr_node.cpp
    Created: 15 August 2023 at 21:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/set_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* set_expr_fmt = R"~({0}SET_EXPR
{1}SET_ELEMENTS
{2}
{1}END_SET_ELEMENTS
{0}END_SET_EXPR)~";

    const char* set_elem_fmt = R"~({0}SET_ELEMENT
{1}
{0}END_SET_ELEMENT)~";
}

namespace ast{
    NodeInfo Set_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Set_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Set_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                         const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                         const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto elems_str = strings::join::join([&ids_trie, &strs_trie, &nested_indent3, &indent2_str](const auto& e) -> std::string
                                                   {
                                                       return fmt::format(set_elem_fmt, 
                                                                          indent2_str, 
                                                                          e->to_string(strs_trie, ids_trie, nested_indent3));
                                                   }, 
                                                   elems_.begin(), 
                                                   elems_.end(), 
                                                   std::string{"\n"});

        return fmt::format(set_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           elems_str);
    }

    std::shared_ptr<Node> Set_expr_node::clone() const
    {
        return std::make_shared<Set_expr_node>(elems_);
    }    
}