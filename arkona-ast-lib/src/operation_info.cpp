/*
    File:    operation_info.cpp
    Created: 30 July 2023 at 15:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/operation_info.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* operation_kinds[] = {
        "Assignment",  "Init",        "Reset",       "Call",
        "Indexation",  "Prefix_inc",  "Postfix_inc", "Log_or",      
        "Log_and",     "Log_not",     "Compare",     "Rel_op",      
        "Bit_or",      "Bit_and",     "Bit_not",     "Add_like",
        "Mul_like",    "Pow_like",    "Unary_plus",  "Sizeof_like", 
        "Deref_op",    "Field_access"
    };

    const char* assignment_kinds[] = {
        "Assignment",             "Copy",                        "Logical_and_assign",       "Logical_and_full_assign", 
        "Logical_and_not_assign", "Logical_and_not_full_assign", "Logical_or_assign",        "Logical_or_full_assign",
        "Logical_or_not_assign",  "Logical_or_not_full_assign",  "Logical_xor_assign",       "Bitwise_and_assign",
        "Bitwise_and_not_assign", "Bitwise_or_assign",           "Bitwise_or_not_assign",    "Bitwise_xor_assign",
        "Left_shift_assign",      "Right_shift_assign",          "Cyclic_left_shift_assign", "Cyclic_right_shift_assign",
        "Plus_assign",            "Minus_assign",                "Type_add_assign",          "Algebraic_separator_assign",
        "Mul_assign",             "Div_assign",                  "Set_difference_assign",    "Symmetric_difference_assign",
        "Remainder_assign",       "Float_remainder_assign",      "Power_assign",             "Float_power_assign",
    };

    const char* init_kinds[] = {
        ""
    };

    const char* reset_kinds[] = {
        ""
    };

    const char* call_kinds[] = {
        ""
    };

    const char* indexation_kinds[] = {
        ""
    };

    const char* prefix_inc_kinds[] = {
        "Inc", "Dec", "Wrapping_inc", "Wrapping_dec"
    };

    const char* postfix_inc_kinds[] = {
        "Inc", "Dec", "Wrapping_inc", "Wrapping_dec"
    };

    const char* log_or_kinds[] = {
        "Logical_or",           "Logical_or_full", "Logical_or_not",  
        "Logical_or_not_full",  "Logical_xor"
    };

    const char* log_and_kinds[] = {
        "Logical_and",     "Logical_and_full", 
        "Logical_and_not", "Logical_and_not_full"
    };

    const char* log_not_kinds[] = {
        ""
    };

    const char* compare_kinds[] = {
        ""
    };

    const char* rel_op_kind[] = {
        "Equivalence", "Is_elem", "LT", "GT", 
        "LEQ",         "GEQ",     "EQ", "NEQ"
    };

    const char* bit_or_kinds[] = {
        "Bit_or", "Bit_or_not", "Bit_xor"
    };

    const char* bit_and_kinds[] = {
        "Bit_and",     "Bit_and_not",       "Left_shift", 
        "Right_shift", "Cyclic_left_shift", "Cyclic_right_shift"
    };

    const char* bit_not_kinds[] = {
        ""
    };

    const char* add_like_kinds[] = {
        "Add", "Sub", "Algebraic_separator", "Type_add"
    };

    const char* mul_like_kinds[] = {
        "Mul",                  "Div",       "Set_difference", 
        "Symmetric_difference", "Remainder", "Float_remainder"
    };

    const char* pow_like_kinds[] = {
        "Power", "Float_power"
    };

    const char* unary_plus_kinds[] = {
        "Unary_plus",                  "Unary_minus",           "Get_expr_type",         "Minimal_value_in_collection",
        "Maximal_value_in_collection", "Minimal_value_of_type", "Maximal_value_of_type",
    };

    const char* sizeof_like_kinds[] = {
        "Sizeof", "Dynamic_array_sizeof", "Num_of_set_elems"
    };

    const char* deref_op_kinds[] = {
        ""
    };

    const char* field_access_kinds[] = {
        ""
    };

    const char* operation_info_fmt = "OPERATION_INFO {{KIND: {0}, SUBKIND: {1}}}";

    const char** subkinds_arrays[] = {
        assignment_kinds, init_kinds,         reset_kinds,       call_kinds,
        indexation_kinds, prefix_inc_kinds,   postfix_inc_kinds, log_or_kinds,
        log_and_kinds,    log_not_kinds,      compare_kinds,     rel_op_kind,
        bit_or_kinds,     bit_and_kinds,      bit_not_kinds,     add_like_kinds,
        mul_like_kinds,   pow_like_kinds,     unary_plus_kinds,  sizeof_like_kinds,
        deref_op_kinds,   field_access_kinds,
    };
}

namespace ast{
        std::string to_string(Operation_info oinfo)
        {
            return fmt::format(operation_info_fmt, 
                               operation_kinds[static_cast<unsigned>(oinfo.kind_)], 
                               subkinds_arrays[static_cast<unsigned>(oinfo.kind_)][static_cast<unsigned>(oinfo.subkind_)]);
        }
}