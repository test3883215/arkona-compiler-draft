/*
    File:    num_of_values_for_index_node.cpp
    Created: 13 August 2023 at 15:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/num_of_values_for_index_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* num_of_values_for_inex_fmt = R"~({0}NUM_OF_VALUES_FOR_INDEX
{1}LHS
{2}
{1}END_LHS
{1}RHS
{3}
{1}END_RHS
{0}END_NUM_OF_VALUES_FOR_INDEX)~";
}

namespace ast{
    NodeInfo Num_of_values_for_index_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Num_of_values_for_index;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Num_of_values_for_index_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                        const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                        const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(num_of_values_for_inex_fmt, 
                           indent_str, 
                           indent1_str,
                           lhs_str,
                           rhs_str);
    }

    std::shared_ptr<Node> Num_of_values_for_index_node::clone() const
    {
        return std::make_shared<Num_of_values_for_index_node>(lhs_, rhs_);
    }
}