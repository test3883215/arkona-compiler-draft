/*
    File:    meta_indexed_forall_stmt_node.cpp
    Created: 12 July 2023 at 20:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/meta_indexed_forall_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* forall_stmt_fmt = R"~({0}META_INDEXED_FORALL_STMT
{1}META_INDEXED_FORALL_LABEL {2}
{1}META_INDEXED_FORALL_INDEX_VAR {3}
{1}META_INDEXED_FORALL_ELEM_VAR {4}
{1}META_INDEXED_FORALL_COLLECTION_EXPR
{5}
{1}END_META_INDEXED_FORALL_COLLECTION_EXPR
{1}META_INDEXED_FORALL_BODY
{6}
{1}END_META_INDEXED_FORALL_BODY
{0}END_META_INDEXED_FORALL)~";
}

namespace ast{
    NodeInfo Meta_indexed_forall_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Meta_indexed_forall_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Meta_indexed_forall_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                         const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                         const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto label_str           = ast::to_string(loop_label_id_, ids_trie);
        const auto index_var_str       = ast::to_string(index_var_id_, ids_trie);
        const auto elem_var_str        = ast::to_string(elem_var_id_, ids_trie);
        const auto collection_expr_str = collection_expr_->to_string(strs_trie, ids_trie, nested_indent2);

        std::string loop_body_str;
        if(loop_body_)
        {
            loop_body_str = loop_body_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        return fmt::format(forall_stmt_fmt, 
                           indent_str, 
                           indent1_str, 
                           label_str, 
                           index_var_str, 
                           elem_var_str,
                           collection_expr_str, 
                           loop_body_str);
    }

    std::shared_ptr<Node> Meta_indexed_forall_stmt_node::clone() const
    {
        return std::make_shared<Meta_indexed_forall_stmt_node>(loop_label_id_, 
                                                               index_var_id_,
                                                               elem_var_id_,
                                                               collection_expr_,    
                                                               loop_body_);
    }
}