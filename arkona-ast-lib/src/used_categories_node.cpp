/*
    File:    used_categories_node.cpp
    Created: 31 July 2023 at 20:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/used_categories_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* used_categories_fmt = R"~({0}USED_CATEGORIES
{1}
{0}END_USED_CATEGORIES)~";
}

namespace ast{
    NodeInfo Used_categories_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Used_categories;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Used_categories_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                        const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                        const Indents&                                   indents) const
    {
        Indents nested_indent{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');

        const auto categories_str = strings::join::join([&nested_indent, &ids_trie, &strs_trie](const auto& c) -> std::string
                                                        {
                                                            return c->to_string(ids_trie, strs_trie, nested_indent);
                                                        }, 
                                                        categories_.begin(), 
                                                        categories_.end(), 
                                                        std::string{"\n"});

        return fmt::format(used_categories_fmt, 
                           indent_str, 
                           categories_str);
    }

    std::shared_ptr<Node> Used_categories_node::clone() const
    {
        return std::make_shared<Used_categories_node>(categories_);
    }    
}