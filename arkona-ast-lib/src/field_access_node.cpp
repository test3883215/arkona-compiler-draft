/*
    File:    field_access_node.cpp
    Created: 23 August 2023 at 18:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/field_access_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* field_access_fmt = R"~({0}FIELD_ACCESS
{1}ID {2}
{0}END_FIELD_ACCESS)~";
}

namespace ast{
    NodeInfo Field_access_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Field_access;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Field_access_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                             const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                             const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto id_str = ast::to_string(id_, ids_trie);

        return fmt::format(field_access_fmt, 
                           indent_str, 
                           indent1_str, 
                           id_str);
    }

    std::shared_ptr<Node> Field_access_node::clone() const
    {
        return std::make_shared<Field_access_node>(id_);
    }    
}