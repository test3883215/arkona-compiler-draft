/*
    File:    struct_type_expr_node.cpp
    Created: 20 August 2023 at 13:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/struct_type_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* struct_type_expr_fmt = R"~({0}STRUCT_TYPE_EXPR
{1}STRUCT_NAME {2}
{1}STRUCT_ELEMS
{3}
{1}END_STRUCT_ELEMS
{0}END_STRUCT_TYPE_EXPR)~";
}

namespace ast{
    NodeInfo Struct_type_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Struct_type_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Struct_type_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie, 
                                                 const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                 const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto struct_name_str = ast::to_string(struct_name_, ids_trie);
        const auto elems_str       = elems_->to_string(strs_trie, ids_trie, nested_indent2);
        
        return fmt::format(struct_type_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           struct_name_str, 
                           elems_str);
    }

    std::shared_ptr<Node> Struct_type_expr_node::clone() const
    {
        return std::make_shared<Struct_type_expr_node>(struct_name_, elems_);
    }    
}