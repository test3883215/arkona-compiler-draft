/*
    File:    spider_branch_node.cpp
    Created: 13 July 2023 at 20:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/spider_branch_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* spider_branch_fmt = R"~({0}SPIDER_BRANCH
{1}SPIDER_BRANCH_EXPRESSION
{2}
{1}END_SPIDER_BRANCH_EXPRESSION
{1}BLOCK_FOR_SPIDER_BRANCH_EXPRESSION
{3}
{1}END_BLOCK_FOR_SPIDER_BRANCH_EXPRESSION
{0}END_SPIDER_BRANCH)~";
}

namespace ast{
    NodeInfo Spider_branch_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Spider_branch;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Spider_branch_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        
        return fmt::format(spider_branch_fmt, 
                           indent_str, 
                           indent1_str, 
                           branch_expression_->to_string(strs_trie, ids_trie, nested_indent2), 
                           block_->to_string(strs_trie, ids_trie, nested_indent2));
    }

    std::shared_ptr<Node> Spider_branch_node::clone() const
    {
        return std::make_shared<Spider_branch_node>(branch_expression_, block_);
    }
}