/*
    File:    continue_stmt_node.cpp
    Created: 09 July 2023 at 19:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/continue_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* continue_stmt_fmt = R"~({0}CONTINUE {1})~";
}

namespace ast{
    NodeInfo Continue_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Continue_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Continue_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                              const Indents&                                   indents) const
    {
        const auto indent_str = std::string(indents.indent_, ' ');
        const auto label_name = ast::to_string(label_id_, ids_trie);

        return fmt::format(continue_stmt_fmt, indent_str, label_name);
    }
    
    std::shared_ptr<Node> Continue_stmt_node::clone() const
    {
        return std::make_shared<Continue_stmt_node>(label_id_);
    }
}