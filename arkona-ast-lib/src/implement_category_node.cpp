/*
    File:    implement_category_node.cpp
    Created: 05 August 2023 at 15:54 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/implement_category_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* implement_category_fmt = R"~({0}IMPLEMENT_CATEGORY
{1}EXPORT_STATUS {2}
{1}CATEGORY_NAME {3}
{1}CATEGORY_ACTUAL_ARGS
{4}
{1}END_CATEGORY_ACTUAL_ARGS
{0}END_IMPLEMENT_CATEGORY)~";

    const char* arg_fmt = R"~({0}CATEGORY_ACTUAL_ARG
{1}PARAMETRIC_KIND {2}
{1}ARGUMENT
{3}
{1}END_ARGUMENT
{0}END_CATEGORY_ACTUAL_ARG)~";

    const char* parametrical_kind_strs[] = {
        "Parametrical", "Non_parametrical"
    };
}

namespace ast{
    NodeInfo Implement_category_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Implement_category;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Implement_category_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                   const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                   const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent4{indents.indent_ + 4 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');
        const auto indent3_str = std::string(nested_indent3.indent_, ' ');

        const auto exported_kind_str      = ast::to_string(exported_kind_);
        const auto category_name_info_str = ast::to_string(category_name_info_, ids_trie);

        const auto args_str = strings::join::join([&ids_trie, &strs_trie, &indent2_str, &indent3_str, &nested_indent4](const auto& arg_info) -> std::string
                                                  {
                                                      return fmt::format(arg_fmt, 
                                                                         indent2_str, 
                                                                         indent3_str,
                                                                         parametrical_kind_strs[static_cast<unsigned>(arg_info.kind_)],
                                                                         arg_info.arg_->to_string(ids_trie, strs_trie, nested_indent4));
                                                  }, 
                                                  args_.begin(), 
                                                  args_.end(), 
                                                  std::string{"\n"});

        return fmt::format(implement_category_fmt, 
                           indent_str, 
                           indent1_str, 
                           exported_kind_str, 
                           category_name_info_str,
                           args_str);
    }

    std::shared_ptr<Node> Implement_category_node::clone() const
    {
        return std::make_shared<Implement_category_node>(exported_kind_, category_name_info_, args_);
    }    
}