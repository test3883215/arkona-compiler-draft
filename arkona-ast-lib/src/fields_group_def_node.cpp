/*
    File:    fields_group_def_node.cpp
    Created: 20 August 2023 at 12:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/fields_group_def_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* fields_group_def_fmt = R"~({0}STRUCT_FIELDS_GROUP
{1}FIELDS
{2}
{1}END_FIELDS
{1}FIELDS_TYPE
{3}
{1}END_FIELDS_TYPE
{0}END_STRUCT_FIELDS_GROUP)~";
}

namespace ast{
    NodeInfo Fields_group_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Fields_group_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Fields_group_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                 const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                 const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto fields_type_str = fields_type_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto fields_str = strings::join::join([&indent2_str, &ids_trie](const auto& info) -> std::string
                                                    {
                                                        return indent2_str + ast::to_string(info, ids_trie);
                                                    }, 
                                                    field_infos_.begin(), 
                                                    field_infos_.end(), 
                                                    std::string{"\n"});

        return fmt::format(fields_group_def_fmt, 
                           indent_str, 
                           indent1_str, 
                           fields_str, 
                           fields_type_str);
    }

    std::shared_ptr<Node> Fields_group_def_node::clone() const
    {
        return std::make_shared<Fields_group_def_node>(field_infos_, fields_type_);
    }
}