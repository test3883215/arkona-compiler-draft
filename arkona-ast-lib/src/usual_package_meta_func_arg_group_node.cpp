/*
    File:    usual_package_meta_func_arg_group_node.cpp
    Created: 01 August 2023 at 21:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/usual_package_meta_func_arg_group_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* usual_var_group_fmt = R"~({0}USUAL_PACKAGE_META_FUNC_ARG_GROUP
{1}ARGS
{2}
{1}END_ARGS
{1}ARGS_TYPE
{3}
{1}END_ARGS_TYPE
{0}END_USUAL_PACKAGE_META_FUNC_ARG_GROUP)~";
}

namespace ast{
    NodeInfo Usual_package_meta_func_arg_group_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Usual_package_meta_func_arg_group;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Usual_package_meta_func_arg_group_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                                  const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                  const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto args_type_str = args_type_->to_string(strs_trie, ids_trie, nested_indent2);

        const auto args_str = strings::join::join([&indent2_str, &ids_trie](const auto& info) -> std::string
                                                  {
                                                      return indent2_str + ast::to_string(info, ids_trie);
                                                  }, 
                                                  arg_infos_.begin(), 
                                                  arg_infos_.end(), 
                                                  std::string{"\n"});

        return fmt::format(usual_var_group_fmt, 
                           indent_str, 
                           indent1_str, 
                           args_str, 
                           args_type_str);
    }

    std::shared_ptr<Node> Usual_package_meta_func_arg_group_node::clone() const
    {
        return std::make_shared<Usual_package_meta_func_arg_group_node>(arg_infos_, args_type_);
    }
}