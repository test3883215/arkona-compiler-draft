/*
    File:    alias_node.cpp
    Created: 29 July 2023 at 21:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/alias_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* alias_node_fmt = R"~({0}ALIAS_DEF
{1}EXPORT_STATUS {4}
{1}ALIAS_NAME {2}
{1}ALIAS_DEF_BODY
{3}
{1}END_ALIAS_DEF_BODY
{0}END_ALIAS_DEF)~";
}

namespace ast{
    NodeInfo Alias_def_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Alias_def;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Alias_def_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                          const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                          const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto alias_def_body_str = alias_def_body_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto alias_name_str     = ast::to_string(alias_name_info_, ids_trie);
        const auto exported_kind_str  = ast::to_string(exported_kind_);

        return fmt::format(alias_node_fmt, 
                           indent_str,
                           indent1_str,
                           alias_name_str,
                           alias_def_body_str,
                           exported_kind_str);
    }

    std::shared_ptr<Node> Alias_def_node::clone() const
    {
        return std::make_shared<Alias_def_node>(exported_kind_,
                                                alias_name_info_,
                                                alias_def_body_);
    }
}