/*
    File:    enum_type_expr_node.cpp
    Created: 20 August 2023 at 11:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/enum_type_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* enum_type_expr_fmt = R"~({0}ENUM_TYPE_EXPR
{1}ENUM_NAME {2}
{1}ENUM_ELEMS
{3}
{1}END_ENUM_ELEMS
{0}END_ENUM_TYPE_EXPR)~";

    const char* enum_type_elem_fmt = R"~({0}ENUM_ELEM
{1}ELEM_NAME {2}
{1}ELEM_VALUE {3}
{0}END_ENUM_ELEM)~";
}

namespace ast{
    NodeInfo Enum_type_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Enum_type_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Enum_type_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>&,
                                               const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                               const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');
        const auto indent3_str = std::string(nested_indent3.indent_, ' ');

        std::size_t elem_value = 0;

        const auto enum_name_str = ast::to_string(enum_name_, ids_trie);
        const auto elems_str     = strings::join::join([&ids_trie, &indent2_str, &indent3_str, &elem_value](const auto& e) -> std::string
                                                       {
                                                           const auto result = fmt::format(enum_type_elem_fmt,
                                                                                           indent2_str,
                                                                                           indent3_str,
                                                                                           ast::to_string(e, ids_trie), 
                                                                                           std::to_string(elem_value));
                                                           ++elem_value;
                                                           return result;
                                                       },
                                                       elems_.begin(), 
                                                       elems_.end(), 
                                                       std::string{"\n"});
        return fmt::format(enum_type_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           enum_name_str, 
                           elems_str);
    }

    std::shared_ptr<Node> Enum_type_expr_node::clone() const
    {
        return std::make_shared<Enum_type_expr_node>(enum_name_, elems_);
    }    
}