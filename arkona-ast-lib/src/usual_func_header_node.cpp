/*
    File:    usual_func_header_node.cpp
    Created: 02 August 2023 at 21:04 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/usual_func_header_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* usual_func_header_fmt = R"~({0}USUAL_FUNC_HEADER
{1}PURE_KIND {2}
{1}FUNC_NAME {3}
{1}FUNC_SIGNATURE
{4}
{1}END_FUNC_SIGNATURE
{0}END_USUAL_FUNC_HEADER)~";
}

namespace ast{
    NodeInfo Usual_func_header_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Usual_func_header;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Usual_func_header_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                               const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                               const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const std::string pure_kind_str      = ast::to_string(pure_kind_);
        const auto        func_name_info_str = ast::to_string(func_name_info_, ids_trie);
        const auto        func_signature_str = func_signature_->to_string(ids_trie, strs_trie, nested_indent2);

        return fmt::format(usual_func_header_fmt, 
                           indent_str, 
                           indent1_str, 
                           pure_kind_str, 
                           func_name_info_str, 
                           func_signature_str);
    }

    std::shared_ptr<Node> Usual_func_header_node::clone() const
    {
        return std::make_shared<Usual_func_header_node>(pure_kind_, func_name_info_, func_signature_);
    }    
}