/*
    File:    package_meta_func_args_package_with_same_parametrical_type_node.cpp
    Created: 01 August 2023 at 22:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/package_meta_func_args_package_with_same_parametrical_type_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* package_meta_func_args_package_with_same_parametric_fmt = R"~({0}ARGS_META_PACKAGE_WITH_SAME_PARAMETRIC_TYPE
{1}ARGS {2}
{1}ARGS_TYPE
{3}
{1}END_ARGS_TYPE
{0}END_ARGS_META_PACKAGE_WITH_SAME_PARAMETRIC_TYPE)~";
}

namespace ast{
    NodeInfo Package_meta_func_args_package_with_same_parametrical_type_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Args_meta_package_with_same_parametric_type;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Package_meta_func_args_package_with_same_parametrical_type_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                                                           const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                                           const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto arg_package_type_str = arg_package_type_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto args_str             = ast::to_string(arg_package_info_, ids_trie);

        return fmt::format(package_meta_func_args_package_with_same_parametric_fmt, 
                           indent_str, 
                           indent1_str, 
                           args_str, 
                           arg_package_type_str);
    }

    std::shared_ptr<Node> Package_meta_func_args_package_with_same_parametrical_type_node::clone() const
    {
        return std::make_shared<Package_meta_func_args_package_with_same_parametrical_type_node>(arg_package_info_, arg_package_type_);
    }
}