/*
    File:    alloc_scale_expr_node.cpp
    Created: 21 August 2023 at 19:20 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/alloc_scale_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* alloc_scale_expr_fmt = R"~({0}ALLOC_SCALE_EXPR
{1}ALLOC_SCALE_DIMS
{2}
{1}END_ALLOC_SCALE_DIMS
{0}END_ALLOC_SCALE_EXPR)~";

    const char* alloc_scale_dim_fmt = R"~({0}ALLOC_SCALE_DIM
{1}
{0}END_ALLOC_SCALE_DIM)~";
}

namespace ast{
    NodeInfo Alloc_scale_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Alloc_scale_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Alloc_scale_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                 const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                 const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent3{indents.indent_ + 3 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto dims_str = strings::join::join([&ids_trie, &strs_trie, &nested_indent3, &indent2_str](const auto& d) -> std::string
                                                   {
                                                       return fmt::format(alloc_scale_dim_fmt, 
                                                                          indent2_str, 
                                                                          d->to_string(strs_trie, ids_trie, nested_indent3));
                                                   }, 
                                                   dims_.begin(), 
                                                   dims_.end(), 
                                                   std::string{"\n"});

        return fmt::format(alloc_scale_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           dims_str);
    }

    std::shared_ptr<Node> Alloc_scale_expr_node::clone() const
    {
        return std::make_shared<Alloc_scale_expr_node>(dims_);
    }    
}