/*
    File:    logical_and_expr_node.cpp
    Created: 08 August 2023 at 20:35 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/logical_and_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* logical_and_expr_fmt = R"~({0}LOGICAL_AND_EXPR
{1}LOGICAL_AND_KIND {2}
{1}LHS
{3}
{1}END_LHS
{1}RHS
{4}
{1}END_RHS
{0}END_LOGICAL_AND_EXPR)~";

    const char* logical_and_kinds[] = {
        "Logical_and",     "Logical_and_full", 
        "Logical_and_not", "Logical_and_not_full"
    };
}

namespace ast{
    NodeInfo Logical_and_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Logical_and_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Logical_and_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto lhs_str = lhs_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto rhs_str = rhs_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(logical_and_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           logical_and_kinds[static_cast<unsigned>(a_kind_)],
                           lhs_str,
                           rhs_str);
    }

    std::shared_ptr<Node> Logical_and_expr_node::clone() const
    {
        return std::make_shared<Logical_and_expr_node>(lhs_, rhs_, a_kind_);
    }
}