/*
    File:    module_node.cpp
    Created: 07 June 2023 at 18:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/module_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* module_fmt = R"~({0}MODULE
{0}NAME: {{
{1}
{0}}}
{2}
{0}{{
{3}
{0}}})~";
};

namespace ast{
    NodeInfo Module_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Module;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Module_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                       const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                       const Indents&                                   indents) const
    {
        std::string result;

        const auto indent_str = std::string(indents.indent_, ' ');

        Indents nested_indent{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        std::string used_modules_str;

        if(used_modules_){
            used_modules_str = used_modules_->to_string(strs_trie, ids_trie, nested_indent);
        }

        std::string body_str;
        
        if(body_){
            body_str = body_->to_string(strs_trie, ids_trie, nested_indent);
        }

        result = fmt::format(module_fmt,
                             indent_str,
                             name_->to_string(strs_trie, ids_trie, nested_indent),
                             used_modules_str, 
                             body_str);

        return result;
    }
    
    std::shared_ptr<Node> Module_node::clone() const
    {
        return std::make_shared<Module_node>(name_, used_modules_, body_);
    }
}