/*
    File:    for_stmt_node.cpp
    Created: 12 July 2023 at 18:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/for_stmt_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    static const char* for_stmt_fmt = R"~({0}FOR_STMT
{1}FOR_LABEL {2}
{1}FOR_COUNTER_VAR {3}
{1}FOR_START_EXPR
{4}
{1}END_FOR_START_EXPR
{1}FOR_END_EXPR
{5}
{1}END_FOR_END_EXPR
{1}FOR_STEP_EXPR
{6}
{1}END_FOR_STEP_EXPR
{1}FOR_BODY
{7}
{1}END_FOR_BODY
{0}END_FOR)~";
}

namespace ast{
    NodeInfo For_stmt_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::For_stmt;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string For_stmt_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                         const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                         const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto label_str       = ast::to_string(loop_label_id_, ids_trie);
        const auto counter_var_str = ast::to_string(counter_var_id_, ids_trie);
        const auto start_expr_str  = start_expr_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto end_expr_str    = end_expr_->to_string(strs_trie, ids_trie, nested_indent2);

        std::string step_expr_str;
        if(step_expr_)
        {
            step_expr_str = step_expr_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        std::string loop_body_str;
        if(loop_body_)
        {
            loop_body_str = loop_body_->to_string(strs_trie, ids_trie, nested_indent2);
        }

        return fmt::format(for_stmt_fmt, 
                           indent_str, 
                           indent1_str, 
                           label_str, 
                           counter_var_str, 
                           start_expr_str, 
                           end_expr_str, 
                           step_expr_str, 
                           loop_body_str);
    }

    std::shared_ptr<Node> For_stmt_node::clone() const
    {
        return std::make_shared<For_stmt_node>(loop_label_id_, 
                                               counter_var_id_,
                                               start_expr_,
                                               end_expr_, 
                                               step_expr_,     
                                               loop_body_);
    }
}