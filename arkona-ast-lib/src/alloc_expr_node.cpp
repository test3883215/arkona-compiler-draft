/*
    File:    alloc_expr_node.cpp
    Created: 21 August 2023 at 18:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/alloc_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* alloc_expr_fmt = R"~({0}ALLOC_EXPR
{1}ALLOC_TYPE
{2}
{1}END_ALLOC_TYPE
{0}END_ALLOC_EXPR)~";
}

namespace ast{
    NodeInfo Alloc_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Alloc_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Alloc_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                           const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                           const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto elem_type_str = elem_type_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(alloc_expr_fmt, 
                           indent_str, 
                           indent1_str, 
                           elem_type_str);
    }

    std::shared_ptr<Node> Alloc_expr_node::clone() const
    {
        return std::make_shared<Alloc_expr_node>(elem_type_);
    }    
}