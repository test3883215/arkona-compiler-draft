/*
    File:    free_expr_node.cpp
    Created: 22 August 2023 at 18:53 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/free_expr_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* free_expr_fmt = R"~({0}FREE_EXPR
{1}FREE_KIND {2}
{1}FREE_ELEM_TYPE
{3}
{1}END_FREE_ELEM_TYPE
{0}END_FREE_EXPR)~";

    const char* free_kinds[] = {
        "Array", "Scale", "Scalar"
    };
}

namespace ast{
    NodeInfo Free_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Free_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Free_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                          const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                          const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto elem_type_str = elem_type_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(free_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           free_kinds[static_cast<unsigned>(kind_)],
                           elem_type_str);
    }

    std::shared_ptr<Node> Free_expr_node::clone() const
    {
        return std::make_shared<Free_expr_node>(elem_type_, kind_);
    }    
}