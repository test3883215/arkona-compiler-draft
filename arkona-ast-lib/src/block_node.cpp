/*
    File:    block_node.cpp
    Created: 24 June 2023 at 21:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/block_node.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* block_fmt = R"~({0}BLOCK
{1}
{0}}})~";
}

namespace ast{
    NodeInfo Block_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Block;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Block_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const
    {
        std::string result;

        const auto indent_str = std::string(indents.indent_, ' ');

        Indents nested_indent{indents.indent_ + indents.indent_inc_, indents.indent_inc_};

        std::vector<std::string> entries;
        for(const auto& entry : entries_)
        {
            entries.push_back(entry->to_string(strs_trie, ids_trie, nested_indent));
        }

        const auto entries_str = strings::join::join(entries.begin(), entries.end(), std::string{"\n"});

        result = fmt::format(block_fmt, indent_str, entries_str);

        return result;
    }

    std::shared_ptr<Node> Block_node::clone() const
    {
        return std::make_shared<Block_node>(entries_);
    }
}