/*
    File:    address_like_expr_node.cpp
    Created: 13 August 2023 at 15:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/address_like_expr_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* address_like_expr_fmt = R"~({0}ADDRESS_LIKE_EXPR
{1}ADDRESS_LIKE_KIND {2}
{1}OP
{3}
{1}END_OP
{0}END_ADDRESS_LIKE_EXPR)~";

    const char* address_like_kinds[] = {
        "Address", "Data_address", "Get_elem_type"
    };
}

namespace ast{
    NodeInfo Address_like_expr_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Address_like_expr;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Address_like_expr_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                  const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                  const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');

        const auto op_str = op_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(address_like_expr_fmt, 
                           indent_str, 
                           indent1_str,
                           address_like_kinds[static_cast<unsigned>(s_kind_)],
                           op_str);
    }

    std::shared_ptr<Node> Address_like_expr_node::clone() const
    {
        return std::make_shared<Address_like_expr_node>(op_, s_kind_);
    }
}