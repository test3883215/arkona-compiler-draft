/*
    File:    initialized_var_node.cpp
    Created: 17 July 2023 at 22:06 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/initialized_var_node.hpp"
#include "../../thirdparty/fmtlib/include/core.h"

namespace{
    const char* initialized_var_fmt = R"~({0}INITIALIZED_VAR
{1}VAR_NAME
{2}
{1}END_VAR_NAME
{1}VAR_TYPE
{3}
{1}END_VAR_TYPE
{1}VAR_VALUE
{4}
{1}END_VAR_VALUE
{0}END_INITIALIZED_VAR)~";
}

namespace ast{
    NodeInfo Initialized_var_node::get_info() const
    {
        NodeInfo ninfo;

        ninfo.kind_    = Node_kind::Initialized_var;
        ninfo.subkind_ = 0;

        return ninfo;
    }

    std::string Initialized_var_node::to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                                const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                const Indents&                                   indents) const
    {
        Indents nested_indent1{indents.indent_ + indents.indent_inc_, indents.indent_inc_};
        Indents nested_indent2{indents.indent_ + 2 * indents.indent_inc_, indents.indent_inc_};

        const auto indent_str  = std::string(indents.indent_, ' ');
        const auto indent1_str = std::string(nested_indent1.indent_, ' ');
        const auto indent2_str = std::string(nested_indent2.indent_, ' ');

        const auto var_type_str  = var_type_->to_string(strs_trie, ids_trie, nested_indent2);
        const auto var_name_str  = indent2_str + ast::to_string(var_info_, ids_trie);
        const auto var_value_str = var_value_->to_string(strs_trie, ids_trie, nested_indent2);

        return fmt::format(initialized_var_fmt, 
                           indent_str, 
                           indent1_str, 
                           var_name_str, 
                           var_type_str, 
                           var_value_str);
    }

    std::shared_ptr<Node> Initialized_var_node::clone() const
    {
        return std::make_shared<Initialized_var_node>(var_info_, var_type_, var_value_);
    }
}