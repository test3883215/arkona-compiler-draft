/*
    File:    expr_node.hpp
    Created: 04 July 2023 at 20:04 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/block_entry_node.hpp"

namespace ast{
    struct Expr_node : public Block_entry_node{
        Expr_node()                 = default;
        Expr_node(const Expr_node&) = default;
        virtual ~Expr_node()        = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;

        std::shared_ptr<Node> clone() const = 0;
    };
}