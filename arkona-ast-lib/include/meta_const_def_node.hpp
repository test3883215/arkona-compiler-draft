/*
    File:    meta_const_def_node.hpp
    Created: 20 July 2023 at 20:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Meta_const_def_node : public Stmt_node{
        Meta_const_def_node()                           = default;
        Meta_const_def_node(const Meta_const_def_node&) = default;
        virtual ~Meta_const_def_node()                  = default;

        struct Const_info{
            Id_info                    const_name_;
            std::shared_ptr<Expr_node> const_type_;
            std::shared_ptr<Expr_node> const_value_;
        };

        Meta_const_def_node(Exported_kind exported_kind, const std::vector<Const_info>& const_infos)
            : exported_kind_{exported_kind}, const_infos_{const_infos}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Exported_kind           exported_kind_ = Exported_kind::Not_exported;
        std::vector<Const_info> const_infos_;
    };
}