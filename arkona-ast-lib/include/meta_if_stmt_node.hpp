/*
    File:    meta_if_stmt_node.hpp
    Created: 06 July 2023 at 22:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <utility>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Meta_if_stmt_node : public Stmt_node{
        Meta_if_stmt_node()                         = default;
        Meta_if_stmt_node(const Meta_if_stmt_node&) = default;
        virtual ~Meta_if_stmt_node()                = default;

        using Branch = std::pair<std::shared_ptr<Expr_node>, std::shared_ptr<Block_node>>;

        Meta_if_stmt_node(const Branch&                      if_branch, 
                          const std::vector<Branch>&         elif_branches,
                          const std::shared_ptr<Block_node>& else_branch)
            : if_branch_{if_branch}, elif_branches_{elif_branches}, else_branch_{else_branch}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Branch                      if_branch_;
        std::vector<Branch>         elif_branches_;
        std::shared_ptr<Block_node> else_branch_;
    };
}