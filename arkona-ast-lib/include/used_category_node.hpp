/*
    File:    used_category_node.hpp
    Created: 31 July 2023 at 19:20 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <vector>
#include "../include/ast_node.hpp"
#include "../include/qualified_id_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Used_category_node : public Node{
        Used_category_node()                          = default;
        Used_category_node(const Used_category_node&) = default;
        virtual ~Used_category_node()                 = default;

        using Category_args = std::vector<std::shared_ptr<Expr_node>>;

        Used_category_node(const std::shared_ptr<Qualified_id_node>& category_name, 
                           const Category_args&                      category_args)
            : category_name_{category_name}
            , category_args_{category_args}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Qualified_id_node> category_name_;
        Category_args                      category_args_;
    };
}