/*
    File:    ast_node.hpp
    Created: 07 June 2023 at 17:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>
#include "../../strings-utils/include/char_trie.hpp"

namespace ast{
    enum class Node_kind : std::uint16_t{
        Unknown,                                                             Module,
        Used_modules,                                                        Qualified_id,
        Block,                                                               Block_stmt,
        If_stmt,                                                             Meta_if_stmt,
        Select_branch,                                                       Select_stmt,
        Meta_select_branch,                                                  Meta_select,
        Match_branch,                                                        Match_stmt,
        Meta_match_branch,                                                   Meta_match_stmt,
        Exit_stmt,                                                           Meta_exit_stmt,
        Continue_stmt,                                                       Meta_continue_stmt,
        Return_stmt,                                                         Meta_return_stmt, 
        While_stmt,                                                          Meta_while_stmt,
        Do_while_stmt,                                                       Meta_do_while_stmt,
        Forever_stmt,                                                        Meta_forever_stmt,
        For_stmt,                                                            Meta_for_stmt,
        Forall_stmt,                                                         Meta_forall_stmt,
        Indexed_forall_stmt,                                                 Meta_indexed_forall_stmt,
        Spider_branch,                                                       Spider_stmt,
        Meta_spider_branch,                                                  Meta_spider_stmt,
        Usual_var_group,                                                     Initialized_var,
        Tuple_of_vars,                                                       Var_def,
        Meta_var_def,                                                        Const_def,
        Meta_const_def,                                                      Type_def,
        Func_arg_group,                                                      Func_signature,
        Usual_func_def,                                                      Abstract_generic_func_def,
        Generic_func_def,                                                    Usual_package_func_arg_group,
        Args_package_with_same_non_parametric_type,                          Args_package_with_same_parametric_type,
        Args_package_with_maybe_same_parametric_type,                        Args_tuple_package_with_same_non_parametric_type,
        Args_tuple_package_with_same_parametric_type,                        Args_tuple_package_with_maybe_same_parametric_type,
        Package_func_signature,                                              Abstract_generic_package_func_def,
        Package_generic_func_def,                                            Package_func_def,
        Alias_def,                                                           Operation_header,
        Operation_def,                                                       Used_category,
        Used_categories,                                                     Meta_func_arg_group,
        Meta_func_signature,                                                 Meta_func_def,
        Usual_package_meta_func_arg_group,                                   Args_meta_package_with_same_non_parametric_type,
        Args_meta_package_with_same_parametric_type,                         Args_meta_package_with_maybe_same_parametric_type,
        Args_tuple_meta_package_with_same_non_parametric_type,               Args_tuple_meta_package_with_same_parametric_type,  
        Args_tuple_meta_package_with_maybe_same_parametric_type,             Package_meta_func_signature,
        Package_meta_func_def,                                               Category_arg_group,
        Const_as_category_body,                                              Usual_func_header,
        Package_func_header,                                                 Operation_header_as_category_body_elem,
        Func_header_as_category_body_elem,                                   Category_body,
        Category_def,                                                        Usual_package_category_arg_group,
        Package_category_args_tuple_package_with_maybe_same_parametric_type, Package_category_args_tuple_package_with_same_non_parametric_type,
        Package_category_args_tuple_package_with_same_parametric_type,       Package_category_args_package_with_maybe_same_parametric_type,
        Package_category_args_package_with_same_non_parametric_type,         Package_category_args_package_with_same_parametric_type,
        Package_category_def,                                                Implement_category,
        Const_as_category_impl_elem,                                         Func_header_as_category_impl_elem,
        Operation_header_as_category_impl_elem,                              Category_impl_body,
        Category_impl,                                                       Assignment_expr,
        Ternary_expr,                                                        Logical_or_expr,
        Logical_and_expr,                                                    Logical_not_expr,
        Compare_expr,                                                        Rel_op_expr,
        Range_expr,                                                          Bit_or_expr,
        Bit_and_expr,                                                        Bit_not_expr,
        Add_like_expr,                                                       Mul_like_expr,
        Pow_like_expr,                                                       Prefix_inc_expr,
        Postfix_inc_expr,                                                    Unary_plus_expr,
        Deref_expr,                                                          Ptr_def_expr,
        Sizeof_like_expr,                                                    Num_of_values_for_index,
        Address_like_expr,                                                   Cast_expr,
        Tuple_expr,                                                          Set_expr,
        Array_expr,                                                          Scale_expr,
        Gen_type_expr,                                                       Constructor_expr,
        Destructor_expr,                                                     Lambda_expr,
        Scale_type_expr,                                                     Array_type_expr,
        Set_type_expr,                                                       Ref_type_expr,
        Enum_type_expr,                                                      Fields_group_def,
        Struct_fields_def,                                                   Struct_type_expr,
        Type_type_expr,                                                      Auto_type_expr,
        Func_type_expr,                                                      Fixed_size_base_type_expr,
        Non_fixed_size_base_type_expr,                                       Alloc_expr,
        Alloc_array_expr,                                                    Alloc_scale_expr,
        Free_expr,                                                           Literal_expr,
        Field_values_list,                                                   Scoped_id,
        Field_access,                                                        Call_args,
        Indexing_args,                                                       Meta_call_args,
        Name_expr,
    };

    struct NodeInfo{
        Node_kind     kind_;
        std::uint16_t subkind_;
    };

    struct Indents{
        std::size_t indent_;
        std::size_t indent_inc_;
    };

    struct Node{
        Node()            = default;
        Node(const Node&) = default;
        virtual ~Node()   = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;
                                      
        virtual std::shared_ptr<Node> clone() const = 0;                                      
    };
}