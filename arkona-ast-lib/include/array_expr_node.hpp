/*
    File:    array_expr_node.hpp
    Created: 15 August 2023 at 21:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Array_expr_node : public Value_expr_node{
        Array_expr_node()                       = default;
        Array_expr_node(const Array_expr_node&) = default;
        virtual ~Array_expr_node()              = default;

        using Elems_of_array = std::vector<std::shared_ptr<Expr_node>>;

        explicit Array_expr_node(const Elems_of_array& elems)
            : elems_{elems}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Elems_of_array elems_;
    };
}