/*
    File:    func_type_expr_node.hpp
    Created: 20 August 2023 at 15:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/value_expr_node.hpp"
#include "../include/pure_kind.hpp"
#include "../include/func_signature_node.hpp"

namespace ast{
    struct Func_type_expr_node : public Value_expr_node{
        Func_type_expr_node()                           = default;
        Func_type_expr_node(const Func_type_expr_node&) = default;
        virtual ~Func_type_expr_node()                  = default;

        Func_type_expr_node(Pure_kind                                   pure_kind, 
                            const std::shared_ptr<Func_signature_node>& func_signature)
            : pure_kind_{pure_kind}
            , func_signature_{func_signature}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Pure_kind                            pure_kind_     = Pure_kind::Non_pure;
        std::shared_ptr<Func_signature_node> func_signature_;
    };
}