/*
    File:    func_signature_node.hpp
    Created: 22 July 2023 at 20:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/func_arg_group_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Func_signature_node : public Node{
        Func_signature_node()                           = default;
        Func_signature_node(const Func_signature_node&) = default;
        virtual ~Func_signature_node()                  = default;

        using Groups_of_args = std::vector<std::shared_ptr<Func_arg_group_node>>;
        
        Func_signature_node(const Groups_of_args&             groups_of_args, 
                            const std::shared_ptr<Expr_node>& func_value_type)
            : groups_of_args_{groups_of_args}
            , func_value_type_{func_value_type}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Groups_of_args             groups_of_args_;
        std::shared_ptr<Expr_node> func_value_type_;
    };
}