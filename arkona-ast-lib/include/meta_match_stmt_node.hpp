/*
    File:    meta_match_stmt_node.hpp
    Created: 09 July 2023 at 18:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <utility>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/meta_match_branch_node.hpp"

namespace ast{
    struct Meta_match_stmt_node : public Stmt_node{
        Meta_match_stmt_node()                            = default;
        Meta_match_stmt_node(const Meta_match_stmt_node&) = default;
        virtual ~Meta_match_stmt_node()                   = default;

        using Branches = std::vector<std::shared_ptr<Meta_match_branch_node>>;

        Meta_match_stmt_node(const std::shared_ptr<Expr_node>&  value_to_match, 
                             const Branches&                    branches,
                             const std::shared_ptr<Block_node>& else_branch)
            : value_to_match_{value_to_match}, branches_{branches}, else_branch_{else_branch}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node>  value_to_match_;
        Branches                    branches_;
        std::shared_ptr<Block_node> else_branch_;
    };
}