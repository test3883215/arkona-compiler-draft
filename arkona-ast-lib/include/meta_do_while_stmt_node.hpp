/*
    File:    meta_do_while_stmt_node.hpp
    Created: 11 July 2023 at 20:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Meta_do_while_stmt_node : public Stmt_node{
        Meta_do_while_stmt_node()                          = default;
        Meta_do_while_stmt_node(const Meta_do_while_stmt_node&) = default;
        virtual ~Meta_do_while_stmt_node()                 = default;

        Meta_do_while_stmt_node(const Id_info&                     loop_label_id,
                                const std::shared_ptr<Block_node>& loop_body,
                                const std::shared_ptr<Expr_node>&  loop_condition)
            : loop_label_id_{loop_label_id}
            , loop_body_{loop_body}
            , loop_condition_{loop_condition}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Id_info                     loop_label_id_;
        std::shared_ptr<Block_node> loop_body_       = nullptr;
        std::shared_ptr<Expr_node>  loop_condition_  = nullptr;
    };
}