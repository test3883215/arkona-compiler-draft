/*
    File:    used_categories_node.hpp
    Created: 31 July 2023 at 19:54 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <vector>
#include "../include/ast_node.hpp"
#include "../include/used_category_node.hpp"

namespace ast{
    struct Used_categories_node : public Node{
        Used_categories_node()                            = default;
        Used_categories_node(const Used_categories_node&) = default;
        virtual ~Used_categories_node()                   = default;

        using Categories = std::vector<std::shared_ptr<Used_category_node>>;
        
        explicit Used_categories_node(const Categories& categories)
            : categories_{categories}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Categories categories_;
    };
}