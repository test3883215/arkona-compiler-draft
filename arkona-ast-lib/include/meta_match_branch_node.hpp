/*
    File:    meta_match_branch_node.hpp
    Created: 09 July 2023 at 17:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Meta_match_branch_node : public Node{
        Meta_match_branch_node()                              = default;
        Meta_match_branch_node(const Meta_match_branch_node&) = default;
        virtual ~Meta_match_branch_node()                     = default;

        Meta_match_branch_node(const std::shared_ptr<Expr_node>&  branch_expression, 
                               const std::shared_ptr<Block_node>& block)
            : branch_expression_{branch_expression}, block_{block}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node>  branch_expression_;
        std::shared_ptr<Block_node> block_;
    };
}