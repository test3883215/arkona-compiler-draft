/*
    File:    category_implementation_node.hpp
    Created: 05 August 2023 at 18:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/qualified_id_node.hpp"
#include "../include/category_impl_body_node.hpp"
#include "../include/stmt_node.hpp"

namespace ast{
    struct Category_implementation_node : public Stmt_node{
        Category_implementation_node()                                    = default;
        Category_implementation_node(const Category_implementation_node&) = default;
        virtual ~Category_implementation_node()                           = default;

        enum class Parametric_kind{
            Parametrical, Non_parametrical
        };

        struct Arg_info{
            Parametric_kind            kind_;
            std::shared_ptr<Expr_node> arg_;
        };

        using Args_info = std::vector<Arg_info>;

        Category_implementation_node(Exported_kind                                   exported_kind, 
                                     const std::shared_ptr<Qualified_id_node>&       category_name_info,
                                     const Args_info&                                args,
                                     const std::shared_ptr<Category_impl_body_node>& body)
            : exported_kind_ {exported_kind}
            , category_name_info_{category_name_info}
            , args_{args}
            , body_{body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Exported_kind                            exported_kind_ = Exported_kind::Not_exported;
        std::shared_ptr<Qualified_id_node>       category_name_info_;
        Args_info                                args_;
        std::shared_ptr<Category_impl_body_node> body_;
    };
}