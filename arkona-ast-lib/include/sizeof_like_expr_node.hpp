/*
    File:    sizeof_like_expr_node.hpp
    Created: 12 August 2023 at 19:48 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"
#include "../include/operation_info.hpp"

namespace ast{
    struct Sizeof_like_expr_node : public Expr_node{
        Sizeof_like_expr_node()                             = default;
        Sizeof_like_expr_node(const Sizeof_like_expr_node&) = default;
        virtual ~Sizeof_like_expr_node()                    = default;

        Sizeof_like_expr_node(const std::shared_ptr<Expr_node>& op,
                              Sizeof_like_kind                  s_kind)
            : op_{op}
            , s_kind_{s_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> op_  = nullptr;
        Sizeof_like_kind           s_kind_;
    };
}