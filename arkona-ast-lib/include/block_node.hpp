/*
    File:    block_node.hpp
    Created: 24 June 2023 at 21:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/ast_node.hpp"
#include "../include/block_entry_node.hpp"

namespace ast{
    struct Block_node : public Node{
        Block_node()                  = default;
        Block_node(const Block_node&) = default;
        virtual ~Block_node()         = default;

        using Block_entries = std::vector<std::shared_ptr<Block_entry_node>>;

        explicit Block_node(const Block_entries& entries)
            : entries_{entries}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Block_entries entries_;
    };
}