/*
    File:    name_component_node.hpp
    Created: 23 June 2023 at 17:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/ast_node.hpp"

namespace ast{
    struct Name_component_node : public Node{
        Name_component_node()                           = default;
        Name_component_node(const Name_component_node&) = default;
        virtual ~Name_component_node()                  = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;

        std::shared_ptr<Node> clone() const = 0;
    };
}