/*
    File:    range_expr_node.hpp
    Created: 10 August 2023 at 20:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Range_expr_node : public Expr_node{
        Range_expr_node()                       = default;
        Range_expr_node(const Range_expr_node&) = default;
        virtual ~Range_expr_node()              = default;

        enum class Range_op_kind{
            Including_end, Excluding_end
        };

        Range_expr_node(const std::shared_ptr<Expr_node>& start_value, 
                        const std::shared_ptr<Expr_node>& end_value, 
                        const std::shared_ptr<Expr_node>& step_value, 
                        Range_op_kind                     r_kind)
            : start_value_{start_value}
            , end_value_{end_value}
            , step_value_{step_value}
            , r_kind_{r_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> start_value_  = nullptr;
        std::shared_ptr<Expr_node> end_value_    = nullptr;
        std::shared_ptr<Expr_node> step_value_   = nullptr;
        Range_op_kind              r_kind_       = Range_op_kind::Including_end;
    };
}