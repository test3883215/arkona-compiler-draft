/*
    File:    value_expr_node.hpp
    Created: 16 August 2023 at 19:36 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"

namespace ast{
    struct Value_expr_node : public Expr_node{
        Value_expr_node()                       = default;
        Value_expr_node(const Value_expr_node&) = default;
        virtual ~Value_expr_node()              = default;

        virtual NodeInfo get_info() const = 0;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const = 0;

        std::shared_ptr<Node> clone() const = 0;
    };
}