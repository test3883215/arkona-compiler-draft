/*
    File:    id_info.hpp
    Created: 16 July 2023 at 15:48 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <string>
#include "../../iscanner/include/position.hpp"
#include "../../strings-utils/include/char_trie.hpp"
#include "../../strings-utils/include/idx_to_string.hpp"

namespace ast{
    struct Id_info{
        std::size_t             identifier_id_ = 0;
        iscaner::Position_range pos_;
    };
    
    std::string to_string(const Id_info& id_info, const std::shared_ptr<strings::trie::Char_trie>& ids_trie);

    bool equals(const Id_info& lhs, const Id_info& rhs);

    struct less{
        constexpr bool operator()(const Id_info& lhs, const Id_info& rhs)
        {
            return lhs.identifier_id_ < rhs.identifier_id_;
        }
    };

    bool operator==(const Id_info& lhs, const Id_info& rhs);
}