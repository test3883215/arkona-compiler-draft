/*
    File:    struct_fields_def_node.hpp
    Created: 20 August 2023 at 12:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"
#include "../include/fields_group_def_node.hpp"

namespace ast{
    struct Struct_fields_def_node : public Node{
        Struct_fields_def_node()                              = default;
        Struct_fields_def_node(const Struct_fields_def_node&) = default;
        virtual ~Struct_fields_def_node()                     = default;

        using Struct_fields_groups = std::vector<std::shared_ptr<Fields_group_def_node>>;

        explicit Struct_fields_def_node(const Struct_fields_groups& fields_groups)
            : fields_groups_{fields_groups}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Struct_fields_groups fields_groups_;
    };
}