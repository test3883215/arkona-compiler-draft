/*
    File:    operation_header_as_category_body_elem_node.hpp
    Created: 02 August 2023 at 21:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/category_body_elem_node.hpp"
#include "../include/operation_header_node.hpp"

namespace ast{
    struct Operation_header_as_category_body_elem_node : public Category_body_elem_node{
        Operation_header_as_category_body_elem_node()                                                   = default;
        Operation_header_as_category_body_elem_node(const Operation_header_as_category_body_elem_node&) = default;
        virtual ~Operation_header_as_category_body_elem_node()                                          = default;

        explicit Operation_header_as_category_body_elem_node(const std::shared_ptr<Operation_header_node>& func_header)
            : func_header_{func_header}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Operation_header_node> func_header_;
    };
}