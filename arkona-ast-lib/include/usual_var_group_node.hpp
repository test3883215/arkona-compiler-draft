/*
    File:    usual_var_group_node.hpp
    Created: 16 July 2023 at 15:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/var_group_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Usual_var_group_node : public Var_group_node{
        Usual_var_group_node()                            = default;
        Usual_var_group_node(const Usual_var_group_node&) = default;
        virtual ~Usual_var_group_node()                   = default;

        Usual_var_group_node(const std::vector<Id_info>&       var_infos, 
                             const std::shared_ptr<Expr_node>& vars_type)
            : var_infos_{var_infos}, vars_type_{vars_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::vector<Id_info>       var_infos_;
        std::shared_ptr<Expr_node> vars_type_;
    };
}