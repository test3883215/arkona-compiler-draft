/*
    File:    operation_header_node.hpp
    Created: 30 July 2023 at 13:45 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/exported_kind.hpp"
#include "../include/func_signature_node.hpp"
#include "../include/ast_node.hpp"
#include "../include/operation_info.hpp"
#include "../include/pure_kind.hpp"

namespace ast{
    struct Operation_header_node : public Node{
        Operation_header_node()                             = default;
        Operation_header_node(const Operation_header_node&) = default;
        virtual ~Operation_header_node()                    = default;

        Operation_header_node(Pure_kind                                   pure_kind, 
                              const Operation_info&                       op_info, 
                              const std::shared_ptr<Func_signature_node>& func_signature)
            : pure_kind_{pure_kind}
            , op_info_{op_info}
            , func_signature_{func_signature}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Pure_kind                            pure_kind_     = Pure_kind::Non_pure;
        Operation_info                       op_info_;
        std::shared_ptr<Func_signature_node> func_signature_;
    };
}