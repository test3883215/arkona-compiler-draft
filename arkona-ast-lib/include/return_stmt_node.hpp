/*
    File:    return_stmt_node.hpp
    Created: 10 July 2023 at 20:27 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Return_stmt_node : public Stmt_node{
        Return_stmt_node()                        = default;
        Return_stmt_node(const Return_stmt_node&) = default;
        virtual ~Return_stmt_node()               = default;

        explicit Return_stmt_node(const std::shared_ptr<Expr_node>& returned_expr)
            : returned_expr_{returned_expr}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Expr_node> returned_expr_ = nullptr;
    };
}