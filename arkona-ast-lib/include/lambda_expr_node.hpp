/*
    File:    lambda_expr_node.hpp
    Created: 17 August 2023 at 19:42 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/value_expr_node.hpp"
#include "../include/block_node.hpp"
#include "../include/func_signature_node.hpp"

namespace ast{
    struct Lambda_expr_node : public Value_expr_node{
        Lambda_expr_node()                        = default;
        Lambda_expr_node(const Lambda_expr_node&) = default;
        virtual ~Lambda_expr_node()               = default;
        
        Lambda_expr_node(const std::shared_ptr<Func_signature_node>& func_signature, 
                         const std::shared_ptr<Block_node>&          func_body)
            : func_signature_{func_signature}
            , func_body_{func_body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Func_signature_node> func_signature_;
        std::shared_ptr<Block_node>          func_body_;
    };
}