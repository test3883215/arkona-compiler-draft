/*
    File:    compare_expr_node.hpp
    Created: 09 August 2023 at 20:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Compare_expr_node : public Expr_node{
        Compare_expr_node()                         = default;
        Compare_expr_node(const Compare_expr_node&) = default;
        virtual ~Compare_expr_node()                = default;

        Compare_expr_node(const std::shared_ptr<Expr_node>& lhs, 
                          const std::shared_ptr<Expr_node>& rhs)
            : lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> lhs_  = nullptr;
        std::shared_ptr<Expr_node> rhs_  = nullptr;
    };
}