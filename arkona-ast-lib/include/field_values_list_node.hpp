/*
    File:    field_values_list_node.hpp
    Created: 23 August 2023 at 15:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <map>
#include "../include/ast_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Field_values_list_node : public Node{
        Field_values_list_node()                              = default;
        Field_values_list_node(const Field_values_list_node&) = default;
        virtual ~Field_values_list_node()                     = default;

        using Field_values = std::map<Id_info, std::shared_ptr<Expr_node>, ast::less>;

        explicit Field_values_list_node(const Field_values& values)
            : values_{values}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Field_values values_;
    };
}