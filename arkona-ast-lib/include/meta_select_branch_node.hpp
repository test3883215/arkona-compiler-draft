/*
    File:    meta_select_branch_node.hpp
    Created: 09 July 2023 at 13:55 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <utility>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Meta_select_branch_node : public Node{
        Meta_select_branch_node()                               = default;
        Meta_select_branch_node(const Meta_select_branch_node&) = default;
        virtual ~Meta_select_branch_node()                      = default;

        using Branch_expressions = std::vector<std::shared_ptr<Expr_node>>;

        Meta_select_branch_node(const Branch_expressions&          branch_expressions, 
                                const std::shared_ptr<Block_node>& block)
            : branch_expressions_{branch_expressions}, block_{block}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Branch_expressions          branch_expressions_;
        std::shared_ptr<Block_node> block_;
    };
}