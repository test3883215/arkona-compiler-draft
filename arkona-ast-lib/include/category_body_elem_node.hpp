/*
    File:    category_body_elem_node.hpp
    Created: 02 July 2023 at 20:22 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/ast_node.hpp"

namespace ast{
    struct Category_body_elem_node : public Node{
        Category_body_elem_node()                               = default;
        Category_body_elem_node(const Category_body_elem_node&) = default;
        virtual ~Category_body_elem_node()                      = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;

        virtual std::shared_ptr<Node> clone() const = 0;
    };
}