/*
    File:    address_like_expr_node.hpp
    Created: 13 August 2023 at 15:48 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Address_like_expr_node : public Expr_node{
        Address_like_expr_node()                              = default;
        Address_like_expr_node(const Address_like_expr_node&) = default;
        virtual ~Address_like_expr_node()                     = default;

        enum class Address_like_kind{
            Address, Data_address, Get_elem_type
        };

        Address_like_expr_node(const std::shared_ptr<Expr_node>& op,
                               Address_like_kind                 s_kind)
            : op_{op}
            , s_kind_{s_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> op_  = nullptr;
        Address_like_kind          s_kind_;
    };
}