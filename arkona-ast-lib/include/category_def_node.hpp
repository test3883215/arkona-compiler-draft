/*
    File:    category_def_node.hpp
    Created: 03 August 2023 at 19:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/category_arg_group_node.hpp"
#include "../include/category_body_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/id_info.hpp"
#include "../include/stmt_node.hpp"
#include "../include/used_categories_node.hpp"

namespace ast{
    struct Category_def_node : public Stmt_node{
        Category_def_node()                         = default;
        Category_def_node(const Category_def_node&) = default;
        virtual ~Category_def_node()                = default;

        using Category_arg_groups = std::vector<std::shared_ptr<Category_arg_group_node>>;

        Category_def_node(Exported_kind                                exported_kind, 
                          const Id_info&                               category_name_info,
                          const Category_arg_groups&                   arg_groups,
                          const std::shared_ptr<Used_categories_node>& used_categories,
                          const std::shared_ptr<Category_body_node>&   category_body)
            : exported_kind_ {exported_kind}
            , category_name_info_{category_name_info}
            , arg_groups_{arg_groups}
            , used_categories_{used_categories}
            , category_body_{category_body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Exported_kind                         exported_kind_ = Exported_kind::Not_exported;
        Id_info                               category_name_info_;
        Category_arg_groups                   arg_groups_;
        std::shared_ptr<Used_categories_node> used_categories_ = nullptr;
        std::shared_ptr<Category_body_node>   category_body_;
    };
}