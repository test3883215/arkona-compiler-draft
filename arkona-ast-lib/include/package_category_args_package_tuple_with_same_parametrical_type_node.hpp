/*
    File:    package_category_args_package_tuple_with_same_parametrical_type_node.hpp
    Created: 03 July 2023 at 20:54 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/package_category_arg_group_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Package_category_args_package_tuple_with_same_parametrical_type_node : public Package_category_arg_group_node{
        Package_category_args_package_tuple_with_same_parametrical_type_node()                                                                            = default;
        Package_category_args_package_tuple_with_same_parametrical_type_node(const Package_category_args_package_tuple_with_same_parametrical_type_node&) = default;
        virtual ~Package_category_args_package_tuple_with_same_parametrical_type_node()                                                                   = default;

        Package_category_args_package_tuple_with_same_parametrical_type_node(const std::vector<Id_info>&       arg_infos, 
                                                                         const std::shared_ptr<Expr_node>& args_type)
            : arg_infos_{arg_infos}, args_type_{args_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::vector<Id_info>       arg_infos_;
        std::shared_ptr<Expr_node> args_type_;
    };
}