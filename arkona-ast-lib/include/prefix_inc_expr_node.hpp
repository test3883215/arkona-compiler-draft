/*
    File:    prefix_inc_expr_node.hpp
    Created: 12 August 2023 at 16:51 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/operation_info.hpp"

namespace ast{
    struct Prefix_inc_expr_node : public Expr_node{
        Prefix_inc_expr_node()                            = default;
        Prefix_inc_expr_node(const Prefix_inc_expr_node&) = default;
        virtual ~Prefix_inc_expr_node()                   = default;

        Prefix_inc_expr_node(const std::shared_ptr<Expr_node>&   op, 
                             const std::vector<Prefix_inc_kind>& prefix_incs)
            : op_{op}
            , prefix_incs_{prefix_incs}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node>   op_ = nullptr;
        std::vector<Prefix_inc_kind> prefix_incs_;
    };
}