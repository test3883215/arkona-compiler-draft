/*
    File:    block_stmt_node.hpp
    Created: 04 July 2023 at 20:12 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"

namespace ast{
    struct Block_stmt_node : public Stmt_node{
        Block_stmt_node()                       = default;
        Block_stmt_node(const Block_stmt_node&) = default;
        virtual ~Block_stmt_node()              = default;
        
        explicit Block_stmt_node(const std::shared_ptr<Block_node>& block)
            : block_{block}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Block_node> block_;
    };
}