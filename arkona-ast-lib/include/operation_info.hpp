/*
    File:    operation_info.hpp
    Created: 30 July 2023 at 13:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstdint>
#include <string>

namespace ast{
        enum class Operation_kind : uint32_t{
            Assignment,  Init,        Reset,       Call,
            Indexation,  Prefix_inc,  Postfix_inc, Log_or,      
            Log_and,     Log_not,     Compare,     Rel_op,      
            Bit_or,      Bit_and,     Bit_not,     Add_like,
            Mul_like,    Pow_like,    Unary_plus,  Sizeof_like, 
            Deref_op,    Field_access
        };

        enum class Assignment_kind : uint32_t{
            Assignment,             Copy,                        Logical_and_assign,       Logical_and_full_assign, 
            Logical_and_not_assign, Logical_and_not_full_assign, Logical_or_assign,        Logical_or_full_assign,
            Logical_or_not_assign,  Logical_or_not_full_assign,  Logical_xor_assign,       Bitwise_and_assign,
            Bitwise_and_not_assign, Bitwise_or_assign,           Bitwise_or_not_assign,    Bitwise_xor_assign,
            Left_shift_assign,      Right_shift_assign,          Cyclic_left_shift_assign, Cyclic_right_shift_assign,
            Plus_assign,            Minus_assign,                Type_add_assign,          Algebraic_separator_assign,
            Mul_assign,             Div_assign,                  Set_difference_assign,    Symmetric_difference_assign,
            Remainder_assign,       Float_remainder_assign,      Power_assign,             Float_power_assign,
        };

        enum class Prefix_inc_kind : uint32_t{
            Inc, Dec, Wrapping_inc, Wrapping_dec
        };

        enum class Postfix_inc_kind : uint32_t{
            Inc, Dec, Wrapping_inc, Wrapping_dec
        };

        enum class Log_or_kind : uint32_t{
            Logical_or,           Logical_or_full, Logical_or_not,  
            Logical_or_not_full,  Logical_xor
        };

        enum class Log_and_kind : uint32_t{
            Logical_and,     Logical_and_full, 
            Logical_and_not, Logical_and_not_full
        };

        enum class Rel_op_kind : uint32_t{
            Equivalence, Is_elem, LT, GT, 
            LEQ,         GEQ,     EQ, NEQ
        };

        enum class Bit_or_kind : uint32_t{
            Bit_or, Bit_or_not, Bit_xor
        };

        enum class Bit_and_kind : uint32_t{
            Bit_and,     Bit_and_not,       Left_shift, 
            Right_shift, Cyclic_left_shift, Cyclic_right_shift
        };

        enum class Add_like_kind : uint32_t{
            Add, Sub, Algebraic_separator, Type_add
        };

        enum class Mul_like_kind : uint32_t{
            Mul,                  Div,       Set_difference, 
            Symmetric_difference, Remainder, Float_remainder
        };

        enum class Pow_like_kind : uint32_t{
            Power, Float_power
        };

        enum class Unary_plus_kind : uint32_t{
            Unary_plus,                  Unary_minus,           Get_expr_type,         Minimal_value_in_collection,
            Maximal_value_in_collection, Minimal_value_of_type, Maximal_value_of_type,
        };

        enum class Sizeof_like_kind : uint32_t{
            Sizeof, Dynamic_array_sizeof, Num_of_set_elems
        };

        struct Operation_info{
            Operation_kind kind_;
            uint32_t       subkind_ = 0;     
        };

        std::string to_string(Operation_info oinfo);
}