/*
    File:    meta_return_stmt_node.hpp
    Created: 10 July 2023 at 21:22 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/expr_node.hpp"

namespace ast{
    struct Meta_return_stmt_node : public Stmt_node{
        Meta_return_stmt_node()                             = default;
        Meta_return_stmt_node(const Meta_return_stmt_node&) = default;
        virtual ~Meta_return_stmt_node()                    = default;

        explicit Meta_return_stmt_node(const std::shared_ptr<Expr_node>& returned_expr)
            : returned_expr_{returned_expr}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Expr_node> returned_expr_ = nullptr;
    };
}