/*
    File:    ternary_expr_node.hpp
    Created: 07 August 2023 at 20:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Ternary_expr_node : public Expr_node{
        Ternary_expr_node()                         = default;
        Ternary_expr_node(const Ternary_expr_node&) = default;
        virtual ~Ternary_expr_node()                = default;

        enum class Ternary_op_kind{
            Short_circuit, Full
        };

        Ternary_expr_node(const std::shared_ptr<Expr_node>& condition, 
                          const std::shared_ptr<Expr_node>& true_branch, 
                          const std::shared_ptr<Expr_node>& false_branch, 
                          Ternary_op_kind                   t_kind)
            : condition_{condition}
            , true_branch_{true_branch}
            , false_branch_{false_branch}
            , t_kind_{t_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> condition_     = nullptr;
        std::shared_ptr<Expr_node> true_branch_   = nullptr;
        std::shared_ptr<Expr_node> false_branch_  = nullptr;
        Ternary_op_kind            t_kind_        = Ternary_op_kind::Short_circuit;
    };
}