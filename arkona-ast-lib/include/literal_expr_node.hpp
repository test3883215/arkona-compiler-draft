/*
    File:    literal_expr_node.hpp
    Created: 22 August 2023 at 19:02 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <cstdint>
#include <quadmath.h>
#include <vector>
#include "../include/value_expr_node.hpp"
#include "../../numbers/include/quaternion.hpp"
#include "../../iscanner/include/position.hpp"

namespace ast{
    struct Literal_expr_node : public Value_expr_node{
        Literal_expr_node()                         = default;
        Literal_expr_node(const Literal_expr_node&) = default;
        virtual ~Literal_expr_node()                = default;

        enum class Float_kind : std::size_t{
            Float32, Float64, Float80, Float128
        };

        enum class Complex_kind : std::size_t{
            Complex32, Complex64, Complex80, Complex128
        };

        enum class Quat_kind : std::size_t{
            Quat32, Quat64, Quat80, Quat128
        };

        enum class Char_kind : std::size_t{
            Char8, Char16, Char32
        };

        enum class String_kind : std::size_t{
            String8, String16, String32
        };

        enum class Literal_kind : std::size_t{
            Char,  String,  Integer,
            Float, Complex, Quat,
            Bool,  Ord,     Void
        };
        
        enum class Ordering{
            Less, Equal, Greater
        };

        struct Literal_code{
            Literal_kind kind_;
            std::size_t  subkind_;
        };

        struct Literal_value{
            union{
                unsigned __int128        int_val_;
                __float128               float_val_;
                __complex128             complex_val_;
                quat::quat_t<__float128> quat_val_;
                char32_t                 char_val_;
                std::size_t              str_index_;
                bool                     bool_value_;
                Ordering                 ord_value_;
            };
            Literal_code code_;
        };

        Literal_expr_node(const Literal_value&           value, 
                          const iscaner::Position_range& pos)
            : value_{value}
            , pos_{pos}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Literal_value           value_;
        iscaner::Position_range pos_;
    };
}