/*
    File:    block_entry_node.hpp
    Created: 24 June 2023 at 21:35 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/ast_node.hpp"

namespace ast{
    struct Block_entry_node : public Node{
        Block_entry_node()                        = default;
        Block_entry_node(const Block_entry_node&) = default;
        virtual ~Block_entry_node()               = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;

        std::shared_ptr<Node> clone() const = 0;
    };
}