/*
    File:    unary_plus_expr_node.hpp
    Created: 12 August 2023 at 17:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/operation_info.hpp"

namespace ast{
    struct Unary_plus_expr_node : public Expr_node{
        Unary_plus_expr_node()                            = default;
        Unary_plus_expr_node(const Unary_plus_expr_node&) = default;
        virtual ~Unary_plus_expr_node()                   = default;

        Unary_plus_expr_node(const std::shared_ptr<Expr_node>&    op, 
                              const std::vector<Unary_plus_kind>& unary_pluses)
            : op_{op}
            , unary_pluses_{unary_pluses}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node>   op_ = nullptr;
        std::vector<Unary_plus_kind> unary_pluses_;
    };
}