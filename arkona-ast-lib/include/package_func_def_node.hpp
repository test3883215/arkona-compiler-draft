/*
    File:    package_func_def_node.hpp
    Created: 29 July 2023 at 20:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/block_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/package_func_signature_node.hpp"
#include "../include/pure_kind.hpp"
#include "../include/id_info.hpp"
#include "../include/stmt_node.hpp"

namespace ast{
    struct Package_func_def_node : public Stmt_node{
        Package_func_def_node()                             = default;
        Package_func_def_node(const Package_func_def_node&) = default;
        virtual ~Package_func_def_node()                    = default;

        Package_func_def_node(Exported_kind                                       exported_kind, 
                              Pure_kind                                           pure_kind, 
                              const Id_info&                                      func_name_info, 
                              const std::shared_ptr<Package_func_signature_node>& func_signature, 
                              const std::shared_ptr<Block_node>&                  func_body)
            : exported_kind_ {exported_kind}
            , pure_kind_{pure_kind}
            , func_name_info_{func_name_info}
            , func_signature_{func_signature}
            , func_body_{func_body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Exported_kind                                exported_kind_ = Exported_kind::Not_exported;
        Pure_kind                                    pure_kind_     = Pure_kind::Non_pure;
        Id_info                                      func_name_info_;
        std::shared_ptr<Package_func_signature_node> func_signature_;
        std::shared_ptr<Block_node>                  func_body_     = nullptr;
    };
}