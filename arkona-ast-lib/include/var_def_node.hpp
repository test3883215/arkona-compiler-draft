/*
    File:    var_def_node.hpp
    Created: 17 July 2023 at 23:08 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/var_group_node.hpp"
#include "../include/exported_kind.hpp"

namespace ast{
    struct Var_def_node : public Stmt_node{
        Var_def_node()                    = default;
        Var_def_node(const Var_def_node&) = default;
        virtual ~Var_def_node()           = default;

        using Var_groups = std::vector<std::shared_ptr<Var_group_node>>;
        
        Var_def_node(Exported_kind exported_kind, const Var_groups& var_groups)
            : exported_kind_{exported_kind}, var_groups_{var_groups}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Exported_kind exported_kind_ = Exported_kind::Not_exported;
        Var_groups    var_groups_;
    };
}