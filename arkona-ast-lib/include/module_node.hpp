/*
    File:    module_node.hpp
    Created: 07 June 2023 at 18:34 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>
#include "../include/ast_node.hpp"
#include "../include/qualified_id_node.hpp"
#include "../include/used_modules_node.hpp"
#include "../include/block_node.hpp"

namespace ast{
    struct Module_node : public Node{
        Module_node()                   = default;
        Module_node(const Module_node&) = default;
        virtual ~Module_node()          = default;

        Module_node(const std::shared_ptr<Qualified_id_node>& name,
                    const std::shared_ptr<Used_modules_node>& used_modules,
                    const std::shared_ptr<Block_node>&        body)
            : name_{name}
            , used_modules_{used_modules}
            , body_{body}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Qualified_id_node> name_;
        std::shared_ptr<Used_modules_node> used_modules_;
        std::shared_ptr<Block_node>        body_;
    };
}