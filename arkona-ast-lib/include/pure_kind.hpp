/*
    File:    pure_kind.hpp
    Created: 23 July 2023 at 15:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>

namespace ast{
    enum class Pure_kind{
        Pure, Non_pure
    };
    
    std::string to_string(Pure_kind k);
}