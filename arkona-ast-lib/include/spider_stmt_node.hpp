/*
    File:    spider_stmt_node.hpp
    Created: 13 July 2023 at 20:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/spider_branch_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Spider_stmt_node : public Stmt_node{
        Spider_stmt_node()                        = default;
        Spider_stmt_node(const Spider_stmt_node&) = default;
        virtual ~Spider_stmt_node()               = default;

        using Branches = std::vector<std::shared_ptr<Spider_branch_node>>;

        Spider_stmt_node(const Id_info&                     loop_label_id, 
                         const Branches&                    branches,
                         const std::shared_ptr<Block_node>& else_branch)
            : loop_label_id_{loop_label_id}
            , branches_{branches}
            , else_branch_{else_branch}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Id_info                     loop_label_id_;
        Branches                    branches_;
        std::shared_ptr<Block_node> else_branch_   = nullptr;
    };
}