/*
    File:    auto_type_expr_node.hpp
    Created: 20 August 2023 at 15:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/value_expr_node.hpp"
#include "../include/struct_fields_def_node.hpp"

namespace ast{
    struct Auto_type_expr_node : public Value_expr_node{
        Auto_type_expr_node()                           = default;
        Auto_type_expr_node(const Auto_type_expr_node&) = default;
        virtual ~Auto_type_expr_node()                  = default;

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
    };
}