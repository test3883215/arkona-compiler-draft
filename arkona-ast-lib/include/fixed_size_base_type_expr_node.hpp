/*
    File:    fixed_size_base_type_expr_node.hpp
    Created: 21 August 2023 at 15:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Fixed_size_base_type_expr_node : public Value_expr_node{
        Fixed_size_base_type_expr_node()                                      = default;
        Fixed_size_base_type_expr_node(const Fixed_size_base_type_expr_node&) = default;
        virtual ~Fixed_size_base_type_expr_node()                             = default;

        enum class Fixed_size_base_type_kind{
            Float32,     Float64,    Float80,    Float128,
            Complex32,   Complex64,  Complex80,  Complex128,
            Quat32,      Quat64,     Quat80,     Quat128,
            Unsigned8,   Unsigned16, Unsigned32, Unsigned64,
            Unsigned128, Signed8,    Signed16,   Signed32,
            Signed64,    Signed128,  String8,    String16,
            String32,    Char8,      Char16,     Char32,
            Ord8,        Ord16,      Ord32,      Ord64,
            Bool8,       Bool16,     Bool32,     Bool64,
            Void
        };

        explicit Fixed_size_base_type_expr_node(Fixed_size_base_type_kind kind)
            : kind_{kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Fixed_size_base_type_kind kind_;
    };
}