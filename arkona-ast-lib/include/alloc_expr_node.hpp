/*
    File:    alloc_expr_node.hpp
    Created: 21 August 2023 at 17:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Alloc_expr_node : public Value_expr_node{
        Alloc_expr_node()                       = default;
        Alloc_expr_node(const Alloc_expr_node&) = default;
        virtual ~Alloc_expr_node()              = default;

        explicit Alloc_expr_node(const std::shared_ptr<Expr_node>& elem_type)
            : elem_type_{elem_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Expr_node> elem_type_;
    };
}