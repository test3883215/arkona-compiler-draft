/*
    File:    stmt_node.hpp
    Created: 04 July 2023 at 20:02 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/block_entry_node.hpp"

namespace ast{
    struct Stmt_node : public Block_entry_node{
        Stmt_node()                 = default;
        Stmt_node(const Stmt_node&) = default;
        virtual ~Stmt_node()        = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;

        virtual std::shared_ptr<Node> clone() const = 0;
    };
}