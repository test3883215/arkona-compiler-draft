/*
    File:    implement_category_node.hpp
    Created: 05 August 2023 at 15:18 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/id_info.hpp"
#include "../include/stmt_node.hpp"

namespace ast{
    struct Implement_category_node : public Stmt_node{
        Implement_category_node()                               = default;
        Implement_category_node(const Implement_category_node&) = default;
        virtual ~Implement_category_node()                      = default;

        enum class Parametric_kind{
            Parametrical, Non_parametrical
        };

        struct Arg_info{
            Parametric_kind            kind_;
            std::shared_ptr<Expr_node> arg_;
        };

        using Args_info = std::vector<Arg_info>;

        Implement_category_node(Exported_kind    exported_kind, 
                                const Id_info&   category_name_info,
                                const Args_info& args)
            : exported_kind_ {exported_kind}
            , category_name_info_{category_name_info}
            , args_{args}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Exported_kind exported_kind_ = Exported_kind::Not_exported;
        Id_info       category_name_info_;
        Args_info     args_;
    };
}