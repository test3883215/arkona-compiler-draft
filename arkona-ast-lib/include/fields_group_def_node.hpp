/*
    File:    fields_group_def_node.hpp
    Created: 20 Augus 2023 at 11:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Fields_group_def_node : public Node{
        Fields_group_def_node()                             = default;
        Fields_group_def_node(const Fields_group_def_node&) = default;
        virtual ~Fields_group_def_node()                    = default;

        Fields_group_def_node(const std::vector<Id_info>&       field_infos, 
                              const std::shared_ptr<Expr_node>& fields_type)
            : field_infos_{field_infos}
            , fields_type_{fields_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::vector<Id_info>       field_infos_;
        std::shared_ptr<Expr_node> fields_type_;
    };
}