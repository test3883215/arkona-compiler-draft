/*
    File:    array_type_expr_node.hpp
    Created: 19 August 2023 at 20:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Array_type_expr_node : public Value_expr_node{
        Array_type_expr_node()                            = default;
        Array_type_expr_node(const Array_type_expr_node&) = default;
        virtual ~Array_type_expr_node()                   = default;

        using Indices_of_array_type = std::vector<std::shared_ptr<Expr_node>>;

        Array_type_expr_node(const Indices_of_array_type&      indices, 
                             const std::shared_ptr<Expr_node>& elem_type)
            : indices_{indices}
            , elem_type_{elem_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Indices_of_array_type      indices_;
        std::shared_ptr<Expr_node> elem_type_;
    };
}