/*
    File:    ref_type_expr_node.hpp
    Created: 19 August 2023 at 21:04 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Ref_type_expr_node : public Value_expr_node{
        Ref_type_expr_node()                          = default;
        Ref_type_expr_node(const Ref_type_expr_node&) = default;
        virtual ~Ref_type_expr_node()                 = default;

        enum class Ref_kind{
            Const_ref, Non_const_ref
        };

        Ref_type_expr_node(const std::shared_ptr<Expr_node>& elem_type, 
                           Ref_kind                          kind)
            : elem_type_{elem_type}
            , kind_{kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Expr_node> elem_type_;
        Ref_kind                   kind_ = Ref_kind::Non_const_ref;
    };
}