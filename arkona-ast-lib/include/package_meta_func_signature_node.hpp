/*
    File:    package_meta_func_signature_node.hpp
    Created: 01 August 2023 at 23:03 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/package_meta_func_arg_group_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/used_categories_node.hpp"

namespace ast{
    struct Package_meta_func_signature_node : public Node{
        Package_meta_func_signature_node()                                        = default;
        Package_meta_func_signature_node(const Package_meta_func_signature_node&) = default;
        virtual ~Package_meta_func_signature_node()                               = default;

        using Groups_of_args = std::vector<std::shared_ptr<Package_meta_func_arg_group_node>>;
        
        Package_meta_func_signature_node(const Groups_of_args&                        groups_of_args, 
                                         const std::shared_ptr<Expr_node>&            func_value_type, 
                                         const std::shared_ptr<Used_categories_node>& used_categories)
            : groups_of_args_{groups_of_args}
            , func_value_type_{func_value_type}
            , used_categories_{used_categories}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Groups_of_args                        groups_of_args_;
        std::shared_ptr<Expr_node>            func_value_type_;
        std::shared_ptr<Used_categories_node> used_categories_ = nullptr;

    };
}