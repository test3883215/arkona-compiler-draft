/*
    File:    tuple_of_vars_node.hpp
    Created: 17 July 2023 at 22:26 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/var_group_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Tuple_of_vars_node : public Var_group_node{
        Tuple_of_vars_node()                          = default;
        Tuple_of_vars_node(const Tuple_of_vars_node&) = default;
        virtual ~Tuple_of_vars_node()                 = default;

        Tuple_of_vars_node(const std::vector<Id_info>&       var_infos, 
                           const std::shared_ptr<Expr_node>& tuple_type, 
                           const std::shared_ptr<Expr_node>& tuple_value)
            : var_infos_{var_infos}, tuple_type_{tuple_type}, tuple_value_{tuple_value}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::vector<Id_info>       var_infos_;
        std::shared_ptr<Expr_node> tuple_type_;
        std::shared_ptr<Expr_node> tuple_value_;
    };
}