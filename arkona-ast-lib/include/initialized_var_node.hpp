/*
    File:    initialized_var_node.hpp
    Created: 17 July 2023 at 22:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/var_group_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Initialized_var_node : public Var_group_node{
        Initialized_var_node()                            = default;
        Initialized_var_node(const Initialized_var_node&) = default;
        virtual ~Initialized_var_node()                   = default;

        Initialized_var_node(const Id_info&                   var_info, 
                             const std::shared_ptr<Expr_node>& var_type, 
                             const std::shared_ptr<Expr_node>& var_value)
            : var_info_{var_info}, var_type_{var_type}, var_value_{var_value}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Id_info                    var_info_;
        std::shared_ptr<Expr_node> var_type_;
        std::shared_ptr<Expr_node> var_value_;
    };
}