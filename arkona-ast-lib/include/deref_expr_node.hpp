/*
    File:    deref_expr_node.hpp
    Created: 12 August 2023 at 17:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Deref_expr_node : public Expr_node{
        Deref_expr_node()                       = default;
        Deref_expr_node(const Deref_expr_node&) = default;
        virtual ~Deref_expr_node()              = default;

        Deref_expr_node(const std::shared_ptr<Expr_node>& op, 
                        std::size_t                       num_of_derefs)
            : op_{op}
            , num_of_derefs_{num_of_derefs}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> op_            = nullptr;
        std::size_t                num_of_derefs_ = 0;
    };
}