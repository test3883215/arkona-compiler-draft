/*
    File:    scoped_id_node.hpp
    Created: 23 August 2023 at 18:03 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/name_component_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Scoped_id_node : public Name_component_node{
        Scoped_id_node()                      = default;
        Scoped_id_node(const Scoped_id_node&) = default;
        virtual ~Scoped_id_node()             = default;

        explicit Scoped_id_node(const Id_info& id)
            : id_{id}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Id_info id_;
    };
}