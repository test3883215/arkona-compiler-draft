/*
    File:    package_func_header_node.hpp
    Created: 02 August 2023 at 21:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/func_header_node.hpp"
#include "../include/package_func_signature_node.hpp"
#include "../include/id_info.hpp"
#include "../include/pure_kind.hpp"

namespace ast{
    struct Package_func_header_node : public Func_header_node{
        Package_func_header_node()                                = default;
        Package_func_header_node(const Package_func_header_node&) = default;
        virtual ~Package_func_header_node()                       = default;

        Package_func_header_node(Pure_kind                                           pure_kind, 
                                 const Id_info&                                      func_name_info, 
                                 const std::shared_ptr<Package_func_signature_node>& func_signature)
            : pure_kind_{pure_kind}
            , func_name_info_{func_name_info}
            , func_signature_{func_signature}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Pure_kind                                    pure_kind_     = Pure_kind::Non_pure;
        Id_info                                      func_name_info_;
        std::shared_ptr<Package_func_signature_node> func_signature_;
    };
}