/*
    File:    const_as_category_body_elem_node.hpp
    Created: 02 August 2023 at 20:27 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/category_body_elem_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Const_as_category_body_elem_node : public Category_body_elem_node{
        Const_as_category_body_elem_node()                                        = default;
        Const_as_category_body_elem_node(const Const_as_category_body_elem_node&) = default;
        virtual ~Const_as_category_body_elem_node()                               = default;

        Const_as_category_body_elem_node(const Id_info&                    const_name, 
                                         const std::shared_ptr<Expr_node>& const_type)
            : const_name_{const_name}
            , const_type_{const_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Id_info                    const_name_;
        std::shared_ptr<Expr_node> const_type_;
    };
}