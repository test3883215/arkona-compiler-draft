/*
    File:    exported_kind.hpp
    Created: 17 July 2023 at 23:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>

namespace ast{
    enum class Exported_kind{
        Exported, Not_exported
    };
    
    std::string to_string(Exported_kind k);
}