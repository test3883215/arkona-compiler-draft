/*
    File:    name_expr_node.hpp
    Created: 23 August 2023 at 19:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/name_component_node.hpp"
#include "../include/id_info.hpp"
#include "../include/field_values_list_node.hpp"

namespace ast{
    struct Name_expr_node : public Expr_node{
        Name_expr_node()                      = default;
        Name_expr_node(const Name_expr_node&) = default;
        virtual ~Name_expr_node()             = default;

        using Suffixes     = std::vector<std::shared_ptr<Name_component_node>>;
        using Field_values = std::shared_ptr<Field_values_list_node>;

        Name_expr_node(const Id_info&      base, 
                       const Suffixes&     suffixes, 
                       const Field_values& field_values)
            : base_{base}
            , suffixes_{suffixes}
            , field_values_{field_values}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Id_info      base_;
        Suffixes     suffixes_;
        Field_values field_values_;
    };
}