/*
    File:    func_arg_group_node.hpp
    Created: 22 July 2023 at 20:20 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Func_arg_group_node : public Node{
        Func_arg_group_node()                           = default;
        Func_arg_group_node(const Func_arg_group_node&) = default;
        virtual ~Func_arg_group_node()                  = default;

        Func_arg_group_node(const std::vector<Id_info>&       arg_infos, 
                            const std::shared_ptr<Expr_node>& args_type)
            : arg_infos_{arg_infos}
            , args_type_{args_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::vector<Id_info>       arg_infos_;
        std::shared_ptr<Expr_node> args_type_;
    };
}