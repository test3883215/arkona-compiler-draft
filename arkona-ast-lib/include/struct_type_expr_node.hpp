/*
    File:    struct_type_expr_node.hpp
    Created: 20 August 2023 at 12:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/value_expr_node.hpp"
#include "../include/id_info.hpp"
#include "../include/struct_fields_def_node.hpp"

namespace ast{
    struct Struct_type_expr_node : public Value_expr_node{
        Struct_type_expr_node()                             = default;
        Struct_type_expr_node(const Struct_type_expr_node&) = default;
        virtual ~Struct_type_expr_node()                    = default;

        Struct_type_expr_node(const Id_info&                                 struct_name,
                              const std::shared_ptr<Struct_fields_def_node>& elems)
            : struct_name_{struct_name}
            , elems_{elems}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Id_info                                 struct_name_;
        std::shared_ptr<Struct_fields_def_node> elems_;
    };
}