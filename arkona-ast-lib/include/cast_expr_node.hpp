/*
    File:    cast_expr_node.hpp
    Created: 15 August 2023 at 20:24 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Cast_expr_node : public Value_expr_node{
        Cast_expr_node()                      = default;
        Cast_expr_node(const Cast_expr_node&) = default;
        virtual ~Cast_expr_node()             = default;

        enum class Cast_kind{
            Reinterpret, Convert
        };

        Cast_expr_node(const std::shared_ptr<Expr_node>& from, 
                       const std::shared_ptr<Expr_node>& to, 
                       Cast_kind                         c_kind)
            : from_{from}
            , to_{to}
            , c_kind_{c_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> from_  = nullptr;
        std::shared_ptr<Expr_node> to_    = nullptr;
        Cast_kind                  c_kind_;
    };
}