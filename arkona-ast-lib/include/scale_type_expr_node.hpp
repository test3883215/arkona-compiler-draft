/*
    File:    scale_type_expr_node.hpp
    Created: 19 August 2023 at 19:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Scale_type_expr_node : public Value_expr_node{
        Scale_type_expr_node()                            = default;
        Scale_type_expr_node(const Scale_type_expr_node&) = default;
        virtual ~Scale_type_expr_node()                   = default;

        using Indicis_of_scale_type = std::vector<std::shared_ptr<Expr_node>>;

        explicit Scale_type_expr_node(const Indicis_of_scale_type& indicis)
            : indicis_{indicis}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Indicis_of_scale_type indicis_;
    };
}