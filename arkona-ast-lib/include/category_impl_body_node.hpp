/*
    File:    category_impl_body_node.hpp
    Created: 05 August 2023 at 17:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/category_impl_elem_node.hpp"

namespace ast{
    struct Category_impl_body_node : public Node{
        Category_impl_body_node()                               = default;
        Category_impl_body_node(const Category_impl_body_node&) = default;
        virtual ~Category_impl_body_node()                      = default;

        using Category_impl_body_elems = std::vector<std::shared_ptr<Category_impl_elem_node>>;

        explicit Category_impl_body_node(const Category_impl_body_elems& elems)
            : elems_{elems}
        {
        }

        virtual NodeInfo get_info() const override;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const override;

        virtual std::shared_ptr<Node> clone() const override;

        Category_impl_body_elems elems_;
    };
}