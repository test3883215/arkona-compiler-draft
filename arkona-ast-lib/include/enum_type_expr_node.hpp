/*
    File:    enum_type_expr_node.hpp
    Created: 20 August 2023 at 10:51 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Enum_type_expr_node : public Value_expr_node{
        Enum_type_expr_node()                           = default;
        Enum_type_expr_node(const Enum_type_expr_node&) = default;
        virtual ~Enum_type_expr_node()                  = default;

        using Elements_of_enum_type = std::vector<Id_info>;

        Enum_type_expr_node(const Id_info&               enum_name,
                            const Elements_of_enum_type& elems)
            : enum_name_{enum_name}
            , elems_{elems}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Id_info               enum_name_;
        Elements_of_enum_type elems_;
    };
}