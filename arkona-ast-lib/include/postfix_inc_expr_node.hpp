/*
    File:    postfix_inc_expr_node.hpp
    Created: 12 August 2023 at 17:09 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/operation_info.hpp"

namespace ast{
    struct Postfix_inc_expr_node : public Expr_node{
        Postfix_inc_expr_node()                             = default;
        Postfix_inc_expr_node(const Postfix_inc_expr_node&) = default;
        virtual ~Postfix_inc_expr_node()                    = default;

        Postfix_inc_expr_node(const std::shared_ptr<Expr_node>&    op, 
                              const std::vector<Postfix_inc_kind>& postfix_incs)
            : op_{op}
            , postfix_incs_{postfix_incs}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node>    op_ = nullptr;
        std::vector<Postfix_inc_kind> postfix_incs_;
    };
}