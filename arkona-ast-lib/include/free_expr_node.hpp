/*
    File:    free_expr_node.hpp
    Created: 22 August 2023 at 18:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Free_expr_node : public Value_expr_node{
        Free_expr_node()                      = default;
        Free_expr_node(const Free_expr_node&) = default;
        virtual ~Free_expr_node()             = default;

        enum class Free_kind{
            Array, Scale, Scalar
        };

        Free_expr_node(const std::shared_ptr<Expr_node>& elem_type, 
                       Free_kind                         kind)
            : elem_type_{elem_type}
            , kind_{kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Expr_node> elem_type_;
        Free_kind                  kind_ = Free_kind::Scalar;
    };
}