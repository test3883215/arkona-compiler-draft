/*
    File:    set_type_expr_node.hpp
    Created: 19 August 2023 at 20:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Set_type_expr_node : public Value_expr_node{
        Set_type_expr_node()                          = default;
        Set_type_expr_node(const Set_type_expr_node&) = default;
        virtual ~Set_type_expr_node()                 = default;

        explicit Set_type_expr_node(const std::shared_ptr<Expr_node>& elem_type)
            : elem_type_{elem_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        std::shared_ptr<Expr_node> elem_type_;
    };
}