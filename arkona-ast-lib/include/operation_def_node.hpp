/*
    File:    operation_def_node.hpp
    Created: 30 July 2023 at 19:18 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include "../include/block_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/operation_header_node.hpp"
#include "../include/stmt_node.hpp"

namespace ast{
    struct Operation_def_node : public Stmt_node{
        Operation_def_node()                          = default;
        Operation_def_node(const Operation_def_node&) = default;
        virtual ~Operation_def_node()                 = default;

        Operation_def_node(Exported_kind                                 exported_kind, 
                           const std::shared_ptr<Operation_header_node>& operation_header, 
                           const std::shared_ptr<Block_node>&            func_body)
            : exported_kind_ {exported_kind}
            , operation_header_{operation_header}
            , func_body_{func_body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Exported_kind                          exported_kind_ = Exported_kind::Not_exported;
        std::shared_ptr<Operation_header_node> operation_header_;
        std::shared_ptr<Block_node>            func_body_     = nullptr;
    };
}