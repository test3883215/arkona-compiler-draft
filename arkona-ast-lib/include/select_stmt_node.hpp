/*
    File:    select_stmt_node.hpp
    Created: 08 July 2023 at 18:14 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <utility>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/select_branch_node.hpp"

namespace ast{
    struct Select_stmt_node : public Stmt_node{
        Select_stmt_node()                        = default;
        Select_stmt_node(const Select_stmt_node&) = default;
        virtual ~Select_stmt_node()               = default;

        using Branches = std::vector<std::shared_ptr<Select_branch_node>>;

        Select_stmt_node(const std::shared_ptr<Expr_node>&  value_to_select, 
                         const Branches&                    branches,
                         const std::shared_ptr<Block_node>& else_branch)
            : value_to_select_{value_to_select}, branches_{branches}, else_branch_{else_branch}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node>  value_to_select_;
        Branches                    branches_;
        std::shared_ptr<Block_node> else_branch_;
    };
}