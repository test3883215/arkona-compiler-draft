/*
    File:    type_def_node.hpp
    Created: 22 July 2023 at 18:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include "../include/stmt_node.hpp"
#include "../include/exported_kind.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Type_def_node : public Stmt_node{
        Type_def_node()                     = default;
        Type_def_node(const Type_def_node&) = default;
        virtual ~Type_def_node()            = default;

        struct Type_info{
            Id_info                    type_name_;
            std::shared_ptr<Expr_node> type_def_;
        };

        Type_def_node(Exported_kind                 exported_kind, 
                      const std::vector<Type_info>& type_infos)
            : exported_kind_{exported_kind}
            , type_infos_{type_infos}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Exported_kind          exported_kind_ = Exported_kind::Not_exported;
        std::vector<Type_info> type_infos_;
    };
}