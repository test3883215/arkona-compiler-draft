/*
    File:    package_category_args_package_with_same_non_parametrical_type_node.hpp
    Created: 03 July 2023 at 21:18 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/package_category_arg_group_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Package_category_args_package_with_same_non_parametrical_type_node : public Package_category_arg_group_node{
        Package_category_args_package_with_same_non_parametrical_type_node()                                                                          = default;
        Package_category_args_package_with_same_non_parametrical_type_node(const Package_category_args_package_with_same_non_parametrical_type_node&) = default;
        virtual ~Package_category_args_package_with_same_non_parametrical_type_node()                                                                 = default;

        Package_category_args_package_with_same_non_parametrical_type_node(const Id_info&                    arg_package_info, 
                                                                           const std::shared_ptr<Expr_node>& arg_package_type)
            : arg_package_info_{arg_package_info}, arg_package_type_{arg_package_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Id_info                    arg_package_info_;
        std::shared_ptr<Expr_node> arg_package_type_;
    };
}