/*
    File:    bit_not_expr_node.hpp
    Created: 11 August 2023 at 18:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Bit_not_expr_node : public Expr_node{
        Bit_not_expr_node()                         = default;
        Bit_not_expr_node(const Bit_not_expr_node&) = default;
        virtual ~Bit_not_expr_node()                = default;

        Bit_not_expr_node(const std::shared_ptr<Expr_node>& op, 
                          std::size_t                       num_of_nots)
            : op_{op}
            , num_of_nots_{num_of_nots}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> op_          = nullptr;
        std::size_t                num_of_nots_ = 0;
    };
}