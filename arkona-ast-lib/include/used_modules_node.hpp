/*
    File:    used_modules_node.hpp
    Created: 21 Juny 2023 at 21:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include "../include/ast_node.hpp"
#include "../include/qualified_id_node.hpp"

namespace ast{
    struct Used_modules_node : public Node{
        Used_modules_node()                         = default;
        Used_modules_node(const Used_modules_node&) = default;
        virtual ~Used_modules_node()                = default;

        using Names_of_used_modules = std::vector<std::shared_ptr<Qualified_id_node>>;

        explicit Used_modules_node(const Names_of_used_modules& names)
            : names_{names}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        Names_of_used_modules names_;
    };
}