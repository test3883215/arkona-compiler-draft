/*
    File:    call_args_node.hpp
    Created: 23 August 2023 at 18:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/expr_node.hpp"
#include "../include/name_component_node.hpp"

namespace ast{
    struct Call_args_node : public Name_component_node{
        Call_args_node()                      = default;
        Call_args_node(const Call_args_node&) = default;
        virtual ~Call_args_node()             = default;

        using Args = std::vector<std::shared_ptr<Expr_node>>;

        explicit Call_args_node(const Args& args)
            : args_{args}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Args args_;
    };
}