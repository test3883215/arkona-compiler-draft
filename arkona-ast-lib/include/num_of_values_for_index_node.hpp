/*
    File:    num_of_values_for_index_node.hpp
    Created: 13 August 2023 at 15:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"

namespace ast{
    struct Num_of_values_for_index_node : public Expr_node{
        Num_of_values_for_index_node()                                    = default;
        Num_of_values_for_index_node(const Num_of_values_for_index_node&) = default;
        virtual ~Num_of_values_for_index_node()                           = default;

        Num_of_values_for_index_node(const std::shared_ptr<Expr_node>& lhs, 
                                     const std::shared_ptr<Expr_node>& rhs)
            : lhs_{lhs}
            , rhs_{rhs}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> lhs_  = nullptr;
        std::shared_ptr<Expr_node> rhs_  = nullptr;
    };
}