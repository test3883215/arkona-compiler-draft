/*
    File:    scale_expr_node.hpp
    Created: 15 August 2023 at 21:27 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Scale_expr_node : public Value_expr_node{
        Scale_expr_node()                       = default;
        Scale_expr_node(const Scale_expr_node&) = default;
        virtual ~Scale_expr_node()              = default;

        using Elems_of_scale = std::vector<std::shared_ptr<Expr_node>>;

        explicit Scale_expr_node(const Elems_of_scale& elems)
            : elems_{elems}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Elems_of_scale elems_;
    };
}