/*
    File:    qualified_id_node.hpp
    Created: 24 June 2023 at 21:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/ast_node.hpp"
#include "../../scanner/include/scanner.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Qualified_id_node : public Node{
        Qualified_id_node()                         = default;
        Qualified_id_node(const Qualified_id_node&) = default;
        virtual ~Qualified_id_node()                = default;

        explicit Qualified_id_node(const std::vector<Id_info>& components)
            : components_{components}
        {
        }

        NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;
                              
        std::shared_ptr<Node> clone() const override;

        std::vector<Id_info> components_;
    };
}