/*
    File:    package_meta_func_arg_group_node.hpp
    Created: 01 August 2023 at 21:35 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/ast_node.hpp"

namespace ast{
    struct Package_meta_func_arg_group_node : public Node{
        Package_meta_func_arg_group_node()                                        = default;
        Package_meta_func_arg_group_node(const Package_meta_func_arg_group_node&) = default;
        virtual ~Package_meta_func_arg_group_node()                               = default;

        virtual NodeInfo get_info() const = 0;

        virtual std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                                      const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                      const Indents&                                   indents) const = 0;

        virtual std::shared_ptr<Node> clone() const = 0;
    };
}