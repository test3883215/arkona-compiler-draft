/*
    File:    non_fixed_size_base_type_expr_node.hpp
    Created: 21 August 2023 at 16:22 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Non_fixed_size_base_type_expr_node : public Value_expr_node{
        Non_fixed_size_base_type_expr_node()                                          = default;
        Non_fixed_size_base_type_expr_node(const Non_fixed_size_base_type_expr_node&) = default;
        virtual ~Non_fixed_size_base_type_expr_node()                                 = default;

        enum class Modifier{
            Long, Short
        };

        enum class Non_fixed_size_base_type_kind{
            Bool,   Ord,      Char,
            String, Unsigned, Signed,
            Float,  Complex,  Quat
        };

        Non_fixed_size_base_type_expr_node(const std::vector<Modifier>&  modifiers, 
                                           Non_fixed_size_base_type_kind kind)
            : modifiers_{modifiers}
            , kind_{kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::vector<Modifier>         modifiers_;
        Non_fixed_size_base_type_kind kind_;
    };
}