/*
    File:    meta_exit_stmt_node.hpp
    Created: 09 July 2023 at 19:02 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Meta_exit_stmt_node : public Stmt_node{
        Meta_exit_stmt_node()                           = default;
        Meta_exit_stmt_node(const Meta_exit_stmt_node&) = default;
        virtual ~Meta_exit_stmt_node()                  = default;

        explicit Meta_exit_stmt_node(const Id_info& label_id)
            : label_id_{label_id}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Id_info label_id_;
    };
}