/*
    File:    alias_node.hpp
    Created: 29 July 2023 at 20:59 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>

#include "../include/exported_kind.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"
#include "../include/stmt_node.hpp"

namespace ast{
    struct Alias_def_node : public Stmt_node{
        Alias_def_node()                      = default;
        Alias_def_node(const Alias_def_node&) = default;
        virtual ~Alias_def_node()             = default;

        Alias_def_node(Exported_kind                     exported_kind,
                       const Id_info&                    alias_name_info, 
                       const std::shared_ptr<Expr_node>& alias_def_body)
            : exported_kind_ {exported_kind}
            , alias_name_info_{alias_name_info}
            , alias_def_body_{alias_def_body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Exported_kind              exported_kind_ = Exported_kind::Not_exported;
        Id_info                    alias_name_info_;
        std::shared_ptr<Expr_node> alias_def_body_;
    };
}