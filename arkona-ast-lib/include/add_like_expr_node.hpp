/*
    File:    add_like_expr_node.hpp
    Created: 11 August 2023 at 19:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"
#include "../include/operation_info.hpp"

namespace ast{
    struct Add_like_expr_node : public Expr_node{
        Add_like_expr_node()                          = default;
        Add_like_expr_node(const Add_like_expr_node&) = default;
        virtual ~Add_like_expr_node()                 = default;

        Add_like_expr_node(const std::shared_ptr<Expr_node>& lhs, 
                           const std::shared_ptr<Expr_node>& rhs, 
                           Add_like_kind                     a_kind)
            : lhs_{lhs}
            , rhs_{rhs}
            , a_kind_{a_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> lhs_  = nullptr;
        std::shared_ptr<Expr_node> rhs_  = nullptr;
        Add_like_kind              a_kind_;
    };
}