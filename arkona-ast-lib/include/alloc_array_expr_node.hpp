/*
    File:    alloc_array_expr_node.hpp
    Created: 21 August 2023 at 18:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/value_expr_node.hpp"

namespace ast{
    struct Alloc_array_expr_node : public Value_expr_node{
        Alloc_array_expr_node()                             = default;
        Alloc_array_expr_node(const Alloc_array_expr_node&) = default;
        virtual ~Alloc_array_expr_node()                    = default;

        using Dimensions_of_array = std::vector<std::shared_ptr<Expr_node>>;

        Alloc_array_expr_node(const Dimensions_of_array&        dims,
                              const std::shared_ptr<Expr_node>& elem_type)
            : dims_{dims}
            , elem_type_{elem_type}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;
        
        Dimensions_of_array        dims_;
        std::shared_ptr<Expr_node> elem_type_;
    };
}