/*
    File:    meta_forall_stmt_node.hpp
    Created: 12 July 2023 at 20:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/stmt_node.hpp"
#include "../include/block_node.hpp"
#include "../include/expr_node.hpp"
#include "../include/id_info.hpp"

namespace ast{
    struct Meta_forall_stmt_node : public Stmt_node{
        Meta_forall_stmt_node()                             = default;
        Meta_forall_stmt_node(const Meta_forall_stmt_node&) = default;
        virtual ~Meta_forall_stmt_node()                    = default;

        Meta_forall_stmt_node(const Id_info&                     loop_label_id,
                              const Id_info&                     var_id,
                              const std::shared_ptr<Expr_node>&  collection_expr, 
                              const std::shared_ptr<Block_node>& loop_body)
            : loop_label_id_{loop_label_id}
            , var_id_{var_id}
            , collection_expr_{collection_expr}
            , loop_body_{loop_body}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        Id_info                     loop_label_id_;
        Id_info                     var_id_;
        std::shared_ptr<Expr_node>  collection_expr_ = nullptr;
        std::shared_ptr<Block_node> loop_body_       = nullptr;
    };
}