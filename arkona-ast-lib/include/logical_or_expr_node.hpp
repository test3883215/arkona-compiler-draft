/*
    File:    logical_or_expr_node.hpp
    Created: 08 August 2023 at 20:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include "../include/expr_node.hpp"
#include "../include/operation_info.hpp"

namespace ast{
    struct Logical_or_expr_node : public Expr_node{
        Logical_or_expr_node()                            = default;
        Logical_or_expr_node(const Logical_or_expr_node&) = default;
        virtual ~Logical_or_expr_node()                   = default;

        Logical_or_expr_node(const std::shared_ptr<Expr_node>& lhs, 
                             const std::shared_ptr<Expr_node>& rhs, 
                             Log_or_kind                       o_kind)
            : lhs_{lhs}
            , rhs_{rhs}
            , o_kind_{o_kind}
        {
        }

        virtual NodeInfo get_info() const override;

        std::string to_string(const std::shared_ptr<strings::trie::Char_trie>& strs_trie,
                              const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                              const Indents&                                   indents) const override;

        std::shared_ptr<Node> clone() const override;

        std::shared_ptr<Expr_node> lhs_  = nullptr;
        std::shared_ptr<Expr_node> rhs_  = nullptr;
        Log_or_kind                o_kind_;
    };
}