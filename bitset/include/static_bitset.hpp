/*
    File:    static_bitset.hpp
    Created: 18 October 2020 at 16:57 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#ifndef STATIC_BITSET_HPP
#define STATIC_BITSET_HPP
#   include <climits>
#   include <cstdint>
#   include <cstddef>
#   include <initializer_list>
#   include <list>
#   include <string>
#   include <type_traits>
#   include <vector>
#   include "../include/popcount.hpp"
#   include "../../strings-utils/include/join.hpp"

namespace pascal_set{
    template<typename E, E lower_bound, E upper_bound, typename T>
    struct range_len_helper{
        static constexpr std::size_t value = static_cast<std::size_t>(static_cast<T>(upper_bound) - static_cast<T>(lower_bound) + 1);
    };

    template<typename E>
    struct cast_should_be_to_size_t{
        static constexpr bool value = std::is_enum<E>::value           ||
                                      std::is_unsigned<E>::value       ||
                                      std::is_same<E, char>::value     ||
                                      std::is_same<E, char16_t>::value ||
                                      std::is_same<E, char32_t>::value;
    };

    template<typename E>
    struct cast_should_be_to_ssize_t{
        static constexpr bool value = std::is_signed<E>::value          &&
                                      !std::is_same<E, char>::value     &&
                                      !std::is_same<E, char16_t>::value &&
                                      !std::is_same<E, char32_t>::value;
    };

    template<typename E>
    using type_to_cast = typename std::conditional<cast_should_be_to_size_t<E>::value,
                                                   std::size_t,
                                                   typename std::conditional<cast_should_be_to_ssize_t<E>::value,
                                                                             ssize_t,
                                                                             void>::type>::type;

    template<typename E, E lower_bound, E upper_bound>
    struct range_len{
        static constexpr std::size_t value = range_len_helper<E, lower_bound, upper_bound, type_to_cast<E>>::value;
    };

    template<typename E, E start, typename T>
    constexpr std::size_t to_offset_helper(E value)
    {
        return static_cast<std::size_t>(static_cast<T>(value) - static_cast<T>(start));
    }

    template<typename E, E start>
    constexpr std::size_t to_offset(E value)
    {
        return to_offset_helper<E, start, type_to_cast<E>>(value);
    }

    template<typename E, E start, typename T>
    constexpr E from_offset_helper(std::size_t offset)
    {
        return static_cast<E>(static_cast<T>(offset) + static_cast<T>(start));
    }

    template<typename E, E start>
    constexpr E from_offset(std::size_t offset)
    {
        return from_offset_helper<E, start, type_to_cast<E>>(offset);
    }


    template<typename ElemType, ElemType lower_bound, ElemType upper_bound,
             typename = typename std::enable_if<std::is_enum<ElemType>::value ||
                                                std::is_integral<ElemType>::value>::type>
    class set{
    public:
        template<typename It>
        constexpr set(It first, It last) : set()
        {
            for(auto it = first; it != last; ++it){
                set_bit(to_offset<ElemType, lower_bound>(*it));
            }
        }

        constexpr set()
        {
            for(std::size_t& e : set_value_)
            {
                e = 0;
            }
        }

        constexpr set(std::initializer_list<ElemType> l) : set(l.begin(), l.end())
        {
        }

        constexpr set(const set& rhs)  = default;
        set& operator=(const set& rhs) = default;

        ~set()                         = default;

        set<ElemType, lower_bound, upper_bound>& operator += (const set<ElemType, lower_bound, upper_bound>& rhs)
        {
            for(std::size_t i = 0; i < num_of_uint64s; ++i)
            {
                set_value_[i] |= rhs.set_value_[i];
            }
            return *this;
        }

        set<ElemType, lower_bound, upper_bound>& operator *= (const set<ElemType, lower_bound, upper_bound>& rhs)
        {
            for(std::size_t i = 0; i < num_of_uint64s; ++i)
            {
                set_value_[i] &= rhs.set_value_[i];
            }
            return *this;
        }

        set<ElemType, lower_bound, upper_bound>& operator ^= (const set<ElemType, lower_bound, upper_bound>& rhs)
        {
            for(std::size_t i = 0; i < num_of_uint64s; ++i)
            {
                set_value_[i] ^= rhs.set_value_[i];
            }
            return *this;
        }

        set<ElemType, lower_bound, upper_bound>& operator -= (const set<ElemType, lower_bound, upper_bound>& rhs)
        {
            for(std::size_t i = 0; i < num_of_uint64s; ++i)
            {
                set_value_[i] &= ~rhs.set_value_[i];
            }
            return *this;
        }


        std::size_t size() const
        {
            std::size_t result = 0;
            for(std::size_t i = 0; i < num_of_uint64s; ++i){
                result += popcnt(set_value_[i]);
            }
            return result;

        }

        bool empty() const
        {
            std::uint64_t temp = 0;
            for(std::size_t i = 0; i < num_of_uint64s; ++i){
                temp |= set_value_[i];
            }
            return temp == 0;
        }

        set<ElemType, lower_bound, upper_bound> complement() const
        {
            set<ElemType, lower_bound, upper_bound> result{*this};
            std::size_t num_of_unused_bits            = num_of_uint64s * num_of_bits_in_uint64 - n;
            std::size_t num_of_used_bits_of_last_word = num_of_bits_in_uint64 - num_of_unused_bits;
            std::size_t mask = (static_cast<std::size_t>(1u) << num_of_used_bits_of_last_word) - 1;
            for(std::size_t i = 0; i < num_of_uint64s; ++i){
                result.set_value_[i] = ~set_value_[i];
            }
            result.set_value_[num_of_uint64s - 1] &= mask;
            return result;
        }


        class reference{
        public:
            reference() : bitset_ptr_{nullptr}, position_{0} {}
            reference(set<ElemType, lower_bound, upper_bound>* bitset_ptr, std::size_t position) :
                bitset_ptr_{bitset_ptr}, position_{position} {}
            reference(const reference&) = default;
            ~reference() = default;

            // For x = b[i]
            operator bool() const noexcept
            {
                return bitset_ptr_->get_bit(position_);
            }

            // For b[i] = x
            reference& operator = (bool x) noexcept
            {
                if(x){
                    bitset_ptr_->set_bit(position_);
                }else{
                    bitset_ptr_->reset_bit(position_);
                }
                return *this;
            }

            // For b[i] = b[j]
            reference& operator = (const reference& j) noexcept
            {
                if(j){
                    bitset_ptr_->set_bit(position_);
                }else{
                    bitset_ptr_->reset_bit(position_);
                }
                return *this;
            }

            bool operator ~ () const noexcept
            {
                return bitset_ptr_->get_bit(position_) == 0;
            }

            reference& flip() noexcept
            {
                bitset_ptr_->flip_bit(position_);
                return *this;
            }
        private:
            friend class set<ElemType, lower_bound, upper_bound>;

            set<ElemType, lower_bound, upper_bound>* bitset_ptr_;
            std::size_t       position_;
        };

        friend class reference;

        reference operator[](ElemType e)
        {
            return reference{this, to_offset<ElemType, lower_bound>(e)};
        }

        bool operator[](ElemType e) const
        {
            return get_bit(to_offset<ElemType, lower_bound>(e));
        }

        bool operator == (const set<ElemType, lower_bound, upper_bound>& rhs) const
        {

            std::size_t result = 0;
            for(std::size_t i = 0; i < num_of_uint64s; ++i){
                result |= set_value_[i] ^ rhs.set_value_[i];
            }

            return result == 0;

        }

        bool operator <= (const set<ElemType, lower_bound, upper_bound>& rhs) const
        {
            std::size_t result = 0;
            for(std::size_t i = 0; i < num_of_uint64s; ++i){
                std::uint64_t a = set_value_[i];
                std::uint64_t b = rhs.set_value_[i];
                result |= a ^ (a & b);
            }

            return result == 0;
        }

    private:
        static constexpr std::size_t num_of_bits_in_uint64 = sizeof(std::uint64_t) * CHAR_BIT;
        static constexpr std::size_t n = range_len<ElemType, lower_bound, upper_bound>::value;
        static constexpr std::size_t num_of_uint64s = (n == 0) ? 1 :
                                                      ((n / num_of_bits_in_uint64) + (n % num_of_bits_in_uint64 != 0));

        std::uint64_t set_value_[num_of_uint64s];

        bool get_bit(std::size_t position) const
        {
            std::size_t word_number = get_word_number(position);
            std::size_t bit_mask    = get_mask(position);
            return (set_value_[word_number] & bit_mask) != 0;
        }

        void set_bit(std::size_t position)
        {
            std::size_t word_number = get_word_number(position);
            std::size_t bit_mask    = get_mask(position);
            set_value_[word_number] |= bit_mask;
        }

        void reset_bit(std::size_t position)
        {
            std::size_t word_number = get_word_number(position);
            std::size_t bit_mask    = get_mask(position);
            set_value_[word_number] &= ~bit_mask;
        }

        void flip_bit(std::size_t position)
        {
            std::size_t word_number = get_word_number(position);
            std::size_t bit_mask    = get_mask(position);
            set_value_[word_number] ^= bit_mask;
        }

        std::uint64_t get_mask(std::size_t position) const
        {
            std::size_t bit_number  = position % num_of_bits_in_uint64;
            std::size_t bit_mask    = static_cast<std::uint64_t>(1u) << bit_number;
            return bit_mask;
        }

        std::size_t get_word_number(std::size_t position) const
        {
            return position / num_of_bits_in_uint64;
        }
    };


    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    set<ElemType, lower_bound, upper_bound> operator + (const set<ElemType, lower_bound, upper_bound>& lhs,
                                                        const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        set<ElemType, lower_bound, upper_bound> result{lhs};
        result += rhs;
        return result;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    set<ElemType, lower_bound, upper_bound> operator * (const set<ElemType, lower_bound, upper_bound>& lhs,
                                                        const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        set<ElemType, lower_bound, upper_bound> result{lhs};
        result *= rhs;
        return result;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    set<ElemType, lower_bound, upper_bound> operator ^ (const set<ElemType, lower_bound, upper_bound>& lhs,
                                                        const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        set<ElemType, lower_bound, upper_bound> result{lhs};
        result ^= rhs;
        return result;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    set<ElemType, lower_bound, upper_bound> operator - (const set<ElemType, lower_bound, upper_bound>& lhs,
                                                        const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        set<ElemType, lower_bound, upper_bound> result{lhs};
        result -= rhs;
        return result;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    set<ElemType, lower_bound, upper_bound> operator ~ (const set<ElemType, lower_bound, upper_bound>& b)
    {
        return b.complement();
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    bool operator != (const set<ElemType, lower_bound, upper_bound>& lhs,
                      const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        return !(lhs == rhs);
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    bool operator >= (const set<ElemType, lower_bound, upper_bound>& lhs,
                      const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        return rhs <= lhs;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    bool operator < (const set<ElemType, lower_bound, upper_bound>& lhs,
                     const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        return (lhs <= rhs) && (lhs != rhs);
    }

    template<typename ElemType, ElemType lower_bound, ElemType upper_bound>
    bool operator > (const set<ElemType, lower_bound, upper_bound>& lhs,
                     const set<ElemType, lower_bound, upper_bound>& rhs)
    {
        return (rhs < lhs);
    }



    template<typename T>
    struct special_cases
    {
        static constexpr bool value = std::is_same<T, std::uint8_t>::value  ||
                                      std::is_same<T, std::uint16_t>::value ||
                                      std::is_same<T, std::int8_t>::value   ||
                                      std::is_same<T, std::int16_t>::value;
    };

    template<typename ElemType, ElemType lower_bound, ElemType upperbound>
    typename std::enable_if<special_cases<ElemType>::value, std::string>::type to_string(const set<ElemType, lower_bound, upperbound>& s)
    {
        std::list<std::string> elements;
        for(std::size_t i = 0; i < range_len<ElemType, lower_bound, upperbound>::value; ++i){
            if(s[from_offset<ElemType, lower_bound>(i)]){
                elements.push_back(std::to_string(i));
            }
        }
        std::string body   = strings::join::join(elements.begin(), elements.end(), std::string{", "});
        std::string result = "{" + body + "}";
        return result;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upperbound>
    typename std::enable_if<!special_cases<ElemType>::value, std::string>::type to_string(const set<ElemType, lower_bound, upperbound>& s)
    {
        std::list<std::string> elements;
        for(std::size_t i = 0; i < range_len<ElemType, lower_bound, upperbound>::value; ++i){
            if(s[from_offset<ElemType, lower_bound>(i)]){
                elements.push_back(to_string(from_offset<ElemType, lower_bound>(i)));
            }
        }
        std::string body   = strings::join::join(elements.begin(), elements.end(), std::string{", "});
        std::string result = "{" + body + "}";
        return result;
    }

    template<typename F, typename ElemType, ElemType lower_bound, ElemType upperbound>
    std::string to_string(F f, const set<ElemType, lower_bound, upperbound>& s)
    {
        std::list<std::string> elements;
        for(std::size_t i = 0; i < range_len<ElemType, lower_bound, upperbound>::value; ++i){
            if(s[from_offset<ElemType, lower_bound>(i)]){
                elements.push_back(f(from_offset<ElemType, lower_bound>(i)));
            }
        }
        std::string body   = strings::join::join(elements.begin(), elements.end(), std::string{", "});
        std::string result = "{" + body + "}";
        return result;
    }

    template<typename ElemType, ElemType lower_bound, ElemType upperbound>
    std::vector<ElemType> to_vector(const set<ElemType, lower_bound, upperbound>& s)
    {
        std::vector<ElemType> result;
        for(std::size_t i = 0; i < range_len<ElemType, lower_bound, upperbound>::value; ++i){
            if(s[from_offset<ElemType, lower_bound>(i)]){
                result.push_back(from_offset<ElemType, lower_bound>(i));
            }
        }
        return result;
    }
};
#endif
