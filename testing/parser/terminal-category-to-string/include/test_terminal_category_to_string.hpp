/*
    File:    test_terminal_category_to_string.hpp
    Created: 12 October 2024 at 13:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

namespace testing_parser{
    void test_terminal_category_to_string();
}