/*
     File:    main.cpp
     Created: 12 October 2024 at 13:09 MSK
     Author:  Гаврилов Владимир Сергеевич
     E-mails: vladimir.s.gavrilov@gmail.com
              gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/test_terminal_category_to_string.hpp"

int main()
{
    testing_parser::test_terminal_category_to_string();
}