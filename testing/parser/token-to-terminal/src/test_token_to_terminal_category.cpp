/*
    File:    test_token_to_terminal_category.cpp
    Created: 15 October 2024 at 08:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstdio>
#include <iterator>
#include "../include/test_token_to_terminal_category.hpp"
#include "../../../utils/include/lexeme_fab.hpp"
#include "../../../utils/include/testing.hpp"
#include "../../../../parser/include/token_to_terminal_set.hpp"
#include "../../../../parser/include/terminal_enum.hpp"
#include "../../../../bitset/include/static_bitset.hpp"

namespace{
    using Pair_for_test = std::pair<parser::Token, parser::Terminal_set>;
    using namespace testing_parser;

    //! Tests for special lexemes.
    const Pair_for_test special_lexemes_tests[] = {
        {
            {
                {{75,34 }, {75,65 }},
                testing_scanner::unknown_lexeme()
            }, 
            {
                parser::Terminal_category::End_of_text
            }
        }, 
        {
            {
                {{34,65}, {34,65 }}, 
                testing_scanner::nothing_lexeme()
            }, 
            {
                parser::Terminal_category::End_of_text
            }
        }
    };

    const Pair_for_test id_tests[] = {
        {
            {
                {{39,54 }, {39,70 }}, 
                testing_scanner::id_lexeme(733)
            },
            {
                parser::Terminal_category::Block_entry,                parser::Terminal_category::Category_arg_group,     parser::Terminal_category::Expr,                    
                parser::Terminal_category::Expr0,                      parser::Terminal_category::Expr1,                  parser::Terminal_category::Expr10,                  
                parser::Terminal_category::Expr11,                     parser::Terminal_category::Expr12,                 parser::Terminal_category::Expr13,                  
                parser::Terminal_category::Expr14,                     parser::Terminal_category::Expr15,                 parser::Terminal_category::Expr16,                  
                parser::Terminal_category::Expr17,                     parser::Terminal_category::Expr18,                 parser::Terminal_category::Expr19,                  
                parser::Terminal_category::Expr2,                      parser::Terminal_category::Expr20,                 parser::Terminal_category::Expr21,                 
                parser::Terminal_category::Expr3,                      parser::Terminal_category::Expr4,                  parser::Terminal_category::Expr5,                   
                parser::Terminal_category::Expr6,                      parser::Terminal_category::Expr7,                  parser::Terminal_category::Expr8,                
                parser::Terminal_category::Expr9,                      parser::Terminal_category::Field_values_list,      parser::Terminal_category::Fields_group_def,        
                parser::Terminal_category::Func_arg_group,             parser::Terminal_category::Id,                     parser::Terminal_category::Match_branch,            
                parser::Terminal_category::Meta_var_def,               parser::Terminal_category::Metafunc_arg_group,     parser::Terminal_category::Mmatch_branch,           
                parser::Terminal_category::Module_name,                parser::Terminal_category::Mselect_branch,         parser::Terminal_category::Mspider_branch,         
                parser::Terminal_category::Package_category_arg_group, parser::Terminal_category::Package_func_arg_group, parser::Terminal_category::Package_metafunc_arg_group,        
                parser::Terminal_category::Qualified_id,               parser::Terminal_category::Select_branch,          parser::Terminal_category::Spider_branch,        
                parser::Terminal_category::Struct_fields_def,          parser::Terminal_category::Used_categories,        parser::Terminal_category::Var_def,
            }
        }
    };

    const Pair_for_test tests_for_keywords[] = {
        // абстрактная Kw_abstraktnaja 
        {
            {{
                {43,33 }, {43,43 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_abstraktnaja)            
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_abstraktnaja
            }
        },
        // байт      Kw_bajt        
        {
            {
                {{12,7  }, {12,10 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bajt)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_bajt,        parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // беззн     Kw_bezzn      
        {
            {
                {{11,25 }, {11,29 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bezzn)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_bezzn,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,         
            }
        },
        // беззн8    Kw_bezzn8      
        {
            {
                {{6, 41 }, {6, 46 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bezzn8)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_bezzn8,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // беззн16   Kw_bezzn16   
        {
            {
                {{6, 21 }, {6, 27 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bezzn16)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_bezzn16,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // беззн32   Kw_bezzn32     
        {
            {
                {{7, 5  }, {7, 11 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bezzn32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_bezzn32,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // беззн64   Kw_bezzn64   
        {
            {
                {{7, 17 }, {7, 23 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bezzn64)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_bezzn64,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // беззн128  Kw_bezzn128    
        {
            {
                {{7, 33 }, {7, 40 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bezzn128)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_bezzn128,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // больше  Kw_bolshe     
        {
            {
                {{12,9  }, {12,14 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bolshe)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_bolshe,     parser::Terminal_category::Literal,         
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        // большое Kw_bolshoe     
        {
            {
                {{9, 13 }, {9, 19 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_bolshoe)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_bolshoe,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,  parser::Terminal_category::Modifier,         
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,     
            }
        },
        // в       Kw_v          
        {
            {
                {{3, 17 }, {3, 17 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_v)
            },
            {
                parser::Terminal_category::Kw_v
            }
        },
        // вечно   Kw_vechno      
        {
            {
                {{12,48 }, {12,52 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_vechno)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_vechno
            }
        },
        // вещ   Kw_veshch     
        {
            {
                {{12,41 }, {12,43 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_veshch)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_veshch,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            }
        },
        // вещ32   Kw_veshch32
        {
            {
                {{14,46 }, {14,50 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_veshch32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_veshch32,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // вещ64   Kw_veshch64
        {
            {
                {{14,56 }, {14,60 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_veshch64)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_veshch64,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // вещ80   Kw_veshch80
        {
            {
                {{14,23 }, {14,27 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_veshch80)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1, 
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3, 
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6, 
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_veshch80,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // вещ128  Kw_veshch128  
        {
            {
                {{14,35 }, {14,40 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_veshch128)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1, 
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3, 
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6, 
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_veshch128,   parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            },
        },
        // возврат  Kw_vozvrat     
        {
            {
                {{8, 5  }, {8, 11 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_vozvrat)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_vozvrat
            }
        },
        // выбери  Kw_vyberi     
        {
            {
                {{4, 34 }, {4, 39 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_vyberi)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_vyberi
            }
        },
        // выдели  Kw_vydeli      
        {
            {
                {{8, 20 }, {8, 25 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_vydeli)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_vydeli,      parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,  parser::Terminal_category::Alloc_expr,
            }
       },
        // выйди  Kw_vyjdi      
        {
            {
                {{4, 9  }, {4, 13 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_vyjdi)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_vyjdi
            }
        },
        // головная  Kw_golovnaya
        {
            {
                {{11,9  }, {11,16 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_golovnaya)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_golovnaya
            }
        },
        // диапазон  Kw_diapazon   
        {
            {
                {{45,18 }, {45,25 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_diapazon)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_diapazon,    parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Range_type,    parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,
            }
        },
        // для     Kw_dlya        
        {
            {
                {{15,13 }, {15,15 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_dlya)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_dlya
            }
        },
        // если    Kw_esli       
        {
            {
                {{19,26 }, {19,29 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_esli)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_esli
            }
        },
        // из    Kw_iz          
        {
            {
                {{24,1  }, {24,2  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_iz)
            },
            {
                parser::Terminal_category::Kw_iz
            }
        },
        // иначе   Kw_inache     
        {
            {
                {{24,12 }, {24,16 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_inache)
            },
            {
                parser::Terminal_category::Kw_inache
            }
        },
        // инес    Kw_ines        
        {
            {
                {{24,7  }, {24,10 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_ines)
            },
            {
                parser::Terminal_category::Kw_ines
            }
        },
        // использует Kw_ispolzuet  
        {
            {
                {{39,29 }, {39,38 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_ispolzuet)
            },
            {
                parser::Terminal_category::Kw_ispolzuet, parser::Terminal_category::Used_modules, parser::Terminal_category::Used_stmt,
            }
        },
        // истина     Kw_istina      
        {
            {
                {{41,32 }, {41,37 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_istina)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_istina,     parser::Terminal_category::Literal,         
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        // как        Kw_kak        
        {
            {
                {{27,29 }, {27,31 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kak)
            },
            {
                parser::Terminal_category::Kw_kak
            }
        },
        // категория  Kw_kategorija  
        {
            {
                {{35,3  }, {35,11 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kategorija)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_kategorija
            }
        },
        // кват       Kw_kvat       
        {
            {
                {{29,5  }, {29,8  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kvat)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_kvat,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            },
        },
        // кват32     Kw_kvat32
        {
            {
                {{29,35 }, {29,40 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kvat32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kvat32,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // кват64     Kw_kvat64     
        {
            {
                {{31,9  }, {31,14 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kvat64)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kvat64,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            },
        },
        // кват80     Kw_kvat80      
        {
            {
                {{31,44 }, {31,49 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kvat80)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kvat80,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // кват128    Kw_kvat128    
        {
            {
                {{33,17 }, {33,23 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kvat128)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kvat128,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // компл      Kw_kompl       
        {
            {
                {{27,44 }, {27,48 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kompl)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_kompl,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            }
        },
        // компл32    Kw_kompl32    
        {
            {
                {{31,63 }, {31,69 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kompl32) 
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kompl32,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // компл64    Kw_kompl64     
        {
            {
                {{29,16 }, {29,22 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kompl64) 
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kompl64,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // компл80    Kw_kompl80    
        {
            {
                {{29,51 }, {29,57 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kompl80) 
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kompl80,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // компл128   Kw_kompl128
        {
            {
                {{31,27 }, {31,34 }},
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_kompl128)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_kompl128,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // конст      Kw_konst  
        {
            {
                {{27,13 }, {27,17 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_konst)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,              parser::Terminal_category::Expr1,                 
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,             parser::Terminal_category::Expr12,                
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,             parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,             parser::Terminal_category::Expr18,                
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,              parser::Terminal_category::Expr20,                
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,             parser::Terminal_category::Expr3,                 
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,              parser::Terminal_category::Expr6,                 
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,              parser::Terminal_category::Expr9,                 
                parser::Terminal_category::Kw_konst,       parser::Terminal_category::Match_branch,       parser::Terminal_category::Mmatch_branch,         
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch,     parser::Terminal_category::Ref_type,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,      parser::Terminal_category::Type_value, 
            }
        },
        // константы  Kw_konstanty 
        {
            {
                {{47,61 }, {47,69 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_konstanty)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_konstanty
            }
        },
        // лог        Kw_log    
        {
            {
                {{47,20 }, {47,22 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_log)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_log,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            }
        },
        // лог8       Kw_log8    
        {
            {
                {{51,13 }, {51,16 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_log8)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_log8,        parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // лог16      Kw_log16  
        {
            {
                {{51,59 }, {51,63 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_log16)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_log16,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // лог32      Kw_log32   
        {
            {
                {{51,44 }, {51,48 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_log32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_log32,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // лог64      Kw_log64  
        {
            {
                {{49,37 }, {49,41 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_log64)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_log64,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // ложь       Kw_lozh    
        {
            {
                {{47,32 }, {47,35 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_lozh)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_lozh,       parser::Terminal_category::Literal,         
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            },
        },
        // лямбда     Kw_ljambda
        {
            {
                {{49,23 }, {49,28 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_ljambda)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17,         
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2,          
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22,         
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5,          
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8,          
                parser::Terminal_category::Expr9,          parser::Terminal_category::Func_value,    parser::Terminal_category::Kw_ljambda,     
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,  
            }
        },
        // маленькое  Kw_malenkoe
        {
            {
                {{3, 21 }, {3, 29 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_malenkoe)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_malenkoe,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,  parser::Terminal_category::Modifier,         
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // массив     Kw_massiv 
        {
            {
                {{3, 38 }, {3, 43 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_massiv)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0, 
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2, 
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5, 
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8, 
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_massiv,      parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Array_type,    parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch, 
                parser::Terminal_category::Type_value,
            }
        },
        // меньше     Kw_menshe
        {
            {
                {{10,13 }, {10,18 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_menshe)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0, 
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17,
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2, 
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22,
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5, 
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8, 
                parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_menshe,     parser::Terminal_category::Literal,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        // мета       Kw_meta   
        {
            {
                {{8, 5  }, {8, 8  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_meta)
            },
            {
                parser::Terminal_category::Block_entry, parser::Terminal_category::Kw_meta,
                parser::Terminal_category::Meta_stmt,   parser::Terminal_category::Stmt,
            }
        },
        // модуль     Kw_modul   
        {
            {
                {{10,52 }, {10,57 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_modul)
            },
            {
                parser::Terminal_category::Modulo, parser::Terminal_category::Kw_modul
            }
        },
        // настройка  Kw_nastrojka
        {
            {
                {{32,41 }, {32,49 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_nastrojka)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Constructor,   parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_nastrojka,      
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,              
            }
        },
        // ничто      Kw_nichto  
        {
            {
                {{16,21 }, {16,25 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_nichto)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0, 
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17,
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2, 
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22,
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5, 
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8, 
                parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_nichto,     parser::Terminal_category::Literal,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        // обобщение  Kw_obobshchenie 
        {
            {
                {{35,54 }, {35,62 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_obobshchenie)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_obobshchenie, parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Gen_type,      parser::Terminal_category::Select_branch,   parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // обобщённая Kw_obobshchjonnaja 
        {
            {
                {{45,17 }, {45,26 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_obobshchjonnaja)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_obobshchjonnaja
            }
        },
        // операция   Kw_operatsija   
        {
            {
                {{21,13 }, {21,20 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_operatsija)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem, 
                parser::Terminal_category::Kw_operatsija,  parser::Terminal_category::Nonmarked_stmt,     parser::Terminal_category::Operation_header,   
                parser::Terminal_category::Stmt,
            }
        },
        // освободи   Kw_osvobodi      
        {
            {
                {{21,45 }, {21,52 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_osvobodi)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,  
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11, 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14, 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17, 
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,  
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22, 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,  
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_osvobodi,    parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,  parser::Terminal_category::Free_expr,
            }
        },
        // пакет      Kw_paket        
        {
            {
                {{24,53 }, {24,57 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_paket)
            },
            {
                parser::Terminal_category::Kw_paket,               parser::Terminal_category::Package_category_arg_group,    
                parser::Terminal_category::Package_func_arg_group, parser::Terminal_category::Package_metafunc_arg_group,
            }
        },
        // пакетная   Kw_paketnaja     
        {
            {
                {{36,37 }, {36,44 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_paketnaja)
            },
            {
                parser::Terminal_category::Block_entry, parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem,
                parser::Terminal_category::Func_header, parser::Terminal_category::Kw_paketnaja,       parser::Terminal_category::Nonmarked_stmt,
                parser::Terminal_category::Stmt,
            }
        },
        // паук       Kw_pauk         
        {
            {
                {{24,2  }, {24,5  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_pauk)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_pauk
            }
        },
        // перем      Kw_perem         
        {
            {
                {{42,23 }, {42,27 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_perem)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_perem
            }
        },
        // перечисл_множ  Kw_perechisl_mnozh
        {
            {
                {{40,36 }, {40,48 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_perechisl_mnozh)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,               parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,             parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,             parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,             parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,             parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,             parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,              parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,              parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_perechisl_mnozh, parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,     parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Set_type,      parser::Terminal_category::Select_branch,      parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // перечисление Kw_perechislenie 
        {
            {
                {{40,22 }, {40,33 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_perechislenie)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,             parser::Terminal_category::Expr0,  
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,           parser::Terminal_category::Expr11, 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,           parser::Terminal_category::Expr14, 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,           parser::Terminal_category::Expr17, 
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,           parser::Terminal_category::Expr2,  
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,           parser::Terminal_category::Expr22, 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,            parser::Terminal_category::Expr5,  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,            parser::Terminal_category::Expr8,  
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_perechislenie, parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,   parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Enum_type,     parser::Terminal_category::Select_branch,    parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // повторяй      Kw_povtorjaj    
        {
            {
                {{26,47 }, {26,54 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_povtorjaj)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_povtorjaj
            }
        },
        // пока          Kw_poka          
        {
            {
                {{28,3  }, {28,6  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_poka)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_poka
            }
        },
        // покуда        Kw_pokuda       
        {
            {
                {{28,49 }, {28,54 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_pokuda)
            },
            {
                parser::Terminal_category::Kw_pokuda
            }
        },
        // порядок       Kw_porjadok      
        {
            {
                {{28,65 }, {28,71 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_porjadok)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_porjadok,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            }
        },
        // порядок8      Kw_porjadok8    
        {
            {
                {{29,42 }, {29,49 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_porjadok8)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_porjadok8,   parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // порядок16     Kw_porjadok16    
        {
            {
                {{29,53 }, {29,61 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_porjadok16)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_porjadok16,  parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // порядок32     Kw_porjadok32   
        {
            {
                {{29,67 }, {29,75 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_porjadok32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_porjadok32,  parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // порядок64     Kw_porjadok64    
        {
            {
                {{29,30 }, {29,38 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_porjadok64)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_porjadok64,  parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // постфиксная   Kw_postfiksnaja 
        {
            {
                {{31,9  }, {31,19 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_postfiksnaja)
            },
            {
                parser::Terminal_category::Block_entry,     parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem,
                parser::Terminal_category::Kw_postfiksnaja, parser::Terminal_category::Nonmarked_stmt,     parser::Terminal_category::Operation_header,
                parser::Terminal_category::Stmt,
            }
        },
        // преобразуй    Kw_preobrazuj    
        {
            {
                {{31,26 }, {31,35 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_preobrazuj)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Cast_expr,     parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_preobrazuj,  
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,  
            }
        },
        // префиксная    Kw_prefiksnaja  
        {
            {
                {{32,17 }, {32,26 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_prefiksnaja)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem,
                parser::Terminal_category::Kw_prefiksnaja, parser::Terminal_category::Nonmarked_stmt,     parser::Terminal_category::Operation_header,
                parser::Terminal_category::Stmt,
            }
        },
        // продолжи      Kw_prodolzhi     
        {
            {
                {{22,19 }, {22,26 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_prodolzhi)  
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_prodolzhi
            }
        },
        // псевдоним     Kw_psevdonim    
        {
            {
                {{38,13 }, {38,21 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_psevdonim)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_psevdonim
            }
        },
        // пусто         Kw_pusto         
        {
            {
                {{38,48 }, {38,52 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_pusto)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_pusto,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // равно         Kw_ravno        
        {
            {
                {{2, 18 }, {2, 22 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_ravno)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,          parser::Terminal_category::Expr0,  
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,        parser::Terminal_category::Expr11, 
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,        parser::Terminal_category::Expr14, 
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,        parser::Terminal_category::Expr17, 
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,        parser::Terminal_category::Expr2,  
                parser::Terminal_category::Expr20,         parser::Terminal_category::Expr21,        parser::Terminal_category::Expr22, 
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,         parser::Terminal_category::Expr5,  
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,         parser::Terminal_category::Expr8,  
                parser::Terminal_category::Expr9,          parser::Terminal_category::Kw_ravno,      parser::Terminal_category::Literal,         
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        // разбор        Kw_razbor        
        {
            {
                {{4, 9  }, {4, 14 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_razbor)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_razbor
            }
        },
        // рассматривай Kw_rassmatrivaj 
        {
            {
                {{4, 19 }, {4, 30 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_rassmatrivaj)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Cast_expr,     parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_rassmatrivaj,  
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,  
            }
        },
        // реализация    Kw_realizatsija  
        {
            {
                {{6, 21 }, {6, 30 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_realizatsija)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_realizatsija
            }
        },
        // реализуй      Kw_realizuj     
        {
            {
                {{6, 1  }, {6, 8  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_realizuj)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_realizuj
            }
        },
        // само          Kw_samo          
        {
            {
                {
                    {11,28 }, {11,31 }}, 
                    testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_samo)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_samo,         parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Other_type,    parser::Terminal_category::Select_branch,   parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // сброс         Kw_sbros        
        {
            {
                {{34,29 }, {34,33 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_sbros)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Destructor,    parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_sbros,      
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,              
            }
        },
        // симв          Kw_simv          
        {
            {
                {{18,9  }, {18,12 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_simv)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_simv,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            },
        },
        // симв8         Kw_simv8        
        {
            {
                {
                    {18,42 }, {18,46 }}, 
                    testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_simv8)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_simv8,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // симв16        Kw_simv16        
        {
            {
                {{20,17 }, {20,22 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_simv16)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_simv16,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // симв32        Kw_simv32       
        {
            {
                {{18,22 }, {18,27 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_simv32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_simv32,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // сравни_с      Kw_sravni_s      
        {
            {
                {{16,31 }, {16,38 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_sravni_s)
            },
            {
                parser::Terminal_category::Kw_sravni_s, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ссылка        Kw_ssylka       
        {
            {
                {{9, 41 }, {9, 46 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_ssylka)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_ssylka,       parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Ref_type,      parser::Terminal_category::Select_branch,   parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // строка        Kw_stroka        
        {
            {
                {{14,42 }, {14,47 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_stroka)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_stroka,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            }
        },
        // строка8       Kw_stroka8      
        {
            {
                {{14,20 }, {14,26 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_stroka8)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_stroka8,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // строка16      Kw_stroka16      
        {
            {
                {{14,59 }, {14,66 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_stroka16)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_stroka16,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // строка32      Kw_stroka32     
        {
            {
                {{14,5  }, {14,12 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_stroka32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_stroka32,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // структура     Kw_struktura     
        {
            {
                {{11,40 }, {11,48 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_struktura)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_struktura,    parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Struct_type,   parser::Terminal_category::Select_branch,   parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // тип           Kw_tip          
        {
            {
                {{25,10 }, {25,12 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tip)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_tip,          parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Other_type,    parser::Terminal_category::Select_branch,   parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            },
        },
        // типы          Kw_tipy          
        {
            {
                {{47,37 }, {47,40 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tipy)
            },
            {
                parser::Terminal_category::Nonmarked_stmt, parser::Terminal_category::Stmt, 
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Kw_tipy
            }
        },
        // то            Kw_to           
        {
            {
                {{25,1  }, {25,2  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_to)
            },
            {
                parser::Terminal_category::Kw_to
            }
        },
        // функция       Kw_funktsija     
        {
            {
                {{25,15 }, {25,21 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_funktsija)
            },
            {
                parser::Terminal_category::Block_entry, parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem,           
                parser::Terminal_category::Func_header, parser::Terminal_category::Kw_funktsija,       parser::Terminal_category::Nonmarked_stmt,
                parser::Terminal_category::Stmt,
            }
        },
        // чистая        Kw_chistaja     
        {
            {
                {{33,70 }, {33,75 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_chistaja)
            },
            {
                parser::Terminal_category::Block_entry,      parser::Terminal_category::Category_body_elem, parser::Terminal_category::Category_impl_elem,
                parser::Terminal_category::Func_header,      parser::Terminal_category::Kw_chistaja,        parser::Terminal_category::Nonmarked_stmt,
                parser::Terminal_category::Operation_header, parser::Terminal_category::Stmt,
            }
        },
        // цел           Kw_tsel
        {
            {
                {{27,34 }, {27,36 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tsel)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,           parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                 parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                parser::Terminal_category::Expr16,         
                parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                parser::Terminal_category::Expr19,         
                parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                parser::Terminal_category::Expr21,         
                parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                 parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                 parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,                 parser::Terminal_category::Kw_tsel,        
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,         parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,         parser::Terminal_category::Spider_branch,  
                parser::Terminal_category::Type_value,     parser::Terminal_category::Without_size_modifier,
            }
        },
        // цел8          Kw_tsel8
        {
            {
                {{32,44 }, {32,47 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tsel8)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_tsel8,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // цел16         Kw_tsel16
        {
            {
                {{33,21 }, {33,25 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tsel16)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_tsel16,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // цел32         Kw_tsel32
        {
            {
                {{33,50 }, {33,54 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tsel32)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_tsel32,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // цел64         Kw_tsel64
        {
            {
                {{33,32 }, {33,36 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tsel64)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_tsel64,      parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            },
        },
        // цел128        Kw_tsel128
        {
            {
                {{32,13 }, {32,18 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_tsel128)
            },
            {
                parser::Terminal_category::Base_type,      parser::Terminal_category::Block_entry,    parser::Terminal_category::Constant_size_type,            
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,                  
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,                 
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,                
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,                 
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr20,                 
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,         parser::Terminal_category::Expr3,                      
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,                      
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Kw_tsel128,     parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,            
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Type_value,
            }
        },
        // шкала         Kw_shkala
        {
            {
                {{35,5  }, {35,9  }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_shkala)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Kw_shkala,       parser::Terminal_category::Match_branch,               
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Scale_type,    parser::Terminal_category::Select_branch,   parser::Terminal_category::Spider_branch,              
                parser::Terminal_category::Type_value,
            }
        },
        // шкалу         Kw_shkalu
        {
            {
                {{35,16 }, {35,20 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_shkalu)
            },
            {
                parser::Terminal_category::Kw_shkalu
            }
        },
        // эквив         Kw_ekviv
        {
            {
                {{39,42 }, {39,46 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_ekviv)
            },
            {
                parser::Terminal_category::Kw_ekviv, parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Rel_op,
            }
        },
        // экспорт       Kw_eksport
        {
            {
                {{38,73 }, {38,79 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_eksport)
            },
            {
                parser::Terminal_category::Block_entry, parser::Terminal_category::Exported_stmt,
                parser::Terminal_category::Kw_eksport,  parser::Terminal_category::Stmt,
            }
        },
        // элемент       Kw_element            
        {
            {
                {{38,31 }, {38,37 }}, 
                testing_scanner::keyword_lexeme(scanner::Keyword_kind::Kw_element)
            },
            {
                parser::Terminal_category::Kw_element, parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Rel_op,
            },
        },
    };

#if defined(_WIN32) || defined(_WIN64)
#   define HERR_MANNELIG 142
#   define POPE_AND_MAIDEN_LEN 1494
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#else
#   define HERR_MANNELIG 139
#   define POPE_AND_MAIDEN_LEN (1494 - 14)
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#endif

    const quat::quat_t<__float128> quat_vals[] = {
        {0.0q, 0.0q, 45760.91234567892345E-223q, 0.0q                      },
        {0.0q, 0.0q, 0.0q,                       45760.91234567892345E-223q},
        {0.0q, 0.0q, 0.618281828459045e+4q,      0.0q                      },
        {0.0q, 0.0q, 0.0q,                       6.141592653148e+8q        },
        {0.0q, 0.0q, 45.67891234567892345E-12q,  0.0q                      },
    };

    const Pair_for_test literals_tests[] = {
        {
            {
                {{11,68 }, {11,70 }}, 
                testing_scanner::char_lexeme(U'Ё')
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{5, 13 }, {5, 21 }}, 
                testing_scanner::encoded_char_lexeme(U'Ѭ')
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{3, 15 }, {6, 31 }}, 
                testing_scanner::string_lexeme(HERR_MANNELIG)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{2, 12 }, {2, 21 }}, 
                testing_scanner::integer_lexeme(0x1238912345ULL)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{6, 26 }, {6, 43 }}, 
                testing_scanner::float_lexeme(6.141592653148e+8q, scanner::Float_kind::Float32)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{9, 13 }, {9, 28 }}, 
                testing_scanner::float_lexeme(3.141592653e-78q, scanner::Float_kind::Float128)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{9, 51 }, {9, 66 }}, 
                testing_scanner::float_lexeme(2.141592653e+11q, scanner::Float_kind::Float64)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{19,11 }, {19,31 }}, 
                testing_scanner::float_lexeme(0.618281828459045e-4q, scanner::Float_kind::Float80)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{15,57 }, {15,86 }}, 
                testing_scanner::complex_lexeme({0.0q, 45760.91234567892345E-223q})
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{16,17 }, {16,46 }}, 
                testing_scanner::quat_lexeme(quat_vals[0])
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{19,61 }, {19,82 }}, 
                testing_scanner::quat_lexeme(quat_vals[2], scanner::Quat_kind::Quat80)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{20,17 }, {20,35 }}, 
                testing_scanner::quat_lexeme(quat_vals[3], scanner::Quat_kind::Quat32)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
        {
            {
                {{20,37 }, {20,68 }}, 
                testing_scanner::quat_lexeme(quat_vals[4], scanner::Quat_kind::Quat128)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,        
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                        
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,        
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                         
                parser::Terminal_category::Expr9,         parser::Terminal_category::Literal,         parser::Terminal_category::Match_branch,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch,  parser::Terminal_category::Mspider_branch,
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,
            }
        },
    };

    const Pair_for_test delimiters_tests[] = {
        // !      Logical_not                 
        {
            {
                {{1, 8  }, {1, 8  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_not)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           parser::Terminal_category::Expr0, 
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr2,          parser::Terminal_category::Expr3, 
                parser::Terminal_category::Logical_not,    parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,
            }
        },
        // !&&    Logical_and_not            
        {
            {
                {{3, 5  }, {3, 7  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_not)
            },
            {
                parser::Terminal_category::Log_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // !&&.   Logical_and_not_full        
        {
            {
                {{6, 9  }, {6, 12 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_not_full)
            },
            {
                parser::Terminal_category::Log_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // !&&.=  Logical_and_not_full_assign
        {
            {
                {{10,37 }, {10,41 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_not_full_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // !&&=   Logical_and_not_assign      
        {
            {
                {{8, 13 }, {8, 16 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_not_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // !=     NEQ                        
        {
            {
                {{2, 32 }, {2, 33 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::NEQ)
            },
            {
                parser::Terminal_category::Rel_op, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // !||    Logical_or_not              
        {
            {
                {{5, 29 }, {5, 31 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_not)
            },
            {
                parser::Terminal_category::Log_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // !||.   Logical_or_not_full        
        {
            {
                {{6, 21 }, {6, 24 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_not_full)
            },
            {
                parser::Terminal_category::Log_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // !||.=  Logical_or_not_full_assign  
        {
            {
                {{13,21 }, {13,25 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_not_full_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // !||=   Logical_or_not_assign      
        {
            {
                {{8, 29 }, {8, 32 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_not_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // #      Sharp                       
        {
            {
                {{5, 41 }, {5, 41 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Sharp)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,         parser::Terminal_category::Expr2,
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,
                parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Sizeof_like,    parser::Terminal_category::Spider_branch,
            }
        },
        // ##     Data_size                  
        {
            {
                {{7, 15 }, {7, 16 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Data_size)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,         parser::Terminal_category::Expr2,
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,
                parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Sizeof_like,    parser::Terminal_category::Spider_branch,
            }
        },
        // %      Remainder                   
        {
            {
                {{7, 32 }, {7, 32 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Remainder)
            },
            {
                parser::Terminal_category::Mul_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // %.     Float_remainder            
        {
            {
                {{9, 24 }, {9, 25 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Float_remainder)
            },
            {
                parser::Terminal_category::Mul_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // %.=    Float_remainder_assign      
        {
            {
                {{15,21 }, {15,23 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Float_remainder_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // %=     Remainder_assign           
        {
            {
                {{15,3  }, {15,4  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Remainder_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // &      Bitwise_and                 
        {
            {
                {{20,32 }, {20,32 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_and)
            },
            {
                parser::Terminal_category::Bit_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // &&     Logical_and                
        {
            {
                {{21,19 }, {21,20 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and)
            },
            {
                parser::Terminal_category::Log_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // &&.    Logical_and_full            
        {
            {
                {{22,4  }, {22,6  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_full)
            },
            {
                parser::Terminal_category::Log_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // &&.=   Logical_and_full_assign    
        {
            {
                {{20,11 }, {20,14 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_full_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // &&=    Logical_and_assign          
        {
            {
                {{22,14 }, {22,16 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_and_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // &=     Bitwise_and_assign         
        {
            {
                {{21,37 }, {21,38 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_and_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // (      Round_bracket_opened        
        {
            {
                {{20,49 }, {20,49 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Round_bracket_opened)
            },
            {
                parser::Terminal_category::Block_entry,          parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,               parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,               parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,
                parser::Terminal_category::Expr18,               parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,
                parser::Terminal_category::Expr20,               parser::Terminal_category::Expr21,         parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,                parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,                parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Func_signature,       parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch,       parser::Terminal_category::Mspider_branch, parser::Terminal_category::Package_func_signature,
                parser::Terminal_category::Round_bracket_opened, parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,
            }
        },
        // (:     Tuple_begin                
        {
            {
                {{20,42 }, {20,43 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Tuple_begin)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,                      
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,                     
                parser::Terminal_category::Expr15,        parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,                     
                parser::Terminal_category::Expr18,        parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr20,        parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,                     
                parser::Terminal_category::Expr3,         parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,         parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,         parser::Terminal_category::Match_branch,   parser::Terminal_category::Meta_var_def,
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch,             
                parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,  parser::Terminal_category::Tuple_begin,
                parser::Terminal_category::Tuple_expr,    parser::Terminal_category::Var_def,
            }
        },
        // )      Round_bracket_closed        
        {
            {
                {{22,27 }, {22,27 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Round_bracket_closed)
            },
            {
                parser::Terminal_category::Round_bracket_closed
            }
        },
        // *      Mul                        
        {
            {
                {{32,18 }, {32,18 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Mul)
            },
            {
                parser::Terminal_category::Mul_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // **     Power                       
        {
            {
                {{32,13 }, {32,14 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Power)
            },
            {
                parser::Terminal_category::Pow_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // **.    Float_power                
        {
            {
                {{33,25 }, {33,27 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Float_power)
            },
            {
                parser::Terminal_category::Pow_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // **.=   Float_power_assign          
        {
            {
                {{34,17 }, {34,20 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Float_power_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // **=    Power_assign               
        {
            {
                {{33,41 }, {33,43 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Power_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // */     Comment_end                 
        // *=     Mul_assign                 
        {
            {
                {{33,31 }, {33,32 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Mul_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // +      Plus                        
        {
            {
                {{34,34 }, {34,34 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Plus)
            },
            {
                parser::Terminal_category::Add_like,       parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
        // ++     Inc                        
        {
            {
                {{34,56 }, {34,57 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Inc)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr2,         parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,         parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,         parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Postfix_inc,   parser::Terminal_category::Prefix_inc,     parser::Terminal_category::Select_branch,                 
                parser::Terminal_category::Spider_branch,
            }
        },
        // ++<    Inc_with_wrap               
        {
            {
                {{37,23 }, {37,25 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Inc_with_wrap)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr2,         parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,         parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,         parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Postfix_inc,   parser::Terminal_category::Prefix_inc,     parser::Terminal_category::Select_branch,                 
                parser::Terminal_category::Spider_branch,
            }
        },
        // +=     Plus_assign                
        {
            {
                {{37,7  }, {37,8  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Plus_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ,      Comma                       
        {
            {
                {{30,1  }, {30,1  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Comma)
            },
            {
                parser::Terminal_category::Comma
            }
        },
        // -      Minus                      
        {
            {
                {{17,33 }, {17,33 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Minus)
            },
            {
                parser::Terminal_category::Add_like,       parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           
                parser::Terminal_category::Expr0,          parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         
                parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         
                parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,                      
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                      
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                      
                parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
        // --     Dec                         
        {
            {
                {{17,7  }, {17,8  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Dec)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr2,         parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,         parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,         parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Postfix_inc,   parser::Terminal_category::Prefix_inc,     parser::Terminal_category::Select_branch,                 
                parser::Terminal_category::Spider_branch,              
            }
        },
        // --<    Dec_with_wrap              
        {
            {
                {{12,11 }, {12,13 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Dec_with_wrap)
            },
            {
                parser::Terminal_category::Block_entry,   parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,         parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,        parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,         
                parser::Terminal_category::Expr2,         parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          
                parser::Terminal_category::Expr5,         parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          
                parser::Terminal_category::Expr8,         parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   
                parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, 
                parser::Terminal_category::Postfix_inc,   parser::Terminal_category::Prefix_inc,     parser::Terminal_category::Select_branch,                 
                parser::Terminal_category::Spider_branch,
            }
        },
        // -=     Minus_assign                
        {
            {
                {{10,50 }, {10,51 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Minus_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ->     Right_arrow                
        {
            {
                {{5, 46 }, {5, 47 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Right_arrow)
            }, 
            {
                parser::Terminal_category::Right_arrow
            }
        },
        // .      Dot                         
        {
            {
                {{39,16 }, {39,16 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Dot)
            },
            {
                parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Dot
            }
        },
        // ..     Range                      
        {
            {
                {{39,27 }, {39,28 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Range)
            },
            {
                parser::Terminal_category::Range
            }
        },
        // ...    Range_excluded_end          
        {
            {
                {{65,21 }, {65,23 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Range_excluded_end)
            },
            {
                parser::Terminal_category::Range_excluded_end
            }
        },
        // .|.    Algebraic_separator        
        {
            {
                {{39,53 }, {39,55 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Algebraic_separator)
            },
            {
                parser::Terminal_category::Add_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // .|.=   Algebraic_separator_assign  
        {
            {
                {{70,32 }, {70,35 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Algebraic_separator_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // /      Div                        
        {
            {
                {{28,23 }, {28,23 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Div)
            },
            {
                parser::Terminal_category::Mul_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // /*     Comment_begin               
        // /=     Div_assign                 
        {
            {
                {{28,9  }, {28,10 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Div_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // /\     Symmetric_difference        
        {
            {
                {{28,18 }, {28,19 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Symmetric_difference)
            },
            {
                parser::Terminal_category::Mul_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // /\=    Symmetric_difference_assign
        {
            {
                {{28,13 }, {28,15 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Symmetric_difference_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // :      Colon                       
        {
            {
                {{25,4  }, {25,4  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Colon)
            },
            {
                parser::Terminal_category::Colon
            }
        },
        // :)     Tuple_end                  
        {
            {
                {{25,1  }, {25,2  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Tuple_end)
            },
            {
                parser::Terminal_category::Tuple_end
            }
        },
        // ::     Scope_resolution            
        {
            {
                {{25,6  }, {25,7  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Scope_resolution)
            },
            {
                parser::Terminal_category::Scope_resolution
            }
        },
        // :=     Copy                       
        {
            {
                {{25,9  }, {25,10 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Copy)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // :>     Meta_bracket_closed         
        {
            {
                {{26,5  }, {26,6  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Meta_bracket_closed)
            },
            {
                parser::Terminal_category::Meta_bracket_closed
            }
        },
        // :]     Array_literal_end          
        {
            {
                {{26,18 }, {26,19 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Array_literal_end)
            },
            {
                parser::Terminal_category::Array_literal_end
            }
        },
        // :}     Set_literal_end             
        {
            {
                {{26,11 }, {26,12 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Set_literal_end)
            },
            {
                parser::Terminal_category::Set_literal_end
            }
        },
        // ;      Semicolon                  
        {
            {
                {{1, 1  }, {1, 1  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Semicolon)
            },
            {
                parser::Terminal_category::Block_entry, parser::Terminal_category::Nonmarked_stmt,
                parser::Terminal_category::Semicolon,   parser::Terminal_category::Stmt,
            }
        },
        // <      LT                          
        {
            {
                {{44,3  }, {44,3  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::LT)
            },
            {
                parser::Terminal_category::Rel_op, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // <!!>   Common_template_type       
        {
            {
                {{55,8  }, {55,11 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Common_template_type)
            },
            {
                parser::Terminal_category::Common_template_type
            }
        },
        // <!>    Template_type               
        {
            {
                {{48,7  }, {48,9  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Template_type)
            },
            {
                parser::Terminal_category::Template_type
            }
        },
        // <&&>   Data_address               
        {
            {
                {{48,32 }, {48,35 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Data_address)
            },
            {
                parser::Terminal_category::Address_like,   parser::Terminal_category::Block_entry,   parser::Terminal_category::Data_address,
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,         parser::Terminal_category::Expr1,         
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,        parser::Terminal_category::Expr12,        
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,        parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,        parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,         parser::Terminal_category::Expr3,          
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,         parser::Terminal_category::Expr6,          
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,         parser::Terminal_category::Expr9,          
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,    
            }
        },
        // <&>    Address                     
        {
            {
                {{48,16 }, {48,18 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Address)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,          
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,         
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,   
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,
                parser::Terminal_category::Expr18,         parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,         
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,         
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,         
                parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, 
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,                            
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Address_like,   parser::Terminal_category::Address,
            }
        },
        // <+>    Type_add                   
        {
            {
                {{48,24 }, {48,26 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Type_add)
            },
            {
                parser::Terminal_category::Add_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // <+>=   Type_add_assign             
        {
            {
                {{48,46 }, {48,49 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Type_add_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // <-     Left_arrow                 
        {
            {
                {{44,30 }, {44,31 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Left_arrow)
            },
            {
                parser::Terminal_category::Left_arrow
            }
        },
        // <:     Meta_bracket_opened         
        {
            {
                {{44,43 }, {44,44 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Meta_bracket_opened)
            },
            {
                parser::Terminal_category::Meta_bracket_opened, parser::Terminal_category::Package_metafunc_signature,
                parser::Terminal_category::Metafunc_signature,
            }
        },
        // <:>    Common_type                
        {
            {
                {{50,10 }, {50,12 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Common_type)
            },
            {
                parser::Terminal_category::Common_type            
            }
        },
        // <<     Left_shift                  
        {
            {
                {{45,12 }, {45,13 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Left_shift)
            },
            {
                parser::Terminal_category::Bit_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // <<<    Cyclic_left_shift          
        {
            {
                {{70,9  }, {70,11 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cyclic_left_shift)
            },
            {
                parser::Terminal_category::Bit_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // <<<=   Cyclic_left_shift_assign    
        {
            {
                {{70,44 }, {70,47 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cyclic_left_shift_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // <<=    Left_shift_assign          
        {
            {
                {{50,28 }, {50,30 }}, testing_scanner::delim_lexeme(scanner::Delimiter_kind::Left_shift_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // <=     LEQ                         
        {
            {
                {{45,23 }, {45,24 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::LEQ)
            },
            {
                parser::Terminal_category::Rel_op, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // <?>    Get_expr_type              
        {
            {
                {{50,46 }, {50,48 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Get_expr_type)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Get_expr_type,  parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
        // <??>   Get_elem_type               
        {
            {
                {{50,52 }, {50,55 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Get_elem_type)
            },
            {
                parser::Terminal_category::Address_like,   parser::Terminal_category::Block_entry,   parser::Terminal_category::Get_elem_type,
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,         parser::Terminal_category::Expr1,         
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,        parser::Terminal_category::Expr12,        
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,        parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,        parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,         parser::Terminal_category::Expr3,          
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,         parser::Terminal_category::Expr6,          
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,         parser::Terminal_category::Expr9,          
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch, parser::Terminal_category::Mselect_branch, 
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch, parser::Terminal_category::Spider_branch,    
            }
        },
        // <|     Label_prefix               
        {
            {
                {{45,58 }, {45,59 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Label_prefix)
            },
            {
                parser::Terminal_category::Labeled_loop, parser::Terminal_category::Stmt,
                parser::Terminal_category::Block_entry,  parser::Terminal_category::Label_prefix,
            }
        },
        // =      Assign                      
        {
            {
                {{42,29 }, {42,29 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ==     EQ                         
        {
            {
                {{42,18 }, {42,19 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::EQ)
            },
            {
                parser::Terminal_category::Rel_op, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // >      GT                          
        {
            {
                {{60,30 }, {60,30 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::GT)
            },
            {
                parser::Terminal_category::Rel_op, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            },
        },
        // >=     GEQ                        
        {
            {
                {{60,33 }, {60,34 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::GEQ)
            },
            {
                parser::Terminal_category::Rel_op, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // >>     Right_shift                 
        {
            {
                {{60,38 }, {60,39 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Right_shift)
            },
            {
                parser::Terminal_category::Bit_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // >>=    Right_shift_assign         
        {
            {
                {{60,55 }, {60,57 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Right_shift_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // >>>    Cyclic_right_shift          
        {
            {
                {{67,40 }, {67,42 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cyclic_right_shift)
            },
            {
                parser::Terminal_category::Bit_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // >>>=   Cyclic_right_shift_assign  
        {
            {
                {{67,7  }, {67,10 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cyclic_right_shift_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ?      Cond_op                     
        {
            {
                {{70,67 }, {70,67 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cond_op)
            },
            {
                parser::Terminal_category::Cond_op
            }
        },
        // ?.     Cond_op_full               
        {
            {
                {{70,59 }, {70,60 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cond_op_full)
            },
            {
                parser::Terminal_category::Cond_op
            }
        },
        // @      At                          
        {
            {
                {{53,1  }, {53,1  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::At)
            },
            {
                parser::Terminal_category::Ptr_def,                       parser::Terminal_category::Deref_op,       parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,                         parser::Terminal_category::Expr2,          parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,                         parser::Terminal_category::Expr5,          parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,                         parser::Terminal_category::Expr8,          parser::Terminal_category::Expr9,
                parser::Terminal_category::Expr10,                        parser::Terminal_category::Expr11,         parser::Terminal_category::Expr12,
                parser::Terminal_category::Expr13,                        parser::Terminal_category::Expr14,         parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,                        parser::Terminal_category::Expr17,         parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,                        parser::Terminal_category::Expr20,         parser::Terminal_category::Expr,
                parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Spider_branch,
                parser::Terminal_category::Mmatch_branch,                 parser::Terminal_category::Match_branch,   parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Select_branch,                 parser::Terminal_category::Block_entry,
            }
        },
        // [      Square_bracket_opened      
        {
            {
                {{52,27 }, {52,27 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Square_bracket_opened)
            },
            {
                parser::Terminal_category::Square_bracket_opened
            }
        },
        // [:     Array_literal_begin         
        {
            {
                {{52,10 }, {52,11 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Array_literal_begin)
            },
            {
                parser::Terminal_category::Array_expr,      parser::Terminal_category::Array_literal_begin, parser::Terminal_category::Block_entry,
                parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,               parser::Terminal_category::Expr1,                         
                parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,              parser::Terminal_category::Expr12,
                parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,              parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,              parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,               parser::Terminal_category::Expr20,         
                parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,              parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,               parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,               parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,    parser::Terminal_category::Mmatch_branch,       parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch,  parser::Terminal_category::Select_branch,       parser::Terminal_category::Spider_branch,
            }
        },
        // \      Set_difference             
        {
            {
                {{52,3  }, {52,3  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Set_difference)
            },
            {
                parser::Terminal_category::Mul_like, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // \=     Set_difference_assign       
        {
            {
                {{52,17 }, {52,18 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Set_difference_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ]      Square_bracket_closed      
        {
            {
                {{30,5  }, {30,5  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Square_bracket_closed)
            },
            {
                parser::Terminal_category::Square_bracket_closed
            }
        },
        // ^      Bitwise_xor                 
        {
            {
                {{53,36 }, {53,36 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_xor)
            },
            {
                parser::Terminal_category::Bit_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ^=     Bitwise_xor_assign         
        {
            {
                {{53,14 }, {53,15 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_xor_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ^^     Logical_xor                 
        {
            {
                {{53,6  }, {53,7  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_xor)
            },
            {
                parser::Terminal_category::Log_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ^^=    Logical_xor_assign         
        {
            {
                {{53,29 }, {53,31 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_xor_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // {      Figure_bracket_opened       
        {
            {
                {{60,8  }, {60,8  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Figure_bracket_opened)
            },
            {
                parser::Terminal_category::Block,              parser::Terminal_category::Block_entry,           parser::Terminal_category::Category_body,
                parser::Terminal_category::Category_impl_body, parser::Terminal_category::Figure_bracket_opened, parser::Terminal_category::Nonmarked_stmt,
                parser::Terminal_category::Stmt,
            }
        },
        // {..}   Pattern                    
        {
            {
                {{60,3  }, {60,6  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Pattern)
            },
            {
                parser::Terminal_category::Pattern
            }
        },
        // {:     Set_literal_begin           
        {
            {
                {{67,17 }, {67,18 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Set_literal_begin)
            },
            {
                parser::Terminal_category::Set_expr,       parser::Terminal_category::Set_literal_begin, parser::Terminal_category::Block_entry,
                parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,             parser::Terminal_category::Expr1,                         
                parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,            parser::Terminal_category::Expr12,
                parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,            parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,         parser::Terminal_category::Expr17,            parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,         parser::Terminal_category::Expr2,             parser::Terminal_category::Expr20,         
                parser::Terminal_category::Expr21,         parser::Terminal_category::Expr22,            parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,             parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,             parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,     parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Select_branch,     parser::Terminal_category::Spider_branch,
            }
        },
        // |      Bitwise_or                 
        {
            {
                {{57,6  }, {57,6  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_or)
            },
            {
                parser::Terminal_category::Bit_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // |#|    Cardinality                 
        {
            {
                {{57,21 }, {57,23 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Cardinality)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,           parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,         parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,         parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr16,         parser::Terminal_category::Expr2,
                parser::Terminal_category::Expr3,          parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,
                parser::Terminal_category::Expr6,          parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,
                parser::Terminal_category::Expr9,          parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,
                parser::Terminal_category::Mselect_branch, parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name,
                parser::Terminal_category::Select_branch,  parser::Terminal_category::Sizeof_like,    parser::Terminal_category::Spider_branch,
            }
        },
        // |=     Bitwise_or_assign          
        {
            {
                {{57,31 }, {57,32 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_or_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ||     Logical_or                  
        {
            {
                {{58,12 }, {58,13 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or)
            },
            {
                parser::Terminal_category::Log_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ||.    Logical_or_full            
        {
            {
                {{58,17 }, {58,19 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_full)
            },
            {
                parser::Terminal_category::Log_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ||.=   Logical_or_full_assign      
        {
            {
                {{58,36 }, {58,39 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_full_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ||=    Logical_or_assign          
        {
            {
                {{58,25 }, {58,27 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Logical_or_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // }      Figure_bracket_closed       
        {
            {
                {{65,5  }, {65,5  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Figure_bracket_closed)
            },
            {
                parser::Terminal_category::Figure_bracket_closed
            }
        },
        // ~      Bitwise_not                
        {
            {
                {{63,10 }, {63,10 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_not)
            },
            {
                parser::Terminal_category::Bitwise_not
            }
        },
        // ~&     Bitwise_and_not             
        {
            {
                {{63,49 }, {63,50 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_and_not)
            },
            {
                parser::Terminal_category::Bit_and, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ~&=    Bitwise_and_not_assign     
        {
            {
                {{63,35 }, {63,37 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_and_not_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // ~|     Bitwise_or_not              
        {
            {
                {{63,24 }, {63,25 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_or_not)
            },
            {
                parser::Terminal_category::Bit_or, parser::Terminal_category::Nonbracket_maybe_pure_op_name
            }
        },
        // ~|=    Bitwise_or_not_assign      
        {
            {
                {{63,53 }, {63,55 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitwise_or_not_assign)
            },
            {
                parser::Terminal_category::Assignment
            }
        },
        // |:     Bitscale_literal_begin      
        {
            {
                {{73,54 }, {73,55 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitscale_literal_begin)
            },
            {
                parser::Terminal_category::Scale_expr,      parser::Terminal_category::Bitscale_literal_begin, parser::Terminal_category::Block_entry,
                parser::Terminal_category::Expr,            parser::Terminal_category::Expr0,                  parser::Terminal_category::Expr1,                         
                parser::Terminal_category::Expr10,          parser::Terminal_category::Expr11,                 parser::Terminal_category::Expr12,
                parser::Terminal_category::Expr13,          parser::Terminal_category::Expr14,                 parser::Terminal_category::Expr15,
                parser::Terminal_category::Expr16,          parser::Terminal_category::Expr17,                 parser::Terminal_category::Expr18,
                parser::Terminal_category::Expr19,          parser::Terminal_category::Expr2,                  parser::Terminal_category::Expr20,         
                parser::Terminal_category::Expr21,          parser::Terminal_category::Expr22,                 parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,           parser::Terminal_category::Expr5,                  parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,           parser::Terminal_category::Expr8,                  parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,    parser::Terminal_category::Mmatch_branch,          parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch,  parser::Terminal_category::Select_branch,          parser::Terminal_category::Spider_branch,            
            }
        },
        // :|     Bitscale_literal_end       
        {
            {
                {{73,17 }, {73,18 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Bitscale_literal_end)
            },
            {
                parser::Terminal_category::Bitscale_literal_end
            }
        },
        // <#     Minimal_value_in_collection 
        {
            {
                {
                    {75,35 }, {75,36 }}, 
                    testing_scanner::delim_lexeme(scanner::Delimiter_kind::Minimal_value_in_collection)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,                          parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,                        parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,                        parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,                         parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                         parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                         parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,                 parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
        // #>     Maximal_value_in_collection
        {
            {
                {{75,2  }, {75,3  }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Maximal_value_in_collection)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,                          parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,                        parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,                        parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,                         parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                         parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                         parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,                 parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
        // <<#    Minimal_value_of_type       
        {
            {
                {{75,41 }, {75,43 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Minimal_value_of_type)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,                          parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,                        parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,                        parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,                         parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                         parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                         parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,                 parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
        // #>>    Maximal_value_of_type      
        {
            {
                {{75,62 }, {75,64 }}, 
                testing_scanner::delim_lexeme(scanner::Delimiter_kind::Maximal_value_of_type)
            },
            {
                parser::Terminal_category::Block_entry,    parser::Terminal_category::Expr,                          parser::Terminal_category::Expr0,
                parser::Terminal_category::Expr1,          parser::Terminal_category::Expr10,                        parser::Terminal_category::Expr11,
                parser::Terminal_category::Expr12,         parser::Terminal_category::Expr13,                        parser::Terminal_category::Expr14,
                parser::Terminal_category::Expr15,         parser::Terminal_category::Expr2,                         parser::Terminal_category::Expr3,
                parser::Terminal_category::Expr4,          parser::Terminal_category::Expr5,                         parser::Terminal_category::Expr6,
                parser::Terminal_category::Expr7,          parser::Terminal_category::Expr8,                         parser::Terminal_category::Expr9,
                parser::Terminal_category::Match_branch,   parser::Terminal_category::Mmatch_branch,                 parser::Terminal_category::Mselect_branch,
                parser::Terminal_category::Mspider_branch, parser::Terminal_category::Nonbracket_maybe_pure_op_name, parser::Terminal_category::Select_branch,
                parser::Terminal_category::Spider_branch,  parser::Terminal_category::Unary_plus,
            }
        },
    };
}

namespace testing_parser{
    void test_token_to_terminal_category()
    {
        puts("Testing of converting of tokens to a set of categories of terminals...\n");
        puts("Testing for special tokens:");
        testing::test(std::begin(special_lexemes_tests), 
                      std::end(special_lexemes_tests), 
                      [](const parser::Token& tok) -> parser::Terminal_set
                      {
                          return parser::token_to_terminal_set(tok);
                      });

        puts("\nTesting for identifiers:");
        testing::test(std::begin(id_tests), 
                      std::end(id_tests), 
                      [](const parser::Token& tok) -> parser::Terminal_set
                      {
                          return parser::token_to_terminal_set(tok);
                      });

        puts("\nTesting for keywords:");
        testing::test(std::begin(tests_for_keywords), 
                      std::end(tests_for_keywords), 
                      [](const parser::Token& tok) -> parser::Terminal_set
                      {
                          return parser::token_to_terminal_set(tok);
                      });

        puts("\nTesting for literals:");
        testing::test(std::begin(literals_tests), 
                      std::end(literals_tests), 
                      [](const parser::Token& tok) -> parser::Terminal_set
                      {
                          return parser::token_to_terminal_set(tok);
                      });

        puts("\nTesting for delimiters:");
        testing::test(std::begin(delimiters_tests), 
                      std::end(delimiters_tests), 
                      [](const parser::Token& tok) -> parser::Terminal_set
                      {
                          return parser::token_to_terminal_set(tok);
                      });
    }
}