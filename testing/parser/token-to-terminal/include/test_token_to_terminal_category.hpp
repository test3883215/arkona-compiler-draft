/*
    File:    test_token_to_terminal_category.hpp
    Created: 15 October 2024 at 08:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

namespace testing_parser{
    void test_token_to_terminal_category();
}