/*
    File:    usage.hpp
    Created: 14 May 2023 at 17:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/


#pragma once

void usage(const char* program_name);