/*
    File:    testing-func.hpp
    Created: 13 May 2023 at 15:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>
#include "../../../../scanner/include/scanner.hpp"

namespace testing_scanner{
    void test_func(const std::shared_ptr<scanner::Scanner>& arkonasc);
}