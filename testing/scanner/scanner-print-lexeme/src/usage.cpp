/*
    File:    usage.cpp
    Created: 14 June 2023 at 17:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/usage.hpp"

static const char* usage_str =
    R"~(Arkona scanner print lexeme testing
Copyright (c) Gavrilov V.S., 2021
testing is a program for a testing of lexical analysis of the programming language Arkona.

This program is free sofwtware, and it is licensed under the GPLv3 license.
There is NO warranty, not even MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Usage: %s test_file
)~";

void usage(const char* program_name)
{
    printf(usage_str, program_name);
}