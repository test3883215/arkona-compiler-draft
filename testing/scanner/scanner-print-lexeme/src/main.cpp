/*
    File:    main.cpp
    Created: 13 May 2023 at 15:23 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../../../../file-utils/include/get_processed_text.hpp"
#include "../../../../iscanner/include/error_count.hpp"
#include "../../../../iscanner/include/errors_and_tries.hpp"
#include "../../../../iscanner/include/location.hpp"
#include "../../../../iscanner/include/position.hpp"
#include "../../../../iscanner/include/token.hpp"
#include "../../../../iscanner/include/warning_count.hpp"
#include "../../../../strings-utils/include/char_trie.hpp"
#include "../../../../scanner/include/scanner.hpp"

#include "../include/testing-func.hpp"
#include "../include/usage.hpp"

enum Exit_codes{
    Success, No_args, File_processing_error
};

int main(int argc, char* argv[])
{
    if(1 == argc){
        usage(argv[0]);
        return No_args;
    }

    auto              text      = file_utils::get_processed_text(argv[1]);
    if(text.empty()){
        return File_processing_error;
    }

    char32_t*         p         = const_cast<char32_t*>(text.c_str());
    auto              loc       = std::make_shared<iscaner::Location>(p);
    Errors_and_tries  et;
    et.ec_                      = std::make_shared<Error_count>();
    et.wc_                      = std::make_shared<Warning_count>();
    et.ids_trie_                = std::make_shared<strings::trie::Char_trie>();
    et.strs_trie_               = std::make_shared<strings::trie::Char_trie>();
    auto              scanner   = std::make_shared<scanner::Scanner>(loc, et);

    testing_scanner::test_func(scanner);
    et.print();

    return Success;
}