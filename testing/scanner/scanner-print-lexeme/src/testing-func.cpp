/*
    File:    testing-func.cpp
    Created: 13 May 2023 at 15:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/testing-func.hpp"

namespace testing_scanner{
    void test_func(const std::shared_ptr<scanner::Scanner>& arkonasc)
    {
        scanner::Arkona_token ati;
        scanner::Lexem_kind   alk;
        do{
            ati    = arkonasc->current_lexeme();
            alk    = ati.lexeme_.code_.kind_;
            auto s = arkonasc->token_to_string(ati);
            puts(s.c_str());
        }while(alk != scanner::Lexem_kind::Nothing);
    }
}