/*
    File:    test_back_function.cpp
    Created: 22 May 2023 at 22:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstddef>
#include <cstdio>
#include <iterator>
#include <memory>
#include <string>
#include <vector>
#include "../include/test_back_function.hpp"
#include "../../../../iscanner/include/errors_and_tries.hpp"
#include "../../../../iscanner/include/location.hpp"
#include "../../../../numbers/include/digit_to_int.hpp"
#include "../../../../scanner/include/scanner.hpp"
#include "../../../../strings-utils/include/char_trie.hpp"
#include "../../../utils/include/lexeme_fab.hpp"
#include "../../../utils/include/testing.hpp"


namespace{
    using namespace testing_scanner;

    const char32_t* test_text = UR"~(категория аддитивный_моноид<:T : тип:>
{
    конст нуль: T;
    операция + (x: T, y: T) : T;
    операция += (x: ссылка T, y: T) : ссылка T ;
    /*
       Данная категория определяет моноид по сложению, называемый ещё
       аддитивным моноидом.

       Напомним, что моноидом называется алгебраическая
       структура G с определённой на ней операцией ◊ : G x G → G,
       которая обладает следующими свойствами:
       1) для всех a, b, c справедливо соотношение
          (a ◊ b) ◊ c = a ◊ (b ◊ c)
       2) найдётся элемент e из G, такой, что
          a ◊ e = e ◊ a
          для всех элементов a из G.

       Первое свойство называется ассоциативностью операции, а элемент e
       из второго свойства --- нейтральным элементом.

       Аддитивным же моноидом называется моноид, в котором операция обозначается
       знаком +, а нейтральный элемент обозначается как 0.
    */
}

категория мультипликативный_моноид<:T : тип:>
{
    конст единица: T; операция * (x: T, y: T) : T;
    операция *= (x: ссылка T, y: T) : ссылка T ;
    /*
       Данная категория определяет моноид по умножению, называемый ещё
       мультипликативным моноидом.

       Напомним, что моноидом называется алгебраическая
       структура G с определённой на ней операцией ◊ : G x G → G,
       которая обладает следующими свойствами:
       1) для всех a, b, c справедливо соотношение
          (a ◊ b) ◊ c = a ◊ (b ◊ c)
       2) найдётся элемент e из G, такой, что
          a ◊ e = e ◊ a
          для всех элементов a из G.

       Первое свойство называется ассоциативностью операции, а элемент e
       из второго свойства --- нейтральным элементом.

       Мультипликативным же моноидом называется моноид, в котором операция обозначается
       знаком *, а нейтральный элемент обозначается как 1.

            /*
                Данная категория определяет моноид по сложению.
                Напомним, что моноидом называется алгебраическая
                структура G с определённой на ней операцией ◊ : G x G → G,
                которая обладает следующими свойствами:
                1) для всех a, b, c ∊ G справедливо соотношение
                    (a ◊ b) ◊ c = a ◊ (b ◊ c)
                2) найдётся элемент e ∊ G, такой, что
                    a ◊ e = e ◊ a
                    для всех элементов a из G.
                    /* А здесь ещё комментарий. */

                Первое свойство называется ассоциативностью операции, а элемент e
                из второго свойства --- нейтральным элементом.

                Аддитивным же моноидом называется моноид, в котором операция обозначается
                знаком +, а нейтральный элемент обозначается как 0.
            */

    */
})~";

    enum class Action{
        Get_current, Get_previous
    };

    using Scanner_ptr     = std::shared_ptr<scanner::Scanner>;
    using Token_generator = testing::Generator<scanner::Arkona_token>;

    class Scanner_as_generator_with_actions : public Token_generator{
    public:
        Scanner_as_generator_with_actions()                                         = default;
        Scanner_as_generator_with_actions(const Scanner_as_generator_with_actions&) = default;
        virtual ~Scanner_as_generator_with_actions()                                = default;

        Scanner_as_generator_with_actions(const Scanner_ptr&         scanner,
                                          const std::vector<Action>& actions):
            scanner_{scanner}, actions_{actions}
        {
            current_idx_ = 0;
        }

        scanner::Arkona_token yield()                                                override;
        bool                             is_finished(const scanner::Arkona_token& t) override;
    private:
        std::shared_ptr<scanner::Scanner> scanner_;
        const std::vector<Action>&        actions_;
        std::size_t                       current_idx_;
    };

    scanner::Arkona_token Scanner_as_generator_with_actions::yield()
    {
        if(actions_[current_idx_] == Action::Get_previous){
            scanner_->back();
        }
        current_idx_++;
        return scanner_->current_lexeme();
    }

    bool Scanner_as_generator_with_actions::is_finished(const scanner::Arkona_token& t)
    {
        return t.lexeme_.code_.kind_ == scanner::Lexem_kind::Nothing;
    }


    const std::vector<Action> actions = {
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous, // 9

        Action::Get_previous, Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 18

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 27

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 36

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 45

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_previous, Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 54

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 63

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 72

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_previous,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 81

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,  Action::Get_current,  // 90

        Action::Get_current,  Action::Get_current,  Action::Get_current,
        Action::Get_current,  Action::Get_current,
    };

    const std::vector<scanner::Arkona_token> tokens_for_text = {
            {{{1, 1  }, {1, 9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{1, 11 }, {1, 27 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид
            {{{1, 11 }, {1, 27 }}, id_lexeme(17)                                            }, // Id аддитивный_моноид

            {{{1, 28 }, {1, 29 }}, delim_lexeme(Delimiter_kind::Meta_bracket_opened)        }, // Delimiter Meta_bracket_opened
            {{{1, 28 }, {1, 29 }}, delim_lexeme(Delimiter_kind::Meta_bracket_opened)        }, // Delimiter Meta_bracket_opened
            {{{1, 30 }, {1, 30 }}, id_lexeme(18)                                            }, // Id T

            {{{1, 32 }, {1, 32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{1, 34 }, {1, 36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{1, 34 }, {1, 36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
// 9
            {{{1, 34 }, {1, 36 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{1, 37 }, {1, 38 }}, delim_lexeme(Delimiter_kind::Meta_bracket_closed)        }, // Delimiter Meta_bracket_closed
            {{{2, 1  }, {2, 1  }}, delim_lexeme(Delimiter_kind::Figure_bracket_opened)      }, // Delimiter Figure_bracket_opened

            {{{3, 5  }, {3, 9  }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{3, 11 }, {3, 14 }}, id_lexeme(22)                                            }, // Id нуль
            {{{3, 15 }, {3, 15 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{3, 17 }, {3, 17 }}, id_lexeme(18)                                            }, // Id T
            {{{3, 18 }, {3, 18 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{4, 5  }, {4, 12 }}, keyword_lexeme(Keyword_kind::Kw_operatsija)              }, // Keyword Kw_operatsija
// 18
            {{{4, 14 }, {4, 14 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // Delimiter Plus
            {{{4, 16 }, {4, 16 }}, delim_lexeme(Delimiter_kind::Round_bracket_opened)       }, // Delimiter Round_bracket_opened
            {{{4, 17 }, {4, 17 }}, id_lexeme(23)                                            }, // Id x

            {{{4, 18 }, {4, 18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4, 20 }, {4, 20 }}, id_lexeme(18)                                            }, // Id T
            {{{4, 21 }, {4, 21 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{4, 23 }, {4, 23 }}, id_lexeme(24)                                            }, // Id y
            {{{4, 24 }, {4, 24 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4, 26 }, {4, 26 }}, id_lexeme(18)                                            }, // Id T
// 27
            {{{4, 27 }, {4, 27 }}, delim_lexeme(Delimiter_kind::Round_bracket_closed)       }, // Delimiter Round_bracket_closed
            {{{4, 29 }, {4, 29 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{4, 31 }, {4, 31 }}, id_lexeme(18)                                            }, // Id T

            {{{4, 32 }, {4, 32 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{5, 5  }, {5, 12 }}, keyword_lexeme(Keyword_kind::Kw_operatsija)              }, // Keyword Kw_operatsija
            {{{5, 14 }, {5, 15 }}, delim_lexeme(Delimiter_kind::Plus_assign)                }, // Delimiter Plus_assign

            {{{5, 17 }, {5, 17 }}, delim_lexeme(Delimiter_kind::Round_bracket_opened)       }, // Delimiter Round_bracket_opened
            {{{5, 18 }, {5, 18 }}, id_lexeme(23)                                            }, // Id x
            {{{5, 19 }, {5, 19 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 36
            {{{5, 21 }, {5, 26 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{5, 28 }, {5, 28 }}, id_lexeme(18)                                            }, // Id x
            {{{5, 29 }, {5, 29 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{5, 31 }, {5, 31 }}, id_lexeme(24)                                            }, // Id y
            {{{5, 32 }, {5, 32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{5, 34 }, {5, 34 }}, id_lexeme(18)                                            }, // Id T

            {{{5, 35 }, {5, 35 }}, delim_lexeme(Delimiter_kind::Round_bracket_closed)       }, // Delimiter Round_bracket_closed
            {{{5, 37 }, {5, 37 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{5, 39 }, {5, 44 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
// 45
            {{{5, 46 }, {5, 46 }}, id_lexeme(18)                                            }, // Id T
            {{{5, 48 }, {5, 48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{25,1  }, {25,1  }}, delim_lexeme(Delimiter_kind::Figure_bracket_closed)      }, // Delimiter Figure_bracket_closed

            {{{27,1  }, {27,9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{27,1  }, {27,9  }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{27,11 }, {27,34 }}, id_lexeme(48)                                            }, // Id мультипликативный_моноид

            {{{27,35 }, {27,36 }}, delim_lexeme(Delimiter_kind::Meta_bracket_opened)        }, // Delimiter Meta_bracket_opened
            {{{27,37 }, {27,37 }}, id_lexeme(18)                                            }, // Id T
            {{{27,39 }, {27,39 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 54
            {{{27,41 }, {27,43 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{27,44 }, {27,45 }}, delim_lexeme(Delimiter_kind::Meta_bracket_closed)        }, // Delimiter Meta_bracket_closed
            {{{28,1  }, {28,1  }}, delim_lexeme(Delimiter_kind::Figure_bracket_opened)      }, // Delimiter Figure_bracket_opened

            {{{29,5  }, {29,9  }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{29,11 }, {29,17 }}, id_lexeme(55)                                            }, // Id единица
            {{{29,18 }, {29,18 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{29,20 }, {29,20 }}, id_lexeme(18)                                            }, // Id T
            {{{29,21 }, {29,21 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{29,23 }, {29,30 }}, keyword_lexeme(Keyword_kind::Kw_operatsija)              }, // Keyword Kw_operatsija
// 63
            {{{29,32 }, {29,32 }}, delim_lexeme(Delimiter_kind::Mul)                        }, // Delimiter Mul
            {{{29,34 }, {29,34 }}, delim_lexeme(Delimiter_kind::Round_bracket_opened)       }, // Delimiter Round_bracket_opened
            {{{29,35 }, {29,35 }}, id_lexeme(23)                                            }, // Id x

            {{{29,36 }, {29,36 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29,38 }, {29,38 }}, id_lexeme(18)                                            }, // Id T
            {{{29,39 }, {29,39 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma

            {{{29,41 }, {29,41 }}, id_lexeme(24)                                            }, // Id y
            {{{29,42 }, {29,42 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29,44 }, {29,44 }}, id_lexeme(18)                                            }, // Id T
// 72
            {{{29,45 }, {29,45 }}, delim_lexeme(Delimiter_kind::Round_bracket_closed)       }, // Delimiter Round_bracket_closed
            {{{29,47 }, {29,47 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{29,49 }, {29,49 }}, id_lexeme(18)                                            }, // Id T

            {{{29,50 }, {29,50 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon
            {{{30,5  }, {30,12 }}, keyword_lexeme(Keyword_kind::Kw_operatsija)              }, // Keyword Kw_operatsija
            {{{30,5  }, {30,12 }}, keyword_lexeme(Keyword_kind::Kw_operatsija)              }, // Keyword Kw_operatsija

            {{{30,14 }, {30,15 }}, delim_lexeme(Delimiter_kind::Mul_assign)                 }, // Delimiter Mul_assign
            {{{30,17 }, {30,17 }}, delim_lexeme(Delimiter_kind::Round_bracket_opened)       }, // Delimiter Round_bracket_opened
            {{{30,18 }, {30,18 }}, id_lexeme(23)                                            }, // Id x
// 81
            {{{30,19 }, {30,19 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
            {{{30,21 }, {30,26 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{30,28 }, {30,28 }}, id_lexeme(18)                                            }, // Id T

            {{{30,29 }, {30,29 }}, delim_lexeme(Delimiter_kind::Comma)                      }, // Delimiter Comma
            {{{30,31 }, {30,31 }}, id_lexeme(24)                                            }, // Id y
            {{{30,32 }, {30,32 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon

            {{{30,34 }, {30,34 }}, id_lexeme(18)                                            }, // Id T
            {{{30,35 }, {30,35 }}, delim_lexeme(Delimiter_kind::Round_bracket_closed)       }, // Delimiter Round_bracket_closed
            {{{30,37 }, {30,37 }}, delim_lexeme(Delimiter_kind::Colon)                      }, // Delimiter Colon
// 90
            {{{30,39 }, {30,44 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{30,46 }, {30,46 }}, id_lexeme(18)                                            }, // Id T
            {{{30,48 }, {30,48 }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // Delimiter Semicolon

            {{{70,1  }, {70,1  }}, delim_lexeme(Delimiter_kind::Figure_bracket_closed)      }, // Delimiter Figure_bracket_closed
            {{{70,2  }, {70,2  }}, nothing_lexeme()                                         }, // Nothing
    };
}

namespace testing_scanner{
    void test_back_function()
    {
        Errors_and_tries  et;
        et.ec_         = std::make_shared<Error_count>();
        et.wc_         = std::make_shared<Warning_count>();
        et.ids_trie_   = std::make_shared<strings::trie::Char_trie>();
        et.strs_trie_  = std::make_shared<strings::trie::Char_trie>();

        auto p         = const_cast<char32_t*>(test_text);
        auto loc       = std::make_shared<iscaner::Location>(p);
        auto scanner   = std::make_shared<scanner::Scanner>(loc, et);
        Scanner_as_generator_with_actions gen{scanner, actions};
        puts("Testing of the method back()...");
        testing::test(gen,                         [](auto t){return t;},
                      std::begin(tokens_for_text), std::end(tokens_for_text));
    }
}