/*
     File:    main.cpp
     Created: 22 May 2023 at 22:40 MSK
     Author:  Гаврилов Владимир Сергеевич
     E-mails: vladimir.s.gavrilov@gmail.com
              gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/test_back_function.hpp"

int main()
{
    testing_scanner::test_back_function();
}