/*
    File:    test_back_function.hpp
    Created: 22 May 2023 at 22:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

namespace testing_scanner{
    void test_back_function();
}