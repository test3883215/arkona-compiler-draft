/*
    File:    test_lexeme_to_string.cpp
    Created: 10 May 2023 at 21:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstdio>
#include <iterator>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include "../include/test_lexeme_to_string.hpp"
#include "../../../utils/include/lexeme_fab.hpp"
#include "../../../utils/include/testing.hpp"
#include "../../../../scanner/include/scanner.hpp"
#include "../../../../scanner/include/lexeme_to_str.hpp"
#include "../../../../strings-utils/include/char_trie.hpp"
#include "../../../../char-conv/include/char_conv.hpp"


namespace{
    using Pair_for_test = std::pair<scanner::LexemeInfo, std::string>;
    using namespace testing_scanner;

    //! Tests for special lexemes.
    const Pair_for_test special_lexemes_tests[] = {
        {unknown_lexeme(), "UnknownLexem"}, {nothing_lexeme(), "Nothing"}
    };

    //! Tests for keywords.
    const Pair_for_test keywords_tests[] = {
        {keyword_lexeme(Keyword_kind::Kw_log64),           "Keyword Kw_log64"          },
        {keyword_lexeme(Keyword_kind::Kw_ljambda),         "Keyword Kw_ljambda"        },
        {keyword_lexeme(Keyword_kind::Kw_tip),             "Keyword Kw_tip"            },
        {keyword_lexeme(Keyword_kind::Kw_bezzn16),         "Keyword Kw_bezzn16"        },
        {keyword_lexeme(Keyword_kind::Kw_porjadok64),      "Keyword Kw_porjadok64"     },
        {keyword_lexeme(Keyword_kind::Kw_vyberi),          "Keyword Kw_vyberi"         },
        {keyword_lexeme(Keyword_kind::Kw_element),         "Keyword Kw_element"        },
        {keyword_lexeme(Keyword_kind::Kw_malenkoe),        "Keyword Kw_malenkoe"       },
        {keyword_lexeme(Keyword_kind::Kw_veshch128),       "Keyword Kw_veshch128"      },
        {keyword_lexeme(Keyword_kind::Kw_bajt),            "Keyword Kw_bajt"           },
        {keyword_lexeme(Keyword_kind::Kw_shkala),          "Keyword Kw_shkala"         },
        {keyword_lexeme(Keyword_kind::Kw_meta),            "Keyword Kw_meta"           },
        {keyword_lexeme(Keyword_kind::Kw_veshch64),        "Keyword Kw_veshch64"       },
        {keyword_lexeme(Keyword_kind::Kw_obobshchenie),    "Keyword Kw_obobshchenie"   },
        {keyword_lexeme(Keyword_kind::Kw_vyjdi),           "Keyword Kw_vyjdi"          },
        {keyword_lexeme(Keyword_kind::Kw_shkalu),          "Keyword Kw_shkalu"         },
        {keyword_lexeme(Keyword_kind::Kw_paket),           "Keyword Kw_paket"          },
        {keyword_lexeme(Keyword_kind::Kw_bezzn32),         "Keyword Kw_bezzn32"        },
        {keyword_lexeme(Keyword_kind::Kw_to),              "Keyword Kw_to"             },
        {keyword_lexeme(Keyword_kind::Kw_golovnaya),       "Keyword Kw_golovnaya"      },
        {keyword_lexeme(Keyword_kind::Kw_realizatsija),    "Keyword Kw_realizatsija"   },
        {keyword_lexeme(Keyword_kind::Kw_diapazon),        "Keyword Kw_diapazon"       },
        {keyword_lexeme(Keyword_kind::Kw_bezzn128),        "Keyword Kw_bezzn128"       },
        {keyword_lexeme(Keyword_kind::Kw_menshe),          "Keyword Kw_menshe"         },
        {keyword_lexeme(Keyword_kind::Kw_tsel32),          "Keyword Kw_tsel32"         },
        {keyword_lexeme(Keyword_kind::Kw_bolshe),          "Keyword Kw_bolshe"         },
        {keyword_lexeme(Keyword_kind::Kw_razbor),          "Keyword Kw_razbor"         },
        {keyword_lexeme(Keyword_kind::Kw_kvat80),          "Keyword Kw_kvat80"         },
        {keyword_lexeme(Keyword_kind::Kw_chistaja),        "Keyword Kw_chistaja"       },
        {keyword_lexeme(Keyword_kind::Kw_massiv),          "Keyword Kw_massiv"         },
        {keyword_lexeme(Keyword_kind::Kw_kompl128),        "Keyword Kw_kompl128"       },
        {keyword_lexeme(Keyword_kind::Kw_eksport),         "Keyword Kw_eksport"        },
        {keyword_lexeme(Keyword_kind::Kw_iz),              "Keyword Kw_iz"             },
        {keyword_lexeme(Keyword_kind::Kw_nichto),          "Keyword Kw_nichto"         },
        {keyword_lexeme(Keyword_kind::Kw_vozvrat),         "Keyword Kw_vozvrat"        },
        {keyword_lexeme(Keyword_kind::Kw_porjadok16),      "Keyword Kw_porjadok16"     },
        {keyword_lexeme(Keyword_kind::Kw_bolshoe),         "Keyword Kw_bolshoe"        },
        {keyword_lexeme(Keyword_kind::Kw_ssylka),          "Keyword Kw_ssylka"         },
        {keyword_lexeme(Keyword_kind::Kw_bezzn64),         "Keyword Kw_bezzn64"        },
        {keyword_lexeme(Keyword_kind::Kw_perechisl_mnozh), "Keyword Kw_perechisl_mnozh"},
        {keyword_lexeme(Keyword_kind::Kw_ekviv),           "Keyword Kw_ekviv"          },
        {keyword_lexeme(Keyword_kind::Kw_esli),            "Keyword Kw_esli"           },
        {keyword_lexeme(Keyword_kind::Kw_struktura),       "Keyword Kw_struktura"      },
        {keyword_lexeme(Keyword_kind::Kw_veshch),          "Keyword Kw_veshch"         },
        {keyword_lexeme(Keyword_kind::Kw_paketnaja),       "Keyword Kw_paketnaja"      },
        {keyword_lexeme(Keyword_kind::Kw_bezzn8),          "Keyword Kw_bezzn8"         },
        {keyword_lexeme(Keyword_kind::Kw_dlya),            "Keyword Kw_dlya"           },
        {keyword_lexeme(Keyword_kind::Kw_samo),            "Keyword Kw_samo"           },
        {keyword_lexeme(Keyword_kind::Kw_vydeli),          "Keyword Kw_vydeli"         },
        {keyword_lexeme(Keyword_kind::Kw_funktsija),       "Keyword Kw_funktsija"      },
        {keyword_lexeme(Keyword_kind::Kw_bezzn),           "Keyword Kw_bezzn"          },
        {keyword_lexeme(Keyword_kind::Kw_tsel16),          "Keyword Kw_tsel16"         },
        {keyword_lexeme(Keyword_kind::Kw_postfiksnaja),    "Keyword Kw_postfiksnaja"   },
        {keyword_lexeme(Keyword_kind::Kw_ispolzuet),       "Keyword Kw_ispolzuet"      },
        {keyword_lexeme(Keyword_kind::Kw_stroka),          "Keyword Kw_stroka"         },
        {keyword_lexeme(Keyword_kind::Kw_kategorija),      "Keyword Kw_kategorija"     },
        {keyword_lexeme(Keyword_kind::Kw_simv16),          "Keyword Kw_simv16"         },
        {keyword_lexeme(Keyword_kind::Kw_veshch80),        "Keyword Kw_veshch80"       },
        {keyword_lexeme(Keyword_kind::Kw_stroka32),        "Keyword Kw_stroka32"       },
        {keyword_lexeme(Keyword_kind::Kw_veshch32),        "Keyword Kw_veshch32"       },
        {keyword_lexeme(Keyword_kind::Kw_preobrazuj),      "Keyword Kw_preobrazuj"     },
        {keyword_lexeme(Keyword_kind::Kw_v),               "Keyword Kw_v"              },
        {keyword_lexeme(Keyword_kind::Kw_simv8),           "Keyword Kw_simv8"          },
        {keyword_lexeme(Keyword_kind::Kw_istina),          "Keyword Kw_istina"         },
        {keyword_lexeme(Keyword_kind::Kw_vechno),          "Keyword Kw_vechno"         },
        {keyword_lexeme(Keyword_kind::Kw_realizuj),        "Keyword Kw_realizuj"       },
        {keyword_lexeme(Keyword_kind::Kw_kvat32),          "Keyword Kw_kvat32"         },
        {keyword_lexeme(Keyword_kind::Kw_tsel128),         "Keyword Kw_tsel128"        },
        {keyword_lexeme(Keyword_kind::Kw_inache),          "Keyword Kw_inache"         },
        {keyword_lexeme(Keyword_kind::Kw_ravno),           "Keyword Kw_ravno"          },
        {keyword_lexeme(Keyword_kind::Kw_tsel64),          "Keyword Kw_tsel64"         },
        {keyword_lexeme(Keyword_kind::Kw_ines),            "Keyword Kw_ines"           },
        {keyword_lexeme(Keyword_kind::Kw_prefiksnaja),     "Keyword Kw_prefiksnaja"    },
        {keyword_lexeme(Keyword_kind::Kw_perechislenie),   "Keyword Kw_perechislenie"  },
        {keyword_lexeme(Keyword_kind::Kw_kvat),            "Keyword Kw_kvat"           },
        {keyword_lexeme(Keyword_kind::Kw_rassmatrivaj),    "Keyword Kw_rassmatrivaj"   },
        {keyword_lexeme(Keyword_kind::Kw_kvat128),         "Keyword Kw_kvat128"        },
        {keyword_lexeme(Keyword_kind::Kw_prodolzhi),       "Keyword Kw_prodolzhi"      },
        {keyword_lexeme(Keyword_kind::Kw_kak),             "Keyword Kw_kak"            },
        {keyword_lexeme(Keyword_kind::Kw_tsel8),           "Keyword Kw_tsel8"          },
        {keyword_lexeme(Keyword_kind::Kw_kvat64),          "Keyword Kw_kvat64"         },
        {keyword_lexeme(Keyword_kind::Kw_porjadok),        "Keyword Kw_porjadok"       },
        {keyword_lexeme(Keyword_kind::Kw_tsel),            "Keyword Kw_tsel"           },
        {keyword_lexeme(Keyword_kind::Kw_log16),           "Keyword Kw_log16"          },
        {keyword_lexeme(Keyword_kind::Kw_stroka16),        "Keyword Kw_stroka16"       },
        {keyword_lexeme(Keyword_kind::Kw_kompl),           "Keyword Kw_kompl"          },
        {keyword_lexeme(Keyword_kind::Kw_porjadok32),      "Keyword Kw_porjadok32"     },
        {keyword_lexeme(Keyword_kind::Kw_stroka8),         "Keyword Kw_stroka8"        },
        {keyword_lexeme(Keyword_kind::Kw_pokuda),          "Keyword Kw_pokuda"         },
        {keyword_lexeme(Keyword_kind::Kw_kompl32),         "Keyword Kw_kompl32"        },
        {keyword_lexeme(Keyword_kind::Kw_lozh),            "Keyword Kw_lozh"           },
        {keyword_lexeme(Keyword_kind::Kw_porjadok8),       "Keyword Kw_porjadok8"      },
        {keyword_lexeme(Keyword_kind::Kw_log8),            "Keyword Kw_log8"           },
        {keyword_lexeme(Keyword_kind::Kw_simv32),          "Keyword Kw_simv32"         },
        {keyword_lexeme(Keyword_kind::Kw_kompl80),         "Keyword Kw_kompl80"        },
        {keyword_lexeme(Keyword_kind::Kw_log32),           "Keyword Kw_log32"          },
        {keyword_lexeme(Keyword_kind::Kw_kompl64),         "Keyword Kw_kompl64"        },
        {keyword_lexeme(Keyword_kind::Kw_sbros),           "Keyword Kw_sbros"          },
        {keyword_lexeme(Keyword_kind::Kw_log),             "Keyword Kw_log"            },
        {keyword_lexeme(Keyword_kind::Kw_sravni_s),        "Keyword Kw_sravni_s"       },
        {keyword_lexeme(Keyword_kind::Kw_nastrojka),       "Keyword Kw_nastrojka"      },
        {keyword_lexeme(Keyword_kind::Kw_konst),           "Keyword Kw_konst"          },
        {keyword_lexeme(Keyword_kind::Kw_simv),            "Keyword Kw_simv"           },
        {keyword_lexeme(Keyword_kind::Kw_modul),           "Keyword Kw_modul"          },
        {keyword_lexeme(Keyword_kind::Kw_pusto),           "Keyword Kw_pusto"          },
        {keyword_lexeme(Keyword_kind::Kw_operatsija),      "Keyword Kw_operatsija"     },
        {keyword_lexeme(Keyword_kind::Kw_poka),            "Keyword Kw_poka"           },
        {keyword_lexeme(Keyword_kind::Kw_psevdonim),       "Keyword Kw_psevdonim"      },
        {keyword_lexeme(Keyword_kind::Kw_osvobodi),        "Keyword Kw_osvobodi"       },
        {keyword_lexeme(Keyword_kind::Kw_perem),           "Keyword Kw_perem"          },
        {keyword_lexeme(Keyword_kind::Kw_povtorjaj),       "Keyword Kw_povtorjaj"      },
        {keyword_lexeme(Keyword_kind::Kw_pauk),            "Keyword Kw_pauk"           },
        {keyword_lexeme(Keyword_kind::Kw_abstraktnaja),    "Keyword Kw_abstraktnaja"   },
        {keyword_lexeme(Keyword_kind::Kw_obobshchjonnaja), "Keyword Kw_obobshchjonnaja"},
        {keyword_lexeme(Keyword_kind::Kw_tipy),            "Keyword Kw_tipy"           },
        {keyword_lexeme(Keyword_kind::Kw_konstanty),       "Keyword Kw_konstanty"      },
    };


    const Pair_for_test create_test_for_identifier(const std::u32string&                            id_str,
                                                   const std::shared_ptr<strings::trie::Char_trie>& trie)
    {
        Pair_for_test       result;
        scanner::LexemeInfo li;
        li.code_.kind_    = scanner::Lexem_kind::Id;
        li.code_.subkind_ = 0;
        li.id_index_      = trie->insert(id_str);
        result.first      = li;
        result.second     = u32string_to_utf8(U"Id " + id_str);
        return result;
    }

    const std::u32string identifiers[] = {
        U"крокодил_Гена128_и_Cheburashka", U"__cosinus"
    };

    std::vector<Pair_for_test> tests_for_identifiers;

    void create_identifiers_tests(const std::shared_ptr<strings::trie::Char_trie>& trie)
    {
        for(auto it = std::begin(identifiers); it != std::end(identifiers); ++it){
            tests_for_identifiers.push_back(create_test_for_identifier(*it, trie));
        }
    }

    //! Tests for delimiters.
    const Pair_for_test delimiterss_tests[] = {
        {delim_lexeme(Delimiter_kind::Logical_not),                 "Delimiter Logical_not"                },
        {delim_lexeme(Delimiter_kind::Logical_and_not),             "Delimiter Logical_and_not"            },
        {delim_lexeme(Delimiter_kind::Maybe_Type_add),              "Delimiter Maybe_Type_add"             },
        {delim_lexeme(Delimiter_kind::Range),                       "Delimiter Range"                      },
        {delim_lexeme(Delimiter_kind::Maybe_Logical_and_not),       "Delimiter Maybe_Logical_and_not"      },
        {delim_lexeme(Delimiter_kind::Logical_and_not_full_assign), "Delimiter Logical_and_not_full_assign"},
        {delim_lexeme(Delimiter_kind::Common_type),                 "Delimiter Common_type"                },
        {delim_lexeme(Delimiter_kind::Logical_and_not_full),        "Delimiter Logical_and_not_full"       },
        {delim_lexeme(Delimiter_kind::Maybe_Get_expr_type),         "Delimiter Maybe_Get_expr_type"        },
        {delim_lexeme(Delimiter_kind::Logical_or),                  "Delimiter Logical_or"                 },
        {delim_lexeme(Delimiter_kind::Logical_and_not_assign),      "Delimiter Logical_and_not_assign"     },
        {delim_lexeme(Delimiter_kind::Scope_resolution),            "Delimiter Scope_resolution"           },
        {delim_lexeme(Delimiter_kind::Power_assign),                "Delimiter Power_assign"               },
        {delim_lexeme(Delimiter_kind::Logical_or_not_full),         "Delimiter Logical_or_not_full"        },
        {delim_lexeme(Delimiter_kind::Type_add),                    "Delimiter Type_add"                   },
        {delim_lexeme(Delimiter_kind::Maybe_Logical_or_not),        "Delimiter Maybe_Logical_or_not"       },
        {delim_lexeme(Delimiter_kind::Set_difference),              "Delimiter Set_difference"             },
        {delim_lexeme(Delimiter_kind::Algebraic_separator),         "Delimiter Algebraic_separator"        },
        {delim_lexeme(Delimiter_kind::Logical_or_not),              "Delimiter Logical_or_not"             },
        {delim_lexeme(Delimiter_kind::Maybe_Pattern),               "Delimiter Maybe_Pattern"              },
        {delim_lexeme(Delimiter_kind::Logical_or_full_assign),      "Delimiter Logical_or_full_assign"     },
        {delim_lexeme(Delimiter_kind::Cyclic_right_shift),          "Delimiter Cyclic_right_shift"         },
        {delim_lexeme(Delimiter_kind::Data_size),                   "Delimiter Data_size"                  },
        {delim_lexeme(Delimiter_kind::NEQ),                         "Delimiter NEQ"                        },
        {delim_lexeme(Delimiter_kind::LT),                          "Delimiter LT"                         },
        {delim_lexeme(Delimiter_kind::Maybe_Get_elem_type),         "Delimiter Maybe_Get_elem_type"        },
        {delim_lexeme(Delimiter_kind::Assign),                      "Delimiter Assign"                     },
        {delim_lexeme(Delimiter_kind::Symmetric_difference),        "Delimiter Symmetric_difference"       },
        {delim_lexeme(Delimiter_kind::Logical_or_not_assign),       "Delimiter Logical_or_not_assign"      },
        {delim_lexeme(Delimiter_kind::Set_difference_assign),       "Delimiter Set_difference_assign"      },
        {delim_lexeme(Delimiter_kind::Pattern),                     "Delimiter Pattern"                    },
        {delim_lexeme(Delimiter_kind::Right_arrow),                 "Delimiter Right_arrow"                },
        {delim_lexeme(Delimiter_kind::Logical_or_not_full_assign),  "Delimiter Logical_or_not_full_assign" },
        {delim_lexeme(Delimiter_kind::Set_literal_begin),           "Delimiter Set_literal_begin"          },
        {delim_lexeme(Delimiter_kind::Bitwise_and_not_assign),      "Delimiter Bitwise_and_not_assign"     },
        {delim_lexeme(Delimiter_kind::Minus),                       "Delimiter Minus"                      },
        {delim_lexeme(Delimiter_kind::Remainder),                   "Delimiter Remainder"                  },
        {delim_lexeme(Delimiter_kind::Sharp),                       "Delimiter Sharp"                      },
        {delim_lexeme(Delimiter_kind::Maybe_Data_address),          "Delimiter Maybe_Data_address"         },
        {delim_lexeme(Delimiter_kind::Symmetric_difference_assign), "Delimiter Symmetric_difference_assign"},
        {delim_lexeme(Delimiter_kind::Array_literal_end),           "Delimiter Array_literal_end"          },
        {delim_lexeme(Delimiter_kind::Bitwise_and),                 "Delimiter Bitwise_and"                },
        {delim_lexeme(Delimiter_kind::Remainder_assign),            "Delimiter Remainder_assign"           },
        {delim_lexeme(Delimiter_kind::Maybe_Cardinality),           "Delimiter Maybe_Cardinality"          },
        {delim_lexeme(Delimiter_kind::Maybe_Common_template_type),  "Delimiter Maybe_Common_template_type" },
        {delim_lexeme(Delimiter_kind::Dot),                         "Delimiter Dot"                        },
        {delim_lexeme(Delimiter_kind::Float_remainder),             "Delimiter Float_remainder"            },
        {delim_lexeme(Delimiter_kind::Bitwise_or_not),              "Delimiter Bitwise_or_not"             },
        {delim_lexeme(Delimiter_kind::Logical_or_assign),           "Delimiter Logical_or_assign"          },
        {delim_lexeme(Delimiter_kind::Cyclic_right_shift_assign),   "Delimiter Cyclic_right_shift_assign"  },
        {delim_lexeme(Delimiter_kind::Common_template_type),        "Delimiter Common_template_type"       },
        {delim_lexeme(Delimiter_kind::Figure_bracket_closed),       "Delimiter Figure_bracket_closed"      },
        {delim_lexeme(Delimiter_kind::Float_remainder_assign),      "Delimiter Float_remainder_assign"     },
        {delim_lexeme(Delimiter_kind::Bitwise_and_not),             "Delimiter Bitwise_and_not"            },
        {delim_lexeme(Delimiter_kind::Algebraic_separator_assign),  "Delimiter Algebraic_separator_assign" },
        {delim_lexeme(Delimiter_kind::Logical_and),                 "Delimiter Logical_and"                },
        {delim_lexeme(Delimiter_kind::Bitwise_not),                 "Delimiter Bitwise_not"                },
        {delim_lexeme(Delimiter_kind::Bitwise_or_assign),           "Delimiter Bitwise_or_assign"          },
        {delim_lexeme(Delimiter_kind::Mul),                         "Delimiter Mul"                        },
        {delim_lexeme(Delimiter_kind::Maybe_Algebraic_separator),   "Delimiter Maybe_Algebraic_separator"  },
        {delim_lexeme(Delimiter_kind::Logical_xor),                 "Delimiter Logical_xor"                },
        {delim_lexeme(Delimiter_kind::Data_address),                "Delimiter Data_address"               },
        {delim_lexeme(Delimiter_kind::Div_assign),                  "Delimiter Div_assign"                 },
        {delim_lexeme(Delimiter_kind::At),                          "Delimiter At"                         },
        {delim_lexeme(Delimiter_kind::Mul_assign),                  "Delimiter Mul_assign"                 },
        {delim_lexeme(Delimiter_kind::Cardinality),                 "Delimiter Cardinality"                },
        {delim_lexeme(Delimiter_kind::Left_shift),                  "Delimiter Left_shift"                 },
        {delim_lexeme(Delimiter_kind::Bitwise_or_not_assign),       "Delimiter Bitwise_or_not_assign"      },
        {delim_lexeme(Delimiter_kind::Round_bracket_closed),        "Delimiter Round_bracket_closed"       },
        {delim_lexeme(Delimiter_kind::Logical_and_full_assign),     "Delimiter Logical_and_full_assign"    },
        {delim_lexeme(Delimiter_kind::Comment_end),                 "Delimiter Comment_end"                },
        {delim_lexeme(Delimiter_kind::Tuple_begin),                 "Delimiter Tuple_begin"                },
        {delim_lexeme(Delimiter_kind::Logical_and_assign),          "Delimiter Logical_and_assign"         },
        {delim_lexeme(Delimiter_kind::GT),                          "Delimiter GT"                         },
        {delim_lexeme(Delimiter_kind::Logical_or_full),             "Delimiter Logical_or_full"            },
        {delim_lexeme(Delimiter_kind::Set_literal_end),             "Delimiter Set_literal_end"            },
        {delim_lexeme(Delimiter_kind::Float_power_assign),          "Delimiter Float_power_assign"         },
        {delim_lexeme(Delimiter_kind::Get_elem_type),               "Delimiter Get_elem_type"              },
        {delim_lexeme(Delimiter_kind::Bitwise_or),                  "Delimiter Bitwise_or"                 },
        {delim_lexeme(Delimiter_kind::Logical_and_full),            "Delimiter Logical_and_full"           },
        {delim_lexeme(Delimiter_kind::Tuple_end),                   "Delimiter Tuple_end"                  },
        {delim_lexeme(Delimiter_kind::Plus),                        "Delimiter Plus"                       },
        {delim_lexeme(Delimiter_kind::Cyclic_left_shift_assign),    "Delimiter Cyclic_left_shift_assign"   },
        {delim_lexeme(Delimiter_kind::Bitwise_and_assign),          "Delimiter Bitwise_and_assign"         },
        {delim_lexeme(Delimiter_kind::Logical_xor_assign),          "Delimiter Logical_xor_assign"         },
        {delim_lexeme(Delimiter_kind::Float_power),                 "Delimiter Float_power"                },
        {delim_lexeme(Delimiter_kind::Bitwise_xor_assign),          "Delimiter Bitwise_xor_assign"         },
        {delim_lexeme(Delimiter_kind::Template_type),               "Delimiter Template_type"              },
        {delim_lexeme(Delimiter_kind::Comma),                       "Delimiter Comma"                      },
        {delim_lexeme(Delimiter_kind::Meta_bracket_opened),         "Delimiter Meta_bracket_opened"        },
        {delim_lexeme(Delimiter_kind::Round_bracket_opened),        "Delimiter Round_bracket_opened"       },
        {delim_lexeme(Delimiter_kind::Figure_bracket_opened),       "Delimiter Figure_bracket_opened"      },
        {delim_lexeme(Delimiter_kind::Left_arrow),                  "Delimiter Left_arrow"                 },
        {delim_lexeme(Delimiter_kind::Type_add_assign),             "Delimiter Type_add_assign"            },
        {delim_lexeme(Delimiter_kind::Minus_assign),                "Delimiter Minus_assign"               },
        {delim_lexeme(Delimiter_kind::Address),                     "Delimiter Address"                    },
        {delim_lexeme(Delimiter_kind::Plus_assign),                 "Delimiter Plus_assign"                },
        {delim_lexeme(Delimiter_kind::Cond_op_full),                "Delimiter Cond_op_full"               },
        {delim_lexeme(Delimiter_kind::Power),                       "Delimiter Power"                      },
        {delim_lexeme(Delimiter_kind::Inc),                         "Delimiter Inc"                        },
        {delim_lexeme(Delimiter_kind::Dec_with_wrap),               "Delimiter Dec_with_wrap"              },
        {delim_lexeme(Delimiter_kind::Range_excluded_end),          "Delimiter Range_excluded_end"         },
        {delim_lexeme(Delimiter_kind::Array_literal_begin),         "Delimiter Array_literal_begin"        },
        {delim_lexeme(Delimiter_kind::LEQ),                         "Delimiter LEQ"                        },
        {delim_lexeme(Delimiter_kind::Inc_with_wrap),               "Delimiter Inc_with_wrap"              },
        {delim_lexeme(Delimiter_kind::Bitwise_xor),                 "Delimiter Bitwise_xor"                },
        {delim_lexeme(Delimiter_kind::Comment_begin),               "Delimiter Comment_begin"              },
        {delim_lexeme(Delimiter_kind::Meta_bracket_closed),         "Delimiter Meta_bracket_closed"        },
        {delim_lexeme(Delimiter_kind::Div),                         "Delimiter Div"                        },
        {delim_lexeme(Delimiter_kind::Right_shift_assign),          "Delimiter Right_shift_assign"         },
        {delim_lexeme(Delimiter_kind::Left_shift_assign),           "Delimiter Left_shift_assign"          },
        {delim_lexeme(Delimiter_kind::Square_bracket_closed),       "Delimiter Square_bracket_closed"      },
        {delim_lexeme(Delimiter_kind::Colon),                       "Delimiter Colon"                      },
        {delim_lexeme(Delimiter_kind::Copy),                        "Delimiter Copy"                       },
        {delim_lexeme(Delimiter_kind::EQ),                          "Delimiter EQ"                         },
        {delim_lexeme(Delimiter_kind::Right_shift),                 "Delimiter Right_shift"                },
        {delim_lexeme(Delimiter_kind::Label_prefix),                "Delimiter Label_prefix"               },
        {delim_lexeme(Delimiter_kind::Square_bracket_opened),       "Delimiter Square_bracket_opened"      },
        {delim_lexeme(Delimiter_kind::Semicolon),                   "Delimiter Semicolon"                  },
        {delim_lexeme(Delimiter_kind::Get_expr_type),               "Delimiter Get_expr_type"              },
        {delim_lexeme(Delimiter_kind::Cond_op),                     "Delimiter Cond_op"                    },
        {delim_lexeme(Delimiter_kind::GEQ),                         "Delimiter GEQ"                        },
        {delim_lexeme(Delimiter_kind::Dec),                         "Delimiter Dec"                        },
        {delim_lexeme(Delimiter_kind::Cyclic_left_shift),           "Delimiter Cyclic_left_shift"          },
        {delim_lexeme(Delimiter_kind::Bitscale_literal_begin),      "Delimiter Bitscale_literal_begin"     },
        {delim_lexeme(Delimiter_kind::Bitscale_literal_end),        "Delimiter Bitscale_literal_end"       },
        {delim_lexeme(Delimiter_kind::Minimal_value_in_collection), "Delimiter Minimal_value_in_collection"},
        {delim_lexeme(Delimiter_kind::Maximal_value_in_collection), "Delimiter Maximal_value_in_collection"},
        {delim_lexeme(Delimiter_kind::Minimal_value_of_type),       "Delimiter Minimal_value_of_type"      },
        {delim_lexeme(Delimiter_kind::Maximal_value_of_type),       "Delimiter Maximal_value_of_type"      },
    };
};

namespace testing_scanner{
    void test_lexeme_to_string()
    {
        auto ids_trie         = std::make_shared<strings::trie::Char_trie>();
        auto strs_trie        = std::make_shared<strings::trie::Char_trie>();
        auto to_string_lambda = [&ids_trie, &strs_trie](const scanner::LexemeInfo& li){
            return scanner::to_string(li, ids_trie, strs_trie);
        };
        puts("Testing of converting of a lexeme into the string...\n\n");

        puts("Running tests for special lexemes (UnknownLexem and None)...");
        testing::test(std::begin(special_lexemes_tests),
                      std::end(special_lexemes_tests),
                      to_string_lambda);

        puts("Running tests for keywords...");
        testing::test(std::begin(keywords_tests),
                      std::end(keywords_tests),
                      to_string_lambda);

        create_identifiers_tests(ids_trie);
        puts("Running tests for identifiers...");
        testing::test(std::begin(tests_for_identifiers),
                      std::end(tests_for_identifiers),
                      to_string_lambda);

        puts("Running tests for delimiters...");
        testing::test(std::begin(delimiterss_tests),
                      std::end(delimiterss_tests),
                      to_string_lambda);
    }
};