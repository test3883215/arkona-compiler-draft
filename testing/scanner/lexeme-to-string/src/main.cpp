/*
    File:    main.cpp
    Created: 10 May 2023 at 21:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/test_lexeme_to_string.hpp"

int main()
{
    testing_scanner::test_lexeme_to_string();
}