/*
    File:    test_lexeme_to_string.hpp
    Created: 10 May 2023 at 21:36 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

namespace testing_scanner{
    void test_lexeme_to_string();
}