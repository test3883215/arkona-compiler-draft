/*
    File:    test_lexeme_recognition.cpp
    Created: 22 May 2023 at 21:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstddef>
#include <cstdio>
#include <iterator>
#include <memory>
#include <string>
#include <vector>
#include "../include/test_lexeme_recognition.hpp"
#include "../../../../iscanner/include/error_count.hpp"
#include "../../../../iscanner/include/errors_and_tries.hpp"
#include "../../../../iscanner/include/location.hpp"
#include "../../../../iscanner/include/position.hpp"
#include "../../../../iscanner/include/warning_count.hpp"
#include "../../../../numbers/include/digit_to_int.hpp"
#include "../../../../numbers/include/is_digit.hpp"
#include "../../../../scanner/include/scanner.hpp"
#include "../../../../strings-utils/include/char_trie.hpp"
#include "../../../utils/include/lexeme_fab.hpp"
#include "../../../utils/include/testing.hpp"

namespace{
    using namespace testing_scanner;

    unsigned __int128 str_to_int(const char32_t* str, unsigned base = 10)
    {
        unsigned __int128 result = 0;
        char32_t          c;
        while((c = *str++)){
            if(is_digit(c)){
                result = result * base + digit_to_int(c);
            }
        }
        return result;
    }

    const unsigned __int128 int_vals[] = {
        str_to_int(UR"~(0123'45689)~"),
        str_to_int(UR"~(302'231'454'903'657'293'676'544)~"),
        str_to_int(UR"~(158'456'325'028'528'675'187'087'900'672)~"),
        str_to_int(UR"~(1'329'227'995'784'915'872'903'807'060'280'344'576)~"),
        str_to_int(UR"~(340'282'366'920'938'463'463'374'607'431'768'211'455)~"),
        str_to_int(UR"~(340282366920938463463374607431768211455)~"),
        str_to_int(UR"~(0xffff'ffff'ffffff'ffffffffffffff'ffff)~", 16),
        str_to_int(UR"~(0x1238912345)~", 16),
        str_to_int(UR"~(0o123'755'5551'232'4561'2345)~", 8),
        str_to_int(UR"~(0o123755555123245612345)~", 8),
        str_to_int(UR"~(0xdeadbeafde123456789abcde)~", 16),
        str_to_int(UR"~(1111111110000111)~", 2),
        str_to_int(UR"~(1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101)~", 2),
        str_to_int(UR"~(10101)~", 2),
    };

    const quat::quat_t<__float128> quat_vals[] = {
        {0.0q, 0.0q, 45760.91234567892345E-223q, 0.0q                      },
        {0.0q, 0.0q, 0.0q,                       45760.91234567892345E-223q},
        {0.0q, 0.0q, 0.618281828459045e+4q,      0.0q                      },
        {0.0q, 0.0q, 0.0q,                       6.141592653148e+8q        },
        {0.0q, 0.0q, 45.67891234567892345E-12q,  0.0q                      },
    };

    const char32_t* test_texts[] = {
        // Text #0:
        UR"~(
    б              бег

            бор
                 бе
        бо

белый     безмолвный                  безмятежный
        бурый
                болото

      байт)~",

        // Text #1:
        UR"~(безликий            беззнаковый
        беззн1024           больной_кот

            большеголовый

                    беззн16             беззн8
    беззн32     беззн64         беззн128

            большое

                        беззн
        больше

  __лунатики_прилетели _негр_литературный)~",

        // Text #2:
        UR"~(веский_довод

                в
        выйди         вон        выбери

                выбери_меня         выдели_цветом

    возврат        выдели

            вещественных_чисел_поле

                                        вещ    вечно

        вечный_кипр   вещ80       вещ128     вещ32     вещ64    вещ256)~" ,

        // Text #3:
        UR"~(веский_довод

                в
        выйди         вон        выбери

                выбери_меня         выдели_цветом

    возврат        выдели


главный головная       грач

            вещественных_чисел_поле

            для     длительный         деловая_колбаса

                                        вещ    вечно

                         если          есть          ел_ем_и_буду_есть

        вечный_кипр   вещ80       вещ128     вещ32     вещ64    вещ256


из    инес иначе


            конст           как            компл

    кват       компл64            кват32          компл80

        кват64            компл128         кват80             компл32

                кват128

  категория

                комплексное_значение     кватернион          колонист

        исторический        использует

                исконный       истина           используемый     истинный

            иностранный

                 диапазон

    логарифм       лог         ложь         лживый     логический

        лягушка       лямбда        лог64     линейный

            лог8              ля           лог32          лог16)~",

        // Text #4:
        UR"~(

        маляр       маленькое        массив

                мандрагора      маска


    мета             мечта             метафора

            меньше          меньшинство            модуль



        магический

                    ничто     нечто     начальный        новый




            операция       оператор         освободи       крылатый


 паук   опёнок  поучайте_лучше_ваших_паучат         пакет

            пакетный_менеджер                 повторяй        повторение

  пока        позволить          показывать     покуда          порядок
    порядковое_числительное  порядок64   порядок8   порядок16     порядок32

        постфиксная      преобразуй
                префиксная              настройка

                            сброс
                                                     обобщение
                                    пакетная

            псевдоним                          пусто         пустота   вакуум

                мета)~",

        // Text #5:
        UR"~(
    равенство    равно     разность

        разбор    рассматривай   рассказ

реализуй            реализация      концепция_концепции_категории


        сослать        ссылочный_тип    ссылка

                самовар    само        структура


    строка32       строка8               строка           строка16

                сравнение     сравни_с              символьные_данные

        симв         симв32              симв8

                симв16

                  продолжи


то       тип  функция          трогательный     типовой

    целостный             целый  цел

    феррит         флот     фумигатор


            цел128                         цел8
                    цел16      цел64             цел32   честно      чистая

    шкала      шкалу                        область_целостности


            элементарный      элемент           экспортный_товар        экспорт
                экспериментальный        эквив       эквипотенциальный
                     перечисление  перечисл_множ

                      перем
                                абстрактная

                обобщённая

                                    типы                    константы)~",

        // Text #6:
        UR"~(''''

        'Ϣ'   'ϣ'            'Ϥ' 'ϥ'

    'Ϧ'  'ϧ'   'Ϩ'     'ϩ'    'Ϫ'    'ϫ'


            'Ϭ'  'ϭ'  'Ϯ'    'ϯ'   'ϰ'  'ϱ'   'ϲ'   'ϳ'   'ϴ'   'ϵ'  '϶'


                'Ϸ'   'ϸ'  'Ϲ'  'Ϻ'  'ϻ'  'ϼ'  'Ͻ'  'Ͼ'  'Ͽ'  'Ѐ'  'Ё'     'Ђ'   'Ѓ'   'Є'   'Ѕ'

  'І'   'Ї'   'Ј'  'Љ'   'Њ'   'Ћ'   'Ќ'  'Ѝ'  'Ў'  'Џ'  'А'  'Б'  'В'  'Г'   'Д'    'Е'     'Ж'     'З'    'И'

            'Й'   'К'       'Л'    'М'       'Н'


                       'О'      'џ'   'Ѡ'          'ѡ'     'Ѣ'           'ѣ'

          'Ѥ'               'ѥ'            'Ѧ'           'ѧ'      'Ѩ'          'ѩ'

      'Ѫ'         'ѫ'                'Ѭ'             'ѭ'       'Ѯ'      'ѯ'

            'Ѱ'        'ѱ'    'Ѳ'        'ѳ'    'Ѵ'       'ѵ'       'Ѷ'   '"')~",

        // Text #7:
        UR"~(    $123
          $0x0401    $0x41

            $0o103

               $0b01000010)~",

        // Text #8:
        UR"~(  $123

$1025
      $10'25
            $0x0'46'C
                $0x4d'e   $0b0001'1010'0010

                    $0o1746  $0o1'74'5)~",

        // Text #9:
        UR"~(

              "Bittida en morgon innan solen upprann
Innan foglarna började sjunga
Bergatroliet friade till fager ungersven
Hon hade en falskeliger tunga "

            "Тебе, девка, житье у меня будет лёгкое, --- не столько работать, сколько отдыхать будешь!
Утром станешь, ну, как подобат, --- до свету. Избу вымоешь, двор уберешь, коров подоишь, на поскотину выпустишь, в хлеву приберешься и --- спи-отдыхай!
Завтрак состряпаешь, самовар согреешь, нас с матушкой завтраком накормишь --- спи-отдыхай!
В поле поработашь, али в огороде пополешь, коли зимой --- за дровами али за сеном съездишь и --- спи-отдыхай!
Обед сваришь, пирогов напечешь: мы с матушкой обедать сядем, а ты --- спи-отдыхай!
После обеда посуду вымоешь, избу приберешь и --- спи-отдыхай!
Коли время подходяче --- в лес по ягоду, по грибы сходишь, али матушка в город спосылат, дак сбегашь. До городу --- рукой подать, и восьми верст не будет, а потом --- спи-отдыхай!
Из городу прибежишь, самовар поставишь. Мы с матушкой чай станем пить, а ты --- спи-отды­хай!
Вечером коров встретишь, подоишь, попоишь, корм задашь и --- спи-отдыхай!
Ужену сваришь, мы с матушкой съедим, а ты --- спи-отдыхай!
Воды наносишь, дров наколешь --- это к завтрему, и --- спи-отдыхай!
Постели наладишь, нас с матушкой спать повалишь. А ты, девка, день-деньской проспишь-про­отдыхаешь --- во что ночь-то будешь спать?
Ночью попрядёшь, поткешь, повышивашь, пошьешь и опять --- спи-отдыхай!
Ну, под утро белье постирать, которо надо --- поштопашь да зашьешь и --- спи-отдыхай!
Да ведь, девка, не даром. Деньги платить буду. Кажной год по рублю! Сама подумай. Сто годов --- сто рублев. Богатейкой станешь! "


"""У попа была собака""")~",

        // Text #10:
        UR"~(
           0123'45689
                         302'231'454'903'657'293'676'544         158'456'325'028'528'675'187'087'900'672



       1'329'227'995'784'915'872'903'807'060'280'344'576
                  340'282'366'920'938'463'463'374'607'431'768'211'455

                  340282366920938463463374607431768211455



 0xffff'ffff'ffffff'ffffffffffffff'ffff

                0x1238912345                   0o123'755'5551'232'4561'2345       0o123755555123245612345



        0xdeadbeafde123456789abcde

0b1111111110000111

                           0b1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101

                           0b10101)~",

        // Text #11:
        UR"~(
          3.1415926535897932384626)~",

        // Text #12:
        UR"~(

            45678.912'34'567'8923'45E-12           3.45678e+35


        5141.592653e+8   6.141592653148e+8f


            3.141592653e-78q  9141.59265356e-78q  2.141592653e+11d


      71415926537.182818e-8d     6.141592653148e+8f


        4'5.678912'34'567'8923'45E-1'2                  45760.912'34'567'8923'45E-223i
                45760.912'34'567'8923'45E-223j    45760.912'34'567'8923'45E-223k


          0.618281828459045e-4x    0.618281828459045e+4x    0.618281828459045e+4xj
                6.141592653148e+8fk 4'5.678912'34'567'8923'45E-1'2qj 0.6)~",

        // Text #13:
        UR"~(;      !
            !&                 !=
    !&&

                !|          !||         #    ->
        !&&.        !||.
              ##               %
            !&&=            !||=
                       %.
                                    !&&.=        -=

          --<
                    !||.=

  %=                %.=

      --                        -


          &&.=                 &         (:     (
                  &&                &=
   &&.       &&=          )


:) : :: :=
    :>    :}     :]

   /*   /=  /\=  /\   /

,   ]

        */  **   *
                        **.   *=        **=
                **.=             +                     ++


      +=              ++<

               .          ..             .|         .|.


                 ==         =

  < <! <&        <+          <-           <: <!!
           <<         <=           <?                    <|


      <!> <&&  <&>     <+>     <&&>          <+>=

         <:>               <<=           <?? <?>   <??>

  \      [:     \=        [
@    ^^      ^=             ^^=    ^

       <!!>

     |  |#          |#|       |=
           ||   ||.     ||=        ||.=

  {..} {    {.  {..          >  >=   >>               >>=


         ~             ~|         ~&=           ~&  ~|=

    }               ...     ..

      >>>=      {:                     >>>                                  %=


        <<<                    .|.=        <<<=           ?.      ?


                :|                                   |:

 #>                               <#    <<#                  #>>)~",
    };

    const std::vector<scanner::Arkona_token> tokens_for_texts[] = {
        {
            // References for text #0:
            {{{2, 5  }, {2, 5  }}, id_lexeme(1)                                             }, // Id б
            {{{2, 20 }, {2, 22 }}, id_lexeme(3)                                             }, // Id бег
            {{{4, 13 }, {4, 15 }}, id_lexeme(5)                                             }, // Id бор
            {{{5, 18 }, {5, 19 }}, id_lexeme(2)                                             }, // Id бе
            {{{6, 9  }, {6, 10 }}, id_lexeme(4)                                             }, // Id бо
            {{{8, 1  }, {8, 5  }}, id_lexeme(8)                                             }, // Id белый
            {{{8, 11 }, {8, 20 }}, id_lexeme(16)                                            }, // Id безмолвный
            {{{8, 39 }, {8, 49 }}, id_lexeme(23)                                            }, // Id безмятежный
            {{{9, 9  }, {9, 13 }}, id_lexeme(27)                                            }, // Id бурый
            {{{10,17 }, {10,22 }}, id_lexeme(31)                                            }, // Id болото
            {{{12,7  }, {12,10 }}, keyword_lexeme(Keyword_kind::Kw_bajt)                    }, // Keyword Kw_bajt
            {{{12,11 }, {12,11 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #1:
            {{{1, 1  }, {1, 8  }}, id_lexeme(36)                                            }, // Id безликий
            {{{1, 21 }, {1, 31 }}, id_lexeme(44)                                            }, // Id беззнаковый
            {{{2, 9  }, {2, 17 }}, id_lexeme(48)                                            }, // Id беззн1024
            {{{2, 29 }, {2, 39 }}, id_lexeme(56)                                            }, // Id больной_кот
            {{{4, 13 }, {4, 25 }}, id_lexeme(65)                                            }, // Id большеголовый
            {{{6, 21 }, {6, 27 }}, keyword_lexeme(Keyword_kind::Kw_bezzn16)                 }, // Keyword Kw_bezzn16
            {{{6, 41 }, {6, 46 }}, keyword_lexeme(Keyword_kind::Kw_bezzn8)                  }, // Keyword Kw_bezzn8
            {{{7, 5  }, {7, 11 }}, keyword_lexeme(Keyword_kind::Kw_bezzn32)                 }, // Keyword Kw_bezzn32
            {{{7, 17 }, {7, 23 }}, keyword_lexeme(Keyword_kind::Kw_bezzn64)                 }, // Keyword Kw_bezzn64
            {{{7, 33 }, {7, 40 }}, keyword_lexeme(Keyword_kind::Kw_bezzn128)                }, // Keyword Kw_bezzn128
            {{{9, 13 }, {9, 19 }}, keyword_lexeme(Keyword_kind::Kw_bolshoe)                 }, // Keyword Kw_bolshoe
            {{{11,25 }, {11,29 }}, keyword_lexeme(Keyword_kind::Kw_bezzn)                   }, // Keyword Kw_bezzn
            {{{12,9  }, {12,14 }}, keyword_lexeme(Keyword_kind::Kw_bolshe)                  }, // Keyword Kw_bolshe
            {{{14,3  }, {14,22 }}, id_lexeme(85)                                            }, // Id __лунатики_прилетели
            {{{14,24 }, {14,41 }}, id_lexeme(102)                                           }, // Id _негр_литературный
            {{{14,42 }, {14,42 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #2:
            {{{1, 1  }, {1, 12 }}, id_lexeme(114)                                           }, // Id веский_довод
            {{{3, 17 }, {3, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)                       }, // Keyword Kw_v
            {{{4, 9  }, {4, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)                   }, // Keyword Kw_vyjdi
            {{{4, 23 }, {4, 25 }}, id_lexeme(116)                                           }, // Id вон
            {{{4, 34 }, {4, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)                  }, // Keyword Kw_vyberi
            {{{6, 17 }, {6, 27 }}, id_lexeme(126)                                           }, // Id выбери_меня
            {{{6, 37 }, {6, 49 }}, id_lexeme(137)                                           }, // Id выдели_цветом
            {{{8, 5  }, {8, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{8, 20 }, {8, 25 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)                  }, // Keyword Kw_vydeli
            {{{10,13 }, {10,35 }}, id_lexeme(158)                                           }, // Id вещественных_чисел_поле
            {{{12,41 }, {12,43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)                  }, // Keyword Kw_veshch
            {{{12,48 }, {12,52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)                  }, // Keyword Kw_vechno
            {{{14,9  }, {14,19 }}, id_lexeme(167)                                           }, // Id вечный_кипр
            {{{14,23 }, {14,27 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)                }, // Keyword Kw_veshch80
            {{{14,35 }, {14,40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)               }, // Keyword Kw_veshch128
            {{{14,46 }, {14,50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)                }, // Keyword Kw_veshch32
            {{{14,56 }, {14,60 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)                }, // Keyword Kw_veshch64
            {{{14,65 }, {14,70 }}, id_lexeme(170)                                           }, // Id вещ256
            {{{14,71 }, {14,71 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #3:
            {{{1, 1  }, {1, 12 }}, id_lexeme(114)                                           }, // Id веский_довод
            {{{3, 17 }, {3, 17 }}, keyword_lexeme(Keyword_kind::Kw_v)                       }, // Keyword Kw_v
            {{{4, 9  }, {4, 13 }}, keyword_lexeme(Keyword_kind::Kw_vyjdi)                   }, // Keyword Kw_vyjdi
            {{{4, 23 }, {4, 25 }}, id_lexeme(116)                                           }, // Id вон
            {{{4, 34 }, {4, 39 }}, keyword_lexeme(Keyword_kind::Kw_vyberi)                  }, // Keyword Kw_vyberi
            {{{6, 17 }, {6, 27 }}, id_lexeme(126)                                           }, // Id выбери_меня
            {{{6, 37 }, {6, 49 }}, id_lexeme(137)                                           }, // Id выдели_цветом
            {{{8, 5  }, {8, 11 }}, keyword_lexeme(Keyword_kind::Kw_vozvrat)                 }, // Keyword Kw_vozvrat
            {{{8, 20 }, {8, 25 }}, keyword_lexeme(Keyword_kind::Kw_vydeli)                  }, // Keyword Kw_vydeli
            {{{11,1  }, {11,7  }}, id_lexeme(177)                                           }, // Id главный
            {{{11,9  }, {11,16 }}, keyword_lexeme(Keyword_kind::Kw_golovnaya)               }, // Keyword Kw_golovnaya
            {{{11,24 }, {11,27 }}, id_lexeme(180)                                           }, // Id грач
            {{{13,13 }, {13,35 }}, id_lexeme(158)                                           }, // Id вещественных_чисел_поле
            {{{15,13 }, {15,15 }}, keyword_lexeme(Keyword_kind::Kw_dlya)                    }, // Keyword Kw_dlya
            {{{15,21 }, {15,30 }}, id_lexeme(190)                                           }, // Id длительный
            {{{15,40 }, {15,54 }}, id_lexeme(204)                                           }, // Id деловая_колбаса
            {{{17,41 }, {17,43 }}, keyword_lexeme(Keyword_kind::Kw_veshch)                  }, // Keyword Kw_veshch
            {{{17,48 }, {17,52 }}, keyword_lexeme(Keyword_kind::Kw_vechno)                  }, // Keyword Kw_vechno
            {{{19,26 }, {19,29 }}, keyword_lexeme(Keyword_kind::Kw_esli)                    }, // Keyword Kw_esli
            {{{19,40 }, {19,43 }}, id_lexeme(208)                                           }, // Id есть
            {{{19,54 }, {19,70 }}, id_lexeme(224)                                           }, // Id ел_ем_и_буду_есть
            {{{21,9  }, {21,19 }}, id_lexeme(167)                                           }, // Id вечный_кипр
            {{{21,23 }, {21,27 }}, keyword_lexeme(Keyword_kind::Kw_veshch80)                }, // Keyword Kw_veshch80
            {{{21,35 }, {21,40 }}, keyword_lexeme(Keyword_kind::Kw_veshch128)               }, // Keyword Kw_veshch128
            {{{21,46 }, {21,50 }}, keyword_lexeme(Keyword_kind::Kw_veshch32)                }, // Keyword Kw_veshch32
            {{{21,56 }, {21,60 }}, keyword_lexeme(Keyword_kind::Kw_veshch64)                }, // Keyword Kw_veshch64
            {{{21,65 }, {21,70 }}, id_lexeme(170)                                           }, // Id вещ256
            {{{24,1  }, {24,2  }}, keyword_lexeme(Keyword_kind::Kw_iz)                      }, // Keyword Kw_iz
            {{{24,7  }, {24,10 }}, keyword_lexeme(Keyword_kind::Kw_ines)                    }, // Keyword Kw_ines
            {{{24,12 }, {24,16 }}, keyword_lexeme(Keyword_kind::Kw_inache)                  }, // Keyword Kw_inache
            {{{27,13 }, {27,17 }}, keyword_lexeme(Keyword_kind::Kw_konst)                   }, // Keyword Kw_konst
            {{{27,29 }, {27,31 }}, keyword_lexeme(Keyword_kind::Kw_kak)                     }, // Keyword Kw_kak
            {{{27,44 }, {27,48 }}, keyword_lexeme(Keyword_kind::Kw_kompl)                   }, // Keyword Kw_kompl
            {{{29,5  }, {29,8  }}, keyword_lexeme(Keyword_kind::Kw_kvat)                    }, // Keyword Kw_kvat
            {{{29,16 }, {29,22 }}, keyword_lexeme(Keyword_kind::Kw_kompl64)                 }, // Keyword Kw_kompl64
            {{{29,35 }, {29,40 }}, keyword_lexeme(Keyword_kind::Kw_kvat32)                  }, // Keyword Kw_kvat32
            {{{29,51 }, {29,57 }}, keyword_lexeme(Keyword_kind::Kw_kompl80)                 }, // Keyword Kw_kompl80
            {{{31,9  }, {31,14 }}, keyword_lexeme(Keyword_kind::Kw_kvat64)                  }, // Keyword Kw_kvat64
            {{{31,27 }, {31,34 }}, keyword_lexeme(Keyword_kind::Kw_kompl128)                }, // Keyword Kw_kompl128
            {{{31,44 }, {31,49 }}, keyword_lexeme(Keyword_kind::Kw_kvat80)                  }, // Keyword Kw_kvat80
            {{{31,63 }, {31,69 }}, keyword_lexeme(Keyword_kind::Kw_kompl32)                 }, // Keyword Kw_kompl32
            {{{33,17 }, {33,23 }}, keyword_lexeme(Keyword_kind::Kw_kvat128)                 }, // Keyword Kw_kvat128
            {{{35,3  }, {35,11 }}, keyword_lexeme(Keyword_kind::Kw_kategorija)              }, // Keyword Kw_kategorija
            {{{37,17 }, {37,36 }}, id_lexeme(244)                                           }, // Id комплексное_значение
            {{{37,42 }, {37,51 }}, id_lexeme(253)                                           }, // Id кватернион
            {{{37,62 }, {37,69 }}, id_lexeme(259)                                           }, // Id колонист
            {{{39,9  }, {39,20 }}, id_lexeme(271)                                           }, // Id исторический
            {{{39,29 }, {39,38 }}, keyword_lexeme(Keyword_kind::Kw_ispolzuet)               }, // Keyword Kw_ispolzuet
            {{{41,17 }, {41,24 }}, id_lexeme(277)                                           }, // Id исконный
            {{{41,32 }, {41,37 }}, keyword_lexeme(Keyword_kind::Kw_istina)                  }, // Keyword Kw_istina
            {{{41,49 }, {41,60 }}, id_lexeme(287)                                           }, // Id используемый
            {{{41,66 }, {41,73 }}, id_lexeme(292)                                           }, // Id истинный
            {{{43,13 }, {43,23 }}, id_lexeme(302)                                           }, // Id иностранный
            {{{45,18 }, {45,25 }}, keyword_lexeme(Keyword_kind::Kw_diapazon)                }, // Keyword Kw_diapazon
            {{{47,5  }, {47,12 }}, id_lexeme(310)                                           }, // Id логарифм
            {{{47,20 }, {47,22 }}, keyword_lexeme(Keyword_kind::Kw_log)                     }, // Keyword Kw_log
            {{{47,32 }, {47,35 }}, keyword_lexeme(Keyword_kind::Kw_lozh)                    }, // Keyword Kw_lozh
            {{{47,45 }, {47,50 }}, id_lexeme(315)                                           }, // Id лживый
            {{{47,56 }, {47,65 }}, id_lexeme(322)                                           }, // Id логический
            {{{49,9  }, {49,15 }}, id_lexeme(328)                                           }, // Id лягушка
            {{{49,23 }, {49,28 }}, keyword_lexeme(Keyword_kind::Kw_ljambda)                 }, // Keyword Kw_ljambda
            {{{49,37 }, {49,41 }}, keyword_lexeme(Keyword_kind::Kw_log64)                   }, // Keyword Kw_log64
            {{{49,47 }, {49,54 }}, id_lexeme(335)                                           }, // Id линейный
            {{{51,13 }, {51,16 }}, keyword_lexeme(Keyword_kind::Kw_log8)                    }, // Keyword Kw_log8
            {{{51,31 }, {51,32 }}, id_lexeme(323)                                           }, // Id ля
            {{{51,44 }, {51,48 }}, keyword_lexeme(Keyword_kind::Kw_log32)                   }, // Keyword Kw_log32
            {{{51,59 }, {51,63 }}, keyword_lexeme(Keyword_kind::Kw_log16)                   }, // Keyword Kw_log16
            {{{51,64 }, {51,64 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #4:
            {{{3, 9  }, {3, 13 }}, id_lexeme(340)                                           }, // Id маляр
            {{{3, 21 }, {3, 29 }}, keyword_lexeme(Keyword_kind::Kw_malenkoe)                }, // Keyword Kw_malenkoe
            {{{3, 38 }, {3, 43 }}, keyword_lexeme(Keyword_kind::Kw_massiv)                  }, // Keyword Kw_massiv
            {{{5, 17 }, {5, 26 }}, id_lexeme(348)                                           }, // Id мандрагора
            {{{5, 33 }, {5, 37 }}, id_lexeme(351)                                           }, // Id маска
            {{{8, 5  }, {8, 8  }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{8, 22 }, {8, 26 }}, id_lexeme(355)                                           }, // Id мечта
            {{{8, 40 }, {8, 47 }}, id_lexeme(361)                                           }, // Id метафора
            {{{10,13 }, {10,18 }}, keyword_lexeme(Keyword_kind::Kw_menshe)                  }, // Keyword Kw_menshe
            {{{10,29 }, {10,39 }}, id_lexeme(370)                                           }, // Id меньшинство
            {{{10,52 }, {10,57 }}, keyword_lexeme(Keyword_kind::Kw_modul)                   }, // Keyword Kw_modul
            {{{14,9  }, {14,18 }}, id_lexeme(378)                                           }, // Id магический
            {{{16,21 }, {16,25 }}, keyword_lexeme(Keyword_kind::Kw_nichto)                  }, // Keyword Kw_nichto
            {{{16,31 }, {16,35 }}, id_lexeme(383)                                           }, // Id нечто
            {{{16,41 }, {16,49 }}, id_lexeme(391)                                           }, // Id начальный
            {{{16,58 }, {16,62 }}, id_lexeme(395)                                           }, // Id новый
            {{{21,13 }, {21,20 }}, keyword_lexeme(Keyword_kind::Kw_operatsija)              }, // Keyword Kw_operatsija
            {{{21,28 }, {21,35 }}, id_lexeme(403)                                           }, // Id оператор
            {{{21,45 }, {21,52 }}, keyword_lexeme(Keyword_kind::Kw_osvobodi)                }, // Keyword Kw_osvobodi
            {{{21,60 }, {21,67 }}, id_lexeme(410)                                           }, // Id крылатый
            {{{24,2  }, {24,5  }}, keyword_lexeme(Keyword_kind::Kw_pauk)                    }, // Keyword Kw_pauk
            {{{24,9  }, {24,14 }}, id_lexeme(414)                                           }, // Id опёнок
            {{{24,17 }, {24,43 }}, id_lexeme(441)                                           }, // Id поучайте_лучше_ваших_паучат
            {{{24,53 }, {24,57 }}, keyword_lexeme(Keyword_kind::Kw_paket)                   }, // Keyword Kw_paket
            {{{26,13 }, {26,29 }}, id_lexeme(457)                                           }, // Id пакетный_менеджер
            {{{26,47 }, {26,54 }}, keyword_lexeme(Keyword_kind::Kw_povtorjaj)               }, // Keyword Kw_povtorjaj
            {{{26,63 }, {26,72 }}, id_lexeme(465)                                           }, // Id повторение
            {{{28,3  }, {28,6  }}, keyword_lexeme(Keyword_kind::Kw_poka)                    }, // Keyword Kw_poka
            {{{28,15 }, {28,23 }}, id_lexeme(472)                                           }, // Id позволить
            {{{28,34 }, {28,43 }}, id_lexeme(480)                                           }, // Id показывать
            {{{28,49 }, {28,54 }}, keyword_lexeme(Keyword_kind::Kw_pokuda)                  }, // Keyword Kw_pokuda
            {{{28,65 }, {28,71 }}, keyword_lexeme(Keyword_kind::Kw_porjadok)                }, // Keyword Kw_porjadok
            {{{29,5  }, {29,27 }}, id_lexeme(501)                                           }, // Id порядковое_числительное
            {{{29,30 }, {29,38 }}, keyword_lexeme(Keyword_kind::Kw_porjadok64)              }, // Keyword Kw_porjadok64
            {{{29,42 }, {29,49 }}, keyword_lexeme(Keyword_kind::Kw_porjadok8)               }, // Keyword Kw_porjadok8
            {{{29,53 }, {29,61 }}, keyword_lexeme(Keyword_kind::Kw_porjadok16)              }, // Keyword Kw_porjadok16
            {{{29,67 }, {29,75 }}, keyword_lexeme(Keyword_kind::Kw_porjadok32)              }, // Keyword Kw_porjadok32
            {{{31,9  }, {31,19 }}, keyword_lexeme(Keyword_kind::Kw_postfiksnaja)            }, // Keyword Kw_postfiksnaja
            {{{31,26 }, {31,35 }}, keyword_lexeme(Keyword_kind::Kw_preobrazuj)              }, // Keyword Kw_preobrazuj
            {{{32,17 }, {32,26 }}, keyword_lexeme(Keyword_kind::Kw_prefiksnaja)             }, // Keyword Kw_prefiksnaja
            {{{32,41 }, {32,49 }}, keyword_lexeme(Keyword_kind::Kw_nastrojka)               }, // Keyword Kw_nastrojka
            {{{34,29 }, {34,33 }}, keyword_lexeme(Keyword_kind::Kw_sbros)                   }, // Keyword Kw_sbros
            {{{35,54 }, {35,62 }}, keyword_lexeme(Keyword_kind::Kw_obobshchenie)            }, // Keyword Kw_obobshchenie
            {{{36,37 }, {36,44 }}, keyword_lexeme(Keyword_kind::Kw_paketnaja)               }, // Keyword Kw_paketnaja
            {{{38,13 }, {38,21 }}, keyword_lexeme(Keyword_kind::Kw_psevdonim)               }, // Keyword Kw_psevdonim
            {{{38,48 }, {38,52 }}, keyword_lexeme(Keyword_kind::Kw_pusto)                   }, // Keyword Kw_pusto
            {{{38,62 }, {38,68 }}, id_lexeme(507)                                           }, // Id пустота
            {{{38,72 }, {38,77 }}, id_lexeme(512)                                           }, // Id вакуум
            {{{40,17 }, {40,20 }}, keyword_lexeme(Keyword_kind::Kw_meta)                    }, // Keyword Kw_meta
            {{{40,21 }, {40,21 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #5:
            {{{2, 5  }, {2, 13 }}, id_lexeme(521)                                           }, // Id равенство
            {{{2, 18 }, {2, 22 }}, keyword_lexeme(Keyword_kind::Kw_ravno)                   }, // Keyword Kw_ravno
            {{{2, 28 }, {2, 35 }}, id_lexeme(527)                                           }, // Id разность
            {{{4, 9  }, {4, 14 }}, keyword_lexeme(Keyword_kind::Kw_razbor)                  }, // Keyword Kw_razbor
            {{{4, 19 }, {4, 30 }}, keyword_lexeme(Keyword_kind::Kw_rassmatrivaj)            }, // Keyword Kw_rassmatrivaj
            {{{4, 34 }, {4, 40 }}, id_lexeme(532)                                           }, // Id рассказ
            {{{6, 1  }, {6, 8  }}, keyword_lexeme(Keyword_kind::Kw_realizuj)                }, // Keyword Kw_realizuj
            {{{6, 21 }, {6, 30 }}, keyword_lexeme(Keyword_kind::Kw_realizatsija)            }, // Keyword Kw_realizatsija
            {{{6, 37 }, {6, 65 }}, id_lexeme(559)                                           }, // Id концепция_концепции_категории
            {{{9, 9  }, {9, 15 }}, id_lexeme(566)                                           }, // Id сослать
            {{{9, 24 }, {9, 36 }}, id_lexeme(578)                                           }, // Id ссылочный_тип
            {{{9, 41 }, {9, 46 }}, keyword_lexeme(Keyword_kind::Kw_ssylka)                  }, // Keyword Kw_ssylka
            {{{11,17 }, {11,23 }}, id_lexeme(584)                                           }, // Id самовар
            {{{11,28 }, {11,31 }}, keyword_lexeme(Keyword_kind::Kw_samo)                    }, // Keyword Kw_samo
            {{{11,40 }, {11,48 }}, keyword_lexeme(Keyword_kind::Kw_struktura)               }, // Keyword Kw_struktura
            {{{14,5  }, {14,12 }}, keyword_lexeme(Keyword_kind::Kw_stroka32)                }, // Keyword Kw_stroka32
            {{{14,20 }, {14,26 }}, keyword_lexeme(Keyword_kind::Kw_stroka8)                 }, // Keyword Kw_stroka8
            {{{14,42 }, {14,47 }}, keyword_lexeme(Keyword_kind::Kw_stroka)                  }, // Keyword Kw_stroka
            {{{14,59 }, {14,66 }}, keyword_lexeme(Keyword_kind::Kw_stroka16)                }, // Keyword Kw_stroka16
            {{{16,17 }, {16,25 }}, id_lexeme(592)                                           }, // Id сравнение
            {{{16,31 }, {16,38 }}, keyword_lexeme(Keyword_kind::Kw_sravni_s)                }, // Keyword Kw_sravni_s
            {{{16,53 }, {16,69 }}, id_lexeme(608)                                           }, // Id символьные_данные
            {{{18,9  }, {18,12 }}, keyword_lexeme(Keyword_kind::Kw_simv)                    }, // Keyword Kw_simv
            {{{18,22 }, {18,27 }}, keyword_lexeme(Keyword_kind::Kw_simv32)                  }, // Keyword Kw_simv32
            {{{18,42 }, {18,46 }}, keyword_lexeme(Keyword_kind::Kw_simv8)                   }, // Keyword Kw_simv8
            {{{20,17 }, {20,22 }}, keyword_lexeme(Keyword_kind::Kw_simv16)                  }, // Keyword Kw_simv16
            {{{22,19 }, {22,26 }}, keyword_lexeme(Keyword_kind::Kw_prodolzhi)               }, // Keyword Kw_prodolzhi
            {{{25,1  }, {25,2  }}, keyword_lexeme(Keyword_kind::Kw_to)                      }, // Keyword Kw_to
            {{{25,10 }, {25,12 }}, keyword_lexeme(Keyword_kind::Kw_tip)                     }, // Keyword Kw_tip
            {{{25,15 }, {25,21 }}, keyword_lexeme(Keyword_kind::Kw_funktsija)               }, // Keyword Kw_funktsija
            {{{25,32 }, {25,43 }}, id_lexeme(620)                                           }, // Id трогательный
            {{{25,49 }, {25,55 }}, id_lexeme(626)                                           }, // Id типовой
            {{{27,5  }, {27,13 }}, id_lexeme(635)                                           }, // Id целостный
            {{{27,27 }, {27,31 }}, id_lexeme(637)                                           }, // Id целый
            {{{27,34 }, {27,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel)                    }, // Keyword Kw_tsel
            {{{29,5  }, {29,10 }}, id_lexeme(643)                                           }, // Id феррит
            {{{29,20 }, {29,23 }}, id_lexeme(646)                                           }, // Id флот
            {{{29,29 }, {29,37 }}, id_lexeme(654)                                           }, // Id фумигатор
            {{{32,13 }, {32,18 }}, keyword_lexeme(Keyword_kind::Kw_tsel128)                 }, // Keyword Kw_tsel128
            {{{32,44 }, {32,47 }}, keyword_lexeme(Keyword_kind::Kw_tsel8)                   }, // Keyword Kw_tsel8
            {{{33,21 }, {33,25 }}, keyword_lexeme(Keyword_kind::Kw_tsel16)                  }, // Keyword Kw_tsel16
            {{{33,32 }, {33,36 }}, keyword_lexeme(Keyword_kind::Kw_tsel64)                  }, // Keyword Kw_tsel64
            {{{33,50 }, {33,54 }}, keyword_lexeme(Keyword_kind::Kw_tsel32)                  }, // Keyword Kw_tsel32
            {{{33,58 }, {33,63 }}, id_lexeme(660)                                           }, // Id честно
            {{{33,70 }, {33,75 }}, keyword_lexeme(Keyword_kind::Kw_chistaja)                }, // Keyword Kw_chistaja
            {{{35,5  }, {35,9  }}, keyword_lexeme(Keyword_kind::Kw_shkala)                  }, // Keyword Kw_shkala
            {{{35,16 }, {35,20 }}, keyword_lexeme(Keyword_kind::Kw_shkalu)                  }, // Keyword Kw_shkalu
            {{{35,45 }, {35,63 }}, id_lexeme(678)                                           }, // Id область_целостности
            {{{38,13 }, {38,24 }}, id_lexeme(690)                                           }, // Id элементарный
            {{{38,31 }, {38,37 }}, keyword_lexeme(Keyword_kind::Kw_element)                 }, // Keyword Kw_element
            {{{38,49 }, {38,64 }}, id_lexeme(705)                                           }, // Id экспортный_товар
            {{{38,73 }, {38,79 }}, keyword_lexeme(Keyword_kind::Kw_eksport)                 }, // Keyword Kw_eksport
            {{{39,17 }, {39,33 }}, id_lexeme(718)                                           }, // Id экспериментальный
            {{{39,42 }, {39,46 }}, keyword_lexeme(Keyword_kind::Kw_ekviv)                   }, // Keyword Kw_ekviv
            {{{39,54 }, {39,70 }}, id_lexeme(733)                                           }, // Id эквипотенциальный
            {{{40,22 }, {40,33 }}, keyword_lexeme(Keyword_kind::Kw_perechislenie)           }, // Keyword Kw_perechislenie
            {{{40,36 }, {40,48 }}, keyword_lexeme(Keyword_kind::Kw_perechisl_mnozh)         }, // Keyword Kw_perechisl_mnozh
            {{{42,23 }, {42,27 }}, keyword_lexeme(Keyword_kind::Kw_perem)                   }, // Keyword Kw_perem
            {{{43,33 }, {43,43 }}, keyword_lexeme(Keyword_kind::Kw_abstraktnaja)            }, // Keyword Kw_abstraktnaja
            {{{45,17 }, {45,26 }}, keyword_lexeme(Keyword_kind::Kw_obobshchjonnaja)         }, // Keyword Kw_obobshchjonnaja
            {{{47,37 }, {47,40 }}, keyword_lexeme(Keyword_kind::Kw_tipy)                    }, // Keyword Kw_tipy
            {{{47,61 }, {47,69 }}, keyword_lexeme(Keyword_kind::Kw_konstanty)               }, // Keyword Kw_konstanty
            {{{47,70 }, {47,70 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #6:
            {{{1, 1  }, {1, 4  }}, char_lexeme(U'\'')                                       }, // Char \'
            {{{3, 9  }, {3, 11 }}, char_lexeme(U'Ϣ')                                        }, // Char 'Ϣ'
            {{{3, 15 }, {3, 17 }}, char_lexeme(U'ϣ')                                        }, // Char 'ϣ'
            {{{3, 30 }, {3, 32 }}, char_lexeme(U'Ϥ')                                        }, // Char 'Ϥ'
            {{{3, 34 }, {3, 36 }}, char_lexeme(U'ϥ')                                        }, // Char 'ϥ'
            {{{5, 5  }, {5, 7  }}, char_lexeme(U'Ϧ')                                        }, // Char 'Ϧ'
            {{{5, 10 }, {5, 12 }}, char_lexeme(U'ϧ')                                        }, // Char 'ϧ'
            {{{5, 16 }, {5, 18 }}, char_lexeme(U'Ϩ')                                        }, // Char 'Ϩ'
            {{{5, 24 }, {5, 26 }}, char_lexeme(U'ϩ')                                        }, // Char 'ϩ'
            {{{5, 31 }, {5, 33 }}, char_lexeme(U'Ϫ')                                        }, // Char 'Ϫ'
            {{{5, 38 }, {5, 40 }}, char_lexeme(U'ϫ')                                        }, // Char 'ϫ'
            {{{8, 13 }, {8, 15 }}, char_lexeme(U'Ϭ')                                        }, // Char 'Ϭ'
            {{{8, 18 }, {8, 20 }}, char_lexeme(U'ϭ')                                        }, // Char 'ϭ'
            {{{8, 23 }, {8, 25 }}, char_lexeme(U'Ϯ')                                        }, // Char 'Ϯ'
            {{{8, 30 }, {8, 32 }}, char_lexeme(U'ϯ')                                        }, // Char 'ϯ'
            {{{8, 36 }, {8, 38 }}, char_lexeme(U'ϰ')                                        }, // Char 'ϰ'
            {{{8, 41 }, {8, 43 }}, char_lexeme(U'ϱ')                                        }, // Char 'ϱ'
            {{{8, 47 }, {8, 49 }}, char_lexeme(U'ϲ')                                        }, // Char 'ϲ'
            {{{8, 53 }, {8, 55 }}, char_lexeme(U'ϳ')                                        }, // Char 'ϳ'
            {{{8, 59 }, {8, 61 }}, char_lexeme(U'ϴ')                                        }, // Char 'ϴ'
            {{{8, 65 }, {8, 67 }}, char_lexeme(U'ϵ')                                        }, // Char 'ϵ'
            {{{8, 70 }, {8, 72 }}, char_lexeme(U'϶')                                        }, // Char '϶'
            {{{11,17 }, {11,19 }}, char_lexeme(U'Ϸ')                                        }, // Char 'Ϸ'
            {{{11,23 }, {11,25 }}, char_lexeme(U'ϸ')                                        }, // Char 'ϸ'
            {{{11,28 }, {11,30 }}, char_lexeme(U'Ϲ')                                        }, // Char 'Ϲ'
            {{{11,33 }, {11,35 }}, char_lexeme(U'Ϻ')                                        }, // Char 'Ϻ'
            {{{11,38 }, {11,40 }}, char_lexeme(U'ϻ')                                        }, // Char 'ϻ'
            {{{11,43 }, {11,45 }}, char_lexeme(U'ϼ')                                        }, // Char 'ϼ'
            {{{11,48 }, {11,50 }}, char_lexeme(U'Ͻ')                                        }, // Char 'Ͻ'
            {{{11,53 }, {11,55 }}, char_lexeme(U'Ͼ')                                        }, // Char 'Ͼ'
            {{{11,58 }, {11,60 }}, char_lexeme(U'Ͽ')                                        }, // Char 'Ͽ'
            {{{11,63 }, {11,65 }}, char_lexeme(U'Ѐ')                                        }, // Char 'Ѐ'
            {{{11,68 }, {11,70 }}, char_lexeme(U'Ё')                                        }, // Char 'Ё'
            {{{11,76 }, {11,78 }}, char_lexeme(U'Ђ')                                        }, // Char 'Ђ'
            {{{11,82 }, {11,84 }}, char_lexeme(U'Ѓ')                                        }, // Char 'Ѓ'
            {{{11,88 }, {11,90 }}, char_lexeme(U'Є')                                        }, // Char 'Є'
            {{{11,94 }, {11,96 }}, char_lexeme(U'Ѕ')                                        }, // Char 'Ѕ'
            {{{13,3  }, {13,5  }}, char_lexeme(U'І')                                        }, // Char 'І'
            {{{13,9  }, {13,11 }}, char_lexeme(U'Ї')                                        }, // Char 'Ї'
            {{{13,15 }, {13,17 }}, char_lexeme(U'Ј')                                        }, // Char 'Ј'
            {{{13,20 }, {13,22 }}, char_lexeme(U'Љ')                                        }, // Char 'Љ'
            {{{13,26 }, {13,28 }}, char_lexeme(U'Њ')                                        }, // Char 'Њ'
            {{{13,32 }, {13,34 }}, char_lexeme(U'Ћ')                                        }, // Char 'Ћ'
            {{{13,38 }, {13,40 }}, char_lexeme(U'Ќ')                                        }, // Char 'Ќ'
            {{{13,43 }, {13,45 }}, char_lexeme(U'Ѝ')                                        }, // Char 'Ѝ'
            {{{13,48 }, {13,50 }}, char_lexeme(U'Ў')                                        }, // Char 'Ў'
            {{{13,53 }, {13,55 }}, char_lexeme(U'Џ')                                        }, // Char 'Џ'
            {{{13,58 }, {13,60 }}, char_lexeme(U'А')                                        }, // Char 'А'
            {{{13,63 }, {13,65 }}, char_lexeme(U'Б')                                        }, // Char 'Б'
            {{{13,68 }, {13,70 }}, char_lexeme(U'В')                                        }, // Char 'В'
            {{{13,73 }, {13,75 }}, char_lexeme(U'Г')                                        }, // Char 'Г'
            {{{13,79 }, {13,81 }}, char_lexeme(U'Д')                                        }, // Char 'Д'
            {{{13,86 }, {13,88 }}, char_lexeme(U'Е')                                        }, // Char 'Е'
            {{{13,94 }, {13,96 }}, char_lexeme(U'Ж')                                        }, // Char 'Ж'
            {{{13,102}, {13,104}}, char_lexeme(U'З')                                        }, // Char 'З'
            {{{13,109}, {13,111}}, char_lexeme(U'И')                                        }, // Char 'И'
            {{{15,13 }, {15,15 }}, char_lexeme(U'Й')                                        }, // Char 'Й'
            {{{15,19 }, {15,21 }}, char_lexeme(U'К')                                        }, // Char 'К'
            {{{15,29 }, {15,31 }}, char_lexeme(U'Л')                                        }, // Char 'Л'
            {{{15,36 }, {15,38 }}, char_lexeme(U'М')                                        }, // Char 'М'
            {{{15,46 }, {15,48 }}, char_lexeme(U'Н')                                        }, // Char 'Н'
            {{{18,24 }, {18,26 }}, char_lexeme(U'О')                                        }, // Char 'О'
            {{{18,33 }, {18,35 }}, char_lexeme(U'џ')                                        }, // Char 'џ'
            {{{18,39 }, {18,41 }}, char_lexeme(U'Ѡ')                                        }, // Char 'Ѡ'
            {{{18,52 }, {18,54 }}, char_lexeme(U'ѡ')                                        }, // Char 'ѡ'
            {{{18,60 }, {18,62 }}, char_lexeme(U'Ѣ')                                        }, // Char 'Ѣ'
            {{{18,74 }, {18,76 }}, char_lexeme(U'ѣ')                                        }, // Char 'ѣ'
            {{{20,11 }, {20,13 }}, char_lexeme(U'Ѥ')                                        }, // Char 'Ѥ'
            {{{20,29 }, {20,31 }}, char_lexeme(U'ѥ')                                        }, // Char 'ѥ'
            {{{20,44 }, {20,46 }}, char_lexeme(U'Ѧ')                                        }, // Char 'Ѧ'
            {{{20,58 }, {20,60 }}, char_lexeme(U'ѧ')                                        }, // Char 'ѧ'
            {{{20,67 }, {20,69 }}, char_lexeme(U'Ѩ')                                        }, // Char 'Ѩ'
            {{{20,80 }, {20,82 }}, char_lexeme(U'ѩ')                                        }, // Char 'ѩ'
            {{{22,7  }, {22,9  }}, char_lexeme(U'Ѫ')                                        }, // Char 'Ѫ'
            {{{22,19 }, {22,21 }}, char_lexeme(U'ѫ')                                        }, // Char 'ѫ'
            {{{22,38 }, {22,40 }}, char_lexeme(U'Ѭ')                                        }, // Char 'Ѭ'
            {{{22,54 }, {22,56 }}, char_lexeme(U'ѭ')                                        }, // Char 'ѭ'
            {{{22,64 }, {22,66 }}, char_lexeme(U'Ѯ')                                        }, // Char 'Ѯ'
            {{{22,73 }, {22,75 }}, char_lexeme(U'ѯ')                                        }, // Char 'ѯ'
            {{{24,13 }, {24,15 }}, char_lexeme(U'Ѱ')                                        }, // Char 'Ѱ'
            {{{24,24 }, {24,26 }}, char_lexeme(U'ѱ')                                        }, // Char 'ѱ'
            {{{24,31 }, {24,33 }}, char_lexeme(U'Ѳ')                                        }, // Char 'Ѳ'
            {{{24,42 }, {24,44 }}, char_lexeme(U'ѳ')                                        }, // Char 'ѳ'
            {{{24,49 }, {24,51 }}, char_lexeme(U'Ѵ')                                        }, // Char 'Ѵ'
            {{{24,59 }, {24,61 }}, char_lexeme(U'ѵ')                                        }, // Char 'ѵ'
            {{{24,69 }, {24,71 }}, char_lexeme(U'Ѷ')                                        }, // Char 'Ѷ'
            {{{24,75 }, {24,77 }}, char_lexeme(U'"')                                        }, // Char '"'
            {{{24,78 }, {24,78 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #7:
            {{{1, 5  }, {1, 8  }}, encoded_char_lexeme(U'{')                                }, // Encoded_char '{'
            {{{2, 11 }, {2, 17 }}, encoded_char_lexeme(U'Ё')                                }, // Encoded_char 'Ё'
            {{{2, 22 }, {2, 26 }}, encoded_char_lexeme(U'A')                                }, // Encoded_char 'A' (Latin)
            {{{4, 13 }, {4, 18 }}, encoded_char_lexeme(U'C')                                }, // Encoded_char 'C' (Latin)
            {{{6, 16 }, {6, 26 }}, encoded_char_lexeme(U'B')                                }, // Encoded_char 'B' (Latin)
            {{{6, 27 }, {6, 27 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #8:
            {{{1, 3  }, {1, 6  }}, encoded_char_lexeme(U'{')                                }, // Encoded_char '{'
            {{{3, 1  }, {3, 5  }}, encoded_char_lexeme(U'Ё')                                }, // Encoded_char 'Ё'
            {{{4, 7  }, {4, 12 }}, encoded_char_lexeme(U'Ё')                                }, // Encoded_char 'Ё'
            {{{5, 13 }, {5, 21 }}, encoded_char_lexeme(U'Ѭ')                                }, // Encoded_char 'Ѭ'
            {{{6, 17 }, {6, 23 }}, encoded_char_lexeme(U'Ӟ')                                }, // Encoded_char 'Ӟ'
            {{{6, 27 }, {6, 43 }}, encoded_char_lexeme(U'Ƣ')                                }, // Encoded_char 'Ƣ'
            {{{8, 21 }, {8, 27 }}, encoded_char_lexeme(U'Ϧ')                                }, // Encoded_char 'Ϧ'
            {{{8, 30 }, {8, 38 }}, encoded_char_lexeme(U'ϥ')                                }, // Encoded_char 'ϥ'
            {{{8, 39 }, {8, 39 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #9:
#if defined(_WIN32) || defined(_WIN64)
#   define HERR_MANNELIG 142
#   define POPE_AND_MAIDEN_LEN 1494
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#else
#   define HERR_MANNELIG 139
#   define POPE_AND_MAIDEN_LEN (1494 - 14)
#   define POPE_AND_MAIDEN (HERR_MANNELIG + POPE_AND_MAIDEN_LEN)
#   define POPE_AND_DOG_LEN 20
#   define POPE_AND_DOG ((POPE_AND_MAIDEN) + POPE_AND_DOG_LEN)
#endif
            {{{3, 15 }, {6, 31 }}, string_lexeme(HERR_MANNELIG)                             }, // String "Bettida..."
            {{{8, 13 }, {22,129}}, string_lexeme(POPE_AND_MAIDEN)                           }, // String "Тебе, девка, житье..."
            {{{25,1  }, {25,24 }}, string_lexeme(POPE_AND_DOG)                              }, // String """У попа была собака..."
            {{{25,25 }, {25,25 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #10:
            {{{2, 12 }, {2, 21 }}, integer_lexeme(int_vals[0])                              }, // Integer 0123'45689
            {{{3, 26 }, {3, 56 }}, integer_lexeme(int_vals[1])                              }, // Integer 302'231'454'903'657'293'676'544
            {{{3, 66 }, {3, 104}}, integer_lexeme(int_vals[2])                              }, // Integer 158'456'325'028'528'675'187'087'900'672
            {{{7, 8  }, {7, 56 }}, integer_lexeme(int_vals[3])                              }, // Integer 1'329'227'995'784'915'872'903'807'060'280'344'576
            {{{8, 19 }, {8, 69 }}, integer_lexeme(int_vals[4])                              }, // Integer 340'282'366'920'938'463'463'374'607'431'768'211'455
            {{{10,19 }, {10,57 }}, integer_lexeme(int_vals[5])                              }, // Integer 340282366920938463463374607431768211455
            {{{14,2  }, {14,39 }}, integer_lexeme(int_vals[6])                              }, // Integer 0xffff'ffff'ffffff'ffffffffffffff'ffff
            {{{16,17 }, {16,28 }}, integer_lexeme(int_vals[7])                              }, // Integer 0x1238912345
            {{{16,48 }, {16,75 }}, integer_lexeme(int_vals[8])                              }, // Integer 0o123'755'5551'232'4561'2345
            {{{16,83 }, {16,105}}, integer_lexeme(int_vals[9])                              }, // Integer 0o123755555123245612345
            {{{20,9  }, {20,34 }}, integer_lexeme(int_vals[10])                             }, // Integer 0xdeadbeafde123456789abcde
            {{{22,1  }, {22,18 }}, integer_lexeme(int_vals[11])                             }, // Integer 0b1111111110000111
            {{{24,28 }, {24,156}}, integer_lexeme(int_vals[12])                             }, // Integer 0b1111111110000111101010001110001111000001111100000001111111110111111010101000001011110011110100001111110111111011010100101010101
            {{{26,28 }, {26,34 }}, integer_lexeme(int_vals[13])                             }, // Integer 0b10101
            {{{26,35 }, {26,35 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #11:
            {{{2, 11 }, {2, 34 }}, float_lexeme(3.1415926535897932384626q)                  }, // Float 3.1415926535897932384626
            {{{2, 35 }, {2, 35 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #12:
            {{{3, 13 }, {3, 40 }}, float_lexeme(45678.91234567892345E-12q)                  }, // Float 45678.91234567892345E-12
            {{{3, 52 }, {3, 62 }}, float_lexeme(3.45678e+35q)                               }, // Float 3.45678e+35
            {{{6, 9  }, {6, 22 }}, float_lexeme(5141.592653e+8q)                            }, // Float 5141.592653e+8
            {{{6, 26 }, {6, 43 }}, float_lexeme(6.141592653148e+8q, Float_kind::Float32)    }, // Float 6.141592653148e+8
            {{{9, 13 }, {9, 28 }}, float_lexeme(3.141592653e-78q, Float_kind::Float128)     }, // Float 3.141592653e-78
            {{{9, 31 }, {9, 48 }}, float_lexeme(9141.59265356e-78q, Float_kind::Float128)   }, // Float 9141.59265356e-78
            {{{9, 51 }, {9, 66 }}, float_lexeme(2.141592653e+11q, Float_kind::Float64)      }, // Float 2.141592653e+11
            {{{12,7  }, {12,28 }}, float_lexeme(71415926537.182818e-8q)                     }, // Float 71415926537.182818e-8
            {{{12,34 }, {12,51 }}, float_lexeme(6.141592653148e+8q, Float_kind::Float32)    }, // Float 6.141592653148e+8
            {{{15,9  }, {15,38 }}, float_lexeme(4'5.678912'34'567'8923'45E-1'2q)            }, // Float 45.67891234567892345E-12
            {{{15,57 }, {15,86 }}, complex_lexeme({0.0q, 45760.91234567892345E-223q})       }, // Complex 45760.912'34'567'8923'45E-223i
            {{{16,17 }, {16,46 }}, quat_lexeme(quat_vals[0])                                }, // Quat 45760.912'34'567'8923'45E-223j
            {{{16,51 }, {16,80 }}, quat_lexeme(quat_vals[1])                                }, // Quat 45760.912'34'567'8923'45E-223k
            {{{19,11 }, {19,31 }}, float_lexeme(0.618281828459045e-4q, Float_kind::Float80) }, // Float 0.618281828459045e-4
            {{{19,36 }, {19,56 }}, float_lexeme(0.618281828459045e+4q, Float_kind::Float80) }, // Float 0.618281828459045e+4
            {{{19,61 }, {19,82 }}, quat_lexeme(quat_vals[2], Quat_kind::Quat80)             }, // Quat 0.618281828459045e+4j
            {{{20,17 }, {20,35 }}, quat_lexeme(quat_vals[3], Quat_kind::Quat32)             }, // Quat 6.141592653148e+8k
            {{{20,37 }, {20,68 }}, quat_lexeme(quat_vals[4], Quat_kind::Quat128)            }, // Quat 45.67891234567892345E-12j
            {{{20,70 }, {20,72 }}, float_lexeme(0.6q)                                       }, // Float 0.6
            {{{20,73 }, {20,73 }}, nothing_lexeme()                                         }, // Nothing
        },

        {
            // References for text #13:
            {{{1, 1  }, {1, 1  }}, delim_lexeme(Delimiter_kind::Semicolon)                  }, // 1:   Delimiter Semicolon
            {{{1, 8  }, {1, 8  }}, delim_lexeme(Delimiter_kind::Logical_not)                }, // 2:   Delimiter Logical_not
            {{{2, 13 }, {2, 14 }}, delim_lexeme(Delimiter_kind::Maybe_Logical_and_not)      }, // 3:   Delimiter Maybe_Logical_and_not
            {{{2, 32 }, {2, 33 }}, delim_lexeme(Delimiter_kind::NEQ)                        }, // 4:   Delimiter NEQ
            {{{3, 5  }, {3, 7  }}, delim_lexeme(Delimiter_kind::Logical_and_not)            }, // 5:   Delimiter Logical_and_not
            {{{5, 17 }, {5, 18 }}, delim_lexeme(Delimiter_kind::Maybe_Logical_or_not)       }, // 6:   Delimiter Maybe_Logical_or_not
            {{{5, 29 }, {5, 31 }}, delim_lexeme(Delimiter_kind::Logical_or_not)             }, // 7:   Delimiter Logical_or_not
            {{{5, 41 }, {5, 41 }}, delim_lexeme(Delimiter_kind::Sharp)                      }, // 8:   Delimiter Sharp
            {{{5, 46 }, {5, 47 }}, delim_lexeme(Delimiter_kind::Right_arrow)                }, // 9:   Delimiter Right_arrow
            {{{6, 9  }, {6, 12 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full)       }, // 10:  Delimiter Logical_and_not_full
            {{{6, 21 }, {6, 24 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full)        }, // 11:  Delimiter Logical_or_not_full
            {{{7, 15 }, {7, 16 }}, delim_lexeme(Delimiter_kind::Data_size)                  }, // 12:  Delimiter Data_size
            {{{7, 32 }, {7, 32 }}, delim_lexeme(Delimiter_kind::Remainder)                  }, // 13:  Delimiter Remainder
            {{{8, 13 }, {8, 16 }}, delim_lexeme(Delimiter_kind::Logical_and_not_assign)     }, // 14:  Delimiter Logical_and_not_assign
            {{{8, 29 }, {8, 32 }}, delim_lexeme(Delimiter_kind::Logical_or_not_assign)      }, // 15:  Delimiter Logical_or_not_assign
            {{{9, 24 }, {9, 25 }}, delim_lexeme(Delimiter_kind::Float_remainder)            }, // 16:  Delimiter Float_remainder
            {{{10,37 }, {10,41 }}, delim_lexeme(Delimiter_kind::Logical_and_not_full_assign)}, // 17:  Delimiter Logical_and_not_full_assign
            {{{10,50 }, {10,51 }}, delim_lexeme(Delimiter_kind::Minus_assign)               }, // 18:  Delimiter Minus_assign
            {{{12,11 }, {12,13 }}, delim_lexeme(Delimiter_kind::Dec_with_wrap)              }, // 19:  Delimiter Dec_with_wrap
            {{{13,21 }, {13,25 }}, delim_lexeme(Delimiter_kind::Logical_or_not_full_assign) }, // 20:  Delimiter Logical_or_not_full_assign
            {{{15,3  }, {15,4  }}, delim_lexeme(Delimiter_kind::Remainder_assign)           }, // 21:  Delimiter Remainder_assign
            {{{15,21 }, {15,23 }}, delim_lexeme(Delimiter_kind::Float_remainder_assign)     }, // 22:  Delimiter Float_remainder_assign
            {{{17,7  }, {17,8  }}, delim_lexeme(Delimiter_kind::Dec)                        }, // 23:  Delimiter Dec
            {{{17,33 }, {17,33 }}, delim_lexeme(Delimiter_kind::Minus)                      }, // 24:  Delimiter Minus
            {{{20,11 }, {20,14 }}, delim_lexeme(Delimiter_kind::Logical_and_full_assign)    }, // 25:  Delimiter Logical_and_full_assign
            {{{20,32 }, {20,32 }}, delim_lexeme(Delimiter_kind::Bitwise_and)                }, // 26:  Delimiter Bitwise_and
            {{{20,42 }, {20,43 }}, delim_lexeme(Delimiter_kind::Tuple_begin)                }, // 27:  Delimiter Tuple_begin
            {{{20,49 }, {20,49 }}, delim_lexeme(Delimiter_kind::Round_bracket_opened)       }, // 28:  Delimiter Round_bracket_opened
            {{{21,19 }, {21,20 }}, delim_lexeme(Delimiter_kind::Logical_and)                }, // 29:  Delimiter Logical_and
            {{{21,37 }, {21,38 }}, delim_lexeme(Delimiter_kind::Bitwise_and_assign)         }, // 30:  Delimiter Bitwise_and_assign
            {{{22,4  }, {22,6  }}, delim_lexeme(Delimiter_kind::Logical_and_full)           }, // 31:  Delimiter Logical_and_full
            {{{22,14 }, {22,16 }}, delim_lexeme(Delimiter_kind::Logical_and_assign)         }, // 32:  Delimiter Logical_and_assign
            {{{22,27 }, {22,27 }}, delim_lexeme(Delimiter_kind::Round_bracket_closed)       }, // 33:  Delimiter Round_bracket_closed
            {{{25,1  }, {25,2  }}, delim_lexeme(Delimiter_kind::Tuple_end)                  }, // 34:  Delimiter Tuple_end
            {{{25,4  }, {25,4  }}, delim_lexeme(Delimiter_kind::Colon)                      }, // 35:  Delimiter Colon
            {{{25,6  }, {25,7  }}, delim_lexeme(Delimiter_kind::Scope_resolution)           }, // 36:  Delimiter Scope_resolution
            {{{25,9  }, {25,10 }}, delim_lexeme(Delimiter_kind::Copy)                       }, // 37:  Delimiter Copy
            {{{26,5  }, {26,6  }}, delim_lexeme(Delimiter_kind::Meta_bracket_closed)        }, // 38:  Delimiter Meta_bracket_closed
            {{{26,11 }, {26,12 }}, delim_lexeme(Delimiter_kind::Set_literal_end)            }, // 39:  Delimiter Set_literal_end
            {{{26,18 }, {26,19 }}, delim_lexeme(Delimiter_kind::Array_literal_end)          }, // 40:  Delimiter Array_literal_end
            {{{28,4  }, {28,5  }}, delim_lexeme(Delimiter_kind::Comment_begin)              }, // 41:  Delimiter Comment_begin
            {{{28,9  }, {28,10 }}, delim_lexeme(Delimiter_kind::Div_assign)                 }, // 42:  Delimiter Div_assign
            {{{28,13 }, {28,15 }}, delim_lexeme(Delimiter_kind::Symmetric_difference_assign)}, // 43:  Delimiter Symmetric_difference_assign
            {{{28,18 }, {28,19 }}, delim_lexeme(Delimiter_kind::Symmetric_difference)       }, // 44:  Delimiter Symmetric_difference
            {{{28,23 }, {28,23 }}, delim_lexeme(Delimiter_kind::Div)                        }, // 45:  Delimiter Div
            {{{30,1  }, {30,1  }}, delim_lexeme(Delimiter_kind::Comma)                      }, // 46:  Delimiter Comma
            {{{30,5  }, {30,5  }}, delim_lexeme(Delimiter_kind::Square_bracket_closed)      }, // 47:  Delimiter Square_bracket_closed
            {{{32,9  }, {32,10 }}, delim_lexeme(Delimiter_kind::Comment_end)                }, // 48:  Delimiter Comment_end
            {{{32,13 }, {32,14 }}, delim_lexeme(Delimiter_kind::Power)                      }, // 49:  Delimiter Power
            {{{32,18 }, {32,18 }}, delim_lexeme(Delimiter_kind::Mul)                        }, // 50:  Delimiter Mul
            {{{33,25 }, {33,27 }}, delim_lexeme(Delimiter_kind::Float_power)                }, // 51:  Delimiter Float_power
            {{{33,31 }, {33,32 }}, delim_lexeme(Delimiter_kind::Mul_assign)                 }, // 52:  Delimiter Mul_assign
            {{{33,41 }, {33,43 }}, delim_lexeme(Delimiter_kind::Power_assign)               }, // 53:  Delimiter Power_assign
            {{{34,17 }, {34,20 }}, delim_lexeme(Delimiter_kind::Float_power_assign)         }, // 54:  Delimiter Float_power_assign
            {{{34,34 }, {34,34 }}, delim_lexeme(Delimiter_kind::Plus)                       }, // 55:  Delimiter Plus
            {{{34,56 }, {34,57 }}, delim_lexeme(Delimiter_kind::Inc)                        }, // 56:  Delimiter Inc
            {{{37,7  }, {37,8  }}, delim_lexeme(Delimiter_kind::Plus_assign)                }, // 57:  Delimiter Plus_assign
            {{{37,23 }, {37,25 }}, delim_lexeme(Delimiter_kind::Inc_with_wrap)              }, // 58:  Delimiter Inc_with_wrap
            {{{39,16 }, {39,16 }}, delim_lexeme(Delimiter_kind::Dot)                        }, // 59:  Delimiter Dot
            {{{39,27 }, {39,28 }}, delim_lexeme(Delimiter_kind::Range)                      }, // 60:  Delimiter Range
            {{{39,42 }, {39,43 }}, delim_lexeme(Delimiter_kind::Maybe_Algebraic_separator)  }, // 61:  Delimiter Maybe_Algebraic_separator
            {{{39,53 }, {39,55 }}, delim_lexeme(Delimiter_kind::Algebraic_separator)        }, // 62:  Delimiter Algebraic_separator
            {{{42,18 }, {42,19 }}, delim_lexeme(Delimiter_kind::EQ)                         }, // 63:  Delimiter EQ
            {{{42,29 }, {42,29 }}, delim_lexeme(Delimiter_kind::Assign)                     }, // 64:  Delimiter Assign
            {{{44,3  }, {44,3  }}, delim_lexeme(Delimiter_kind::LT)                         }, // 65:  Delimiter LT
            {{{44,5  }, {44,6  }}, delim_lexeme(Delimiter_kind::Maybe_Common_template_type) }, // 66:  Delimiter Maybe_Common_template_type
            {{{44,8  }, {44,9  }}, delim_lexeme(Delimiter_kind::Maybe_Data_address)         }, // 67:  Delimiter Maybe_Data_address
            {{{44,18 }, {44,19 }}, delim_lexeme(Delimiter_kind::Maybe_Type_add)             }, // 68:  Delimiter Maybe_Type_add
            {{{44,30 }, {44,31 }}, delim_lexeme(Delimiter_kind::Left_arrow)                 }, // 69:  Delimiter Left_arrow
            {{{44,43 }, {44,44 }}, delim_lexeme(Delimiter_kind::Meta_bracket_opened)        }, // 70:  Delimiter Meta_bracket_opened
            {{{44,46 }, {44,48 }}, delim_lexeme(Delimiter_kind::Maybe_Common_template_type) }, // 71:  Delimiter Maybe_Common_template_type
            {{{45,12 }, {45,13 }}, delim_lexeme(Delimiter_kind::Left_shift)                 }, // 72:  Delimiter Left_shift
            {{{45,23 }, {45,24 }}, delim_lexeme(Delimiter_kind::LEQ)                        }, // 73:  Delimiter LEQ
            {{{45,36 }, {45,37 }}, delim_lexeme(Delimiter_kind::Maybe_Get_expr_type)        }, // 74:  Delimiter Maybe_Get_expr_type
            {{{45,58 }, {45,59 }}, delim_lexeme(Delimiter_kind::Label_prefix)               }, // 75:  Delimiter Label_prefix
            {{{48,7  }, {48,9  }}, delim_lexeme(Delimiter_kind::Template_type)              }, // 76:  Delimiter Template_type
            {{{48,11 }, {48,13 }}, delim_lexeme(Delimiter_kind::Maybe_Data_address)         }, // 77:  Delimiter Maybe_Data_address
            {{{48,16 }, {48,18 }}, delim_lexeme(Delimiter_kind::Address)                    }, // 78:  Delimiter Address
            {{{48,24 }, {48,26 }}, delim_lexeme(Delimiter_kind::Type_add)                   }, // 79:  Delimiter Type_add
            {{{48,32 }, {48,35 }}, delim_lexeme(Delimiter_kind::Data_address)               }, // 80:  Delimiter Data_address
            {{{48,46 }, {48,49 }}, delim_lexeme(Delimiter_kind::Type_add_assign)            }, // 81:  Delimiter Type_add_assign
            {{{50,10 }, {50,12 }}, delim_lexeme(Delimiter_kind::Common_type)                }, // 82:  Delimiter Common_type
            {{{50,28 }, {50,30 }}, delim_lexeme(Delimiter_kind::Left_shift_assign)          }, // 83:  Delimiter Left_shift_assign
            {{{50,42 }, {50,44 }}, delim_lexeme(Delimiter_kind::Maybe_Get_elem_type)        }, // 84:  Delimiter Maybe_Get_elem_type
            {{{50,46 }, {50,48 }}, delim_lexeme(Delimiter_kind::Get_expr_type)              }, // 85:  Delimiter Get_expr_type
            {{{50,52 }, {50,55 }}, delim_lexeme(Delimiter_kind::Get_elem_type)              }, // 86:  Delimiter Get_elem_type
            {{{52,3  }, {52,3  }}, delim_lexeme(Delimiter_kind::Set_difference)             }, // 87:  Delimiter Set_difference
            {{{52,10 }, {52,11 }}, delim_lexeme(Delimiter_kind::Array_literal_begin)        }, // 88:  Delimiter Array_literal_begin
            {{{52,17 }, {52,18 }}, delim_lexeme(Delimiter_kind::Set_difference_assign)      }, // 89:  Delimiter Set_difference_assign
            {{{52,27 }, {52,27 }}, delim_lexeme(Delimiter_kind::Square_bracket_opened)      }, // 90:  Delimiter Square_bracket_opened
            {{{53,1  }, {53,1  }}, delim_lexeme(Delimiter_kind::At)                         }, // 91:  Delimiter At
            {{{53,6  }, {53,7  }}, delim_lexeme(Delimiter_kind::Logical_xor)                }, // 92:  Delimiter Logical_xor
            {{{53,14 }, {53,15 }}, delim_lexeme(Delimiter_kind::Bitwise_xor_assign)         }, // 93:  Delimiter Bitwise_xor_assign
            {{{53,29 }, {53,31 }}, delim_lexeme(Delimiter_kind::Logical_xor_assign)         }, // 94:  Delimiter Logical_xor_assign
            {{{53,36 }, {53,36 }}, delim_lexeme(Delimiter_kind::Bitwise_xor)                }, // 95:  Delimiter Bitwise_xor
            {{{55,8  }, {55,11 }}, delim_lexeme(Delimiter_kind::Common_template_type)       }, // 96:  Delimiter Common_template_type
            {{{57,6  }, {57,6  }}, delim_lexeme(Delimiter_kind::Bitwise_or)                 }, // 97:  Delimiter Bitwise_or
            {{{57,9  }, {57,10 }}, delim_lexeme(Delimiter_kind::Maybe_Cardinality)          }, // 98:  Delimiter Maybe_Cardinality
            {{{57,21 }, {57,23 }}, delim_lexeme(Delimiter_kind::Cardinality)                }, // 99:  Delimiter Cardinality
            {{{57,31 }, {57,32 }}, delim_lexeme(Delimiter_kind::Bitwise_or_assign)          }, // 100: Delimiter Bitwise_or_assign
            {{{58,12 }, {58,13 }}, delim_lexeme(Delimiter_kind::Logical_or)                 }, // 101: Delimiter Logical_or
            {{{58,17 }, {58,19 }}, delim_lexeme(Delimiter_kind::Logical_or_full)            }, // 102: Delimiter Logical_or_full
            {{{58,25 }, {58,27 }}, delim_lexeme(Delimiter_kind::Logical_or_assign)          }, // 103: Delimiter Logical_or_assign
            {{{58,36 }, {58,39 }}, delim_lexeme(Delimiter_kind::Logical_or_full_assign)     }, // 104: Delimiter Logical_or_full_assign
            {{{60,3  }, {60,6  }}, delim_lexeme(Delimiter_kind::Pattern)                    }, // 105: Delimiter Pattern
            {{{60,8  }, {60,8  }}, delim_lexeme(Delimiter_kind::Figure_bracket_opened)      }, // 106: Delimiter Figure_bracket_opened
            {{{60,13 }, {60,14 }}, delim_lexeme(Delimiter_kind::Maybe_Pattern)              }, // 107: Delimiter Maybe_Pattern
            {{{60,17 }, {60,19 }}, delim_lexeme(Delimiter_kind::Maybe_Pattern)              }, // 108: Delimiter Maybe_Pattern
            {{{60,30 }, {60,30 }}, delim_lexeme(Delimiter_kind::GT)                         }, // 109: Delimiter GT
            {{{60,33 }, {60,34 }}, delim_lexeme(Delimiter_kind::GEQ)                        }, // 110: Delimiter GEQ
            {{{60,38 }, {60,39 }}, delim_lexeme(Delimiter_kind::Right_shift)                }, // 111: Delimiter Right_shift
            {{{60,55 }, {60,57 }}, delim_lexeme(Delimiter_kind::Right_shift_assign)         }, // 112: elimiter Right_shift_assign
            {{{63,10 }, {63,10 }}, delim_lexeme(Delimiter_kind::Bitwise_not)                }, // 113: elimiter Bitwise_not
            {{{63,24 }, {63,25 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not)             }, // 114: elimiter Bitwise_or_not
            {{{63,35 }, {63,37 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not_assign)     }, // 115: Delimiter Bitwise_and_not_assign
            {{{63,49 }, {63,50 }}, delim_lexeme(Delimiter_kind::Bitwise_and_not)            }, // 116: Delimiter Bitwise_and_not
            {{{63,53 }, {63,55 }}, delim_lexeme(Delimiter_kind::Bitwise_or_not_assign)      }, // 117: Delimiter Bitwise_or_not_assign
            {{{65,5  }, {65,5  }}, delim_lexeme(Delimiter_kind::Figure_bracket_closed)      }, // 118: Delimiter Figure_bracket_closed
            {{{65,21 }, {65,23 }}, delim_lexeme(Delimiter_kind::Range_excluded_end)         }, // 119: Delimiter Range_excluded_end
            {{{65,29 }, {65,30 }}, delim_lexeme(Delimiter_kind::Range)                      }, // 120: Delimiter Range
            {{{67,7  }, {67,10 }}, delim_lexeme(Delimiter_kind::Cyclic_right_shift_assign)  }, // 121: Delimiter Cyclic_right_shift_assign
            {{{67,17 }, {67,18 }}, delim_lexeme(Delimiter_kind::Set_literal_begin)          }, // 122: Delimiter Set_literal_begin
            {{{67,40 }, {67,42 }}, delim_lexeme(Delimiter_kind::Cyclic_right_shift)         }, // 123: Delimiter Cyclic_right_shift
            {{{67,77 }, {67,78 }}, delim_lexeme(Delimiter_kind::Remainder_assign)           }, // 124: Delimiter Remainder_assign
            {{{70,9  }, {70,11 }}, delim_lexeme(Delimiter_kind::Cyclic_left_shift)          }, // 125: Delimiter Cyclic_left_shift
            {{{70,32 }, {70,35 }}, delim_lexeme(Delimiter_kind::Algebraic_separator_assign) }, // 126: Delimiter Algebraic_separator_assign
            {{{70,44 }, {70,47 }}, delim_lexeme(Delimiter_kind::Cyclic_left_shift_assign)   }, // 127: Delimiter Cyclic_left_shift_assign
            {{{70,59 }, {70,60 }}, delim_lexeme(Delimiter_kind::Cond_op_full)               }, // 128: Delimiter Cond_op_full
            {{{70,67 }, {70,67 }}, delim_lexeme(Delimiter_kind::Cond_op)                    }, // 129: Delimiter Cond_op
            {{{73,17 }, {73,18 }}, delim_lexeme(Delimiter_kind::Bitscale_literal_end)       }, // 130: Delimiter Bitscale_literal_end
            {{{73,54 }, {73,55 }}, delim_lexeme(Delimiter_kind::Bitscale_literal_begin)     }, // 131: Delimiter Bitscale_literal_begin
            {{{75,2  }, {75,3  }}, delim_lexeme(Delimiter_kind::Maximal_value_in_collection)}, // 132: Delimiter Maximal_value_in_collection
            {{{75,35 }, {75,36 }}, delim_lexeme(Delimiter_kind::Minimal_value_in_collection)}, // 133: Delimiter Minimal_value_in_collection
            {{{75,41 }, {75,43 }}, delim_lexeme(Delimiter_kind::Minimal_value_of_type)      }, // 134: Delimiter Minimal_value_of_type
            {{{75,62 }, {75,64 }}, delim_lexeme(Delimiter_kind::Maximal_value_of_type)      }, // 135: Delimiter Maximal_value_of_type
            {{{75,65 }, {75,65 }}, nothing_lexeme()                                         }, // Nothing
        }
    };


    using Mode = scanner::Scanner::Begin_comment_mode;

    class Scanner_as_generator : public testing::Generator<scanner::Arkona_token>{
    public:
        Scanner_as_generator()                            = default;
        Scanner_as_generator(const Scanner_as_generator&) = default;
        virtual ~Scanner_as_generator()                   = default;

        Scanner_as_generator(const iscaner::Location_ptr& location,
                             const Errors_and_tries&      et,
                             Mode                         mode = Mode::As_delimiter):
            scanner_{std::make_shared<scanner::Scanner>(location, et)}
        {
            scanner_->set_begin_comment_mode(mode);
        }

        scanner::Arkona_token yield()                                     override;
        bool                  is_finished(const scanner::Arkona_token& t) override;
    private:
        std::shared_ptr<scanner::Scanner> scanner_;
    };

    scanner::Arkona_token Scanner_as_generator::yield()
    {
        return scanner_->current_lexeme();
    }

    bool Scanner_as_generator::is_finished(const scanner::Arkona_token& t)
    {
        return t.lexeme_.code_.kind_ == scanner::Lexem_kind::Nothing;
    }
}

namespace testing_scanner{
    void test_lexeme_recognition()
    {
        Errors_and_tries  et;
        et.ec_                      = std::make_shared<Error_count>();
        et.wc_                      = std::make_shared<Warning_count>();
        et.ids_trie_                = std::make_shared<strings::trie::Char_trie>();
        et.strs_trie_               = std::make_shared<strings::trie::Char_trie>();

        std::size_t i = 0;
        for(auto test_text: test_texts)
        {
            auto                 p   = const_cast<char32_t*>(test_text);
            auto                 loc = std::make_shared<iscaner::Location>(p);
            Scanner_as_generator gen{loc, et};
            printf("Testing for text #%zu\n", i);
            testing::test(gen,                             [](auto t){return t;},
                          std::begin(tokens_for_texts[i]), std::end(tokens_for_texts[i]));
            ++i;
        }
    }
}