/*
    File:    test_lexeme_recognition.hpp
    Created: 22 May 2023 at 21:42 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

namespace testing_scanner{
    void test_lexeme_recognition();
}