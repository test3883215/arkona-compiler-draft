/*
    File:    testing.hpp
    Created: 10 May 2023 at 21:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <set>
#include <cstddef>
#include <cstdio>
#include <string>
#include <iostream>
namespace testing{
    template<typename T>
    class Generator{
    public:
        Generator<T>()                 = default;
        Generator(const Generator<T>&) = default;
        virtual ~Generator<T>()        = default;

        using value_type = T;

        virtual value_type yield()                    = 0;
        virtual bool is_finished(const value_type& t) = 0;
    };

    template<typename T, typename Func, typename InputIterator>
    void test(Generator<T>& g, Func f, InputIterator first, InputIterator last)
    {
        size_t        total_num_of_tests  = 0;
        size_t        num_of_failed_tests = 0;
        size_t        num_of_passed_tests = 0;
        auto          it                  = first;

        InputIterator pre_last;
        if(first != last){
            pre_last = last;
            --pre_last;
        }

        std::set<size_t> failed_tests;

        T arg;
        while(it != last){
            arg = g.yield();
            if(g.is_finished(arg) && (it != pre_last)){
                printf("Unexpected end of input stream.\n");
                break;
            }
            auto val  = f(arg);
            auto dest = *it;
            if(val == dest){
                ++num_of_passed_tests;
            }else{
                ++num_of_failed_tests;
                failed_tests.insert(total_num_of_tests + 1);
            }
            ++total_num_of_tests;
            ++it;
        }

        arg = g.yield();
        if(!g.is_finished(arg)){
            printf("Unexpected end of tests.\n");
        }

        printf("Total tests: %zu\nPassed: %zu\nFailed: %zu\n\n",
               total_num_of_tests,
               num_of_passed_tests,
               num_of_failed_tests);
        if(num_of_failed_tests){
            printf("Failed tests have the following numbers:\n");
            std::string s;
            for(size_t n : failed_tests){
                s += std::to_string(n) + ", ";
            }
            s.pop_back();
            s.pop_back();
            printf("%s\n", s.c_str());
        }
    }

    template<typename InputIterator, typename Func>
    void test(InputIterator first, InputIterator last, Func f)
    {
        size_t num_of_total_tests  = 0;
        size_t num_of_failed_tests = 0;
        size_t num_of_passed_tests = 0;

        std::set<size_t> failed_tests;
        for(auto it = first; it != last; ++it){
            auto arg = it->first;
            auto val = f(arg);
            auto dest = it->second;
            if(val == dest){
                num_of_passed_tests++;
            }else{
                ++num_of_failed_tests;
                failed_tests.insert(num_of_total_tests + 1);
            }
            num_of_total_tests++;
        }

        printf("Total tests: %zu\nPassed: %zu\nFailed: %zu\n\n",
               num_of_total_tests,
               num_of_passed_tests,
               num_of_failed_tests);
        if(num_of_failed_tests){
            printf("Failed tests have the following numbers:\n");
            std::string s;
            for(size_t n : failed_tests){
                s += std::to_string(n) + ", ";
            }
            s.pop_back();
            s.pop_back();
            printf("%s\n", s.c_str());
        }
    }
}