/*
    File:    lexeme_fab.cpp
    Created: 10 May 2023 at 21:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/lexeme_fab.hpp"

namespace testing_scanner{
    const Lexeme unknown_lexeme()
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::UnknownLexem;
        li.code_.subkind_ = 0;
        return li;
    }

    const Lexeme nothing_lexeme()
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Nothing;
        li.code_.subkind_ = 0;
        return li;
    }


    //! Function that return keywords.
    const Lexeme keyword_lexeme(scanner::Keyword_kind kw)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Keyword;
        li.code_.subkind_ = kw;
        return li;
    }

    //! Function that return identifier.
    const Lexeme id_lexeme(std::size_t idx)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Id;
        li.code_.subkind_ = 0;
        li.id_index_      = idx;
        return li;
    }

    //! Function that return character lexeme.
    const Lexeme char_lexeme(char32_t c)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Char;
        li.code_.subkind_ = 0;
        li.char_val_      = c;
        return li;
    }

    //! Function that return encoded character lexeme.
    const Lexeme encoded_char_lexeme(char32_t c)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Encoded_char;
        li.code_.subkind_ = 0;
        li.char_val_      = c;
        return li;
    }

    //! Function that return string lexeme.
    const Lexeme string_lexeme(size_t str_idx)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::String;
        li.code_.subkind_ = static_cast<uint8_t>(scanner::String_kind::String32);
        li.str_index_     = str_idx;
        return li;
    }

    //! Function that return integer lexeme.
    const Lexeme integer_lexeme(unsigned __int128 int_val)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Integer;
        li.code_.subkind_ = 0;
        li.int_val_       = int_val;
        return li;
    }

    //! Function that return float lexeme.
    const Lexeme float_lexeme(__float128 float_val, Float_kind precision)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Float;
        li.code_.subkind_ = static_cast<uint8_t>(precision);
        li.float_val_     = float_val;
        return li;
    }

    //! Function that return complex lexeme.
    const Lexeme complex_lexeme(__complex128 complex_val, Complex_kind precision)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Complex;
        li.code_.subkind_ = static_cast<uint8_t>(precision);
        li.complex_val_   = complex_val;
        return li;
    }

    //! Function that return quaternion lexeme.
    const Lexeme quat_lexeme(const quat::quat_t<__float128>& quat_val, Quat_kind precision)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Quat;
        li.code_.subkind_ = static_cast<uint8_t>(precision);
        li.quat_val_      = quat_val;
        return li;
    }

    //! Function that return delimiter lexeme.
    const Lexeme delim_lexeme(Delimiter_kind delim)
    {
        Lexeme li;
        li.code_.kind_    = scanner::Lexem_kind::Delimiter;
        li.code_.subkind_ = delim;
        return li;
    }
};