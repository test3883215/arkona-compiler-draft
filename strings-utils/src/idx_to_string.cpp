/*
    File:    idx_to_string.cpp
    Created: 09 May 2022 at 15:16 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/idx_to_string.hpp"
#include "../../char-conv/include/char_conv.hpp"

namespace strings::trie{
    std::string idx_to_string(const std::shared_ptr<Char_trie>& t,
                              std::size_t                       idx,
                              const std::string&                default_value)
    {
        const auto  u32str = t->get(idx);
        std::string s      = u32string_to_utf8(u32str);

        return idx ? s : default_value;
    }
}