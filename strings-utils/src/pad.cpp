/*
    File:    pad.cpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 02 April 2023 at 10:36 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#include "../include/pad.hpp"

#include "../../char-conv/include/char_conv.hpp"

namespace strings::pad{
    std::string utf8_pad_left(const std::string& str, std::size_t n, char padded_char)
    {
        std::u32string str32         = utf8_to_u32string(str);
        char32_t       padded_char32 = utf8_to_u32string(std::string(1, padded_char))[0];
        std::u32string padded_str32  = str32;
        if(str32.size() < n)
        {
            padded_str32 = std::u32string(n - str32.size(), padded_char32) + str32;
        }
        
        const auto result = u32string_to_utf8(padded_str32);
        
        return result;
    }

    std::string utf8_pad_right(const std::string& str, std::size_t n, char32_t padded_char)
    {
        std::u32string str32         = utf8_to_u32string(str);
        char32_t       padded_char32 = utf8_to_u32string(std::string(1, padded_char))[0];
        std::u32string padded_str32  = str32;
        if(str32.size() < n)
        {
            padded_str32 = str32 + std::u32string(n - str32.size(), padded_char32);
        }
        
        const auto result = u32string_to_utf8(padded_str32);
        
        return result;
    }
}