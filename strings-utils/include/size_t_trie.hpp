/*
    File:    size_t_trie.hpp
    Created: 26 August 2023 at 17:10 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
*/

#pragma once

#include <cstddef>
#include <vector>
#include "../include/trie.hpp"
#include "../../arkona-ast-lib/include/id_info.hpp"

namespace strings::trie{
    using Size_t_trie = Trie<ast::Id_info, std::vector<ast::Id_info>>;
}