/*
    File:    config_from_text.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 19 February 2023 at 15:01 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

#include "../include/string_traits.hpp"
#include "../include/split.hpp"

#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <utility>
#include <vector>

namespace strings::config{
    template<typename S>
    struct Lexeme_with_code
    {
        S lexeme_;
        S code_;
    };

    template<typename S>
    std::vector<Lexeme_with_code<S>> get_config_from_text(const S& text)
    {
        const auto lines = strings::split::split_by_end_of_line<S>(text);

        std::vector<Lexeme_with_code<S>> result;

        for(const auto& l : lines)
        {
            const auto parts = strings::split::split_by_space<S>(l);

            std::vector<S> lc_as_vector;

            std::copy_if(parts.begin(),
                         parts.end(),
                         std::back_inserter(lc_as_vector),
                         [](const S& s)
                         {
                             return !s.empty();
                         });
            if(lc_as_vector.size() != 2)
            {
                throw std::runtime_error("Error: all lines of config file must be non-empty strings with two words: lexeme and code.");
            }
            result.push_back(Lexeme_with_code<S>{lc_as_vector[0], lc_as_vector[1]});
        }

        return result;
    }
}