/*
    File:    attributed_trie.hpp
    Created: 08 May 2022 at 10:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
*/

#pragma once

#include <algorithm>
#include <cstddef>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace strings::trie::attributed{
    template<typename T, typename A>
    using Attributed_string = std::pair<std::basic_string<T>, A>;

    template<typename T, typename A>
    struct Jump_chars{
        std::basic_string<T> jump_chars;
        A                    code;
        std::size_t          first_state;
    };

    template<typename T, typename A>
    using Jumps = std::vector<Jump_chars<T, A>>;

    template<typename T>
    using Init  = std::pair<std::size_t, T>;

    template<typename T>
    using Inits = std::vector<Init<T>>;

    template<typename T, typename A>
    struct Jumps_and_inits {
        Jumps<T, A> jumps;
        Inits<T>    init_table;
    };

    template<typename T, typename A>
    std::map<std::basic_string<T>, std::size_t> get_jump_strings_offsets(const Jumps_and_inits<T, A>& jumps)
    {
        std::map<std::basic_string<T>, std::size_t> result;
        std::size_t current_offset = 0;
        for(auto&& jump : jumps.jumps)
        {
            const auto& jc = jump.jump_chars;
            auto        it = result.find(jc);
            if(it == result.end())
            {
                result[jc]     =  current_offset;
                current_offset += jc.length() + 1;
            }
        }
        return result;
    }

    template<typename T>
    auto get_jump_strings_offsets_as_vector(const std::map<std::basic_string<T>, std::size_t>& offsets)
    {
        std::vector<std::pair<std::basic_string<T>, std::size_t>> result(offsets.begin(), offsets.end());
        std::sort(result.begin(),
                  result.end(),
                  [](const auto& p, const auto& q)
                  {
                      return p.second < q.second;
                  });
        return result;
    }

    template<typename T, typename A>
    class Attributed_trie{
    public:
        Attributed_trie<T, A>();

        ~Attributed_trie()                                 = default;

        Attributed_trie(const Attributed_trie<T, A>& orig) = default;

        /**
         * @brief The function of inserting into the prefix tree.
         * @param [in] s Inserted string s.
         * @return       Index of the string s in the prefix tree.
         */
        std::size_t insert(const Attributed_string<T, A>& s);

        /**
         * @brief Calculation of the maximum degree of the vertices of the prefix tree
         *        (the root of the tree is not taken into account).
         * @return The maximum degree of the vertices of the prefix tree
         *         (the root of the tree is not taken into account)
         */
        std::size_t maximal_degree();

        /**
         * @brief This function gets the attributed string corresponding to the given index.
         *
         * @param [in] idx Index to get attributed string.
         * @return         Attributed string corresponding to the index idx
         */
        Attributed_string<T, A> get_string(std::size_t idx);

        /* This function builds the intermediate repesentation of the transition table. */
        Jumps_and_inits<T, A> jumps();
    private:
        /**
         * @struct node
         * @brief Node type of the prefix tree.
         * @details All child nodes of the current node are organized in the form of a
         *          simply-connected list, the first element of which is an element with
         *          the index first_child. The  field parent contains the index of the
         *          parent node, and in the next field the next descendant of the parent
         *          node. If the current node has no children, then the field  first_child
         *          contains zero. Similarly, the last element in the list of children in
         *          the  field next contains zero. Here, the subscript is the index in the
         *          field node_buffer, which is a vector (in the sense of the STL library)
         *          of the nodes of the prefix tree.
         */
        struct node{
            std::size_t parent, first_child, next;

            /// @brief The length of the path from the current node to the root of the tree.
            std::size_t path_len;

            /// @brief The degree of the node, that is, the number of edges emerging
            /// from the node
            std::size_t degree;

            /// @brief The character of the inserted string that is the
            /// label of the current node
            T c;
            A attr;

            node()
            {
                next = parent = path_len = first_child = 0;
                degree = 0;
                c      = T();
                attr   = A();
            }
        };

        std::vector<node>   node_buffer;
        std::vector<std::size_t> nodes_indeces;

        /**
        * @brief This function adds a node marked with a value of x of type T to the list of
        *        children of the node parent_idx.
        * @param [in] parent_idx An index of a parent.
        * @param [in] x          An inserted value.
        * @return                The index of inserted node.
        */
        std::size_t add_child(size_t parent_idx, T x);

        void get_next_level(const std::vector<size_t>& current_level,
                            std::vector<std::size_t>& next_level);
        std::size_t jumps_for_subtrie(std::size_t root_child, std::size_t current_state,
                                      Jumps<T, A>& current_jumps);

        using Level = std::vector<std::size_t>;
        std::map<std::size_t, std::size_t> get_recode_map(const std::vector<Level>& levels, std::size_t current_state);
    };

    template<typename T, typename A>
    Attributed_trie<T, A>::Attributed_trie()
    {
        node_buffer = std::vector<node>(1);
        nodes_indeces = std::vector<std::size_t>();
    }

    template<typename T, typename A>
    std::size_t Attributed_trie<T, A>::maximal_degree()
    {
        std::size_t deg = 0;
        std::size_t len = node_buffer.size();
        for(std::size_t i = 1; i < len; i++){
            deg = std::max(deg, node_buffer[i].degree);
        }
        return deg;
    }

    template<typename T, typename A>
    std::size_t Attributed_trie<T, A>::add_child(std::size_t parent_idx, T x)
    {
        std::size_t current, previous;
        node        temp;
        current = previous = node_buffer[parent_idx].first_child;
        /* The variable temp contains a node that you might need to insert. */
        temp.c        = x;
        temp.degree   = 0;
        temp.next     = 0;
        temp.parent   = parent_idx;
        temp.path_len = node_buffer[parent_idx].path_len + 1;
        if(!current){
            /* We can be here only if the node with the parent_idx index has no children at
             * all. This means that the added node will be the first in the list of children.
             * In this case the degree of node parent_idx will increase by one, and will
             * become equal to 1.
             */
            node_buffer.push_back(temp);
            std::size_t child_idx = node_buffer.size() - 1;
            node_buffer[parent_idx].first_child = child_idx;
            node_buffer[parent_idx].degree = 1;
            return child_idx;
        }
        while(current)
        {
            // If there are children, then you need to go through the list of children.
            node current_node = node_buffer[current];
            if(current_node.c == x){
              /* If there is a child marked with the desired symbol (the symbol x),
               * then we need to return the index of this child. */
              return current;
            }else{
              previous = current; current = current_node.next;
            }
        }
        /* If there is no such child, then we need to add this child to the end
         * of the list of children. */
        node_buffer.push_back(temp);
        std::size_t next_child = node_buffer.size() - 1;
        node_buffer[previous].next = next_child;
        node_buffer[parent_idx].degree++;
        return next_child;
    }

    template<typename T, typename A>
    std::size_t Attributed_trie<T, A>::insert(const Attributed_string<T, A>& s)
    {
        const auto [str, attr] = s;

        std::size_t current_root = 0;
        for(const auto& e : str)
        {
            current_root = add_child(current_root, e);
        }
        nodes_indeces.push_back(current_root);

        node_buffer[current_root].attr = attr;
        return current_root;
    }

    template<typename T, typename A>
    Attributed_string<T, A> Attributed_trie<T, A>::get_string(std::size_t idx)
    {
        std::size_t id_len  = node_buffer[idx].path_len;
        auto   p            = std::make_unique<T[]>(id_len + 1);

        p[id_len]           = T{};
        std::size_t current = idx;
        std::size_t i       = id_len - 1;
        /* Since idx is the index of the element in node_buffer containing the last
         * character of the inserted string, and each element of the vector node_buffer
         * contains the field parent that points to the element with the previous
         * character of the string, then to get the inserted string, which corresponds
         * to the index idx, as an array of characters, it is necessary to walk from
         * the element with index idx to the root. The characters of the inserted
         * string will be read from the end to the beginning. */
        for( ; current; current = node_buffer[current].parent)
        {
            p[i--] = node_buffer[current].c;
        }

        auto str = std::basic_string<T>(p.get());

        Attributed_string<T, A> result{str, node_buffer[idx].attr};
        return result;
    }

    template<typename T, typename A>
    void Attributed_trie<T, A>::get_next_level(const std::vector<std::size_t>& current_level,
                                               std::vector<std::size_t>&       next_level)
    {
        next_level = std::vector<std::size_t>();
        for(std::size_t x : current_level){
            std::size_t current_child = node_buffer[x].first_child;
            while(current_child)
            {
                next_level.push_back(current_child);
                current_child = node_buffer[current_child].next;
            }
        }
    }

    template<typename T, typename A>
    std::map<std::size_t, std::size_t> Attributed_trie<T, A>::get_recode_map(const std::vector<Level>& levels,
                                                                             std::size_t               current_state)
    {
        std::map<std::size_t, std::size_t> result;
        std::size_t state = current_state;
        for(auto&& level : levels)
        {
            for(std::size_t idx : level)
            {
                result[idx] = state++;
            }
        }
        return result;
    }

    template<typename T, typename A>
    std::size_t Attributed_trie<T, A>::jumps_for_subtrie(std::size_t       subtrie_root,
                                                         std::size_t       current_state,
                                                         Jumps<T, A>&      current_jumps)
    {
        /* Create a vector from the subtree levels, whose root is subtrie_root.
         * The zero level is the subtrie_root node itself. */
        std::vector<Level> levels = std::vector<Level>(1);
        levels[0].push_back(subtrie_root);
        while(true)
        {
            Level next_level;
            get_next_level(levels.back(), next_level);
            if(next_level.empty()){
                break;
            }
            levels.push_back(next_level);
        }

        /* Now we can finish the draft for the transition table. */
        auto recode_map = get_recode_map(levels, current_state);
        for(const auto& layer : levels)
        {
        /* Cycle through layers. */
            for(std::size_t x : layer)
            {
            /* Cycle through the current layer. */
                Jump_chars<T, A> jc;
                jc.jump_chars             = std::basic_string<T>();
                jc.code                   = node_buffer[x].attr;
                std::size_t current_child = node_buffer[x].first_child;
                jc.first_state            = recode_map[current_child];
                while(current_child)
                {
                /* Cycle through children of the current layer. */
                    jc.jump_chars += node_buffer[current_child].c;
                    current_child =  node_buffer[current_child].next;
                }
                if(jc.jump_chars.empty()){
                    jc.first_state = 0;
                }
                current_jumps.push_back(jc);
            }
        }
        return current_state + recode_map.size() - 1;
    }

    template<typename T, typename A>
    Jumps_and_inits<T, A> Attributed_trie<T, A>::jumps()
    {
        Jumps_and_inits<T, A> ji;
        std::size_t           subtrie_root  = node_buffer[0].first_child;
        std::size_t           current_state = 0;
        while(subtrie_root){
            ji.init_table.push_back(
                std::pair<std::size_t, T>(current_state, node_buffer[subtrie_root].c)
            );
            current_state = jumps_for_subtrie(subtrie_root, current_state, ji.jumps);
            subtrie_root = node_buffer[subtrie_root].next;
        }
        if(!ji.init_table.empty()){
            std::sort(ji.init_table.begin(),
                      ji.init_table.end(),
                      [](const Init<T>& a, const Init<T>& b)
                      {
                          return a.second < b.second;
                      });
        }
        return ji;
    }

}