/*
    File:    indents.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 13 March 2022 at 09:02 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

#include <algorithm>
#include <cstddef>
#include <limits>

#include "../include/count.hpp"
#include "../include/join.hpp"
#include "../include/split.hpp"
#include "../include/string_traits.hpp"

namespace strings::indents{
    template<typename S>
    S set_up_left_indent(const S& text, std::size_t new_indent)
    {
        S result;

        if(text.empty()){
            return text;
        }

        const auto lines = strings::split::split_by_end_of_line(text);

        std::size_t previous_indent = std::numeric_limits<std::size_t>::max();

        for(const auto& line : lines){
            if(line.empty()){
                continue;
            }
            previous_indent = std::min(previous_indent, strings::count::get_num_of_spaces_from_left(line));
        }

        if(previous_indent == std::numeric_limits<std::size_t>::max()){
            previous_indent = 0;
        }

        result = strings::join::join([previous_indent, new_indent](const S& line){
                                        if(line.empty()){
                                            return line;
                                        }
                                        return S(new_indent, strings::traits::make_space_char<S>()) + line.substr(previous_indent);
                                     },
                                     lines.begin(),
                                     lines.end(),
                                     strings::traits::make_end_of_line<S>());

        return result;
    }
}