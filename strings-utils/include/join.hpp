/*
    File:    join.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 12 March 2022 at 19:26 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

namespace strings::join{
    template<typename BidirectionalIt, typename S>
    S join(BidirectionalIt first, BidirectionalIt last, const S& separator)
    {
        S result;
        if(first == last){
            return result;
        }
        auto pre_last = --last;
        if(first == pre_last){
            return *first;
        }
        for(auto it = first; it != pre_last; ++it){
            result += *it + separator;
        }
        result += *pre_last;
        return result;
    }

    template<typename Functor, typename BidirectionalIt, typename S>
    S join(Functor f, BidirectionalIt first, BidirectionalIt last, const S& separator)
    {
        S result;
        if(first == last){
            return result;
        }
        auto pre_last = --last;
        if(first == pre_last){
            return f(*first);
        }
        for(auto it = first; it != pre_last; ++it){
            result += f(*it) + separator;
        }
        result += f(*pre_last);
        return result;
    }

    template<typename S>
    S join2(const S& prefix, const S& suffix, const S& separator)
    {
        S result;
        if(prefix.empty()){
            return suffix;
        }
        if(suffix.empty()){
            return prefix;
        }
        result = prefix + separator + suffix;
        return result;
    }
}