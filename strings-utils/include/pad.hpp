/*
    File:    pad.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 02 April 2023 at 10:18 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <string>

#include "../include/string_traits.hpp"
namespace strings::pad{
    template<typename S>
    S pad_left(const S&                      str, 
               std::size_t                   n, 
               strings::traits::char_type<S> padded_char = strings::traits::make_space_char<S>())
    {
        S padded_str = str;
        if(str.size() < n)
        {
            padded_str = S(n - str.size(), padded_char) + str;
        }
        return padded_str;
    }

    template<typename S>
    S right_pad(const S&                      str, 
                std::size_t                   n, 
                strings::traits::char_type<S> padded_char = strings::traits::make_space_char<S>())
    {
        S padded_str = str;
        if(str.size() < n)
        {
            padded_str = str + S(n - str.size(), padded_char);
        }
        return padded_str;
    }

    std::string utf8_pad_left(const std::string& str, std::size_t n, char padded_char = ' ');

    std::string utf8_pad_right(const std::u32string& str, std::size_t n, char32_t padded_char = U' ');
}