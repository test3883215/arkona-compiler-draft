/*
    File:    trie.hpp
    Created: 20 April 2022 at 17:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
*/

#pragma once

#include <memory>
#include <set>
#include <string>
#include <type_traits>

#include "../include/trie_base.hpp"

namespace strings::trie{
    template<typename T, typename Container>
    class Trie : public details::Trie_base<T, Container>{
    public:
        Trie()                               = default;
        Trie(const Trie<T, Container>& orig) = default;
        virtual ~Trie<T, Container>()        = default;

        /**
         * \brief The function of inserting into the prefix tree.
         * \param [in] s Inserted set s.
         * \return       Index of the set s in the prefix tree.
         */
        std::size_t insert(const Container& s)
        {
            return details::Trie_base<T, Container>::insert(s);
        }

        /**
         * \brief Calculation of the maximum degree of the vertices of the prefix tree
         *        (the root of the tree is not taken into account).
         * \return The maximum degree of the vertices of the prefix tree
         *         (the root of the tree is not taken into account)
         */
        std::size_t maximal_degree() const
        {
            return details::Trie_base<T, Container>::maximal_degree();
        }

        Container get(std::size_t idx) const
        {
            if constexpr(std::is_same_v<Container, std::set<T>>)
            {
                std::set<T> s;
                std::size_t current = idx;
                for( ; current; current = this->node_buffer[current].parent){
                    s.insert(this->node_buffer[current].c);
                }
                return s;
            }
            if constexpr(std::is_same_v<Container, std::basic_string<T>>)
            {
                const std::size_t id_len = this->node_buffer[idx].path_len;
                auto              p      = std::make_unique<T[]>(id_len + 1);

                p[id_len]           = T{};
                std::size_t current = idx;
                std::size_t i       = id_len - 1;
                /* Since idx is the index of the element in node_buffer containing the last
                 * character of the inserted string, and each element of the vector node_buffer
                 * contains the field parent that points to the element with the previous
                 * character of the string, then to get the inserted string, which corresponds
                 * to the index idx, as an array of characters, it is necessary to walk from
                 * the element with index idx to the root. The characters of the inserted
                 * string will be read from the end to the beginning. */
                for( ; current; current = this->node_buffer[current].parent){
                    p[i--] = this->node_buffer[current].c;
                }
                const auto str = std::basic_string<T>(p.get());

                return str;
            }
            if constexpr(std::is_same_v<Container, std::vector<T>>)
            {
                const std::size_t id_len = this->node_buffer[idx].path_len;

                std::vector<T> v(id_len);

                /* Since idx is the index of the element in node_buffer containing the last
                 * character of the inserted string, and each element of the vector node_buffer
                 * contains the field parent that points to the element with the previous
                 * character of the string, then to get the inserted string, which corresponds
                 * to the index idx, as an array of characters, it is necessary to walk from
                 * the element with index idx to the root. The characters of the inserted
                 * string will be read from the end to the beginning. */
                std::size_t current = idx;
                std::size_t i       = id_len - 1;
                for( ; current; current = this->node_buffer[current].parent){
                    v[i--] = this->node_buffer[current].c;
                }

                return v;
            }
        }
    };
}