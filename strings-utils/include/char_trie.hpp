/*
    File:    char_trie.hpp
    Created: 08 May 2022 at 09:49 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
*/

#pragma once

#include <string>
#include "../include/trie.hpp"

namespace strings::trie{
    using Char_trie = Trie<char32_t, std::u32string>;
}