/*
    File:    strings_to_columns.hpp
    Created: 13 March 2022 at 18:12 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>

#include "../include/join.hpp"
#include "../include/string_traits.hpp"
#include "../include/trim.hpp"

namespace strings::columns{
    struct Format{
        std::size_t indent_                 = 0; //< number of spaces before each line
        std::size_t number_of_columns_      = 2; //< required number of columns
        std::size_t spaces_between_columns_ = 0; //< number of spaces between columns
    };

    namespace details{
        template<typename S>
        auto calculate_columns_width(std::size_t num_of_columns, const std::vector<S>& l)
        {
            std::vector<size_t> result = std::vector<size_t>(num_of_columns);
            for(std::size_t& z : result){
                z = 0;
            }
            std::size_t counter = 0;
            for(const auto& s : l){
                std::size_t i = counter % num_of_columns;
                result[i] = std::max(result[i], s.length());
                counter++;
            }
            return result;
        }
    }

    /**
    * \param [in] l   vector of the processed strings
    * \param [in] f   information about formatting
    * \param [in] d   separator
    *
    * \return strings from l are separated by d and then formatted corresponding
    *         to format information f
    */
    template<typename S>
    S strings_to_columns(const std::vector<S>&         l,
                         const Format&                 f,
                         strings::traits::char_type<S> d = strings::traits::make_comma_char<S>())
    {
        S           result;
        std::size_t num_of_strs                = l.size();
        std::size_t num_of_columns             = f.number_of_columns_;

        if(!num_of_strs || (num_of_columns <= 0)){
            return result;
        }
        if(num_of_columns > num_of_strs){
            num_of_columns = num_of_strs;
        }
        std::size_t num_of_rows                = num_of_strs / num_of_columns;
        std::size_t rest                       = num_of_strs % num_of_columns;

        using Row = std::vector<S>;

        S      delim;
        if(d){
            delim     += d;
        }
        auto        begin_indent                = S(f.indent_, strings::traits::make_space_char<S>());
        auto        interm_indent               = S(f.spaces_between_columns_, strings::traits::make_space_char<S>());
        auto        column_width                = details::calculate_columns_width(num_of_columns, l);

        std::vector<S> joined_rows(num_of_rows);
        for(std::size_t i = 0; i < num_of_rows; i++){
            Row         current_row;
            for(std::size_t j = 0; j < num_of_columns; j++){
                const auto        current_line = l[num_of_columns * i + j];
                const std::size_t num_of_padded_spaces = column_width[j] - current_line.length();
                const auto        temp = current_line + delim + S(num_of_padded_spaces, strings::traits::make_space_char<S>());
                current_row.push_back(temp);
            }
            const auto joined_row = begin_indent + strings::join::join([&interm_indent](const S& str) -> S{
                                                                           return str + interm_indent;
                                                                       },
                                                                       current_row.begin(),
                                                                       current_row.end(),
                                                                       S{});
//             result += strings::trim::trim_right(joined_row) + strings::traits::make_end_of_line<S>();
            joined_rows[i] = strings::trim::trim_right(joined_row);
        }
        result = strings::join::join(joined_rows.begin(), joined_rows.end(), strings::traits::make_end_of_line<S>());
        if(rest){
            result += strings::traits::make_end_of_line<S>();
            Row    current_row;
            for(std::size_t i = num_of_strs - rest; i < num_of_strs; i++){
                const auto        current_line         = l[i];
                const std::size_t num_of_padded_spaces = column_width[i + rest - num_of_strs] - current_line.length();
                const auto        temp                 = current_line + delim + S(num_of_padded_spaces, strings::traits::make_space_char<S>());
                current_row.push_back(temp);
            }
            const auto joined_row = begin_indent + strings::join::join([&interm_indent](const S& str) -> S{
                                                                           return str + interm_indent;
                                                                       },
                                                                       current_row.begin(),
                                                                       current_row.end(),
                                                                       S{});

            result += strings::trim::trim_right(joined_row);
        }

        return result;
    }
}