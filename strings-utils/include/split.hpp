/*
    File:    split.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 12 March 2022 at 18:53 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/


#pragma once

#include <cstddef>
#include <vector>

#include "../include/string_traits.hpp"

namespace strings::split{
    // The function split uses the approach suggested in https://stackoverflow.com/a/14267455 for std::string.
    // Our changes is rewriting this approach as a function template, to enable processing any string type.
    template<typename S>
    std::vector<S> split(const S& str, const S& delim)
    {
        std::size_t pos_start = 0;
        std::size_t pos_end;
        std::size_t delim_len = delim.length();

        S token;

        std::vector<S> result;

        while((pos_end = str.find(delim, pos_start)) != S::npos)
        {
            token = str.substr(pos_start, pos_end - pos_start);
            pos_start = pos_end + delim_len;
            result.push_back(token);
        }
        result.push_back(str.substr(pos_start));

        return result;
    }

    template<typename S>
    std::vector<S> split_by_end_of_line(const S& str)
    {
        const auto eol = strings::traits::make_end_of_line<S>();
        return split(str, eol);
    }

    template<typename S>
    std::vector<S> split_by_space(const S& str)
    {
        const auto sp = S{strings::traits::make_space_char<S>()};
        return split(str, sp);
    }
}