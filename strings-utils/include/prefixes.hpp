/*
    File:    prefixes.hpp
    Created: 10 May 2022 at 16:01 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <algorithm>
#include <cstddef>
#include <iterator>
#include <list>
#include <memory>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include "../include/char_trie.hpp"
#include "../include/attributed_trie.hpp"

namespace strings::prefixes{
    /**
     * @brief This function generates all prefixes of the specified string.
     *
     * For any string c_0c_1...c_{n_1} will be generated the list of strings
     *   c_0, c_0c_1, ...,c_0c_1...c_{n_1}
     * For example, for the string 'Hello' we will get
     *   H, He, Hel, Hell, Hello
     *
     * @tparam StringT The type of the string.
     *
     * @param s        The string to generate prefixes
     *
     * @return The list of prefixes of the string s.
     */
    template<typename StringT>
    auto prefixes_for_string(const StringT& s)
    {
        std::list<StringT> result;

        std::size_t len = s.length();
        for(std::size_t l = 1; l <= len; ++l){
            result.push_back(s.substr(0, l));
        }

        return result;
    }


    /**
     * @brief Helper metafunction to get the value type for the iterator type.
     *
     * @tparam It The iterator type
     */
    template<typename It>
    using iterator_value_type = typename std::iterator_traits<It>::value_type;

    /**
     * @brief Helper metafunction to get the first element type of the pair.
     *
     * This metafunction supposes that the value type for the iterator type is some std::pair< T, U >,
     * and returns the type T.
     *
     * @tparam It The iterator type to get the first element type of the pair.
     */
    template<typename It>
    using pair_first_type = typename iterator_value_type<It>::first_type;

    /**
     * @brief Helper metafunction to get the second element type of the pair.
     *
     * This metafunction supposes that the value type for the iterator type is some std::pair< T, U >,
     * and returns the type U.
     *
     * @tparam It The iterator type to get the second element type of the pair.
     */
    template<typename It>
    using pair_second_type = typename iterator_value_type<It>::second_type;

    /**
     * @brief This metafunction gets the string type and the attribute type for the attributed string,
     *
     * Let the attributed string type be the type of the form std::pair< S, A > and let It be such
     * iterator type that its value type is std::pair< S, A >. Finally, suppose that S is some string type.
     * This metafunction returns S as StringT, A as AttributeT.
     *
     * @tparam It The iterator type such that its value type is std::pair< StringT, AttributeT >.
     */
    template<typename It>
    struct Attributed_string_info
    {
        using StringT    = pair_first_type<It>;
        using AttributeT = pair_second_type<It>;
    };

    /**
     * @details This fuction takes the range of iterators, [first, last) such that the value type of
     * the iterator type is std::pair< StringT, AttributeT > where StringT is some string type,
     * and returns the list of prefixes of all attributed strings of the range.
     *
     * For any attributed string
     *     (s_0s_1...s_n, a)
     * we will have the sublist
     *     (s_0, AttributeT{}), (s_0s_1, AttributeT{}), ..., (s_0...s_{n-1}, AttributeT{}), (s_0s_1...s_n, a)
     *
     * @tparam It    The iterator type
     *
     * @param  first The start of the range.
     *
     * @param  last  The end of the range.
     *
     * @return The list of prefixes.
     */
    template<typename It>
    auto prefixes_for_attributed_strings(It first, It last)
    {
        std::list<typename std::iterator_traits<It>::value_type> result;

        using StringT    = typename Attributed_string_info<It>::StringT;
        using AttributeT = typename Attributed_string_info<It>::AttributeT;

        for(auto it = first; it != last; ++it)
        {
            auto added_prefixes = prefixes_for_string<StringT>(it->first);

        decltype(result) added_attributed_prefixes;
        for(const auto& s : added_prefixes)
        {
            added_attributed_prefixes.push_back({s, AttributeT{}});
        }
        added_attributed_prefixes.back().second = it->second;

        result.insert(result.end(), added_attributed_prefixes.begin(), added_attributed_prefixes.end());
        }
        return result;
    }

    /**
     * @brief This function groups prefixes of attributed strings.
     *
     * @tparam It The iterator type with attributed string as value type.
     *
     * @param  first The start of the range.
     *
     * @param  last  The end of the range.
     *
     * @return The vector of groups of prefixes.
     */
    template<typename It>
    auto group_prefixes_for_attributed_strings(It first, It last)
    {
        using value_type = typename std::iterator_traits<It>::value_type;
        using AttributeT = typename Attributed_string_info<It>::AttributeT;

        std::vector<std::list<value_type>> result;
        std::list<value_type>              group = {*first};

        for(auto it = ++first; it != last; ++it)
        {
            group.push_back(*it);
            if(it->second != AttributeT{})
            {
                result.push_back(group);
            group.clear();
            }
        }
        if(!group.empty()){
            result.push_back(group);
        }
        return result;
    }

    /**
     * @brief This function corrects groups of prefixes of attributed strings.
     *
     * @tparam Groups Type of container that contains groups.
     *
     * @param  groups The reference to container containing corrected groups.
     */
    template<typename Groups>
    void correct_groups(Groups& groups)
    {
        for(auto& group : groups)
        {
            if(group.size() > 1)
            {
                auto end_it = group.end();
                std::advance(end_it, -2);
                if(group.back().first == end_it->first){
                    group.erase(end_it);
                }
            }
        }
    }

    enum class Table_mode{
        Keyword_mode, Delimiter_mode
    };


    template<typename T>
    struct Prefix_maybe
    {
        static std::basic_string<T> get_prefix();
    };

    template<>
    struct Prefix_maybe<char>
    {
        static std::string get_prefix()
        {
            return "Maybe_";
        }
    };

    template<>
    struct Prefix_maybe<char32_t>
    {
        static std::u32string get_prefix()
        {
            return U"Maybe_";
        }
    };

    template<typename T>
    std::basic_string<T> make_prefix_maybe()
    {
        return Prefix_maybe<T>::get_prefix();
    }

    template<typename T>
    struct Str_Id
    {
        static std::basic_string<T> get_str();
    };

    template<>
    struct Str_Id<char>
    {
        static std::string get_str()
        {
            return "Id";
        }
    };

    template<>
    struct Str_Id<char32_t>
    {
        static std::u32string get_str()
        {
            return U"Id";
        }
    };

    template<typename T>
    std::basic_string<T> make_str_id()
    {
        return Str_Id<T>::get_str();
    }

    template<typename Groups>
    struct Groups_traits
    {
        using GroupT     = typename Groups::value_type;
        using GroupElemT = typename GroupT::value_type;
        using StringT    = typename GroupElemT::second_type;
        using CharT      = typename StringT::value_type;
    };

    /**
     * @brief This function corrects attributes of elements of groups of prefixes of attributed strings.
     *
     * @tparam Groups           Type of container that contains groups.
     *
     * @param  groups           The reference to container containing corrected groups.
     *
     * @param  mode             Processing mode.
     *
     * @param  additional_codes Codes with names 'Maybe_*' (only for Delimiter_mode)
     */
    template<typename Groups>
    void correct_attributes_for_groups(Groups&                                            groups,
                                       Table_mode                                         mode,
                                       std::set<typename Groups_traits<Groups>::StringT>& additional_codes)
    {
        using GroupT     = typename Groups::value_type;
        using GroupElemT = typename GroupT::value_type;
        using StringT    = typename GroupElemT::second_type;
        using CharT      = typename StringT::value_type;

        for(auto& group : groups)
        {
            const auto&              last_elem_of_group = group.back();
            std::basic_string<CharT> new_attribute;
            if(mode == Table_mode::Keyword_mode)
            {
                new_attribute = make_str_id<CharT>();
            }else
            {
                new_attribute = make_prefix_maybe<CharT>() + last_elem_of_group.second;
            }

            for(auto& e : group)
            {
                if(e.second.empty())
                {
                    e.second = new_attribute;
                    additional_codes.insert(new_attribute);
                }
            }
        }
    }

    /**
     * @brief This function prepares initial attributed strings to write into trie.
      *
     * @tparam It The iterator type with attributed string as value type.
     *
     * @param  first The start of the range.
     *
     * @param  last  The end of the range.
     *
     * @param  mode  Processing mode.
     *
     * @param  additional_codes Codes with names of the form 'Maybe_*' (only for Delimiter_mode).
     *
     * @return The list of prepared attributed strings.
    */
    template<typename It>
    auto pairs_to_write_into_trie(It                                                         first,
                                  It                                                         last,
                                  Table_mode                                                 mode,
                                  std::set<typename Attributed_string_info<It>::AttributeT>& additional_codes)
    {
        using value_type = typename std::iterator_traits<It>::value_type;

        // Calculate prefixes for all attributed strings.
        auto prefixes = prefixes_for_attributed_strings<It>(first, last);

        // Sort generated prefixes and delete duplicates,
        std::vector<value_type> vprefixes(prefixes.begin(), prefixes.end());
        std::sort(vprefixes.begin(), vprefixes.end());

        auto first_deleted = std::unique(vprefixes.begin(), vprefixes.end());
        vprefixes.erase(first_deleted, vprefixes.end());

        // Group prefixes.
        auto grouped = group_prefixes_for_attributed_strings(vprefixes.begin(), vprefixes.end());

        // Correct groups.
        correct_groups(grouped);
        correct_attributes_for_groups(grouped, mode, additional_codes);

        // Merge groups.
        std::list<value_type> result;
        for(const auto& group : grouped)
        {
            result.insert(result.end(), group.begin(), group.end());
        }

        return result;
    }

    /**
     * @brief This function compress prepared attributed strings.
      *
     * @tparam It The iterator type with attributed string as value type.
     *
     * @param  first The start of the range.
     *
     * @param  last  The end of the range.
     *
     * @param  trie  Trie to write attributed.
     *
     * @param  additional_codes Codes with names of the form 'Maybe_*' (only for Delimiter_mode).
     *
     * @return The list of attributed strings where attributes were replaced with its indices in the 'trie'.
    */
    template<typename It>
    auto compressed_pairs_to_write_into_trie(It                                                         first,
                                             It                                                         last,
                                             Table_mode                                                 mode,
                                             const std::shared_ptr<strings::trie::Char_trie>&           trie,
                                             std::set<typename Attributed_string_info<It>::AttributeT>& additional_codes)
    {
        auto non_compressed = pairs_to_write_into_trie(first, last, mode, additional_codes);

        using StringT        = typename Attributed_string_info<It>::StringT;
        using ret_value_type = std::pair<StringT, size_t>;

        std::list<ret_value_type> result;

        for(auto&& [str, attr] : non_compressed)
        {
            size_t idx = trie->insert(attr);
            result.push_back({str, idx});
        }

        return result;
    }

    /**
     * @brief This metafunction constructs type Attributed_trie from the iterator type It.
     *
     * @tparam It Iterator type that is such that It::value_type is attributed string.
     */
    template<typename It>
    struct Attributed_trie_info
    {
        using Attributed_trieT = strings::trie::attributed::Attributed_trie<typename Attributed_string_info<It>::StringT::value_type,
                                                                            typename Attributed_string_info<It>::AttributeT>;
    };

    template<typename It>
    using AttributedTrie = typename Attributed_trie_info<It>::Attributed_trieT;

    template<typename It>
    void write_compressed_pairs(It first, It last, const std::shared_ptr<AttributedTrie<It>>& trie)
    {
        for(auto it = first; it != last; ++it)
        {
            trie->insert(*it);
        }
    }
}