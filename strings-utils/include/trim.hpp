/*
    File:    trim.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 13 March 2022 at 18:06 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

#include <cstddef>

#include "../include/string_traits.hpp"

namespace strings::trim{
    template<typename S>
    S trim_right(const S& s)
    {
        if(s.empty()){
            return s;
        }

        std::size_t pos = s.length() - 1;
        while(pos > 0){
            if(s[pos] != strings::traits::make_space_char<S>()){
                break;
            }
            pos--;
        }
        if(pos == 0 && s[0] == strings::traits::make_space_char<S>()){
            return S{};
        }
        return s.substr(0, pos + 1);
    }
}