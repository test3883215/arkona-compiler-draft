/*
    File:    string_traits.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 12 March 2022 at 18:55 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

#include <string>

namespace strings::traits{
    template<typename S>
    struct End_of_line_trait{
        static S end_of_line();
    };

    template<typename S>
    using char_type = typename S::value_type;

    template<typename S>
    struct space_trait{
        static constexpr char_type<S> get_space_char();
    };

    template<typename S>
    struct comma_trait{
        static constexpr char_type<S> get_comma_char();
    };

    // Implementations of a trait for the end of line marker:
    template<>
    struct End_of_line_trait<std::string>
    {
        static std::string end_of_line()
        {
#if defined(_WIN32) || defined(_WIN64)
            return "\r\n";
#else
            return "\n";
#endif
        }
    };

    template<>
    struct End_of_line_trait<std::u32string>
    {
        static std::u32string end_of_line()
        {
#if defined(_WIN32) || defined(_WIN64)
            return U"\r\n";
#else
            return U"\n";
#endif
        }
    };

    template<typename S>
    S make_end_of_line()
    {
        return End_of_line_trait<S>::end_of_line();
    }

    // Implementations of a trait for the space character:
    template<>
    struct space_trait<std::string>{
        static constexpr char get_space_char()
        {
            return ' ';
        }
    };

    template<>
    struct space_trait<std::u32string>{
        static constexpr char32_t get_space_char()
        {
            return U' ';
        }
    };

    template<typename S>
    constexpr char_type<S> make_space_char()
    {
        return space_trait<S>::get_space_char();
    }

    // Implementations of a trait for the comma character:
    template<>
    struct comma_trait<std::string>{
        static constexpr char get_comma_char()
        {
            return ',';
        }
    };

    template<>
    struct comma_trait<std::u32string>{
        static constexpr char32_t get_comma_char()
        {
            return U',';
        }
    };

    template<typename S>
    constexpr char_type<S> make_comma_char()
    {
        return comma_trait<S>::get_comma_char();
    }
}