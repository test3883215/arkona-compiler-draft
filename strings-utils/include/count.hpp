/*
    File:    count.hpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 12 March 2022 at 19:12 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>

#include "../include/string_traits.hpp"

namespace strings::count{
    template<typename S>
    std::size_t get_num_of_specified_chars_from_left(const S& str, strings::traits::char_type<S> c)
    {
        const std::size_t len         = str.length();
        std::size_t       current_pos = 0;
        while((current_pos < len) && str[current_pos] == c)
        {
            current_pos++;
        }
        return current_pos;
    }

    template<typename S>
    std::size_t get_num_of_spaces_from_left(const S& str)
    {
        const auto space_char = strings::traits::make_space_char<S>();
        return get_num_of_specified_chars_from_left(str, space_char);
    }
}