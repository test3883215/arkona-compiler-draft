/*
    File:    idx_to_string.hpp
    Created: 09 May 2022 at 15:07 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <string>
#include "../include/char_trie.hpp"

namespace strings::trie{
    /**
     *  \param [in] t    pointer to prefix tree
     *  \param [in] idx  index of string in the prefix tree t
     *
     *  \return          string corresponding to the index idx
     *  */
    std::string idx_to_string(const std::shared_ptr<Char_trie>& t,
                              std::size_t                       idx,
                              const std::string&                default_value = std::string{});
}