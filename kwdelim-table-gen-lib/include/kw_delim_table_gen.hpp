/*
    File:    kw_delim_table_gen.hpp
    Created: 03 April 2023 at 20:31 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <map>
#include <string>
#include <vector>

#include "../../strings-utils/include/config_from_text.hpp"

struct Keyword_and_delimiters_info
{
    std::map<std::string, std::string> includes_;
    std::map<std::string, std::string> srcs_;
    std::u32string                     delimiter_begin_chars_;
    std::u32string                     keyword_begin_chars_;
};

using Text_config = std::vector<strings::config::Lexeme_with_code<std::u32string>>;

struct Text_configs
{
    Text_config kw_text_config_;
    Text_config delim_text_config_;
};

Keyword_and_delimiters_info build_kw_delim_tables_from_text_config(const Text_configs& configs);