/*
    File:    keywords_or_delimiters_transition_table_builder.hpp
    Created: 04 April 2023 at 21:19 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include "../../strings-utils/include/char_trie.hpp"
#include "../../strings-utils/include/attributed_trie.hpp"
#include "../../strings-utils/include/config_from_text.hpp"

enum class Table_kind
{
    Keyword, Delimiter
};

using Lexeme_with_code = strings::config::Lexeme_with_code<std::u32string>;

struct Info_for_transition_table
{
    std::vector<Lexeme_with_code> lexemes_and_codes_;
    std::u32string                additions_for_header_;
    std::u32string                additions_for_impl_;
    Table_kind                    kind_;
    std::u32string                namespace_name_;
    std::u32string                author_;
    std::vector<std::u32string>   emails_;
    std::u32string                lexem_info_type_name_;
    std::u32string                licence_id_;
};

struct Transition_table
{
    std::string              main_header_content_;
    std::string              main_header_name_;
    std::string              main_impl_content_;
    std::string              main_impl_name_;
    std::set<std::u32string> additional_codes_;
};

struct Auxiliary_files_content
{
    std::map<std::string, std::string> includes_;
    std::map<std::string, std::string> srcs_;
};

struct Info_for_enum_creating
{
    std::vector<std::u32string> codes_;
    Table_kind                  kind_;
    std::u32string              namespace_name_;
    std::u32string              author_;
    std::vector<std::u32string> emails_;
    std::u32string              licence_id_;
    std::u32string              underlying_type_name_;
};

struct To_string_info
{
    std::string header_content_;
    std::string header_name_;
    std::string impl_content_;
    std::string impl_name_;
};

class Transition_table_builder {
public:
    Transition_table_builder()
        : char_trie_{std::make_shared<strings::trie::Char_trie>()}
        , attributed_trie_{std::make_shared<strings::trie::attributed::Attributed_trie<char32_t, size_t>>()}
    {
    }

    Transition_table_builder(const Transition_table_builder& rhs) = default;
    ~Transition_table_builder()                                   = default;

    Transition_table operator()(const Info_for_transition_table& info);

    Auxiliary_files_content build_auxiliary_files_content(const std::string& namespace_name) const;

    std::string build_enum_for_codes(const Info_for_enum_creating& info);

    To_string_info build_enum_to_string_conversion_function(const Info_for_enum_creating& info) const;
private:
    std::shared_ptr<strings::trie::Char_trie>                                     char_trie_;
    std::shared_ptr<strings::trie::attributed::Attributed_trie<char32_t, size_t>> attributed_trie_;
};
