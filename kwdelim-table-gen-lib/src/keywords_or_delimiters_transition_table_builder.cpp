/*
    File:    keywords_or_delimiters_transition_table_builder.cpp
    Created: 04 April 2023 at 21:30 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/keywords_or_delimiters_transition_table_builder.hpp"

#include <ctime>
#include <set>
#include <utility>
// #include "../include/keywords_or_delimiters_transition_table_builder.hpp"
#include "../../strings-utils/include/prefixes.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
// #include "../include/idx_to_string.hpp"
#include "../../char-conv/include/char_conv.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../numbers/include/num_digits.hpp"
#include "../../strings-utils/include/strings_to_columns.hpp"
#include "../../char-conv/include/print_char32.hpp"

static const char* months[] = {
    "January", "February", "March",     "April",   "May",      "June",
    "July",    "August",   "September", "October", "November", "December"
};

static std::string current_datetime_as_string()
{
    std::time_t now             = time(nullptr);
    std::tm*    utc_datetime    = std::gmtime(&now);
    std::string fmt_string      = "{:02} {} {} at {:02}:{:02} UTC";
    std::string result          = fmt::format(fmt_string,
                                              utc_datetime->tm_mday,
                                              months[utc_datetime->tm_mon],
                                              1900 + utc_datetime->tm_year,
                                              utc_datetime->tm_hour,
                                              utc_datetime->tm_min);
    return result;
}

static const char* trans_table_header_fmt_string = R"~(/*
    File:    {0}
    Created: {1}
    Author:  {2}
    E-mails: {3}
    License: {4}
*/

#pragma once

{5}
namespace {6} {{
    class {7} {{
    public:
        static constexpr int there_is_no_transition = -1;

        int init_state(char32_t c) const;
        int next_state(int state, char32_t c) const;
        {8} lexeme_for_state(int state) const;
    }};
}};)~";

static const char* trans_table_impl_fmt_string = R"~(/*
    File:    {0}
    Created: {1}
    Author:  {2}
    E-mails: {3}
    License: {4}
*/

#include <cstddef>
#include <cstdint>
#include <iterator>
#include "../include/{5}"
#include "../include/get_init_state.hpp"
#include "../include/state_for_char.hpp"
#include "../include/trans-elem.hpp"
#include "../include/search_char.hpp"

namespace {6} {{
    static const State_for_char init_table[] = {{
{7}
    }};

    static constexpr std::size_t init_table_size = std::size(init_table);

    static const char32_t trans_strs[] = {{
{8}
    }};

    static const trans_table::Trans_elem trans_table[] = {{
{9}
    }};

    int {10}::init_state(char32_t c) const
    {{
        return get_init_state(c, init_table, init_table_size);
    }}

    int {10}::next_state(int state, char32_t c) const
    {{
        int             result           = {10}::there_is_no_transition;
        auto            elem             = trans_table[state];
        const char32_t* transition_chars = trans_strs + elem.offset_of_first_transition_char_;
        int             y                = search_char(c, transition_chars);
        if(y != THERE_IS_NO_CHAR){{
            result = elem.first_state_ + y;
        }}
        return result;
    }}

    {11} {10}::lexeme_for_state(int state) const
    {{
        {11} result;
        result.code_     = trans_table[state].code_;
        result.id_index_ = 0;
        return result;
    }}
}};)~";

struct Info_for_constructing
{
    std::string main_header_name_;
    std::string main_impl_name_;
    std::string datetime_;
    std::string author_;
    std::string emails_;
    std::string licence_;
    std::string lexeme_info_type_name_;
    std::string namespace_name_;
    std::string class_name_;
    std::string additions_for_header_;
    Table_kind  kind_;
};

static std::string build_author(const std::u32string& author)
{
    return author.empty() ? "Unknown" : u32string_to_utf8(author);
}

static std::string build_licence(const std::u32string& licence)
{
    return licence.empty() ? "None" : u32string_to_utf8(licence);
}

static std::string build_lexeme_info_type_name(const std::u32string& type_name)
{
    return type_name.empty() ? "void" : u32string_to_utf8(type_name);
}

static std::string build_namespace_name(const std::u32string& name)
{
    return name.empty() ? "details" : u32string_to_utf8(name);
}

static std::string build_emails_section(const std::vector<std::u32string>& emails)
{
    std::string result;

    if(emails.empty())
    {
        return "Unknown";
    }

    result = u32string_to_utf8(emails[0]);

    if(emails.size() == 1)
    {
        return result;
    }

    result += "\n" + strings::join::join([](const std::u32string& s)
                                         {
                                             return std::string(13, ' ') + u32string_to_utf8(s);
                                         },
                                         emails.begin() + 1,
                                         emails.end(),
                                         std::string{"\n"});

    return result;
}

static std::string build_main_header_filename(Table_kind kind)
{
    std::string result = (kind == Table_kind::Keyword) ? "keyw_trans_table.hpp" : "delim_trans_table.hpp";
    return result;
}

static std::string build_main_impl_filename(Table_kind kind)
{
    std::string result = (kind == Table_kind::Keyword) ? "keyw_trans_table.cpp" : "delim_trans_table.cpp";
    return result;
}

static std::string build_table_class_name(Table_kind kind)
{
    std::string result = (kind == Table_kind::Keyword) ? "Keyw_trans_table" : "Delim_trans_table";
    return result;
}

static std::string build_trans_table_header(const Info_for_constructing& info)
{
    std::string result;

    const auto& filename             = info.main_header_name_;
    const auto& datetime             = info.datetime_;
    const auto& author               = info.author_;
    const auto& emails               = info.emails_;
    const auto& licence              = info.licence_;
    const auto& lexeme_info_name     = info.lexeme_info_type_name_;
    const auto& namespace_name       = info.namespace_name_;
    const auto& table_class          = info.class_name_;
    const auto& additions_for_header = info.additions_for_header_;

    result = fmt::format(trans_table_header_fmt_string,
                         filename,
                         datetime,
                         author,
                         emails,
                         licence,
                         additions_for_header,
                         namespace_name,
                         table_class,
                         lexeme_info_name);
    return result;
}


static Info_for_constructing build_info_for_constructing(const Info_for_transition_table& info)
{
    Info_for_constructing result;

    result.main_header_name_      = build_main_header_filename(info.kind_);
    result.main_impl_name_        = build_main_impl_filename(info.kind_);
    result.datetime_              = current_datetime_as_string();
    result.author_                = build_author(info.author_);
    result.emails_                = build_emails_section(info.emails_);
    result.licence_               = build_licence(info.licence_id_);
    result.lexeme_info_type_name_ = build_lexeme_info_type_name(info.lexem_info_type_name_);
    result.namespace_name_        = build_namespace_name(info.namespace_name_);
    result.class_name_            = build_table_class_name(info.kind_);
    result.additions_for_header_  = u32string_to_utf8(info.additions_for_header_);
    result.kind_                  = info.kind_;

    return result;
}

static std::string build_init_table_content(const strings::trie::attributed::Inits<char32_t>& init_table)
{
    std::string                 result;

    std::size_t                 max_len = max_num_of_digits([](auto& p){return p.first;},
                                                            init_table.begin(),
                                                            init_table.end(),
                                                            static_cast<size_t>(10));

    std::vector<std::u32string> elems;
    for(const auto& [state, ch] : init_table)
    {
        const auto state_as_utf8_str = std::to_string(state);
        const auto state_as_str      = utf8_to_u32string(state_as_utf8_str.c_str());
        const auto ch_as_utf8_str    = show_char32(ch);
        const auto ch_as_str         = U"U"+ utf8_to_u32string(ch_as_utf8_str.c_str());
        const auto fst_part          = state_as_str + std::u32string(max_len - state_as_str.length(), U' ') + U",";
        const auto current           = U"{" + fst_part + U" " + ch_as_str + U"}";

        elems.push_back(current);
    }

    strings::columns::Format f;
    f.indent_                 = 8;
    f.number_of_columns_      = 4;
    f.spaces_between_columns_ = 1;

    const auto content = strings::columns::strings_to_columns(elems, f);

    result = u32string_to_utf8(content);
    return result;
}

static std::string build_trans_strs(const std::vector<std::pair<std::u32string, size_t>>& offsets)
{
    std::string              result;

    std::vector<std::string> strs;
    for(const auto& [trans_str, offset] : offsets)
    {
        std::vector<std::string> chars_as_strings;
        for(char32_t c : trans_str)
        {
            auto temp = "U" + show_char32(c);
            chars_as_strings.push_back(temp);
        }
        chars_as_strings.push_back(std::string{R"~(U'\0')~"});
        auto current = std::string(8, ' ') +
                       strings::join::join(chars_as_strings.begin(), chars_as_strings.end(), std::string{", "});
        strs.push_back(current);
    }

    result = strings::join::join(strs.begin(), strs.end(), std::string{",\n"});
    return result;
}

static const char*          trans_table_elem_fmt = "{{{0}, {1}, {2}}}";
static const std::u32string id_str               = U"Id";

static std::string build_trans_table_content(const strings::trie::attributed::Jumps<char32_t, size_t>&  jumps,
                                             const std::map<std::u32string, size_t>&                    str_offset_map,
                                             const std::shared_ptr<strings::trie::Char_trie>&           char_trie,
                                             Table_kind                                                 kind)
{
    std::string result;

    std::string lexeme_kind_str;
    std::string code_prefix;

    if(kind == Table_kind::Keyword)
    {
        lexeme_kind_str = "Keyword";
        code_prefix     = "Keyword_kind";
    }else{
        lexeme_kind_str = "Delimiter";
        code_prefix     = "Delimiter_kind";
    }

    std::vector<std::string> elems;

    for(auto&& [jump_chars, code, first_state] : jumps)
    {
        size_t         jump_str_offset = str_offset_map.at(jump_chars);
        std::u32string code_str        = char_trie->get(code);
        std::string    lexeme_code_str;

        if(code_str == id_str)
        {
            lexeme_code_str = "{Lexem_kind::Id, 0}";
        }else{
            lexeme_code_str = fmt::format("{{Lexem_kind::{0}, {1}::{2}}}",
                                          lexeme_kind_str,
                                          code_prefix, 
                                          u32string_to_utf8(code_str));
        }

        std::string elem = std::string(8, ' ')               +
                           fmt::format(trans_table_elem_fmt,
                                       jump_str_offset,
                                       lexeme_code_str,
                                       first_state);


        elems.push_back(elem);
    }

    result = strings::join::join(elems.begin(), elems.end(), std::string{",\n"});
    return result;
}

static std::string build_trans_table_impl(const Info_for_constructing&                              info,
                                          const strings::trie::attributed::Inits<char32_t>&         init_table,
                                          const std::vector<std::pair<std::u32string, size_t>>&     offsets,
                                          const strings::trie::attributed::Jumps<char32_t, size_t>& jumps,
                                          const std::map<std::u32string, size_t>&                   str_offset_map,
                                          const std::shared_ptr<strings::trie::Char_trie>&          char_trie,
                                          Table_kind                                                kind)
{
    std::string result;

    const auto& main_impl_name        = info.main_impl_name_;
    const auto& datetime              = info.datetime_;
    const auto& author                = info.author_;
    const auto& emails                = info.emails_;
    const auto& licence               = info.licence_;
    const auto& main_header_name      = info.main_header_name_;
    const auto& namespace_name        = info.namespace_name_;
    const auto  init_table_str        = build_init_table_content(init_table);
    const auto  trans_strs            = build_trans_strs(offsets);
    const auto  trans_table_content   = build_trans_table_content(jumps,
                                                                  str_offset_map,
                                                                  char_trie,
                                                                  kind);
    const auto& class_name            = info.class_name_;
    const auto& lexeme_info_type_name = info.lexeme_info_type_name_;

    result = fmt::format(trans_table_impl_fmt_string,
                         main_impl_name,
                         datetime,
                         author,
                         emails,
                         licence,
                         main_header_name,
                         namespace_name,
                         init_table_str,
                         trans_strs,
                         trans_table_content,
                         class_name,
                         lexeme_info_type_name);
    return result;
}

Transition_table Transition_table_builder::operator()(const Info_for_transition_table& info)
{
    Transition_table result;

    std::vector<std::pair<std::u32string, std::u32string>> pairs_of_lexemes_and_codes;
    const auto& lexemes_and_codes = info.lexemes_and_codes_;
    for(const auto& [lexeme, code] : lexemes_and_codes)
    {
        pairs_of_lexemes_and_codes.push_back({lexeme, code});
    }

    strings::prefixes::Table_mode mode = (info.kind_ == Table_kind::Keyword) ? 
                                                        strings::prefixes::Table_mode::Keyword_mode : 
                                                        strings::prefixes::Table_mode::Delimiter_mode;

    std::set<std::u32string> additional_codes;
    auto compressed_pairs = strings::prefixes::compressed_pairs_to_write_into_trie(pairs_of_lexemes_and_codes.begin(),
                                                                                   pairs_of_lexemes_and_codes.end(),
                                                                                   mode,
                                                                                   char_trie_,
                                                                                   additional_codes);

    strings::prefixes::write_compressed_pairs(compressed_pairs.begin(), compressed_pairs.end(), attributed_trie_);

    auto jumps            = attributed_trie_->jumps();
    auto str_offset_map   = strings::trie::attributed::get_jump_strings_offsets(jumps);
    auto offsets_vector   = strings::trie::attributed::get_jump_strings_offsets_as_vector(str_offset_map);

    std::map<std::string, std::string> headers;

    const auto        info_for_constructing = build_info_for_constructing(info);
    const std::string table_header_filename = build_main_header_filename(info.kind_);

    result.main_header_content_ = build_trans_table_header(info_for_constructing);
    result.main_header_name_    = info_for_constructing.main_header_name_;
    result.main_impl_content_   = build_trans_table_impl(info_for_constructing,
                                                         jumps.init_table,
                                                         offsets_vector,
                                                         jumps.jumps,
                                                         str_offset_map,
                                                         char_trie_,
                                                         info.kind_);
    result.main_impl_name_      = info_for_constructing.main_impl_name_;
    result.additional_codes_    = additional_codes;

    return result;
}

static const char* get_init_state_hpp = R"~(/*
    File:    get_init_state.hpp
    Created: 13 December 2015 at 09:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include "../include/state_for_char.hpp"
/* The function get_init_state initializes a finite automaton. Namely, this function
 * looking for the character sym in the table sts. Here sts is an array of
 * pairs (state, character), and number of elements in sts is n. The search is
 * performed in accordance with the binary search algorithm B from the section 6.2.1
 * of "Knuth D.E. The Art of Computer programming. V.3. Sorting and search. 2nd ed.
 * --- Addison Wesley, 1998.". */
int get_init_state(char32_t sym, const State_for_char sts[], int n);)~";

static const char* state_for_char_hpp = R"~(/*
    File:    state_for_char.hpp
    Created: 10 February 2019 at 10:39 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

struct State_for_char{
    unsigned st_;
    char32_t c_;
};)~";

static const char* trans_elem_hpp = R"~(/*
    File:    trans-elem.hpp
    Created: 19 October 2019 at 07:15 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstdint>
#include "../include/lexeme.hpp"
namespace trans_table{{
/**
 * Element of transition table.
 */
    struct Trans_elem{{
        uint16_t offset_of_first_transition_char_;
        {0}::Lexem_code code_;
        uint16_t first_state_;
    }};
}};)~";

static const char* search_char_hpp = R"~(/*
    File:    search_char.hpp
    Created: 13 December 2015 at 09:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#define THERE_IS_NO_CHAR (-1)
/**
 * \brief This function searches for a given character of type char32_t
 *        in a string consisting of characters of this type and ending
 *        with a null character.
 *
 * \param [in] c     The wanted character.
 * \param [in] array The string in which the symbol is searched for.
 * \return           Offset (in characters) from the beginning of the string,
 *                   if the desired character is in the string, and (-1)
 *                   otherwise
 */
int search_char(char32_t c, const char32_t* array);)~";

static const char* get_init_state_cpp = R"~(/*
    File:    get_init_state.cpp
    Created: 13 December 2015 at 09:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/get_init_state.hpp"
int get_init_state(char32_t sym, const State_for_char sts[], int n)
{
    int lower, upper, middle;
    lower = 0; upper = n - 1;
    while(lower <= upper){
        middle     = (lower + upper) >> 1;
        char32_t c = sts[middle].c_;
        if(sym == c){
            return sts[middle].st_;
        }else if(sym > c){
            lower = middle + 1;
        }else{
            upper = middle - 1;
        }
    }
    return -1;
})~";

static const char* search_char_cpp = R"~(/*
    File:    search_char.cpp
    Created: 13 December 2015 at 09:05 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/search_char.hpp"
int search_char(char32_t c, const char32_t* array)
{
    char32_t ch;
    int      curr_pos = 0;
    for(char32_t* p = const_cast<char32_t*>(array); (ch = *p++); ){
        if(ch == c){
            return curr_pos;
        }
        curr_pos++;
    }
    return THERE_IS_NO_CHAR;
})~";

Auxiliary_files_content Transition_table_builder::build_auxiliary_files_content(const std::string& namespace_name) const
{
    Auxiliary_files_content result;

    result.includes_["get_init_state.hpp"] = get_init_state_hpp;
    result.includes_["state_for_char.hpp"] = state_for_char_hpp;
    result.includes_["trans-elem.hpp"]     = fmt::format(trans_elem_hpp, namespace_name);
    result.includes_["search_char.hpp"]    = search_char_hpp;
    result.srcs_["get_init_state.cpp"]     = get_init_state_cpp;
    result.srcs_["search_char.cpp"]        = search_char_cpp;

    return result;
}

static const char* enum_codes_header_fmt = R"~(/*
    File:    {0}
    Created: {1}
    Author:  {2}
    E-mails: {3}
    License: {4}
*/

#pragma once

#include <cstdint>
namespace {5} {{
    enum {6} : {7}{{
{8}
    }};
}};)~";

static std::string build_underlying_type_name(const std::u32string& name)
{
    return name.empty() ? "uint8_t" : u32string_to_utf8(name);
}

static std::string build_codes_filename(Table_kind kind)
{
    std::string result = (kind == Table_kind::Keyword) ? "keyword_kind.hpp" : "delimiter_kind.hpp";
    return result;
}

static std::string build_enum_name(Table_kind kind)
{
    std::string result = (kind == Table_kind::Keyword) ? "Keyword_kind" : "Delimiter_kind";
    return result;
}

std::string Transition_table_builder::build_enum_for_codes(const Info_for_enum_creating& info)
{
    std::string result;

    const auto datetime       = current_datetime_as_string();
    const auto author         = build_author(info.author_);
    const auto emails         = build_emails_section(info.emails_);
    const auto licence        = build_licence(info.licence_id_);
    const auto namespace_name = build_namespace_name(info.namespace_name_);

    strings::columns::Format f;
    f.indent_                 = 8;
    f.number_of_columns_      = 4;
    f.spaces_between_columns_ = 1;

    const auto enum_content         = u32string_to_utf8(strings::columns::strings_to_columns(info.codes_, f));
    const auto underlying_type_name = build_underlying_type_name(info.underlying_type_name_);
    const auto filename             = build_codes_filename(info.kind_);
    const auto enum_name            = build_enum_name(info.kind_);

    result = fmt::format(enum_codes_header_fmt,
                         filename,
                         datetime,
                         author,
                         emails,
                         licence,
                         namespace_name,
                         enum_name,
                         underlying_type_name,
                         enum_content);
    return result;
}

static const char* to_string_conversion_function_header_fmt = R"~(/*
    File:    {0}
    Created: {1}
    Author:  {2}
    E-mails: {3}
    License: {4}
*/

#pragma once

#include <string>
#include "../include/{5}.hpp"
namespace {6} {{
    std::string {7}_to_string({8} code);
}};)~";

static std::string build_to_string_conversion_function_header_name(Table_kind kind)
{
    return (kind == Table_kind::Keyword) ? "keyword_kind_to_string.hpp" : "delimiter_kind_to_string.hpp";
}

static std::string build_to_string_conversion_function_arg_type(Table_kind kind)
{
    return (kind == Table_kind::Keyword) ? "Keyword_kind" : "Delimiter_kind";
}

static std::string build_to_string_conversion_function_name_prefix(Table_kind kind)
{
    return (kind == Table_kind::Keyword) ? "keyword_kind" : "delimiter_kind";
}

static std::string build_to_string_conversion_function_include(Table_kind kind)
{
    return (kind == Table_kind::Keyword) ? "keyword_kind" : "delimiter_kind";
}

static const char* to_string_conversion_function_impl_fmt = R"~(/*
    File:    {0}
    Created: {1}
    Author:  {2}
    E-mails: {3}
    License: {4}
*/

#include "../include/{5}"
namespace {{
    static const std::string codes_strings[] = {{
{6}
    }};
}};

namespace {7} {{
    std::string {8}_to_string({9} code)
    {{
        return codes_strings[static_cast<unsigned>(code)];
    }}
}};)~";

static std::string build_to_string_conversion_function_impl_name(Table_kind kind)
{
    return (kind == Table_kind::Keyword) ? "keyword_kind_to_string.cpp" : "delimiter_kind_to_string.cpp";
}

static std::vector<std::u32string> quote_codes(const std::vector<std::u32string>& codes)
{
    if(codes.empty())
    {
        return codes;
    }

    std::size_t                 num_of_codes = codes.size();
    std::vector<std::u32string> result(num_of_codes);
    for(std::size_t i = 0; i < num_of_codes; ++i)
    {
        result[i] = U"\"" + codes[i] + U"\"";
    }

    return result;
}

To_string_info Transition_table_builder::build_enum_to_string_conversion_function(const Info_for_enum_creating& info) const
{
    To_string_info result;

    const auto header_name          = build_to_string_conversion_function_header_name(info.kind_);
    const auto datetime             = current_datetime_as_string();
    const auto author               = build_author(info.author_);
    const auto emails               = build_emails_section(info.emails_);
    const auto licence              = build_licence(info.licence_id_);
    const auto namespace_name       = build_namespace_name(info.namespace_name_);
    const auto arg_type_str         = build_to_string_conversion_function_arg_type(info.kind_);
    const auto codes_include        = build_to_string_conversion_function_include(info.kind_);
    const auto function_name_prefix = build_to_string_conversion_function_name_prefix(info.kind_);
    const auto impl_name            = build_to_string_conversion_function_impl_name(info.kind_);

    strings::columns::Format f;
    f.indent_                 = 8;
    f.number_of_columns_      = 4;
    f.spaces_between_columns_ = 1;

    const auto codes_strings        = u32string_to_utf8(strings::columns::strings_to_columns(quote_codes(info.codes_), f));

    result.header_content_ = fmt::format(to_string_conversion_function_header_fmt,
                                         header_name,
                                         datetime,
                                         author,
                                         emails,
                                         licence,
                                         codes_include,
                                         namespace_name,
                                         function_name_prefix,
                                         arg_type_str);
    result.header_name_    = header_name;
    result.impl_content_   = fmt::format(to_string_conversion_function_impl_fmt,
                                         impl_name,
                                         datetime,
                                         author,
                                         emails,
                                         licence,
                                         header_name,
                                         codes_strings,
                                         namespace_name,
                                         function_name_prefix,
                                         arg_type_str);
    result.impl_name_      = impl_name;

    return result;
}