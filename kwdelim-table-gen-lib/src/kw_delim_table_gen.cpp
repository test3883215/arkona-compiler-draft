/*
    File:    kw_delim_table_gen.cpp
    Created: 03 April 2023 at 20:33 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/kw_delim_table_gen.hpp"
#include "../include/keywords_or_delimiters_transition_table_builder.hpp"
#include <set>
#include <vector>

namespace{
    Transition_table build_keyword_table(const Text_config& kw_text_config)
    {
        Info_for_transition_table info;

        info.lexemes_and_codes_    = kw_text_config;
        info.additions_for_header_ = UR"~(#include "../include/lexeme.hpp")~";
        info.additions_for_impl_   = U"";
        info.kind_                 = Table_kind::Keyword;
        info.namespace_name_       = U"scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                     };
        info.licence_id_           = U"GPLv3";
        info.lexem_info_type_name_ = U"LexemeInfo";

        Transition_table_builder  table_builder;

        return table_builder(info);
    }

    Auxiliary_files_content build_aux_files_content()
    {
        Transition_table_builder  table_builder;
        return table_builder.build_auxiliary_files_content("scanner");
    }

    std::string build_enum_header_for_keyword_kinds(const Text_config& kw_text_config)
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : kw_text_config)
        {
            codes.push_back(code);
        }

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Keyword;
        info.namespace_name_       = U"scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_for_codes(info);
    }

    std::u32string get_first_chars_of_lexemes(const Text_config& text_config)
    {
        std::set<char32_t> fst_chars_set;
        for(auto&& [lexeme_str, _] : text_config)
        {
            fst_chars_set.insert(lexeme_str[0]);
        }

        std::u32string result;
        for(char32_t c : fst_chars_set)
        {
            result += c;
        }
        return result;
    }

    Transition_table build_delimiter_table(const Text_config& delim_text_config)
    {
        Info_for_transition_table info;

        info.lexemes_and_codes_    = delim_text_config;
        info.additions_for_header_ = UR"~(#include "../include/lexeme.hpp")~";
        info.additions_for_impl_   = U"";
        info.kind_                 = Table_kind::Delimiter;
        info.namespace_name_       = U"scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                     };
        info.licence_id_           = U"GPLv3";
        info.lexem_info_type_name_ = U"LexemeInfo";

        Transition_table_builder  table_builder;

        return table_builder(info);
    }
    
    std::string build_enum_header_for_delimiter_kinds(const std::set<std::u32string>& additional_codes, 
                                                      const Text_config&              delim_text_config)
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : delim_text_config)
        {
            codes.push_back(code);
        }

        codes.insert(codes.end(), additional_codes.begin(), additional_codes.end());

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Delimiter;
        info.namespace_name_       = U"scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_for_codes(info);
    }

    To_string_info build_keyword_kind_to_string_conversion(const Text_config& kw_text_config)
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : kw_text_config)
        {
            codes.push_back(code);
        }

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Keyword;
        info.namespace_name_       = U"scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_to_string_conversion_function(info);
    }

    To_string_info build_delimiter_kind_to_string_conversion(const std::set<std::u32string>& additional_codes, 
                                                             const Text_config&              delim_text_config)
    {
        std::vector<std::u32string> codes;
        for(const auto& [_, code] : delim_text_config)
        {
            codes.push_back(code);
        }

        codes.insert(codes.end(), additional_codes.begin(), additional_codes.end());

        Info_for_enum_creating info;
        info.codes_                = codes;
        info.kind_                 = Table_kind::Delimiter;
        info.namespace_name_       = U"scanner";
        info.author_               = U"Гаврилов Владимир Сергеевич";
        info.emails_               = {
                                        U"vladimir.s.gavrilov@gmail.com",
                                        U"gavrilov.vladimir.s@mail.ru",
                                     };
        info.licence_id_           = U"GPLv3";
        info.underlying_type_name_ = U"uint8_t";

        Transition_table_builder  table_builder;

        return table_builder.build_enum_to_string_conversion_function(info);
    }
}

Keyword_and_delimiters_info build_kw_delim_tables_from_text_config(const Text_configs& configs)
{
    Keyword_and_delimiters_info result;

    const auto kw_table                                       = build_keyword_table(configs.kw_text_config_);
    result.includes_[kw_table.main_header_name_]              = kw_table.main_header_content_;
    result.srcs_[kw_table.main_impl_name_]                    = kw_table.main_impl_content_;

    const auto aux_files                                      = build_aux_files_content();
    result.includes_.insert(aux_files.includes_.begin(), aux_files.includes_.end());
    result.srcs_.insert(aux_files.srcs_.begin(), aux_files.srcs_.end());

    result.includes_["keyword_kind.hpp"]                      = build_enum_header_for_keyword_kinds(configs.kw_text_config_);
    result.keyword_begin_chars_                               = get_first_chars_of_lexemes(configs.kw_text_config_);

    const auto delim_table                                    = build_delimiter_table(configs.delim_text_config_);
    result.includes_[delim_table.main_header_name_]           = delim_table.main_header_content_;
    result.srcs_[delim_table.main_impl_name_]                 = delim_table.main_impl_content_;

    result.includes_["delimiter_kind.hpp"]                    = build_enum_header_for_delimiter_kinds(delim_table.additional_codes_, 
                                                                                                      configs.delim_text_config_);
    result.delimiter_begin_chars_                             = get_first_chars_of_lexemes(configs.delim_text_config_);

    const auto keyword_kind_to_str_func                       = build_keyword_kind_to_string_conversion(configs.kw_text_config_);
    result.includes_[keyword_kind_to_str_func.header_name_]   = keyword_kind_to_str_func.header_content_;
    result.srcs_[keyword_kind_to_str_func.impl_name_]         = keyword_kind_to_str_func.impl_content_;

    const auto delimiter_kind_to_str_func                     = build_delimiter_kind_to_string_conversion(delim_table.additional_codes_, 
                                                                                                          configs.delim_text_config_);
    result.includes_[delimiter_kind_to_str_func.header_name_] = delimiter_kind_to_str_func.header_content_;
    result.srcs_[delimiter_kind_to_str_func.impl_name_]       = delimiter_kind_to_str_func.impl_content_;

    return result;    
}