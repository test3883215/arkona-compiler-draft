/*
    File:    lexeme_to_str.hpp
    Created: 06 June 2021 at 12:41 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>
#include <memory>
#include "../include/lexeme.hpp"
#include "../../strings-utils/include/char_trie.hpp"
namespace scanner{
    std::string to_string(const LexemeInfo&                                li,
                          const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                          const std::shared_ptr<strings::trie::Char_trie>& strs_trie);
}