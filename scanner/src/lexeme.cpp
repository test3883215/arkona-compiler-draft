/*
    File:    lexeme.cpp
    Created: 09 June 2021 at 15:55 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/


#include <quadmath.h>
#include "../include/lexeme.hpp"

namespace scanner {
    bool operator==(const LexemeInfo& lhs, const LexemeInfo& rhs)
    {
        bool result = true;
        if(lhs.code_.kind_ != rhs.code_.kind_){
            return false;
        }
        switch(lhs.code_.kind_){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                break;
            case Lexem_kind::Keyword: case Lexem_kind::Delimiter:
                return lhs.code_.subkind_ == rhs.code_.subkind_;
                break;
            case Lexem_kind::Id:
                return lhs.id_index_ == rhs.id_index_;
                break;
            case Lexem_kind::String:
                return lhs.str_index_ == rhs.str_index_;
                break;
            case Lexem_kind::Integer:
                return lhs.int_val_ == rhs.int_val_;
                break;
            case Lexem_kind::Float:
                return (lhs.code_.subkind_ == rhs.code_.subkind_)       &&
                       fabsq(lhs.float_val_ - rhs.float_val_) < 1e-22q;
                break;
            case Lexem_kind::Complex:
                return (lhs.code_.subkind_ == rhs.code_.subkind_) &&
                       cabsq(lhs.complex_val_ - rhs.complex_val_ ) < FLT128_EPSILON;
                break;
            case Lexem_kind::Quat:
                return (lhs.code_.subkind_ == rhs.code_.subkind_)                           &&
                       sqrtq(quat::norm_in_square(lhs.quat_val_ - rhs.quat_val_)) < 1e-22q;
                break;
            case Lexem_kind::Char: case Lexem_kind::Encoded_char:
                return lhs.char_val_ == rhs.char_val_;
                break;
        }
        return result;
    }
};