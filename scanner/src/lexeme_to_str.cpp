/*
    File:    lexeme_to_str.cpp
    Created: 09 June 2021 at 14:29 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/lexeme_to_str.hpp"
#include "../include/delimiter_kind_to_string.hpp"
#include "../include/keyword_kind_to_string.hpp"
#include "../../strings-utils/include/idx_to_string.hpp"
#include "../../numbers/include/float128_to_string.hpp"
#include "../../numbers/include/int128_to_str.hpp"
#include "../../char-conv/include/char_conv.hpp"
#include "../../char-conv/include/print_char32.hpp"

namespace{
    const std::string kind_strs[] = {
        "Nothing",    "UnknownLexem", "Keyword ", "Id ",
        "Delimiter ", "Char ",        "String ",  "Integer ",
        "Float ",     "Complex ",     "Quat ",    "Encoded_char "
    };

    const std::string float_subkind_strs[] = {
        "[Float32] ", "[Float64] ", "[Float80] ", "[Float128] "
    };

    const std::string complex_subkind_strs[] = {
        "[Complex32] ", "[Complex64] ", "[Complex80] ", "[Complex128] "
    };

    const std::string quat_subkind_strs[] = {
        "[Quat32] ", "[Quat64] ", "[Quat80] ", "[Quat128] "
    };
};

namespace scanner{
    std::string to_string(const LexemeInfo&                                li,
                          const std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                          const std::shared_ptr<strings::trie::Char_trie>& strs_trie)
    {
        std::string result;

        auto        lc     = li.code_;
        auto        lk     = lc.kind_;
        auto        lsk    = lc.subkind_;
        result             = kind_strs[static_cast<unsigned>(lk)];

        switch(lk){
            case Lexem_kind::Nothing: case Lexem_kind::UnknownLexem:
                break;
            case Lexem_kind::Id:
                result += idx_to_string(ids_trie, li.id_index_);
                break;
            case Lexem_kind::Keyword:
                result += keyword_kind_to_string(static_cast<Keyword_kind>(lsk));
                break;
            case Lexem_kind::Char:
                result += show_char32(li.char_val_);
                break;
            case Lexem_kind::Encoded_char:
                result += show_char32(li.char_val_);
                break;
            case Lexem_kind::String:
                result += idx_to_string(strs_trie, li.str_index_);
                break;
            case Lexem_kind::Integer:
                result += ::to_string(li.int_val_);
                break;
            case Lexem_kind::Float:
                result += float_subkind_strs[lsk] + ::to_string(li.float_val_);
                break;
            case Lexem_kind::Complex:
                result += complex_subkind_strs[lsk]            +
                          ::to_string(crealq(li.complex_val_)) +
                          " + "                                +
                          ::to_string(cimagq(li.complex_val_)) +
                          "i";
                break;
            case Lexem_kind::Quat:
                result += quat_subkind_strs[lsk]       +
                          ::to_string(li.quat_val_[0]) +
                          ::to_string(li.quat_val_[1]) + "i" +
                          ::to_string(li.quat_val_[2]) + "j" +
                          ::to_string(li.quat_val_[3]) + "k";
                break;
            case Lexem_kind::Delimiter:
                result += delimiter_kind_to_string(static_cast<Delimiter_kind>(lsk));
                break;
            default:
                ;
        }
        return result;
    }
};
