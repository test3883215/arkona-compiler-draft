/*
    File:    file_contents.cpp
    Author:  Гаврилов Владимир Сергеевич
    Created: 04 February 2016 at 13:10 MSK
    e-mails: vladimir.s.gavrilov@gmail.com,
             gavrilov.vladimir.s@mail.ru,
    License: GPLv3
*/

#include <cstddef>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <memory>
#include "../include/file_contents.hpp"

namespace fs = std::filesystem;

namespace file_utils{
    Contents get_contents(const char* name)
    {
        Contents result= std::make_pair(Return_code::Normal, std::string{});

        fs::path p{name};

        if(!fs::exists(p) || fs::is_directory(p)){
            result.first = Return_code::Impossible_open;
            return result;
        }

        std::size_t file_size = fs::file_size(p);
        if(!file_size){
            return result;
        }

        if(std::ifstream bin_file{name, std::ios::binary | std::ios_base::in}){
            auto     text    = std::make_unique<char[]>(file_size + 1);
            char*    q       = text.get();
            bin_file.read(q, file_size);
            text[file_size]  = 0;
            result.second    = std::string{text.get()};
        }else{
            result.first = Return_code::Read_error;
        }

        return result;
    }
};