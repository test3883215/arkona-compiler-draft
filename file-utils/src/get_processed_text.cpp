/*
    File:    get_processed_text.cpp
    Created: 12 June 2021 at 14:42 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstdio>
#include "../include/get_processed_text.hpp"
#include "../../char-conv/include/char_conv.hpp"
#include "../include/file_contents.hpp"

namespace file_utils{
    std::u32string get_processed_text(const char* name)
    {
        auto [ret_code, str] = get_contents(name);
        switch(ret_code){
            case Return_code::Normal:
                if(str.empty()){
                    puts("File length is equal to zero.");
                    return std::u32string();
                }else{
                    return utf8_to_u32string(str);
                }
                break;

            case Return_code::Impossible_open:
                puts("Unable to open file.");
                return std::u32string{};

            case Return_code::Read_error:
                puts("Error reading file.");
                return std::u32string{};
        }
        return std::u32string{};
    }
};