/*
    File:    is_digit.cpp
    Created: 19 June 2021 at 21:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/is_digit.hpp"

bool is_digit(char32_t c)
{
    bool result;
    result = ((U'0' <= c) && (c <= U'9'))  ||
              ((U'A' <= c) && (c <= U'F')) ||
              ((U'a' <= c) && (c <= U'f'));
    return result;
}