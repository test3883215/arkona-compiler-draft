/*
    File:    int128_to_str.hpp
    Created: 27 March 2019 at 08:56 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>
std::string to_string(unsigned __int128 x);