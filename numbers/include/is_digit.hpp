/*
    File:    is_digit.hpp
    Created: 19 June 2021 at 21:42 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once
bool is_digit(char32_t c);