/*
    File:    num_digits.hpp
    Created: 04 April 2021 at 14:36 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#pragma once

#include <algorithm>
#include <cstddef>

template<typename T>
std::size_t num_of_digits(T n, T base = T{10})
{
    std::size_t result  = 0;
    T           current = n;

    do
    {
        current /= base;
        result++;
    }
    while(current != T{0});

    return result;
}

template<typename Func, typename It, typename T>
std::size_t max_num_of_digits(Func f, It first, It last, T base)
{
    std::size_t result = 0;
    for(auto it = first; it != last; ++it)
    {
        T current = static_cast<T>(f(*it));
        result = std::max(result, num_of_digits<T>(current, base));
    }
    return result;
}