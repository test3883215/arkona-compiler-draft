/*
    File:    float128_to_string.hpp
    Created: 21 March 2019 at 14:28 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>
std::string to_string(const __float128 x);