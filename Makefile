LINKER          = g++
LINKERFLAGS     = -s
CXX             = g++
CXXFLAGS        = -O3 -Wall -std=gnu++17 -Wextra
SRC_EXT         = cpp

CHAR_CONV_SRC_PATH   = char-conv/src
CHAR_CONV_BUILD_PATH = char-conv/build
CHAR_CONV_LIB_NAME   = libcharconv.a
CHAR_CONV_SRCS       = $(shell find $(CHAR_CONV_SRC_PATH) -name '*.$(SRC_EXT)')
CHAR_CONV_OBJECTS    = $(CHAR_CONV_SRCS:$(CHAR_CONV_SRC_PATH)/%.$(SRC_EXT)=$(CHAR_CONV_BUILD_PATH)/%.o)

LFILES_SRC_PATH   = file-utils/src
LFILES_BUILD_PATH = file-utils/build
LFILES_LIB_NAME   = libfiles.a
LFILES_SRCS       = $(shell find $(LFILES_SRC_PATH) -name '*.$(SRC_EXT)')
LFILES_OBJECTS    = $(LFILES_SRCS:$(LFILES_SRC_PATH)/%.$(SRC_EXT)=$(LFILES_BUILD_PATH)/%.o)

STR_UTILS_SRC_PATH   = strings-utils/src
STR_UTILS_BUILD_PATH = strings-utils/build
STR_UTILS_LIB_NAME   = libstrutils.a
STR_UTILS_SRCS       = $(shell find $(STR_UTILS_SRC_PATH) -name '*.$(SRC_EXT)')
STR_UTILS_OBJECTS    = $(STR_UTILS_SRCS:$(STR_UTILS_SRC_PATH)/%.$(SRC_EXT)=$(STR_UTILS_BUILD_PATH)/%.o)

FMT_SRC_PATH   = thirdparty/fmtlib/src
FMT_BUILD_PATH = thirdparty/fmtlib/build
FMT_LIB_NAME   = libformatting.a
FMT_SRCS       = $(shell find $(FMT_SRC_PATH) -name '*.$(SRC_EXT)')
FMT_OBJECTS    = $(FMT_SRCS:$(FMT_SRC_PATH)/%.$(SRC_EXT)=$(FMT_BUILD_PATH)/%.o)

CLASSIFICATION_TGEN_SRC_PATH   = classification-table-gen-lib/src
CLASSIFICATION_TGEN_BUILD_PATH = classification-table-gen-lib/build
CLASSIFICATION_TGEN_LIB_NAME   = libclassifytgen.a
CLASSIFICATION_TGEN_SRCS       = $(shell find $(CLASSIFICATION_TGEN_SRC_PATH) -name '*.$(SRC_EXT)')
CLASSIFICATION_TGEN_OBJECTS    = $(CLASSIFICATION_TGEN_SRCS:$(CLASSIFICATION_TGEN_SRC_PATH)/%.$(SRC_EXT)=$(CLASSIFICATION_TGEN_BUILD_PATH)/%.o)

KWDELIM_TGEN_SRC_PATH   = kwdelim-table-gen-lib/src
KWDELIM_TGEN_BUILD_PATH = kwdelim-table-gen-lib/build
KWDELIM_TGEN_LIB_NAME   = libkwdelimtgen.a
KWDELIM_TGEN_SRCS       = $(shell find $(KWDELIM_TGEN_SRC_PATH) -name '*.$(SRC_EXT)')
KWDELIM_TGEN_OBJECTS    = $(KWDELIM_TGEN_SRCS:$(KWDELIM_TGEN_SRC_PATH)/%.$(SRC_EXT)=$(KWDELIM_TGEN_BUILD_PATH)/%.o)

NUMBERS_SRC_PATH   = numbers/src
NUMBERS_BUILD_PATH = numbers/build
NUMBERS_LIB_NAME   = libnumberslib.a
NUMBERS_SRCS       = $(shell find $(NUMBERS_SRC_PATH) -name '*.$(SRC_EXT)')
NUMBERS_OBJECTS    = $(NUMBERS_SRCS:$(NUMBERS_SRC_PATH)/%.$(SRC_EXT)=$(NUMBERS_BUILD_PATH)/%.o)

CONFIGS_PATH = configs
CONFIGS      = $(CONFIGS_PATH)/keywords.txt $(CONFIGS_PATH)/delimiters.txt

ISCANNER_SRC_PATH   = iscanner/src
ISCANNER_BUILD_PATH = iscanner/build
ISCANNER_LIB_NAME   = libiscanner.a
ISCANNER_SRCS       = $(shell find $(ISCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
ISCANNER_OBJECTS    = $(ISCANNER_SRCS:$(ISCANNER_SRC_PATH)/%.$(SRC_EXT)=$(ISCANNER_BUILD_PATH)/%.o)

SCANNER_SRC_PATH   = scanner/src
SCANNER_BUILD_PATH = scanner/build
SCANNER_LIB_NAME   = libscanner.a
SCANNER_SRCS       = $(shell find $(SCANNER_SRC_PATH) -name '*.$(SRC_EXT)')
SCANNER_OBJECTS    = $(SCANNER_SRCS:$(SCANNER_SRC_PATH)/%.$(SRC_EXT)=$(SCANNER_BUILD_PATH)/%.o)

SCANNER_TGEN_PROGRAM_SRC_PATH   = scanner-tables-generation/src
SCANNER_TGEN_PROGRAM_BUILD_PATH = scanner-tables-generation/build
SCANNER_TGEN_PROGRAM_BIN_NAME   = tgen
SCANNER_TGEN_PROGRAM_SRCS       = $(shell find $(SCANNER_TGEN_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
SCANNER_TGEN_PROGRAM_OBJECTS    = $(SCANNER_TGEN_PROGRAM_SRCS:$(SCANNER_TGEN_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(SCANNER_TGEN_PROGRAM_BUILD_PATH)/%.o)

ASTLIB_SRC_PATH   = arkona-ast-lib/src
ASTLIB_BUILD_PATH = arkona-ast-lib/build
ASTLIB_LIB_NAME   = libast.a
ASTLIB_SRCS       = $(shell find $(ASTLIB_SRC_PATH) -name '*.$(SRC_EXT)')
ASTLIB_OBJECTS    = $(ASTLIB_SRCS:$(ASTLIB_SRC_PATH)/%.$(SRC_EXT)=$(ASTLIB_BUILD_PATH)/%.o)

PARSER_SRC_PATH   = parser/src
PARSER_BUILD_PATH = parser/build
PARSER_LIB_NAME   = libparser.a
PARSER_SRCS       = $(shell find $(PARSER_SRC_PATH) -name '*.$(SRC_EXT)')
PARSER_OBJECTS    = $(PARSER_SRCS:$(PARSER_SRC_PATH)/%.$(SRC_EXT)=$(PARSER_BUILD_PATH)/%.o)

TESTING_UTILITY_SRC_PATH   = testing/utils/src
TESTING_UTILITY_BUILD_PATH = testing/utils/build
TESTING_UTILITY_LIB_NAME   = libtestutils.a
TESTING_UTILITY_SRCS       = $(shell find $(TESTING_UTILITY_SRC_PATH) -name '*.$(SRC_EXT)')
TESTING_UTILITY_OBJECTS    = $(TESTING_UTILITY_SRCS:$(TESTING_UTILITY_SRC_PATH)/%.$(SRC_EXT)=$(TESTING_UTILITY_BUILD_PATH)/%.o)

LEXEME_TO_STRING_TESTING_PROGRAM_SRC_PATH   = testing/scanner/lexeme-to-string/src
LEXEME_TO_STRING_TESTING_PROGRAM_BUILD_PATH = testing/scanner/lexeme-to-string/build
LEXEME_TO_STRING_TESTING_PROGRAM_BIN_NAME   = lexeme-to-string-testing
LEXEME_TO_STRING_TESTING_PROGRAM_SRCS       = $(shell find $(LEXEME_TO_STRING_TESTING_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
LEXEME_TO_STRING_TESTING_PROGRAM_OBJECTS    = $(LEXEME_TO_STRING_TESTING_PROGRAM_SRCS:$(LEXEME_TO_STRING_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(LEXEME_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/%.o)

LEXEME_PRINTING_TESTING_PROGRAM_SRC_PATH   = testing/scanner/scanner-print-lexeme/src
LEXEME_PRINTING_TESTING_PROGRAM_BUILD_PATH = testing/scanner/scanner-print-lexeme/build
LEXEME_PRINTING_TESTING_PROGRAM_BIN_NAME   = scanner-print-lexeme-testing
LEXEME_PRINTING_TESTING_PROGRAM_SRCS       = $(shell find $(LEXEME_PRINTING_TESTING_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
LEXEME_PRINTING_TESTING_PROGRAM_OBJECTS    = $(LEXEME_PRINTING_TESTING_PROGRAM_SRCS:$(LEXEME_PRINTING_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(LEXEME_PRINTING_TESTING_PROGRAM_BUILD_PATH)/%.o)

LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_SRC_PATH   = testing/scanner/lexeme-recognition-automated-testing/src
LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BUILD_PATH = testing/scanner/lexeme-recognition-automated-testing/build
LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BIN_NAME   = lexeme-recognition-testing
LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_SRCS       = $(shell find $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_OBJECTS    = $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_SRCS:$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/%.o)

FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_SRC_PATH   = testing/scanner/back-function-automated-testing/src
FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BUILD_PATH = testing/scanner/back-function-automated-testing/build
FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BIN_NAME   = function-back-testing
FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_SRCS       = $(shell find $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_OBJECTS    = $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_SRCS:$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/%.o)

TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_SRC_PATH   = testing/parser/terminal-category-to-string/src
TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BUILD_PATH = testing/parser/terminal-category-to-string/build
TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BIN_NAME   = terminal-category-to-string-testing
TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_SRCS       = $(shell find $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_OBJECTS    = $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_SRCS:$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/%.o)

TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_SRC_PATH   = testing/parser/token-to-terminal/src
TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BUILD_PATH = testing/parser/token-to-terminal/build
TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BIN_NAME   = token-to-terminal-categories-testing
TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_SRCS       = $(shell find $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_SRC_PATH) -name '*.$(SRC_EXT)')
TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_OBJECTS    = $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_SRCS:$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)=$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BUILD_PATH)/%.o)

BITSET_SRC_PATH   = bitset/src
BITSET_BUILD_PATH = bitset/build
BITSET_LIB_NAME   = libpbitset.a
BITSET_SRCS       = $(shell find $(BITSET_SRC_PATH) -name '*.$(SRC_EXT)')
BITSET_OBJECTS    = $(BITSET_SRCS:$(BITSET_SRC_PATH)/%.$(SRC_EXT)=$(BITSET_BUILD_PATH)/%.o)

SYMBOL_TABLE_SRC_PATH   = symbol-table/src
SYMBOL_TABLE_BUILD_PATH = symbol-table/build
SYMBOL_TABLE_LIB_NAME   = libsymtable.a
SYMBOL_TABLE_SRCS       = $(shell find $(SYMBOL_TABLE_SRC_PATH) -name '*.$(SRC_EXT)')
SYMBOL_TABLE_OBJECTS    = $(SYMBOL_TABLE_SRCS:$(SYMBOL_TABLE_SRC_PATH)/%.$(SRC_EXT)=$(SYMBOL_TABLE_BUILD_PATH)/%.o)

CHAR_CONV_LIB           = -L$(CHAR_CONV_BUILD_PATH) -lcharconv
FS_LIB                  = -lstdc++fs
LFILE_LIB               = -L$(LFILES_BUILD_PATH) -lfiles
STR_UTILS_LIB           = -L$(STR_UTILS_BUILD_PATH) -lstrutils
FMT_LIB                 = -L$(FMT_BUILD_PATH) -lformatting
CLASSIFICATION_TGEN_LIB = -L$(CLASSIFICATION_TGEN_BUILD_PATH) -lclassifytgen
KWDELIM_TGEN_LIB        = -L$(KWDELIM_TGEN_BUILD_PATH) -lkwdelimtgen
ISCANNER_LIB            = -L$(ISCANNER_BUILD_PATH) -liscanner
SCANNER_LIB             = -L$(SCANNER_BUILD_PATH) -lscanner
NUMBERS_LIB             = -L$(NUMBERS_BUILD_PATH) -lnumberslib
TESTING_UTILITY_LIB     = -L$(TESTING_UTILITY_BUILD_PATH) -ltestutils
AST_LIB                 = -L$(ASTLIB_BUILD_PATH) -last
BITSET_LIB              = -L$(BITSET_BUILD_PATH) -lpbitset
SYMBOL_TABLE_LIB        = -L$(SYMBOL_TABLE_BUILD_PATH) -lsymtable
PARSER_LIB              = -L$(PARSER_BUILD_PATH) -lparser

TGEN_LIBS = $(STR_UTILS_LIB) $(CLASSIFICATION_TGEN_LIB) $(KWDELIM_TGEN_LIB) $(FMT_LIB) $(LFILE_LIB) $(CHAR_CONV_LIB) $(FS_LIB)

LEXEME_TO_STRING_LIBS = $(TESTING_UTILITY_LIB)\
	$(SCANNER_LIB) $(ISCANNER_LIB)\
	$(NUMBERS_LIB) $(CHAR_CONV_LIB)\
	$(STR_UTILS_LIB)\
	-lquadmath

LEXEME_PRINTING_LIBS = $(TESTING_UTILITY_LIB)\
	$(SCANNER_LIB) $(ISCANNER_LIB)\
	$(NUMBERS_LIB) $(CHAR_CONV_LIB)\
	$(STR_UTILS_LIB)\
	$(LFILE_LIB) $(FS_LIB)\
	-lquadmath

LEXEME_RECOGNITION_LIBS = $(TESTING_UTILITY_LIB)\
	$(SCANNER_LIB) $(ISCANNER_LIB)\
	$(NUMBERS_LIB) $(CHAR_CONV_LIB)\
	$(STR_UTILS_LIB)\
	-lquadmath

FUNCTION_BACK_LIBS = $(TESTING_UTILITY_LIB)\
	$(SCANNER_LIB) $(ISCANNER_LIB)\
	$(NUMBERS_LIB) $(CHAR_CONV_LIB)\
	$(STR_UTILS_LIB)\
	-lquadmath

TERMINALCATEGORY_TO_STRING_LIBS = $(TESTING_UTILITY_LIB)\
	$(PARSER_LIB)\
	$(CHAR_CONV_LIB)\
	$(STR_UTILS_LIB)

TOKEN_TO_TERMINAL_CATEGORIES_LIBS = $(TESTING_UTILITY_LIB)\
	$(PARSER_LIB)\
	$(BITSET_LIB)\
	$(CHAR_CONV_LIB)\
	$(STR_UTILS_LIB)

BUILD_MSG_PRINT = @tput bold; tput setaf 6; echo "$(1)"; tput sgr0

.PHONY: all clean common-libs charconv str-utils lfiles\ 
	formatting lclassify lkwdelim-tgen scanner-prj\ 
	iscanner scanner-tgen scanner-tgen-program libnum\
	lscanner\
	parser-prj ast-library bitset-library symbol-table-library parser-library\
	testing testing-utilities tests testing-scanner\
	test-lexeme-to-string test-lexemes-sc-print test-lexeme-recognition test-function-back\
	testing-parser testing-terminal-category-to-string testing-token-to-terminal-categories

all: common-libs scanner-prj parser-prj testing

common-libs: charconv str-utils lfiles formatting libnum

charconv: $(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME)

$(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME): $(CHAR_CONV_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking character convertation library.")
	ar cr $@ $(CHAR_CONV_OBJECTS)
	@echo "             "

$(CHAR_CONV_BUILD_PATH)/%.o: $(CHAR_CONV_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@


str-utils: $(STR_UTILS_BUILD_PATH)/$(STR_UTILS_LIB_NAME)

$(STR_UTILS_BUILD_PATH)/$(STR_UTILS_LIB_NAME): $(STR_UTILS_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking strings utilities library.")
	ar cr $@ $(STR_UTILS_OBJECTS)
	@echo "             "

$(STR_UTILS_BUILD_PATH)/%.o: $(STR_UTILS_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@


lfiles: $(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME)

$(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME): $(LFILES_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking file utils library.")
	ar cr $@ $(LFILES_OBJECTS)
	@echo "             "

$(LFILES_BUILD_PATH)/%.o: $(LFILES_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

formatting: $(FMT_BUILD_PATH)/$(FMT_LIB_NAME)

$(FMT_BUILD_PATH)/$(FMT_LIB_NAME): $(FMT_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking fmtlib library.")
	ar cr $@ $(FMT_OBJECTS)
	@echo "             "

$(FMT_BUILD_PATH)/%.o: $(FMT_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

libnum: $(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME)

$(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME): $(NUMBERS_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library numberslib.")
	ar cr $@ $(NUMBERS_OBJECTS)
	@echo "             "

$(NUMBERS_BUILD_PATH)/%.o: $(NUMBERS_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

scanner-prj: iscanner lclassify lkwdelim-tgen scanner-tgen lscanner

iscanner: $(ISCANNER_BUILD_PATH)/$(ISCANNER_LIB_NAME)

$(ISCANNER_BUILD_PATH)/$(ISCANNER_LIB_NAME): $(ISCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for abstract scanner.")
	ar cr $@ $(ISCANNER_OBJECTS)
	@echo "             "

$(ISCANNER_BUILD_PATH)/%.o: $(ISCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lclassify: $(CLASSIFICATION_TGEN_BUILD_PATH)/$(CLASSIFICATION_TGEN_LIB_NAME)

$(CLASSIFICATION_TGEN_BUILD_PATH)/$(CLASSIFICATION_TGEN_LIB_NAME): $(CLASSIFICATION_TGEN_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for generating character classification table.")
	ar cr $@ $(CLASSIFICATION_TGEN_OBJECTS)
	@echo "             "

$(CLASSIFICATION_TGEN_BUILD_PATH)/%.o: $(CLASSIFICATION_TGEN_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lkwdelim-tgen: $(KWDELIM_TGEN_BUILD_PATH)/$(KWDELIM_TGEN_LIB_NAME)

$(KWDELIM_TGEN_BUILD_PATH)/$(KWDELIM_TGEN_LIB_NAME): $(KWDELIM_TGEN_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for generating tables for recognition of keywords and delimiters.")
	ar cr $@ $(KWDELIM_TGEN_OBJECTS)
	@echo "             "

$(KWDELIM_TGEN_BUILD_PATH)/%.o: $(KWDELIM_TGEN_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

scanner-tgen: $(CONFIGS) scanner-tgen-program
	$(call BUILD_MSG_PRINT,"Generating scanner tables.")
	$(SCANNER_TGEN_PROGRAM_BUILD_PATH)/$(SCANNER_TGEN_PROGRAM_BIN_NAME) $(CONFIGS)
	@echo "             "

scanner-tgen-program: $(SCANNER_TGEN_PROGRAM_BUILD_PATH)/$(SCANNER_TGEN_PROGRAM_BIN_NAME)

$(SCANNER_TGEN_PROGRAM_BUILD_PATH)/$(SCANNER_TGEN_PROGRAM_BIN_NAME): $(SCANNER_TGEN_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for generating of character classification table and transition tables.")
	$(LINKER) -o $@ $(SCANNER_TGEN_PROGRAM_OBJECTS) $(TGEN_LIBS) $(LINKERFLAGS)
	@echo "             "

$(SCANNER_TGEN_PROGRAM_BUILD_PATH)/%.o: $(SCANNER_TGEN_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

lscanner: $(SCANNER_BUILD_PATH)/$(SCANNER_LIB_NAME)

$(SCANNER_BUILD_PATH)/$(SCANNER_LIB_NAME): $(SCANNER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the scanner")
	ar cr $@ $(SCANNER_OBJECTS)
	@echo "             "

$(SCANNER_BUILD_PATH)/%.o: $(SCANNER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

parser-prj: ast-library bitset-library symbol-table-library parser-library

ast-library: $(ASTLIB_BUILD_PATH)/$(ASTLIB_LIB_NAME)

$(ASTLIB_BUILD_PATH)/$(ASTLIB_LIB_NAME): $(ASTLIB_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the Arkona AST")
	ar cr $@ $(ASTLIB_OBJECTS)
	@echo "             "

$(ASTLIB_BUILD_PATH)/%.o: $(ASTLIB_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

bitset-library: $(BITSET_BUILD_PATH)/$(BITSET_LIB_NAME)

$(BITSET_BUILD_PATH)/$(BITSET_LIB_NAME): $(BITSET_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for Pascal-style sets")
	ar cr $@ $(BITSET_OBJECTS)
	@echo "             "

$(BITSET_BUILD_PATH)/%.o: $(BITSET_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

symbol-table-library: $(SYMBOL_TABLE_BUILD_PATH)/$(SYMBOL_TABLE_LIB_NAME)

$(SYMBOL_TABLE_BUILD_PATH)/$(SYMBOL_TABLE_LIB_NAME): $(SYMBOL_TABLE_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for symbol table")
	ar cr $@ $(SYMBOL_TABLE_OBJECTS)
	@echo "             "

$(SYMBOL_TABLE_BUILD_PATH)/%.o: $(SYMBOL_TABLE_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

parser-library: $(PARSER_BUILD_PATH)/$(PARSER_LIB_NAME)

$(PARSER_BUILD_PATH)/$(PARSER_LIB_NAME): $(PARSER_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for the Arkona parsing")
	ar cr $@ $(PARSER_OBJECTS)
	@echo "             "

$(PARSER_BUILD_PATH)/%.o: $(PARSER_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@



testing: testing-utilities tests

testing-utilities: $(TESTING_UTILITY_BUILD_PATH)/$(TESTING_UTILITY_LIB_NAME)

$(TESTING_UTILITY_BUILD_PATH)/$(TESTING_UTILITY_LIB_NAME): $(TESTING_UTILITY_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking library for testing utilities.")
	ar cr $@ $(TESTING_UTILITY_OBJECTS)
	@echo "             "

$(TESTING_UTILITY_BUILD_PATH)/%.o: $(TESTING_UTILITY_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

tests: testing-scanner testing-parser

testing-scanner: test-lexeme-to-string test-lexemes-sc-print test-lexeme-recognition test-function-back

test-lexeme-to-string: $(LEXEME_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_TO_STRING_TESTING_PROGRAM_BIN_NAME)

$(LEXEME_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_TO_STRING_TESTING_PROGRAM_BIN_NAME): $(LEXEME_TO_STRING_TESTING_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing conversion lexeme to string.")
	$(LINKER) -o $@ $(LEXEME_TO_STRING_TESTING_PROGRAM_OBJECTS) $(LEXEME_TO_STRING_LIBS) $(LINKERFLAGS)
	@echo "             "

$(LEXEME_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/%.o: $(LEXEME_TO_STRING_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

test-lexemes-sc-print: $(LEXEME_PRINTING_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_PRINTING_TESTING_PROGRAM_BIN_NAME)

$(LEXEME_PRINTING_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_PRINTING_TESTING_PROGRAM_BIN_NAME): $(LEXEME_PRINTING_TESTING_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing conversion lexeme to string.")
	$(LINKER) -o $@ $(LEXEME_PRINTING_TESTING_PROGRAM_OBJECTS) $(LEXEME_PRINTING_LIBS) $(LINKERFLAGS)
	@echo "             "

$(LEXEME_PRINTING_TESTING_PROGRAM_BUILD_PATH)/%.o: $(LEXEME_PRINTING_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

test-lexeme-recognition: $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BIN_NAME)

$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BIN_NAME): $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for automated testing lexeme recognition.")
	$(LINKER) -o $@ $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_OBJECTS) $(LEXEME_RECOGNITION_LIBS) $(LINKERFLAGS)
	@echo "             "

$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/%.o: $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

test-function-back: $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BIN_NAME)

$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BIN_NAME): $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for automated testing the method Scanner::back.")
	$(LINKER) -o $@ $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_OBJECTS) $(FUNCTION_BACK_LIBS) $(LINKERFLAGS)
	@echo "             "

$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/%.o: $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

testing-parser: testing-terminal-category-to-string testing-token-to-terminal-categories

testing-terminal-category-to-string: $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BIN_NAME)

$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BIN_NAME): $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing conversion of category of terminals to string.")
	$(LINKER) -o $@ $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_OBJECTS) $(TERMINALCATEGORY_TO_STRING_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/%.o: $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@

testing-token-to-terminal-categories: $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BUILD_PATH)/$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BIN_NAME)

$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BUILD_PATH)/$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BIN_NAME): $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_OBJECTS)
	$(call BUILD_MSG_PRINT,"Linking of program for testing conversion of category of terminals to string.")
	$(LINKER) -o $@ $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_OBJECTS) $(TOKEN_TO_TERMINAL_CATEGORIES_LIBS) $(LINKERFLAGS)
	@echo "             "

$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BUILD_PATH)/%.o: $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_SRC_PATH)/%.$(SRC_EXT)
	@tput bold; tput setaf 2; echo "Compiling: $< -> $@"
	@tput sgr0
	$(CXX) $(CXXFLAGS) -c $< -o $@


clean:
	@tput bold; tput setaf 6
	@echo "Removing object files, library files, and executable files."
	@tput sgr0
	rm -f $(CHAR_CONV_OBJECTS)
	rm -f $(CHAR_CONV_BUILD_PATH)/$(CHAR_CONV_LIB_NAME)
	rm -f $(STR_UTILS_OBJECTS)
	rm -f $(STR_UTILS_BUILD_PATH)/$(STR_UTILS_LIB_NAME)
	rm -f $(LFILES_OBJECTS)
	rm -f $(LFILES_BUILD_PATH)/$(LFILES_LIB_NAME)
	rm -f $(FMT_OBJECTS)
	rm -f $(FMT_BUILD_PATH)/$(FMT_LIB_NAME)
	rm -f $(CLASSIFICATION_TGEN_BUILD_PATH)/$(CLASSIFICATION_TGEN_LIB_NAME)
	rm -f $(CLASSIFICATION_TGEN_OBJECTS)
	rm -f $(KWDELIM_TGEN_BUILD_PATH)/$(KWDELIM_TGEN_LIB_NAME)
	rm -f $(KWDELIM_TGEN_OBJECTS)
	rm -f $(ISCANNER_OBJECTS)
	rm -f $(ISCANNER_BUILD_PATH)/$(ISCANNER_LIB_NAME)
	rm -f $(SCANNER_TGEN_PROGRAM_BUILD_PATH)/$(SCANNER_TGEN_PROGRAM_BIN_NAME)
	rm -f $(SCANNER_TGEN_PROGRAM_OBJECTS)
	rm -f $(NUMBERS_OBJECTS)
	rm -f $(NUMBERS_BUILD_PATH)/$(NUMBERS_LIB_NAME)
	rm -f $(TESTING_UTILITY_OBJECTS)
	rm -f $(TESTING_UTILITY_BUILD_PATH)/$(TESTING_UTILITY_LIB_NAME)
	rm -f $(LEXEME_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_TO_STRING_TESTING_PROGRAM_BIN_NAME)
	rm -f $(LEXEME_TO_STRING_TESTING_PROGRAM_OBJECTS)
	rm -f $(LEXEME_PRINTING_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_PRINTING_TESTING_PROGRAM_BIN_NAME)
	rm -f $(LEXEME_PRINTING_TESTING_PROGRAM_OBJECTS)
	rm -f $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/$(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_BIN_NAME)
	rm -f $(LEXEME_RECOGNITION_AUTOMATED_TESTING_PROGRAM_OBJECTS)
	rm -f $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BUILD_PATH)/$(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_BIN_NAME)
	rm -f $(FUNCTION_BACK_AUTOMATED_TESTING_PROGRAM_OBJECTS)
	rm -f $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BUILD_PATH)/$(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_BIN_NAME)
	rm -f $(TERMINAL_CATEGORY_TO_STRING_TESTING_PROGRAM_OBJECTS)
	rm -f $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BUILD_PATH)/$(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_BIN_NAME)
	rm -f $(TOKEN_TO_TERMINAL_CATEGORIES_TESTING_PROGRAM_OBJECTS)
	rm -f $(ASTLIB_OBJECTS)
	rm -f $(ASTLIB_BUILD_PATH)/$(ASTLIB_LIB_NAME)
	rm -f $(BITSET_OBJECTS)
	rm -f $(BITSET_BUILD_PATH)/$(BITSET_LIB_NAME)
	rm -f $(SYMBOL_TABLE_OBJECTS)
	rm -f $(SYMBOL_TABLE_BUILD_PATH)/$(SYMBOL_TABLE_LIB_NAME)
	rm -f $(PARSER_OBJECTS)
	rm -f $(PARSER_BUILD_PATH)/$(PARSER_LIB_NAME)
	rm -f $(SCANNER_OBJECTS)
	rm -f $(SCANNER_BUILD_PATH)/$(SCANNER_LIB_NAME)
