/*
    File:    create_permutation.cpp
    Created: 17 июля 2017г.
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/
#include "../include/create_permutation.hpp"
#include "../include/create_permutation_tree.hpp"
#include "../include/permutation_tree_to_permutation.hpp"

Permutation create_permutation(size_t n)
{
    Permutation result;
    Permutation_tree pt = create_permutation_tree(n);
    result              = permutation_tree_to_permutation(pt);
    return result;
}