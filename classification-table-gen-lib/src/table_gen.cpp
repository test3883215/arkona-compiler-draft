/*
    File:    table_gen.cpp
    Created: 08 May 2021 at 09:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/table_gen.hpp"

#include <cstddef>
#include <ctime>
#include <vector>

#include "../include/map_as_vector.hpp"
#include "../include/segment.hpp"
#include "../include/group_pairs.hpp"
#include "../include/create_permutation_tree.hpp"
#include "../include/permutation_tree_to_permutation.hpp"
#include "../include/permutation.hpp"
#include "../include/create_permutation.hpp"
#include "../include/myconcepts.hpp"
#include "../../thirdparty/fmtlib/include/core.h"
#include "../../strings-utils/include/strings_to_columns.hpp"
#include "../../char-conv/include/char_conv.hpp"
#include "../../strings-utils/include/join.hpp"

namespace
{
    std::vector<std::u32string> get_category_names(const std::map<std::u32string, Category_info>& category_name_to_info)
    {
        std::vector<std::u32string> result(category_name_to_info.size() + 1);
        for(const auto& [name, info] : category_name_to_info)
        {
            result[info.category_code_] = name;
        }
        result[category_name_to_info.size()] = U"Other";
        return result;
    }

    void add_char(std::map<char32_t, std::uint64_t>& table, char32_t ch, std::uint64_t category_code)
    {
        auto it = table.find(ch);
        if(it != table.end()){
            table[ch] |= static_cast<std::uint64_t>(1U) << category_code;
        }else{
            table[ch] =  static_cast<std::uint64_t>(1U) << category_code;
        }
    }

    void add_category(std::map<char32_t, std::uint64_t>& table, const Category_info& category)
    {
        for(char32_t ch : category.category_chars_)
        {
            add_char(table, ch, category.category_code_);
        }
    }

    std::map<char32_t, std::uint64_t> fill_table(const std::map<std::u32string, Category_info>& category_name_to_info)
    {
        std::map<char32_t, std::uint64_t> result;
        for(const auto& [_, info] : category_name_to_info)
        {
            add_category(result, info);
        }
        return result;
    }


    template<RandomAccessIterator DestIt, RandomAccessIterator SrcIt, Callable F>
    void permutate(DestIt dest_begin, SrcIt src_begin, SrcIt src_end, F f)
    {
        size_t num_of_elems = src_end - src_begin;
        for(size_t i = 0; i < num_of_elems; ++i)
        {
            dest_begin[f(i)] = src_begin[i];
        }
    }

    template<Integral K, typename V>
    SegmentsV<K, V> create_classification_table(const std::map<K, V>& m)
    {
        SegmentsV<K,V> grouped_pairs = group_pairs(map_as_vector(m));
        size_t         n             = grouped_pairs.size();
        auto           result        = SegmentsV<K, V>(n);
        auto           perm          = create_permutation(grouped_pairs.size());
        auto           f             = [&perm](size_t i) -> size_t{return perm[i];};
        permutate(result.begin(), grouped_pairs.begin(), grouped_pairs.end(), f);
        return result;
    }

    const std::map<char32_t, std::string> esc_char_strings = {
        {U'\?',   R"~(U'\?')~"  }, {U'\\',   R"~(U'\\')~"  }, {U'\0',   R"~(U'\0')~"  },
        {U'\a',   R"~(U'\a')~"  }, {U'\b',   R"~(U'\b')~"  }, {U'\f',   R"~(U'\f')~"  },
        {U'\n',   R"~(U'\n')~"  }, {U'\r',   R"~(U'\r')~"  }, {U'\t',   R"~(U'\t')~"  },
        {U'\v',   R"~(U'\v')~"  }, {U'\x01', R"~(U'\x01')~"}, {U'\x02', R"~(U'\x02')~"},
        {U'\x03', R"~(U'\x03')~"}, {U'\x04', R"~(U'\x04')~"}, {U'\x05', R"~(U'\x05')~"},
        {U'\x06', R"~(U'\x06')~"}, {U'\x0e', R"~(U'\x0e')~"}, {U'\x0f', R"~(U'\x0f')~"},
        {U'\x10', R"~(U'\x10')~"}, {U'\x11', R"~(U'\x11')~"}, {U'\x12', R"~(U'\x12')~"},
        {U'\x13', R"~(U'\x13')~"}, {U'\x14', R"~(U'\x14')~"}, {U'\x15', R"~(U'\x15')~"},
        {U'\x16', R"~(U'\x16')~"}, {U'\x17', R"~(U'\x17')~"}, {U'\x18', R"~(U'\x18')~"},
        {U'\x19', R"~(U'\x19')~"}, {U'\x1a', R"~(U'\x1a')~"}, {U'\x1b', R"~(U'\x1b')~"},
        {U'\x1c', R"~(U'\x1c')~"}, {U'\x1d', R"~(U'\x1d')~"}, {U'\x1e', R"~(U'\x1e')~"},
        {U'\x1f', R"~(U'\x1f')~"}, {U'\'',   R"~(U'\'')~"  }, {U'\"',   R"~(U'\"')~"  }
    };

    std::string show_char32(const char32_t c)
    {
        std::string result;
        auto it = esc_char_strings.find(c);
        if(it != esc_char_strings.end()){
            result = it->second;
        }else{
            result = "U\'"  + char32_to_utf8(c) + "\'";
        }
        return result;
    }

    template<typename StringT>
    struct String_trait
    {
        using CharT = typename StringT::value_type;
    };

    template<typename StringT>
    using char_type = typename String_trait<StringT>::CharT;

    template<typename StringT>
    StringT right_pad(const StringT&     str,
                      std::size_t        width,
                      char_type<StringT> padding_char = char_type<StringT>{' '})
    {
        std::size_t len = str.length();

        if(width <= len){
            return str.substr(0, width);
        }

        return str + StringT(width - len, padding_char);
    }

    std::string padding_char32_string(char32_t ch)
    {
        const auto str      = show_char32(ch);
        const auto u32str   = utf8_to_u32string(str.c_str());
        const auto padded_u32str = right_pad(u32str, 7);

        return u32string_to_utf8(padded_u32str);
    }

    const std::string elem_fmt = R"~({{{{{0}, {1}}},  {2:<10}}})~";

    std::string show_table_elem(const Segment_with_value<char32_t, std::uint64_t>& e)
    {
        std::string result;
        auto&       segment = e.bounds;
        result              = fmt::format(elem_fmt,
                                          padding_char32_string(segment.lower_bound),
                                          padding_char32_string(segment.upper_bound),
                                          e.value);
        return result;
    }

    const std::string table_fmt =
R"~(/*
 * It happens that in std::map<K,V> the key type is integer, and a lot of keys with the
 * same corresponding values. If such a map must be a generated constant, then this map
 * can be optimized. Namely, iterating through a map using range-based for, we will
 * build a std::vector<std::pair<K, V>>. Then we group pairs std::pair<K, V> in pairs
 * in the form (segment, a value of type V), where 'segment' is a struct consisting of
 * lower bound and upper bound. Next, we permute the grouped pair in the such way that
 * in order to search for in the array of the resulting values we can use the algorithm
 * from the answer to exercise 6.2.24 of the book
 * Knuth D.E. The art of computer programming. Volume 3. Sorting and search. ---
 * 2nd ed. --- Addison-Wesley, 1998.
*/

#define RandomAccessIterator typename
#define Callable             typename
#define Integral             typename
template<typename T>
struct Segment{{
    T lower_bound;
    T upper_bound;

    Segment()               = default;
    Segment(const Segment&) = default;
    ~Segment()              = default;
}};

template<typename T, typename V>
struct Segment_with_value{{
    Segment<T> bounds;
    V          value;

    Segment_with_value()                          = default;
    Segment_with_value(const Segment_with_value&) = default;
    ~Segment_with_value()                         = default;
}};

/* This function uses algorithm from the answer to the exercise 6.2.24 of the monography
 *  Knuth D.E. The art of computer programming. Volume 3. Sorting and search. --- 2nd ed.
 *  --- Addison-Wesley, 1998.
*/
template<RandomAccessIterator I, typename K>
std::pair<bool, size_t> knuth_find(I it_begin, I it_end, K key)
{{
    std::pair<bool, size_t> result = {{false, 0}};
    size_t                  i      = 1;
    size_t                  n      = it_end - it_begin;
    while(i <= n)
    {{
        const auto& curr        = it_begin[i - 1];
        const auto& curr_bounds = curr.bounds;
        if(key < curr_bounds.lower_bound){{
            i = 2 * i;
        }}else if(key > curr_bounds.upper_bound){{
            i = 2 * i + 1;
        }}else{{
            result.first = true; result.second = i - 1;
            break;
        }}
    }}
    return result;
}}

static const Segment_with_value<char32_t, uint64_t> categories_table[] = {{
{0}
}};

static constexpr size_t num_of_elems_in_categories_table = {1};

uint64_t get_categories_set(char32_t c)
{{
    auto t = knuth_find(categories_table,
                        categories_table + num_of_elems_in_categories_table,
                        c);
    return t.first ?
           categories_table[t.second].value :
           (1ULL << static_cast<uint64_t>(Category::Other));
}}
)~";

    const std::string category_enum_header_fmt_string = R"~(/*
    File:    category.hpp
    Created: {0}
    Author:  {1}
    E-mails: {2}
    License: {3}
*/

#ifndef CATEGORY_H
#define CATEGORY_H
#   include <cstdint>
namespace {4} {{
    enum class Category : std::uint64_t {{
{5}
    }};
}};
#endif)~";

    std::string months[] = {
        "January", "February", "March",     "April",   "May",      "June",
        "July",    "August",   "September", "October", "November", "December"
    };

    std::string current_datetime_as_string()
    {
        std::time_t now             = time(nullptr);
        std::tm*    utc_datetime    = std::gmtime(&now);
        std::string fmt_string      = "{:02} {} {} at {:02}:{:02} UTC";
        std::string result          = fmt::format(fmt_string,
                                                  utc_datetime->tm_mday,
                                                  months[utc_datetime->tm_mon],
                                                  1900 + utc_datetime->tm_year,
                                                  utc_datetime->tm_hour,
                                                  utc_datetime->tm_min);
        return result;
    }


    std::string build_author(const std::u32string& author)
    {
        return author.empty() ? "Unknown" : u32string_to_utf8(author);
    }

    std::string build_licence(const std::u32string& licence)
    {
        return licence.empty() ? "None" : u32string_to_utf8(licence);
    }

    std::string build_namespace_name(const std::u32string& name)
    {
        return name.empty() ? "details" : u32string_to_utf8(name);
    }

    std::string build_emails_section(const std::vector<std::u32string>& emails)
    {
        using namespace strings::join;

        std::string result;

        if(emails.empty())
        {
            return "Unknown";
        }

        result = u32string_to_utf8(emails[0]);

        if(emails.size() == 1)
        {
            return result;
        }

        result += "\n" + join([](const std::u32string& s){
                                return std::string(13, ' ') + u32string_to_utf8(s);
                              },
                              emails.begin() + 1,
                              emails.end(),
                              std::string{"\n"});

        return result;
    }
};

std::string build_category_table(const std::map<std::u32string, Category_info>& category_name_to_info)
{
    using namespace strings::columns;
    
    std::string result;

    const auto table          = fill_table(category_name_to_info);
    auto  t                   = create_classification_table(table);
    size_t num_of_elems       = t.size();

    std::vector<std::u32string> elems;
    for(const auto& e : t){
        auto elem_str = show_table_elem(e);
        elems.push_back(utf8_to_u32string(elem_str.c_str()));
    }

    Format f;
    f.indent_                 = 4;
    f.number_of_columns_      = 2;
    f.spaces_between_columns_ = 2;

    auto table_body           =  u32string_to_utf8(strings_to_columns(elems, f));
    result                    =  fmt::format(table_fmt, table_body, num_of_elems);
    return result;

    return result;
}

std::string build_category_include(const std::map<std::u32string, Category_info>& category_name_to_info,
                                   const Info_for_category_header&                header_info)
{
    using namespace strings::columns;
    
    std::string result;

    auto datetime       = current_datetime_as_string();
    auto author         = build_author(header_info.author_);
    auto emails         = build_emails_section(header_info.emails_);
    auto licence        = build_licence(header_info.licence_id_);
    auto namespace_name = build_namespace_name(header_info.namespace_name_);
    auto names          = get_category_names(category_name_to_info);

    Format f;
    f.indent_                 = 8;
    f.number_of_columns_      = 3;
    f.spaces_between_columns_ = 2;

    auto enum_body = strings_to_columns(names, f);

    result = fmt::format(category_enum_header_fmt_string,
                         datetime,
                         author,
                         emails,
                         licence,
                         namespace_name,
                         u32string_to_utf8(enum_body));

    return result;
}