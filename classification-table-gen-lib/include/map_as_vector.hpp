/*
    File:    map_as_vector.hpp
    Created: 08 May 2021 at 08:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <vector>
#include <map>
#include <utility>

template<typename K, typename V>
std::vector<std::pair<K, V>> map_as_vector(const std::map<K, V>& m)
{
    std::vector<std::pair<K, V>> result;
    for(const auto e : m){
        result.push_back(e);
    }
    return result;
}