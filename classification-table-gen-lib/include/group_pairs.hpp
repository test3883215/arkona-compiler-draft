/*
    File:    group_pairs.hpp
    Created: 08 May 2021 at 08:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <utility>
#include <vector>
#include "../include/myconcepts.hpp"
#include "../include/segment.hpp"

template<Integral K, typename V>
SegmentsV<K, V> group_pairs(const std::vector<std::pair<K, V>>& pairs)
{
    SegmentsV<K, V> result;

    std::size_t num_of_elems        = pairs.size();

    Segment_with_value<K,V> current;

    current.bounds.lower_bound = pairs[0].first;
    current.bounds.upper_bound = pairs[0].first;
    current.value              = pairs[0].second;

    for(std::size_t i = 1; i < num_of_elems; i++){
        auto p = pairs[i];
        if((current.value == p.second) && (current.bounds.upper_bound + 1 == p.first)){
            current.bounds.upper_bound++;
        }else{
            result.push_back(current);
            current.bounds.lower_bound = pairs[i].first;
            current.bounds.upper_bound = pairs[i].first;
            current.value              = pairs[i].second;
        }
    }
    result.push_back(current);
    return result;
}