/*
    File:    permutation_tree_to_permutation.hpp
    Created: 08 May 2021 at 08:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include "../include/permutation.hpp"
#include "../include/create_permutation_tree.hpp"
/*
 * The following function uses the centered algorithm of the tree traversal from
 * Корнеев Г.А., Шамгунов Н.Н., Шалыто А.А. Обход дерева на основе автоматного подхода //
 * Компьютерные инструменты в образовании. --- 2004. №3. --- с.32--37.
*/
Permutation permutation_tree_to_permutation(const Permutation_tree& pt);