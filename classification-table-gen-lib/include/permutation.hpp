/*
    File:    permutation.hpp
    Created: 08 May 2021 at 08:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <vector>
#include <cstddef>
using Permutation = std::vector<std::size_t>;