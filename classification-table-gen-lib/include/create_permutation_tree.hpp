/*
    File:    create_permutation_tree.hpp
    Created: 08 May 2021 at 08:52 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <vector>

struct Permutation_node
{
    std::size_t index  = 0;
    std::size_t left   = 0;
    std::size_t right  = 0;
    std::size_t parent = 0;

    Permutation_node()                        = default;
    Permutation_node(const Permutation_node&) = default;
    ~Permutation_node()                       = default;
};

using Permutation_tree = std::vector<Permutation_node>;
// Root ot the tree is the element with the index 1.

Permutation_tree create_permutation_tree(std::size_t n);