/*
    File:    myconcepts.hpp
    Created: 16 July 2017
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#define RandomAccessIterator typename
#define Callable             typename
#define Integral             typename