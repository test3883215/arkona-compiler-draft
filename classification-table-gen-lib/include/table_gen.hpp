/*
    File:    table_gen.hpp
    Created: 08 May 2021 at 08:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <vector>
struct Category_info
{
    std::uint64_t  category_code_;
    std::u32string category_chars_;
};

std::string build_category_table(const std::map<std::u32string, Category_info>& category_name_to_info);


struct Info_for_category_header
{
    std::u32string              namespace_name_;
    std::u32string              author_;
    std::vector<std::u32string> emails_;
    std::u32string              licence_id_;
};


std::string build_category_include(const std::map<std::u32string, Category_info>& category_name_to_info,
                                   const Info_for_category_header&                header_info);