/*
    File:    module_prelude.cpp
    Created: 10 September 2023 at 17:17 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstdint>
#include <list>

#include "../include/module_prelude.hpp"
#include "../../arkona-ast-lib/include/all_nodes.hpp"
#include "../../iscanner/include/position.hpp"

namespace{
/*
модуль Преамбула
{
    экспорт : мета функция возможно<:T: тип:>: тип
    {
	    мета возврат перечисление нет_значения{нет_значения} .|. структура значение{значение: T}
    }

    экспорт : мета функция вариант<:E, R : тип:> : тип
    {
        мета возврат структура ошибка{код_ошибки : E} .|. структура значение{результат : R}
    }

    экспорт : функция CIm(p : кват32) : компл32;
    экспорт : функция CIm(p : кват64) : компл64;
    экспорт : функция CIm(p : кват80) : компл80;
    экспорт : функция CIm(p : кват128) : компл128;

    экспорт : функция CRe(p : кват32) : компл32;
    экспорт : функция CRe(p : кват64) : компл64;
    экспорт : функция CRe(p : кват80) : компл80;
    экспорт : функция CRe(p : кват128) : компл128;

    экспорт : функция Im(z : компл32) : вещ32;
    экспорт : функция Im(z : компл64) : вещ64;
    экспорт : функция Im(z : компл80) : вещ80;
    экспорт : функция Im(z : компл128) : вещ128;

    экспорт : функция Re(z : компл32) : вещ32;
    экспорт : функция Re(z : компл64) : вещ64;
    экспорт : функция Re(z : компл80) : вещ80;
    экспорт : функция Re(z : компл128) : вещ128;

    экспорт : функция Lg(x : вещ32) : вещ32;
    экспорт : функция Lg(x : вещ64) : вещ64;
    экспорт : функция Lg(x : вещ80) : вещ80;
    экспорт : функция Lg(x : вещ128) : вещ128;

    экспорт : функция Ln(x : вещ32) : вещ32;
    экспорт : функция Ln(x : вещ64) : вещ64;
    экспорт : функция Ln(x : вещ80) : вещ80;
    экспорт : функция Ln(x : вещ128) : вещ128;

    экспорт : функция Log2(x : вещ32) : вещ32;
    экспорт : функция Log2(x : вещ64) : вещ64;
    экспорт : функция Log2(x : вещ80) : вещ80;
    экспорт : функция Log2(x : вещ128) : вещ128;

    экспорт : функция abs(x : цел8) : цел8;
    экспорт : функция abs(x : цел16) : цел16;
    экспорт : функция abs(x : цел32) : цел32;
    экспорт : функция abs(x : цел64) : цел64;
    экспорт : функция abs(x : цел128) : цел128;

    экспорт : функция abs(x : вещ32) : вещ32;
    экспорт : функция abs(x : вещ64) : вещ64;
    экспорт : функция abs(x : вещ80) : вещ80;
    экспорт : функция abs(x : вещ128) : вещ128;

    экспорт : функция abs(z : компл32) : вещ32;
    экспорт : функция abs(z : компл64) : вещ64;
    экспорт : функция abs(z : компл80) : вещ80;
    экспорт : функция abs(z : компл128) : вещ128;

    экспорт : функция abs(p : кват32) : вещ32;
    экспорт : функция abs(p : кват64) : вещ64;
    экспорт : функция abs(p : кват80) : вещ80;
    экспорт : функция abs(p : кват128) : вещ128;

    экспорт : функция arccos(x : вещ32) : вещ32;
    экспорт : функция arccos(x : вещ64) : вещ64;
    экспорт : функция arccos(x : вещ80) : вещ80;
    экспорт : функция arccos(x : вещ128) : вещ128;

    экспорт : функция arcctg(x : вещ32) : вещ32;
    экспорт : функция arcctg(x : вещ64) : вещ64;
    экспорт : функция arcctg(x : вещ80) : вещ80;
    экспорт : функция arcctg(x : вещ128) : вещ128;

    экспорт : функция arcsin(x : вещ32) : вещ32;
    экспорт : функция arcsin(x : вещ64) : вещ64;
    экспорт : функция arcsin(x : вещ80) : вещ80;
    экспорт : функция arcsin(x : вещ128) : вещ128;

    экспорт : функция arctg(x : вещ32) : вещ32;
    экспорт : функция arctg(x : вещ64) : вещ64;
    экспорт : функция arctg(x : вещ80) : вещ80;
    экспорт : функция arctg(x : вещ128) : вещ128;

    экспорт : функция cos(x : вещ32) : вещ32;
    экспорт : функция cos(x : вещ64) : вещ64;
    экспорт : функция cos(x : вещ80) : вещ80;
    экспорт : функция cos(x : вещ128) : вещ128;

    экспорт : функция ctg(x : вещ32) : вещ32;
    экспорт : функция ctg(x : вещ64) : вещ64;
    экспорт : функция ctg(x : вещ80) : вещ80;
    экспорт : функция ctg(x : вещ128) : вещ128;

    экспорт : функция sin(x : вещ32) : вещ32;
    экспорт : функция sin(x : вещ64) : вещ64;
    экспорт : функция sin(x : вещ80) : вещ80;
    экспорт : функция sin(x : вещ128) : вещ128;

    экспорт : функция tg(x : вещ32) : вещ32;
    экспорт : функция tg(x : вещ64) : вещ64;
    экспорт : функция tg(x : вещ80) : вещ80;
    экспорт : функция tg(x : вещ128) : вещ128;

    экспорт : функция exp(x : вещ32) : вещ32;
    экспорт : функция exp(x : вещ64) : вещ64;
    экспорт : функция exp(x : вещ80) : вещ80;
    экспорт : функция exp(x : вещ128) : вещ128;

    экспорт : функция sqrt(x : вещ32) : вещ32;
    экспорт : функция sqrt(x : вещ64) : вещ64;
    экспорт : функция sqrt(x : вещ80) : вещ80;
    экспорт : функция sqrt(x : вещ128) : вещ128;

    экспорт : функция arccosh(x : вещ32) : вещ32;
    экспорт : функция arccosh(x : вещ64) : вещ64;
    экспорт : функция arccosh(x : вещ80) : вещ80;
    экспорт : функция arccosh(x : вещ128) : вещ128;

    экспорт : функция arcctgh(x : вещ32) : вещ32;
    экспорт : функция arcctgh(x : вещ64) : вещ64;
    экспорт : функция arcctgh(x : вещ80) : вещ80;
    экспорт : функция arcctgh(x : вещ128) : вещ128;

    экспорт : функция arcsinh(x : вещ32) : вещ32;
    экспорт : функция arcsinh(x : вещ64) : вещ64;
    экспорт : функция arcsinh(x : вещ80) : вещ80;
    экспорт : функция arcsinh(x : вещ128) : вещ128;

    экспорт : функция arctgh(x : вещ32) : вещ32;
    экспорт : функция arctgh(x : вещ64) : вещ64;
    экспорт : функция arctgh(x : вещ80) : вещ80;
    экспорт : функция arctgh(x : вещ128) : вещ128;

    экспорт : функция cosh(x : вещ32) : вещ32;
    экспорт : функция cosh(x : вещ64) : вещ64;
    экспорт : функция cosh(x : вещ80) : вещ80;
    экспорт : функция cosh(x : вещ128) : вещ128;

    экспорт : функция ctgh(x : вещ32) : вещ32;
    экспорт : функция ctgh(x : вещ64) : вещ64;
    экспорт : функция ctgh(x : вещ80) : вещ80;
    экспорт : функция ctgh(x : вещ128) : вещ128;

    экспорт : функция sinh(x : вещ32) : вещ32;
    экспорт : функция sinh(x : вещ64) : вещ64;
    экспорт : функция sinh(x : вещ80) : вещ80;
    экспорт : функция sinh(x : вещ128) : вещ128;

    экспорт : функция tgh(x : вещ32) : вещ32;
    экспорт : функция tgh(x : вещ64) : вещ64;
    экспорт : функция tgh(x : вещ80) : вещ80;
    экспорт : функция tgh(x : вещ128) : вещ128;

    экспорт : функция sign(x : цел8) : цел8;
    экспорт : функция sign(x : цел16) : цел16;
    экспорт : функция sign(x : цел32) : цел32;
    экспорт : функция sign(x : цел64) : цел64;
    экспорт : функция sign(x : цел128) : цел128;

    экспорт : функция sign(x : беззн цел8) : беззн цел8;
    экспорт : функция sign(x : беззн цел16) : беззн цел16;
    экспорт : функция sign(x : беззн цел32) : беззн цел32;
    экспорт : функция sign(x : беззн цел64) : беззн цел64;
    экспорт : функция sign(x : беззн цел128) : беззн цел128;

    экспорт : функция sign(x : вещ32) : вещ32;
    экспорт : функция sign(x : вещ64) : вещ64;
    экспорт : функция sign(x : вещ80) : вещ80;
    экспорт : функция sign(x : вещ128) : вещ128;

    экспорт : функция большц(x : вещ32) : большое цел;
    экспорт : функция большц(x : вещ64) : большое цел;
    экспорт : функция большц(x : вещ80) : большое цел;
    экспорт : функция большц(x : вещ128) : большое цел;

    экспорт : функция меньшц(x : вещ32) : большое цел;
    экспорт : функция меньшц(x : вещ64) : большое цел;
    экспорт : функция меньшц(x : вещ80) : большое цел;
    экспорт : функция меньшц(x : вещ128) : большое цел;

    экспорт : функция округл(x : вещ32) : большое цел;
    экспорт : функция округл(x : вещ64) : большое цел;
    экспорт : функция округл(x : вещ80) : большое цел;
    экспорт : функция округл(x : вещ128) : большое цел;

    экспорт : функция усеч(x : вещ32) : большое цел;
    экспорт : функция усеч(x : вещ64) : большое цел;
    экспорт : функция усеч(x : вещ80) : большое цел;
    экспорт : функция усеч(x : вещ128) : большое цел;

    экспорт : функция сопр(p : кват32) : кват32;
    экспорт : функция сопр(p : кват64) : кват64;
    экспорт : функция сопр(p : кват80) : кват80;
    экспорт : функция сопр(p : кват128) : кват128;

    экспорт : функция сопр(z : компл32) : компл32;
    экспорт : функция сопр(z : компл64) : компл64;
    экспорт : функция сопр(z : компл80) : компл80;
    экспорт : функция сопр(z : компл128) : компл128;

    экспорт : функция Log(x, a : вещ32) : вещ32;
    экспорт : функция Log(x, a : вещ64) : вещ64;
    экспорт : функция Log(x, a : вещ80) : вещ80;
    экспорт : функция Log(x, a : вещ128) : вещ128;

    экспорт : пакетная функция min(пакет аргументы <!!> T) : общий_тип(аргументы);
    экспорт : пакетная функция max(пакет аргументы <!!> T) : общий_тип(аргументы);

    экспорт : функция пи() : вещ128;

    экспорт : категория равенство<:T, E : тип:>
    {
        операция == (x : T, y : E) : лог;
        операция != (x : T, y : E) : лог;
    }

    экспорт : категория равенство<:T: тип:>
    {
        операция == (x : T, y : T) : лог;
        операция != (x : T, y : T) : лог;
    }

    экспорт : категория упорядочение<:T, E : тип:> -> равенство<:T, E :>
    {
        операция < (x : T, y : E) : лог;
        операция <= (x : T, y : E) : лог;
        операция > (x : T, y : E) : лог;
        операция >= (x : T, y : E) : лог;
        операция сравни_с (x : T, y : E) : лог;
    }

    экспорт : категория упорядочение<:T: тип:> -> равенство<:T:>
    {
        операция < (x : T, y : T) : лог;
        операция <= (x : T, y : T) : лог;
        операция > (x : T, y : T) : лог;
        операция >= (x : T, y : T) : лог;
        операция сравни_с (x : T, y : T) : лог;
    }

    экспорт : категория преобр_в_строку<:T: тип:>
    {
        функция в_строку(x : T) : строка;
    }

    экспорт : категория аддитивный_моноид<:T : тип:>
    {
        конст нуль: T;
        операция + (x: T, y: T) : T;
        операция += (x: ссылка T, y: T) : ссылка T;
    }

    экспорт : категория мультипликативный_моноид<:T : тип:>
    {
        конст единица: T;
        операция * (x: T, y: T) : T;
        операция *= (x: ссылка T, y: T) : ссылка T;
    }

    экспорт : пакетная категория вызов_функции<:T, V : тип, пакет y <!!> E:>
    {
        пакетная функция вызов(x : T, пакет p <!!> типы_пакета(y)) : V;
    }

    экспорт : пакетная категория индексация<:T, V : тип, пакет y <!!> E:>
    {
        пакетная чистая функция получить_значение(x : T, пакет p <!!> типы_пакета(y)) : V;
        пакетная функция получить_ссылку(x : T, пакет p <!!> типы_пакета(y)) : V;
    }
}
 */

/*
    building 

    экспорт : мета функция возможно<:T: тип:>: тип
    {
	    мета возврат перечисление нет_значения{нет_значения} .|. структура значение{значение: T}
    }
*/
    std::shared_ptr<ast::Meta_func_def_node> build_meta_func_vozmozhno(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                       symbol_table::Scopes&                      scopes)
    {
        std::size_t vozmozhno_name_id = ids_trie->insert(std::u32string{U"возможно"});
        const ast::Id_info vozmozhno_name_info = {
            .identifier_id_ = vozmozhno_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 28},
                .end_pos_   = iscaner::Position{3, 35}
            }
        };

        std::size_t T_name_id = ids_trie->insert(std::u32string{U"T"});
        const ast::Id_info T_name_info1 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 38},
                .end_pos_   = iscaner::Position{3, 38}
            }
        };

        const ast::Id_info T_name_info2 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 95},
                .end_pos_   = iscaner::Position{5, 95}
            }
        };

        std::size_t net_znacheniya_name_id = ids_trie->insert(std::u32string{U"нет_значения"});
        const ast::Id_info net_znacheniya_name_info1 = {
            .identifier_id_ = net_znacheniya_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 35},
                .end_pos_   = iscaner::Position{5, 46}
            }
        };

        const ast::Id_info net_znacheniya_name_info2 = {
            .identifier_id_ = net_znacheniya_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 48},
                .end_pos_   = iscaner::Position{5, 59}
            }
        };

        std::size_t znachenie_name_id = ids_trie->insert(std::u32string{U"значение"});
        const ast::Id_info znachenie_name_info1 = {
            .identifier_id_ = znachenie_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 76},
                .end_pos_   = iscaner::Position{5, 83}
            }
        };

        const ast::Id_info znachenie_name_info2 = {
            .identifier_id_ = znachenie_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 85},
                .end_pos_   = iscaner::Position{5, 92}
            }
        };

        const auto meta_func_vozmozhno_signature = std::make_shared<ast::Meta_func_signature_node>(
            // arguments:
            ast::Meta_func_signature_node::Groups_of_args{
                std::make_shared<ast::Meta_func_arg_group_node>(
                    std::vector<ast::Id_info>{T_name_info1},
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            // returned type:
            std::make_shared<ast::Type_type_expr_node>(),
            // used categories:
            nullptr
        );

        const auto meta_func_vozmozhno_body = std::make_shared<ast::Block_node>(
            ast::Block_node::Block_entries{
                std::make_shared<ast::Return_stmt_node>(
                    std::make_shared<ast::Add_like_expr_node>(
                        std::make_shared<ast::Enum_type_expr_node>(
                            net_znacheniya_name_info1,
                            ast::Enum_type_expr_node::Elements_of_enum_type{net_znacheniya_name_info2}
                        ),
                        std::make_shared<ast::Struct_type_expr_node>(
                            znachenie_name_info1,
                            std::make_shared<ast::Struct_fields_def_node>(
                                ast::Struct_fields_def_node::Struct_fields_groups{
                                    std::make_shared<ast::Fields_group_def_node>(
                                        std::vector<ast::Id_info>{znachenie_name_info2},
                                        std::make_shared<ast::Name_expr_node>(
                                            T_name_info2,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    )
                                }
                            )
                        ),
                        ast::Add_like_kind::Algebraic_separator
                    )
                )
            }
        );

        const auto result = std::make_shared<ast::Meta_func_def_node>(
            ast::Exported_kind::Exported,
            vozmozhno_name_info,
            meta_func_vozmozhno_signature,
            meta_func_vozmozhno_body
        );

        scopes.add_id_def(
            vozmozhno_name_id,
            symbol_table::Id_kind::Meta_func_name,
            symbol_table::Def_for_specific_kind{
                vozmozhno_name_info.pos_,
                result,
                0
            }
        );

        return result;
    }

/*
    building

    экспорт : мета функция вариант<:E, R : тип:> : тип
    {
        мета возврат структура ошибка{код_ошибки : E} .|. структура значение{результат : R}
    }
*/
    std::shared_ptr<ast::Meta_func_def_node> build_meta_func_variant(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                     symbol_table::Scopes&                      scopes)
    {
        const std::size_t variant_name_id = ids_trie->insert(std::u32string{U"вариант"});
        const ast::Id_info variant_name_info = {
            .identifier_id_ = variant_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{8, 28},
                .end_pos_   = iscaner::Position{8, 34}
            }
        };

        std::size_t E_name_id = ids_trie->insert(std::u32string{U"E"});
        const ast::Id_info E_name_info1 = {
            .identifier_id_ = E_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{8, 37},
                .end_pos_   = iscaner::Position{8, 37}
            }
        };

        const ast::Id_info E_name_info2 = {
            .identifier_id_ = E_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 52},
                .end_pos_   = iscaner::Position{10, 52}
            }
        };

        std::size_t R_name_id = ids_trie->insert(std::u32string{U"R"});
        const ast::Id_info R_name_info1 = {
            .identifier_id_ = R_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{8, 40},
                .end_pos_   = iscaner::Position{8, 40}
            }
        };

        const ast::Id_info R_name_info2 = {
            .identifier_id_ = R_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 90},
                .end_pos_   = iscaner::Position{10, 90}
            }
        };

        std::size_t oshibka_name_id = ids_trie->insert(std::u32string{U"ошибка"});
        const ast::Id_info oshibka_name_info = {
            .identifier_id_ = oshibka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 32},
                .end_pos_   = iscaner::Position{10, 37}
            }
        };

        std::size_t kod_oshibki_name_id = ids_trie->insert(std::u32string{U"код_ошибки"});
        const ast::Id_info kod_oshibki_name_info = {
            .identifier_id_ = kod_oshibki_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 39},
                .end_pos_   = iscaner::Position{10, 48}
            }
        };

        std::size_t znachenie_name_id = ids_trie->insert(std::u32string{U"значение"});
        const ast::Id_info znachenie_name_info = {
            .identifier_id_ = znachenie_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 69},
                .end_pos_   = iscaner::Position{10, 76}
            }
        };

        std::size_t rezultat_name_id = ids_trie->insert(std::u32string{U"результат"});
        const ast::Id_info rezultat_name_info = {
            .identifier_id_ = rezultat_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 78},
                .end_pos_   = iscaner::Position{10, 86}
            }
        };

        const auto meta_func_variant_signature = std::make_shared<ast::Meta_func_signature_node>(
            // arguments:
            ast::Meta_func_signature_node::Groups_of_args{
                std::make_shared<ast::Meta_func_arg_group_node>(
                    std::vector<ast::Id_info>{E_name_info1, R_name_info1},
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            // returned type:
            std::make_shared<ast::Type_type_expr_node>(),
            // used categories:
            nullptr
        );

        const auto meta_func_variant_body = std::make_shared<ast::Block_node>(
            ast::Block_node::Block_entries{
                std::make_shared<ast::Return_stmt_node>(
                    std::make_shared<ast::Add_like_expr_node>(
                        std::make_shared<ast::Struct_type_expr_node>(
                            oshibka_name_info,
                            std::make_shared<ast::Struct_fields_def_node>(
                                ast::Struct_fields_def_node::Struct_fields_groups{
                                    std::make_shared<ast::Fields_group_def_node>(
                                        std::vector<ast::Id_info>{kod_oshibki_name_info},
                                        std::make_shared<ast::Name_expr_node>(
                                            E_name_info2,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    )
                                }
                            )
                        ),

                        std::make_shared<ast::Struct_type_expr_node>(
                            znachenie_name_info,
                            std::make_shared<ast::Struct_fields_def_node>(
                                ast::Struct_fields_def_node::Struct_fields_groups{
                                    std::make_shared<ast::Fields_group_def_node>(
                                        std::vector<ast::Id_info>{rezultat_name_info},
                                        std::make_shared<ast::Name_expr_node>(
                                            R_name_info2,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    )
                                }
                            )
                        ),

                        ast::Add_like_kind::Algebraic_separator
                    )
                )
            }
        );

        const auto result = std::make_shared<ast::Meta_func_def_node>(
            ast::Exported_kind::Exported,
            variant_name_info,
            meta_func_variant_signature,
            meta_func_variant_body
        );

        scopes.add_id_def(
            variant_name_id,
            symbol_table::Id_kind::Meta_func_name,
            symbol_table::Def_for_specific_kind{
                variant_name_info.pos_,
                result,
                0
            }
        );

        return result;
    }

    using Fixed_size_base_type_kind = ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind;

    constexpr std::size_t max_num_of_overloads = 5;

    struct One_arg_func_info{
        const char32_t*           func_name_;
        std::size_t               func_name_start_pos_;
        std::size_t               func_name_len_;
        const char32_t*           arg_name_;
        std::size_t               arg_name_start_pos_;
        std::size_t               arg_name_len_;
        std::size_t               start_line_;
        std::size_t               num_of_lines_;
        Fixed_size_base_type_kind argument_types_[max_num_of_overloads];
        Fixed_size_base_type_kind value_types_[max_num_of_overloads];
    };

    const One_arg_func_info one_arg_funcs_info[] = {
        One_arg_func_info{
            .func_name_           = U"CIm",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"p",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 13,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Quat32,
                Fixed_size_base_type_kind::Quat64,
                Fixed_size_base_type_kind::Quat80,
                Fixed_size_base_type_kind::Quat128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"CRe",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"p",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 18,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Quat32,
                Fixed_size_base_type_kind::Quat64,
                Fixed_size_base_type_kind::Quat80,
                Fixed_size_base_type_kind::Quat128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"Im",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 2,
            .arg_name_            = U"z",
            .arg_name_start_pos_  = 26,
            .arg_name_len_        = 1,
            .start_line_          = 23,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"Re",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 2,
            .arg_name_            = U"z",
            .arg_name_start_pos_  = 26,
            .arg_name_len_        = 1,
            .start_line_          = 28,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"Lg",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 2,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 26,
            .arg_name_len_        = 1,
            .start_line_          = 33,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"Ln",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 2,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 26,
            .arg_name_len_        = 1,
            .start_line_          = 38,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"Log2",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 43,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arccos",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 75,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arcctg",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 80,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arcsin",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 85,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arctg",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 5,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 29,
            .arg_name_len_        = 1,
            .start_line_          = 90,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"cos",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 95,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }

        },
        One_arg_func_info{
            .func_name_           = U"ctg",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 100,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"sin",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 105,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"tg",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 2,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 26,
            .arg_name_len_        = 1,
            .start_line_          = 110,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"exp",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 115,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"sqrt",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 120,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arccosh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 7,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 31,
            .arg_name_len_        = 1,
            .start_line_          = 125,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arcctgh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 7,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 31,
            .arg_name_len_        = 1,
            .start_line_          = 130,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arcsinh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 7,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 31,
            .arg_name_len_        = 1,
            .start_line_          = 135,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"arctgh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 140,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"cosh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 145,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"ctgh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 150,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"sinh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 155,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"tgh",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 160,
            .num_of_lines_        = 4,
            .argument_types_     = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"abs",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 48,
            .num_of_lines_        = 5,
            .argument_types_      = {
                Fixed_size_base_type_kind::Signed8,
                Fixed_size_base_type_kind::Signed16,
                Fixed_size_base_type_kind::Signed32,
                Fixed_size_base_type_kind::Signed64,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Signed8,
                Fixed_size_base_type_kind::Signed16,
                Fixed_size_base_type_kind::Signed32,
                Fixed_size_base_type_kind::Signed64,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"abs",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 54,
            .num_of_lines_        = 5,
            .argument_types_      = {
                Fixed_size_base_type_kind::Unsigned8,
                Fixed_size_base_type_kind::Unsigned16,
                Fixed_size_base_type_kind::Unsigned32,
                Fixed_size_base_type_kind::Unsigned64,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Unsigned8,
                Fixed_size_base_type_kind::Unsigned16,
                Fixed_size_base_type_kind::Unsigned32,
                Fixed_size_base_type_kind::Unsigned64,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"abs",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 60,
            .num_of_lines_        = 4,
            .argument_types_      = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"abs",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"z",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 65,
            .num_of_lines_        = 4,
            .argument_types_      = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"abs",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 3,
            .arg_name_            = U"p",
            .arg_name_start_pos_  = 27,
            .arg_name_len_        = 1,
            .start_line_          = 65,
            .num_of_lines_        = 4,
            .argument_types_      = {
                Fixed_size_base_type_kind::Quat32,
                Fixed_size_base_type_kind::Quat64,
                Fixed_size_base_type_kind::Quat80,
                Fixed_size_base_type_kind::Quat128,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"sign",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 165,
            .num_of_lines_        = 5,
            .argument_types_      = {
                Fixed_size_base_type_kind::Signed8,
                Fixed_size_base_type_kind::Signed16,
                Fixed_size_base_type_kind::Signed32,
                Fixed_size_base_type_kind::Signed64,
                Fixed_size_base_type_kind::Signed128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Signed8,
                Fixed_size_base_type_kind::Signed16,
                Fixed_size_base_type_kind::Signed32,
                Fixed_size_base_type_kind::Signed64,
                Fixed_size_base_type_kind::Signed128
            }
        },
        One_arg_func_info{
            .func_name_           = U"sign",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 171,
            .num_of_lines_        = 5,
            .argument_types_      = {
                Fixed_size_base_type_kind::Unsigned8,
                Fixed_size_base_type_kind::Unsigned16,
                Fixed_size_base_type_kind::Unsigned32,
                Fixed_size_base_type_kind::Unsigned64,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_        = {
                Fixed_size_base_type_kind::Unsigned8,
                Fixed_size_base_type_kind::Unsigned16,
                Fixed_size_base_type_kind::Unsigned32,
                Fixed_size_base_type_kind::Unsigned64,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"sign",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 177,
            .num_of_lines_        = 4,
            .argument_types_      = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"сопр",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"p",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 202,
            .num_of_lines_        = 4,
            .argument_types_      = {
                Fixed_size_base_type_kind::Quat32,
                Fixed_size_base_type_kind::Quat64,
                Fixed_size_base_type_kind::Quat80,
                Fixed_size_base_type_kind::Quat128,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Quat32,
                Fixed_size_base_type_kind::Quat64,
                Fixed_size_base_type_kind::Quat80,
                Fixed_size_base_type_kind::Quat128,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
        One_arg_func_info{
            .func_name_           = U"сопр",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"z",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 207,
            .num_of_lines_        = 4,
            .argument_types_      = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Unsigned128
            },
            .value_types_         = {
                Fixed_size_base_type_kind::Complex32,
                Fixed_size_base_type_kind::Complex64,
                Fixed_size_base_type_kind::Complex80,
                Fixed_size_base_type_kind::Complex128,
                Fixed_size_base_type_kind::Unsigned128
            }
        },
    };

    std::list<std::shared_ptr<ast::Usual_func_def_node>> build_one_arg_funcs(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                             symbol_table::Scopes&                      scopes)
    {
        std::list<std::shared_ptr<ast::Usual_func_def_node>> result;

        for(const auto& info : one_arg_funcs_info)
        {
            const std::size_t func_name_id        = ids_trie->insert(std::u32string{info.func_name_});
            const std::size_t func_name_start_pos = info.func_name_start_pos_;
            const std::size_t func_name_end_pos   = info.func_name_start_pos_ + info.func_name_len_ - 1;
            const std::size_t num_of_lines        = info.num_of_lines_;
            const std::size_t arg_name_id         = ids_trie->insert(std::u32string{info.arg_name_});
            const std::size_t arg_name_start_pos  = info.arg_name_start_pos_;
            const std::size_t arg_name_end_pos    = info.arg_name_start_pos_ + info.arg_name_len_ - 1;

            std::size_t       current_line        = info.start_line_;

            for(std::size_t i = 0; i < num_of_lines; ++i)
            {
                const ast::Id_info func_name_info = {
                    .identifier_id_ = func_name_id,
                    .pos_           = iscaner::Position_range{
                        .begin_pos_ = iscaner::Position{current_line, func_name_start_pos},
                        .end_pos_   = iscaner::Position{current_line, func_name_end_pos}
                    }
                };

                const ast::Id_info arg_name_info = {
                    .identifier_id_ = arg_name_id,
                    .pos_           = iscaner::Position_range{
                        .begin_pos_ = iscaner::Position{current_line, arg_name_start_pos},
                        .end_pos_   = iscaner::Position{current_line, arg_name_end_pos}
                    }
                };

                const auto func_signature = std::make_shared<ast::Func_signature_node>(
                    ast::Func_signature_node::Groups_of_args{
                        std::make_shared<ast::Func_arg_group_node>(
                            std::vector<ast::Id_info>{arg_name_info},
                            std::make_shared<ast::Fixed_size_base_type_expr_node>(info.argument_types_[i])
                        )
                    },
                    std::make_shared<ast::Fixed_size_base_type_expr_node>(info.value_types_[i])
                );

                const auto func = std::make_shared<ast::Usual_func_def_node>(
                    ast::Exported_kind::Exported,
                    ast::Usual_func_def_node::Func_kind::Pure,
                    func_name_info,
                    func_signature,
                    nullptr
                );

                result.push_back(func);

                scopes.add_id_def(
                    func_name_id,
                    symbol_table::Id_kind::Func_name,
                    symbol_table::Def_for_specific_kind{
                        func_name_info.pos_,
                        func,
                        0
                    }
                );

                current_line++;
            }
        }

        return result;
    }

    struct Rounding_func_info{
        const char32_t*           func_name_;
        std::size_t               func_name_start_pos_;
        std::size_t               func_name_len_;
        const char32_t*           arg_name_;
        std::size_t               arg_name_start_pos_;
        std::size_t               arg_name_len_;
        std::size_t               start_line_;
        Fixed_size_base_type_kind argument_types_[4];
    };

    const Rounding_func_info rounding_funcs_info[] = {
        Rounding_func_info{
            .func_name_           = U"большц",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 182,
            .argument_types_      = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
            }
        },
        Rounding_func_info{
            .func_name_           = U"меньшц",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 187,
            .argument_types_      = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
            }
        },
        Rounding_func_info{
            .func_name_           = U"округл",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 6,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 30,
            .arg_name_len_        = 1,
            .start_line_          = 192,
            .argument_types_      = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
            }
        },
        Rounding_func_info{
            .func_name_           = U"усеч",
            .func_name_start_pos_ = 23,
            .func_name_len_       = 4,
            .arg_name_            = U"x",
            .arg_name_start_pos_  = 28,
            .arg_name_len_        = 1,
            .start_line_          = 197,
            .argument_types_      = {
                Fixed_size_base_type_kind::Float32,
                Fixed_size_base_type_kind::Float64,
                Fixed_size_base_type_kind::Float80,
                Fixed_size_base_type_kind::Float128,
            }
        },
    };

    std::list<std::shared_ptr<ast::Usual_func_def_node>> build_rounding_funcs(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                              symbol_table::Scopes&                      scopes)
    {
        std::list<std::shared_ptr<ast::Usual_func_def_node>> result;

        for(const auto& info : rounding_funcs_info)
        {
            const std::size_t func_name_id        = ids_trie->insert(std::u32string{info.func_name_});
            const std::size_t func_name_start_pos = info.func_name_start_pos_;
            const std::size_t func_name_end_pos   = info.func_name_start_pos_ + info.func_name_len_ - 1;
            const std::size_t arg_name_id         = ids_trie->insert(std::u32string{info.arg_name_});
            const std::size_t arg_name_start_pos  = info.arg_name_start_pos_;
            const std::size_t arg_name_end_pos    = info.arg_name_start_pos_ + info.arg_name_len_ - 1;

            std::size_t       current_line        = info.start_line_;

            for(std::size_t i = 0; i < 4; ++i)
            {
                const ast::Id_info func_name_info = {
                    .identifier_id_ = func_name_id,
                    .pos_           = iscaner::Position_range{
                        .begin_pos_ = iscaner::Position{current_line, func_name_start_pos},
                        .end_pos_   = iscaner::Position{current_line, func_name_end_pos}
                    }
                };

                const ast::Id_info arg_name_info = {
                    .identifier_id_ = arg_name_id,
                    .pos_           = iscaner::Position_range{
                        .begin_pos_ = iscaner::Position{current_line, arg_name_start_pos},
                        .end_pos_   = iscaner::Position{current_line, arg_name_end_pos}
                    }
                };

                const auto func_signature = std::make_shared<ast::Func_signature_node>(
                    ast::Func_signature_node::Groups_of_args{
                        std::make_shared<ast::Func_arg_group_node>(
                            std::vector<ast::Id_info>{arg_name_info},
                            std::make_shared<ast::Fixed_size_base_type_expr_node>(info.argument_types_[i])
                        )
                    },
                    std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                        std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{
                            ast::Non_fixed_size_base_type_expr_node::Modifier::Long
                        },
                        ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                    )
                );

                const auto func = std::make_shared<ast::Usual_func_def_node>(
                    ast::Exported_kind::Exported,
                    ast::Usual_func_def_node::Func_kind::Pure,
                    func_name_info,
                    func_signature,
                    nullptr
                );

                result.push_back(func);

                scopes.add_id_def(
                    func_name_id,
                    symbol_table::Id_kind::Func_name,
                    symbol_table::Def_for_specific_kind{
                        func_name_info.pos_,
                        func,
                        0
                    }
                );

                current_line++;
            }
        }

        return result;
    }

    const Fixed_size_base_type_kind types_for_log[] = {
        Fixed_size_base_type_kind::Float32,
        Fixed_size_base_type_kind::Float64,
        Fixed_size_base_type_kind::Float80,
        Fixed_size_base_type_kind::Float128,
    };

    std::list<std::shared_ptr<ast::Usual_func_def_node>> build_log_funcs(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                         symbol_table::Scopes&                      scopes)
    {
        std::list<std::shared_ptr<ast::Usual_func_def_node>> result;

        const std::size_t func_name_id           = ids_trie->insert(std::u32string{U"Log"});
        const std::size_t func_name_start_pos    = 23;
        const std::size_t func_name_end_pos      = 25;
        const std::size_t fst_arg_name_id        = ids_trie->insert(std::u32string{U"x"});
        const std::size_t fst_arg_name_start_pos = 27;
        const std::size_t fst_arg_name_end_pos   = 27;
        const std::size_t snd_arg_name_id        = ids_trie->insert(std::u32string{U"a"});
        const std::size_t snd_arg_name_start_pos = 30;
        const std::size_t snd_arg_name_end_pos   = 30;

        std::size_t       current_line           = 212;

        for(std::size_t i = 0; i < 4; ++i)
        {
            const ast::Id_info func_name_info = {
                .identifier_id_ = func_name_id,
                .pos_           = iscaner::Position_range{
                    .begin_pos_ = iscaner::Position{current_line, func_name_start_pos},
                    .end_pos_   = iscaner::Position{current_line, func_name_end_pos}
                }
            };

            const ast::Id_info fst_arg_name_info = {
                .identifier_id_ = fst_arg_name_id,
                .pos_           = iscaner::Position_range{
                    .begin_pos_ = iscaner::Position{current_line, fst_arg_name_start_pos},
                    .end_pos_   = iscaner::Position{current_line, fst_arg_name_end_pos}
                }
            };

            const ast::Id_info snd_arg_name_info = {
                .identifier_id_ = snd_arg_name_id,
                .pos_           = iscaner::Position_range{
                    .begin_pos_ = iscaner::Position{current_line, snd_arg_name_start_pos},
                    .end_pos_   = iscaner::Position{current_line, snd_arg_name_end_pos}
                }
            };

            const auto func_signature = std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{fst_arg_name_info, snd_arg_name_info},
                        std::make_shared<ast::Fixed_size_base_type_expr_node>(types_for_log[i])
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(types_for_log[i])
            );

            const auto func = std::make_shared<ast::Usual_func_def_node>(
                ast::Exported_kind::Exported,
                ast::Usual_func_def_node::Func_kind::Pure,
                func_name_info,
                func_signature,
                nullptr
            );

            result.push_back(func);

            scopes.add_id_def(
                func_name_id,
                symbol_table::Id_kind::Func_name,
                symbol_table::Def_for_specific_kind{
                    func_name_info.pos_,
                    func,
                    0
                }
            );

            current_line++;
        }
        return result;
    }
    std::shared_ptr<ast::Usual_func_def_node> build_pi(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                       symbol_table::Scopes&                      scopes)
    {
        const std::size_t func_name_id           = ids_trie->insert(std::u32string{U"пи"});
        const std::size_t func_name_start_pos    = 23;
        const std::size_t func_name_end_pos      = 24;

        const ast::Id_info func_name_info = {
            .identifier_id_ = func_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{220, func_name_start_pos},
                .end_pos_   = iscaner::Position{220, func_name_end_pos}
            }
        };

        const auto func_signature = std::make_shared<ast::Func_signature_node>(
            ast::Func_signature_node::Groups_of_args{},
            std::make_shared<ast::Fixed_size_base_type_expr_node>(Fixed_size_base_type_kind::Float128)
        );

        const auto func = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Pure,
            func_name_info,
            func_signature,
            nullptr
        );

        scopes.add_id_def(
            func_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                func_name_info.pos_,
                func,
                0
            }
        );

        return func;
    }

    std::shared_ptr<ast::Category_def_node> build_category_ravenstvo_TE(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                        symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id         = ids_trie->insert(std::u32string{U"T"});
        const std::size_t E_name_id         = ids_trie->insert(std::u32string{U"E"});
        const std::size_t x_name_id         = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id         = ids_trie->insert(std::u32string{U"y"});
        const std::size_t ravenstvo_name_id = ids_trie->insert(std::u32string{U"равенство"});

        const ast::Id_info T_name_info_eq = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{224, 26},
                .end_pos_   = iscaner::Position{224, 26}
            }
        };

        const ast::Id_info E_name_info_eq = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{224, 33},
                .end_pos_   = iscaner::Position{224, 33}
            }
        };

        const ast::Id_info x_name_info_eq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{224, 22},
                .end_pos_   = iscaner::Position{224, 22}
            }
        };

        const ast::Id_info y_name_info_eq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{224, 29},
                .end_pos_   = iscaner::Position{224, 29}
            }
        };

        const auto op_eq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::EQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_eq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_eq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_eq},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_eq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_neq = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{225, 26},
                .end_pos_   = iscaner::Position{225, 26}
            }
        };

        const ast::Id_info E_name_info_neq = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{225, 33},
                .end_pos_   = iscaner::Position{225, 33}
            }
        };

        const ast::Id_info x_name_info_neq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{225, 22},
                .end_pos_   = iscaner::Position{225, 22}
            }
        };

        const ast::Id_info y_name_info_neq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{225, 29},
                .end_pos_   = iscaner::Position{225, 29}
            }
        };

        const auto op_neq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::NEQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_neq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_neq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_neq},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_neq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{222, 36},
                .end_pos_   = iscaner::Position{222, 36}
            }
        };

        const ast::Id_info E_name_info_category_arg = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{222, 39},
                .end_pos_   = iscaner::Position{222, 39}
            }
        };

        const ast::Id_info ravenstvo_name_info = {
            .identifier_id_ = ravenstvo_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{222, 25},
                .end_pos_   = iscaner::Position{222, 33}
            }
        };

        const auto category_ravenstvo = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            ravenstvo_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg,
                        E_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            nullptr, // there are no used categories
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_eq_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_neq_header)
                }
            )
        );

        scopes.add_id_def(
            ravenstvo_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                ravenstvo_name_info.pos_,
                category_ravenstvo,
                0
            }
        );

        return category_ravenstvo;
    }

    std::shared_ptr<ast::Category_def_node> build_category_ravenstvo_TT(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                        symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id         = ids_trie->insert(std::u32string{U"T"});
        const std::size_t E_name_id         = ids_trie->insert(std::u32string{U"E"});
        const std::size_t x_name_id         = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id         = ids_trie->insert(std::u32string{U"y"});
        const std::size_t ravenstvo_name_id = ids_trie->insert(std::u32string{U"равенство"});

        const ast::Id_info T_name_info_eq1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{230, 26},
                .end_pos_   = iscaner::Position{230, 26}
            }
        };

        const ast::Id_info T_name_info_eq2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{230, 33},
                .end_pos_   = iscaner::Position{230, 33}
            }
        };

        const ast::Id_info x_name_info_eq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{230, 22},
                .end_pos_   = iscaner::Position{224, 22}
            }
        };

        const ast::Id_info y_name_info_eq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{230, 29},
                .end_pos_   = iscaner::Position{230, 29}
            }
        };

        const auto op_eq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::EQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_eq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_eq1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_eq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_eq2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_neq1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{231, 26},
                .end_pos_   = iscaner::Position{231, 26}
            }
        };

        const ast::Id_info T_name_info_neq2 = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{231, 33},
                .end_pos_   = iscaner::Position{231, 33}
            }
        };

        const ast::Id_info x_name_info_neq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{231, 22},
                .end_pos_   = iscaner::Position{231, 22}
            }
        };

        const ast::Id_info y_name_info_neq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{231, 29},
                .end_pos_   = iscaner::Position{231, 29}
            }
        };

        const auto op_neq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::NEQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_neq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_neq1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_neq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_neq2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{228, 36},
                .end_pos_   = iscaner::Position{228, 36}
            }
        };

        const ast::Id_info ravenstvo_name_info = {
            .identifier_id_ = ravenstvo_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{228, 25},
                .end_pos_   = iscaner::Position{228, 33}
            }
        };

        const auto category_ravenstvo = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            ravenstvo_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            nullptr, // there are no used categories
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_eq_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_neq_header)
                }
            )
        );

        scopes.add_id_def(
            ravenstvo_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                ravenstvo_name_info.pos_,
                category_ravenstvo,
                0
            }
        );

        return category_ravenstvo;
    }

    std::shared_ptr<ast::Category_def_node> build_category_uporydochenie_TE(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                            symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id              = ids_trie->insert(std::u32string{U"T"});
        const std::size_t E_name_id              = ids_trie->insert(std::u32string{U"E"});
        const std::size_t x_name_id              = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id              = ids_trie->insert(std::u32string{U"y"});
        const std::size_t uporyadochenie_name_id = ids_trie->insert(std::u32string{U"упорядочение"});
        const std::size_t ravenstvo_name_id      = ids_trie->insert(std::u32string{U"равенство"});

        const ast::Id_info T_name_info_less = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{236, 25},
                .end_pos_   = iscaner::Position{236, 25}
            }
        };

        const ast::Id_info E_name_info_less = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{236, 32},
                .end_pos_   = iscaner::Position{236, 32}
            }
        };

        const ast::Id_info x_name_info_less = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{236, 21},
                .end_pos_   = iscaner::Position{236, 21}
            }
        };

        const ast::Id_info y_name_info_less = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{236, 28},
                .end_pos_   = iscaner::Position{236, 28}
            }
        };

        const auto op_less_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::LT)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_less},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_less,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_less},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_less,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_leq = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{237, 26},
                .end_pos_   = iscaner::Position{237, 26}
            }
        };

        const ast::Id_info E_name_info_leq = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{237, 33},
                .end_pos_   = iscaner::Position{237, 33}
            }
        };

        const ast::Id_info x_name_info_leq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{237, 22},
                .end_pos_   = iscaner::Position{237, 22}
            }
        };

        const ast::Id_info y_name_info_leq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{237, 29},
                .end_pos_   = iscaner::Position{237, 29}
            }
        };

        const auto op_leq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::LEQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_leq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_leq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_leq},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_leq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_greater = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{238, 25},
                .end_pos_   = iscaner::Position{238, 25}
            }
        };

        const ast::Id_info E_name_info_greater = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{238, 32},
                .end_pos_   = iscaner::Position{238, 32}
            }
        };

        const ast::Id_info x_name_info_greater = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{238, 21},
                .end_pos_   = iscaner::Position{238, 21}
            }
        };

        const ast::Id_info y_name_info_greater = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{238, 28},
                .end_pos_   = iscaner::Position{238, 28}
            }
        };

        const auto op_greater_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::GT)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_greater},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_greater,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_greater},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_greater,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_geq = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{239, 26},
                .end_pos_   = iscaner::Position{239, 26}
            }
        };

        const ast::Id_info E_name_info_geq = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{239, 33},
                .end_pos_   = iscaner::Position{239, 33}
            }
        };

        const ast::Id_info x_name_info_geq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{239, 22},
                .end_pos_   = iscaner::Position{239,22}
            }
        };

        const ast::Id_info y_name_info_geq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{239, 29},
                .end_pos_   = iscaner::Position{239, 29}
            }
        };

        const auto op_geq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::GEQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_geq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_geq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_geq},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_geq,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_compare = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{240, 32},
                .end_pos_   = iscaner::Position{240, 32}
            }
        };

        const ast::Id_info E_name_info_compare = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{240, 39},
                .end_pos_   = iscaner::Position{240, 39}
            }
        };

        const ast::Id_info x_name_info_compare = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{240, 28},
                .end_pos_   = iscaner::Position{240, 28}
            }
        };

        const ast::Id_info y_name_info_compare = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{240, 35},
                .end_pos_   = iscaner::Position{240, 35}
            }
        };

        const auto op_compare_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Compare,
                .subkind_ = 0
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_compare},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_compare,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_compare},
                        std::make_shared<ast::Name_expr_node>(
                            E_name_info_compare,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Ord
                )
            )
        );

        const ast::Id_info T_name_info_used_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{234, 66},
                .end_pos_   = iscaner::Position{234, 66}
            }
        };

        const ast::Id_info E_name_info_used_category_arg = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{234, 69},
                .end_pos_   = iscaner::Position{234, 69}
            }
        };

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{234, 39},
                .end_pos_   = iscaner::Position{234, 39}
            }
        };

        const ast::Id_info E_name_info_category_arg = {
            .identifier_id_ = E_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{234, 42},
                .end_pos_   = iscaner::Position{234, 42}
            }
        };

        const ast::Id_info uporyadochenie_name_info = {
            .identifier_id_ = uporyadochenie_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{234, 25},
                .end_pos_   = iscaner::Position{234, 36}
            }
        };

        const ast::Id_info ravenstvo_name_info = {
            .identifier_id_ = ravenstvo_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{234, 55},
                .end_pos_   = iscaner::Position{234, 63}
            }
        };

        const auto category_uporyadochenie = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            uporyadochenie_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg,
                        E_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            std::make_shared<ast::Used_categories_node>(
                ast::Used_categories_node::Categories{
                    std::make_shared<ast::Used_category_node>(
                        std::make_shared<ast::Qualified_id_node>(
                            std::vector<ast::Id_info>{
                                ravenstvo_name_info
                            }
                        ),
                        ast::Used_category_node::Category_args{
                            std::make_shared<ast::Name_expr_node>(
                                T_name_info_used_category_arg,
                                ast::Name_expr_node::Suffixes{},
                                nullptr
                            ),
                            std::make_shared<ast::Name_expr_node>(
                                E_name_info_used_category_arg,
                                ast::Name_expr_node::Suffixes{},
                                nullptr
                            )
                        }
                    )
                }
            ),
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_less_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_leq_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_greater_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_geq_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_compare_header)
                }
            )
        );

        scopes.add_id_def(
            uporyadochenie_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                uporyadochenie_name_info.pos_,
                category_uporyadochenie,
                0
            }
        );

        return category_uporyadochenie;
    }

    std::shared_ptr<ast::Category_def_node> build_category_uporydochenie_TT(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                            symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id              = ids_trie->insert(std::u32string{U"T"});
        const std::size_t x_name_id              = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id              = ids_trie->insert(std::u32string{U"y"});
        const std::size_t uporyadochenie_name_id = ids_trie->insert(std::u32string{U"упорядочение"});
        const std::size_t ravenstvo_name_id      = ids_trie->insert(std::u32string{U"равенство"});

        const ast::Id_info T_name_info_less1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{245, 25},
                .end_pos_   = iscaner::Position{245, 25}
            }
        };

        const ast::Id_info T_name_info_less2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{245, 32},
                .end_pos_   = iscaner::Position{245, 32}
            }
        };

        const ast::Id_info x_name_info_less = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{245, 21},
                .end_pos_   = iscaner::Position{245, 21}
            }
        };

        const ast::Id_info y_name_info_less = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{245, 28},
                .end_pos_   = iscaner::Position{245, 28}
            }
        };

        const auto op_less_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::LT)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_less},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_less1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_less},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_less2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );
        const ast::Id_info T_name_info_leq1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{246, 26},
                .end_pos_   = iscaner::Position{246, 26}
            }
        };

        const ast::Id_info T_name_info_leq2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{246, 33},
                .end_pos_   = iscaner::Position{246, 33}
            }
        };

        const ast::Id_info x_name_info_leq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{246, 22},
                .end_pos_   = iscaner::Position{246, 22}
            }
        };

        const ast::Id_info y_name_info_leq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{246, 29},
                .end_pos_   = iscaner::Position{246, 29}
            }
        };

        const auto op_leq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::LEQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_leq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_leq1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_leq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_leq2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );
        const ast::Id_info T_name_info_greater1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{247, 25},
                .end_pos_   = iscaner::Position{247, 25}
            }
        };

        const ast::Id_info T_name_info_greater2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{247, 32},
                .end_pos_   = iscaner::Position{247, 32}
            }
        };

        const ast::Id_info x_name_info_greater = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{247, 21},
                .end_pos_   = iscaner::Position{247, 21}
            }
        };

        const ast::Id_info y_name_info_greater = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{247, 28},
                .end_pos_   = iscaner::Position{247, 28}
            }
        };

        const auto op_greater_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::GT)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_greater},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_greater1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_greater},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_greater2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );
        const ast::Id_info T_name_info_geq1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{248, 26},
                .end_pos_   = iscaner::Position{248, 26}
            }
        };

        const ast::Id_info T_name_info_geq2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{248, 33},
                .end_pos_   = iscaner::Position{248, 33}
            }
        };

        const ast::Id_info x_name_info_geq = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{248, 22},
                .end_pos_   = iscaner::Position{248, 22}
            }
        };

        const ast::Id_info y_name_info_geq = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{248, 29},
                .end_pos_   = iscaner::Position{248, 29}
            }
        };

        const auto op_geq_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Rel_op,
                .subkind_ = static_cast<uint32_t>(ast::Rel_op_kind::GEQ)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_geq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_geq1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_geq},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_geq2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Bool
                )
            )
        );

        const ast::Id_info T_name_info_compare1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{249, 32},
                .end_pos_   = iscaner::Position{249, 32}
            }
        };

        const ast::Id_info T_name_info_compare2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{249, 39},
                .end_pos_   = iscaner::Position{249, 39}
            }
        };

        const ast::Id_info x_name_info_compare = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{249, 28},
                .end_pos_   = iscaner::Position{249, 28}
            }
        };

        const ast::Id_info y_name_info_compare = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{240, 35},
                .end_pos_   = iscaner::Position{249, 35}
            }
        };

        const auto op_compare_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Compare,
                .subkind_ = 0
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_compare},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_compare1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_compare},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_compare2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Ord
                )
            )
        );

        const ast::Id_info T_name_info_used_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{243, 62},
                .end_pos_   = iscaner::Position{243, 62}
            }
        };

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{243, 39},
                .end_pos_   = iscaner::Position{243, 39}
            }
        };

        const ast::Id_info uporyadochenie_name_info = {
            .identifier_id_ = uporyadochenie_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{243, 25},
                .end_pos_   = iscaner::Position{243, 36}
            }
        };

        const ast::Id_info ravenstvo_name_info = {
            .identifier_id_ = ravenstvo_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{243, 51},
                .end_pos_   = iscaner::Position{243, 59}
            }
        };

        const auto category_uporyadochenie = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            uporyadochenie_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            std::make_shared<ast::Used_categories_node>(
                ast::Used_categories_node::Categories{
                    std::make_shared<ast::Used_category_node>(
                        std::make_shared<ast::Qualified_id_node>(
                            std::vector<ast::Id_info>{
                                ravenstvo_name_info
                            }
                        ),
                        ast::Used_category_node::Category_args{
                            std::make_shared<ast::Name_expr_node>(
                                T_name_info_used_category_arg,
                                ast::Name_expr_node::Suffixes{},
                                nullptr
                            )
                        }
                    )
                }
            ),
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_less_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_leq_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_greater_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_geq_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_compare_header)
                }
            )
        );

        scopes.add_id_def(
            uporyadochenie_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                uporyadochenie_name_info.pos_,
                category_uporyadochenie,
                0
            }
        );

        return category_uporyadochenie;
    }

    std::shared_ptr<ast::Category_def_node> build_category_preobr_v_stroku_T(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                             symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id               = ids_trie->insert(std::u32string{U"T"});
        const std::size_t x_name_id               = ids_trie->insert(std::u32string{U"x"});
        const std::size_t v_stroku_name_id        = ids_trie->insert(std::u32string{U"в_строку"});
        const std::size_t preobr_v_stroku_name_id = ids_trie->insert(std::u32string{U"преобр_в_строку"});

        const ast::Id_info preobr_v_stroku_name_info = {
            .identifier_id_ = preobr_v_stroku_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{252, 25},
                .end_pos_   = iscaner::Position{252, 39}
            }
        };

        const ast::Id_info v_stroku_name_info = {
            .identifier_id_ = v_stroku_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{254, 17},
                .end_pos_   = iscaner::Position{254, 24}
            }
        };

        const ast::Id_info T_name_info = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{254, 30},
                .end_pos_   = iscaner::Position{254, 30}
            }
        };

        const ast::Id_info x_name_info = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{254, 26},
                .end_pos_   = iscaner::Position{254, 26}
            }
        };

        const auto v_stroku_func_header = std::make_shared<ast::Usual_func_header_node>(
            ast::Pure_kind::Non_pure,
            v_stroku_name_info,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{
                            x_name_info
                        },
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                )
            )
        );

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{252, 42},
                .end_pos_   = iscaner::Position{252, 42}
            }
        };

        const auto category_preobr_v_stroku = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            preobr_v_stroku_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            nullptr,
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Func_header_as_category_body_elem_node>(v_stroku_func_header)
                }
            )
        );

        scopes.add_id_def(
            preobr_v_stroku_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                preobr_v_stroku_name_info.pos_,
                category_preobr_v_stroku,
                0
            }
        );

        return category_preobr_v_stroku;
    }

    std::shared_ptr<ast::Category_def_node> build_category_additivnyj_monoid_T(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                               symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id                 = ids_trie->insert(std::u32string{U"T"});
        const std::size_t x_name_id                 = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id                 = ids_trie->insert(std::u32string{U"y"});
        const std::size_t nul_name_id               = ids_trie->insert(std::u32string{U"нуль"});
        const std::size_t additivnyj_monoid_name_id = ids_trie->insert(std::u32string{U"аддитивный_моноид"});

        const ast::Id_info additivnyj_monoid_name_info = {
            .identifier_id_ = additivnyj_monoid_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{257, 25},
                .end_pos_   = iscaner::Position{257, 41}
            }
        };

        const ast::Id_info nul_name_info = {
            .identifier_id_ = nul_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{259, 15},
                .end_pos_   = iscaner::Position{259, 18}
            }
        };

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{257, 44},
                .end_pos_   = iscaner::Position{257, 44}
            }
        };

        const ast::Id_info T_name_info_type_of_nul = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{259, 21},
                .end_pos_   = iscaner::Position{259, 21}
            }
        };

        const auto constant_nul = std::make_shared<ast::Const_as_category_body_elem_node>(
            nul_name_info,
            std::make_shared<ast::Name_expr_node>(
                T_name_info_type_of_nul,
                ast::Name_expr_node::Suffixes{},
                nullptr
            )
        );

        const ast::Id_info T_name_info_add1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{260, 24},
                .end_pos_   = iscaner::Position{260, 24}
            }
        };

        const ast::Id_info T_name_info_add2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{260, 30},
                .end_pos_   = iscaner::Position{260, 30}
            }
        };

        const ast::Id_info T_name_info_add3 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{260, 35},
                .end_pos_   = iscaner::Position{260, 35}
            }
        };

        const ast::Id_info x_name_info_add = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{260, 21},
                .end_pos_   = iscaner::Position{260, 21}
            }
        };

        const ast::Id_info y_name_info_add = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{260, 27},
                .end_pos_   = iscaner::Position{260, 27}
            }
        };

        const auto op_add_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Add_like,
                .subkind_ = static_cast<uint32_t>(ast::Add_like_kind::Add)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_add},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_add1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_add},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_add2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    T_name_info_add3,
                    ast::Name_expr_node::Suffixes{},
                    nullptr
                )
            )
        );

        const ast::Id_info T_name_info_add_assign1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{261, 32},
                .end_pos_   = iscaner::Position{261, 32}
            }
        };

        const ast::Id_info T_name_info_add_assign2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{261, 38},
                .end_pos_   = iscaner::Position{261, 38}
            }
        };

        const ast::Id_info T_name_info_add_assign3 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{261, 50},
                .end_pos_   = iscaner::Position{261, 50}
            }
        };

        const ast::Id_info x_name_info_add_assign = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{261, 22},
                .end_pos_   = iscaner::Position{261, 22}
            }
        };

        const ast::Id_info y_name_info_add_assign = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{261, 35},
                .end_pos_   = iscaner::Position{261, 35}
            }
        };

        const auto op_add_assign_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Assignment,
                .subkind_ = static_cast<uint32_t>(ast::Assignment_kind::Plus_assign)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_add_assign},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Name_expr_node>(
                                T_name_info_add_assign1,
                                ast::Name_expr_node::Suffixes{},
                                nullptr
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Non_const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_add_assign},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_add_assign2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Ref_type_expr_node>(
                    std::make_shared<ast::Name_expr_node>(
                        T_name_info_add_assign3,
                        ast::Name_expr_node::Suffixes{},
                        nullptr
                    ),
                    ast::Ref_type_expr_node::Ref_kind::Non_const_ref
                )
            )
        );

        const auto category_additivnyj_monoid = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            additivnyj_monoid_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            nullptr, // there are no used categories
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    constant_nul,
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_add_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_add_assign_header)
                }
            )
        );

        scopes.add_id_def(
            additivnyj_monoid_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                additivnyj_monoid_name_info.pos_,
                category_additivnyj_monoid,
                0
            }
        );

        return category_additivnyj_monoid;
    }

    std::shared_ptr<ast::Category_def_node> build_category_multiplicativnyj_monoid_T(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                                     symbol_table::Scopes&                      scopes)
    {
        const std::size_t T_name_id                       = ids_trie->insert(std::u32string{U"T"});
        const std::size_t x_name_id                       = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id                       = ids_trie->insert(std::u32string{U"y"});
        const std::size_t edinica_name_id                 = ids_trie->insert(std::u32string{U"единица"});
        const std::size_t multiplicativnyj_monoid_name_id = ids_trie->insert(std::u32string{U"мультипликативный_моноид"});

        const ast::Id_info multiplicativnyj_monoid_name_info = {
            .identifier_id_ = multiplicativnyj_monoid_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{264, 25},
                .end_pos_   = iscaner::Position{264, 48}
            }
        };

        const ast::Id_info edinica_name_info = {
            .identifier_id_ = edinica_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{266, 15},
                .end_pos_   = iscaner::Position{266, 21}
            }
        };

        const ast::Id_info T_name_info_category_arg = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{264, 51},
                .end_pos_   = iscaner::Position{264, 51}
            }
        };

        const ast::Id_info T_name_info_type_of_edinica = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{266, 24},
                .end_pos_   = iscaner::Position{266, 24}
            }
        };

        const auto constant_edinica = std::make_shared<ast::Const_as_category_body_elem_node>(
            edinica_name_info,
            std::make_shared<ast::Name_expr_node>(
                T_name_info_type_of_edinica,
                ast::Name_expr_node::Suffixes{},
                nullptr
            )
        );

        const ast::Id_info T_name_info_mul1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{267, 24},
                .end_pos_   = iscaner::Position{267, 24}
            }
        };

        const ast::Id_info T_name_info_mul2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{267, 30},
                .end_pos_   = iscaner::Position{267, 30}
            }
        };

        const ast::Id_info T_name_info_mul3 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{267, 35},
                .end_pos_   = iscaner::Position{267, 35}
            }
        };

        const ast::Id_info x_name_info_mul = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{267, 21},
                .end_pos_   = iscaner::Position{267, 21}
            }
        };

        const ast::Id_info y_name_info_mul = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{267, 27},
                .end_pos_   = iscaner::Position{267, 27}
            }
        };

        const auto op_mul_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Mul_like,
                .subkind_ = static_cast<uint32_t>(ast::Mul_like_kind::Mul)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_mul},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_mul1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_mul},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_mul2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    T_name_info_mul3,
                    ast::Name_expr_node::Suffixes{},
                    nullptr
                )
            )
        );
        const ast::Id_info T_name_info_mul_assign1 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{268, 32},
                .end_pos_   = iscaner::Position{268, 32}
            }
        };

        const ast::Id_info T_name_info_mul_assign2 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{268, 38},
                .end_pos_   = iscaner::Position{268, 38}
            }
        };

        const ast::Id_info T_name_info_mul_assign3 = {
            .identifier_id_ = T_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{268, 50},
                .end_pos_   = iscaner::Position{268, 50}
            }
        };

        const ast::Id_info x_name_info_mul_assign = {
            .identifier_id_ = x_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{268, 22},
                .end_pos_   = iscaner::Position{268, 22}
            }
        };

        const ast::Id_info y_name_info_mul_assign = {
            .identifier_id_ = y_name_id,
            .pos_           = iscaner::Position_range{
                .begin_pos_ = iscaner::Position{268, 35},
                .end_pos_   = iscaner::Position{268, 35}
            }
        };

        const auto op_mul_assign_header = std::make_shared<ast::Operation_header_node>(
            ast::Pure_kind::Non_pure,
            ast::Operation_info{
                .kind_    = ast::Operation_kind::Assignment,
                .subkind_ = static_cast<uint32_t>(ast::Assignment_kind::Mul_assign)
            },
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{x_name_info_mul_assign},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Name_expr_node>(
                                T_name_info_mul_assign1,
                                ast::Name_expr_node::Suffixes{},
                                nullptr
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Non_const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{y_name_info_mul_assign},
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info_mul_assign2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Ref_type_expr_node>(
                    std::make_shared<ast::Name_expr_node>(
                        T_name_info_mul_assign3,
                        ast::Name_expr_node::Suffixes{},
                        nullptr
                    ),
                    ast::Ref_type_expr_node::Ref_kind::Non_const_ref
                )
            )
        );

        const auto category_multiplicativnyj_monoid = std::make_shared<ast::Category_def_node>(
            ast::Exported_kind::Exported,
            multiplicativnyj_monoid_name_info,
            ast::Category_def_node::Category_arg_groups{
                std::make_shared<ast::Category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info_category_arg
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                )
            },
            nullptr, // there are no used categories
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    constant_edinica,
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_mul_header),
                    std::make_shared<ast::Operation_header_as_category_body_elem_node>(op_mul_assign_header)
                }
            )
        );

        scopes.add_id_def(
            multiplicativnyj_monoid_name_id,
            symbol_table::Id_kind::Category_name,
            symbol_table::Def_for_specific_kind{
                multiplicativnyj_monoid_name_info.pos_,
                category_multiplicativnyj_monoid,
                0
            }
        );

        return category_multiplicativnyj_monoid;
    }

    std::shared_ptr<ast::Package_func_def_node> build_package_func_min(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                       symbol_table::Scopes&                      scopes)
    {
        const std::size_t min_name_id               = ids_trie->insert(std::u32string{U"min"});
        const std::size_t args_name_id              = ids_trie->insert(std::u32string{U"аргументы"});
        const std::size_t T_name_id                 = ids_trie->insert(std::u32string{U"T"});
        const std::size_t common_type_name_id       = ids_trie->insert(std::u32string{U"общий_тип"});

        const ast::Id_info min_name_info = {
            .identifier_id_ = min_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{217, 32},
                .end_pos_   = iscaner::Position{217, 34}
            }
        };

        const ast::Id_info args_name_info1 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{217, 42},
                .end_pos_   = iscaner::Position{217, 50}
            }
        };

        const ast::Id_info args_name_info2 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{217, 72},
                .end_pos_   = iscaner::Position{217, 80}
            }
        };

        const ast::Id_info T_name_info = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{217, 57},
                .end_pos_   = iscaner::Position{217, 57}
            }
        };

        const ast::Id_info common_type_name_info = {
            .identifier_id_ = common_type_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{217, 62},
                .end_pos_   = iscaner::Position{217, 70}
            }
        };

        const auto package_func_min = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Pure,
            min_name_info,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info1,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    common_type_name_info,
                    ast::Name_expr_node::Suffixes{
                        std::make_shared<ast::Call_args_node>(
                            ast::Call_args_node::Args{
                                std::make_shared<ast::Name_expr_node>(
                                    args_name_info2,
                                    ast::Name_expr_node::Suffixes{},
                                    nullptr
                                )
                            }
                        )
                    },
                    nullptr
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            min_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                min_name_info.pos_,
                package_func_min,
                0
            }
        );

        return package_func_min;
    }

    std::shared_ptr<ast::Package_func_def_node> build_package_func_max(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                       symbol_table::Scopes&                      scopes)
    {
        const std::size_t max_name_id               = ids_trie->insert(std::u32string{U"max"});
        const std::size_t args_name_id              = ids_trie->insert(std::u32string{U"аргументы"});
        const std::size_t T_name_id                 = ids_trie->insert(std::u32string{U"T"});
        const std::size_t common_type_name_id       = ids_trie->insert(std::u32string{U"общий_тип"});

        const ast::Id_info max_name_info = {
            .identifier_id_ = max_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{218, 32},
                .end_pos_   = iscaner::Position{217, 34}
            }
        };

        const ast::Id_info args_name_info1 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{218, 42},
                .end_pos_   = iscaner::Position{218, 50}
            }
        };

        const ast::Id_info args_name_info2 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{218, 72},
                .end_pos_   = iscaner::Position{218, 80}
            }
        };

        const ast::Id_info T_name_info = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{218, 57},
                .end_pos_   = iscaner::Position{218, 57}
            }
        };

        const ast::Id_info common_type_name_info = {
            .identifier_id_ = common_type_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{218, 62},
                .end_pos_   = iscaner::Position{218, 70}
            }
        };

        const auto package_func_max = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Pure,
            max_name_info,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info1,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    common_type_name_info,
                    ast::Name_expr_node::Suffixes{
                        std::make_shared<ast::Call_args_node>(
                            ast::Call_args_node::Args{
                                std::make_shared<ast::Name_expr_node>(
                                    args_name_info2,
                                    ast::Name_expr_node::Suffixes{},
                                    nullptr
                                )
                            }
                        )
                    },
                    nullptr
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            max_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                max_name_info.pos_,
                package_func_max,
                0
            }
        );

        return package_func_max;
    }
    std::shared_ptr<ast::Package_category_def_node> build_package_category_function_call(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                                         symbol_table::Scopes&                      scopes)
    {
        const std::size_t vyzov_funkcii_name_id     = ids_trie->insert(std::u32string{U"вызов_функции"});
        const std::size_t vyzov_name_id             = ids_trie->insert(std::u32string{U"вызов"});
        const std::size_t T_name_id                 = ids_trie->insert(std::u32string{U"T"});
        const std::size_t V_name_id                 = ids_trie->insert(std::u32string{U"V"});
        const std::size_t E_name_id                 = ids_trie->insert(std::u32string{U"E"});
        const std::size_t x_name_id                 = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id                 = ids_trie->insert(std::u32string{U"y"});
        const std::size_t p_name_id                 = ids_trie->insert(std::u32string{U"p"});
        const std::size_t package_types_name_id     = ids_trie->insert(std::u32string{U"типы_пакета"});

        const ast::Id_info vyzov_funkcii_name_info = {
            .identifier_id_ = vyzov_funkcii_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{271, 34},
                .end_pos_   = iscaner::Position{271, 46}
            }
        };

        const ast::Id_info vyzov_name_info = {
            .identifier_id_ = vyzov_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 26},
                .end_pos_   = iscaner::Position{273, 30}
            }
        };

        const ast::Id_info T_name_info1 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{271, 49},
                .end_pos_   = iscaner::Position{271, 49}
            }
        };

        const ast::Id_info T_name_info2 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 36},
                .end_pos_   = iscaner::Position{273, 36}
            }
        };

        const ast::Id_info V_name_info1 = {
            .identifier_id_ = V_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{271, 52},
                .end_pos_   = iscaner::Position{271, 52}
            }
        };

        const ast::Id_info V_name_info2 = {
            .identifier_id_ = V_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 70},
                .end_pos_   = iscaner::Position{273, 70}
            }
        };

        const ast::Id_info y_name_info1 = {
            .identifier_id_ = y_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{271, 67},
                .end_pos_   = iscaner::Position{271, 67}
            }
        };

        const ast::Id_info y_name_info2 = {
            .identifier_id_ = y_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 64},
                .end_pos_   = iscaner::Position{273, 64}
            }
        };

        const ast::Id_info E_name_info = {
            .identifier_id_ = E_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{271, 74},
                .end_pos_   = iscaner::Position{271, 74}
            }
        };

        const ast::Id_info x_name_info = {
            .identifier_id_ = x_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 32},
                .end_pos_   = iscaner::Position{273, 32}
            }
        };

        const ast::Id_info p_name_info = {
            .identifier_id_ = p_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 45},
                .end_pos_   = iscaner::Position{273, 45}
            }
        };

        const ast::Id_info package_types_name_info = {
            .identifier_id_ = package_types_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{273, 52},
                .end_pos_   = iscaner::Position{273, 62}
            }
        };

        const auto package_category_function_call = std::make_shared<ast::Package_category_def_node>(
            ast::Exported_kind::Exported,
            vyzov_funkcii_name_info,
            ast::Package_category_def_node::Category_arg_groups{
                std::make_shared<ast::Usual_package_category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info1, V_name_info1
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                ),
                std::make_shared<ast::Package_category_args_package_with_maybe_same_parametrical_type_node>(
                    y_name_info1,
                    std::make_shared<ast::Name_expr_node>(
                        E_name_info,
                        ast::Name_expr_node::Suffixes{},
                        nullptr
                    )
                )
            },
            nullptr, // there are no used categories
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Func_header_as_category_body_elem_node>(
                        std::make_shared<ast::Package_func_header_node>(
                            ast::Pure_kind::Non_pure,
                            vyzov_name_info,
                            std::make_shared<ast::Package_func_signature_node>(
                                ast::Package_func_signature_node::Groups_of_args{
                                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                                        std::vector<ast::Id_info>{
                                            x_name_info
                                        },
                                        std::make_shared<ast::Name_expr_node>(
                                            T_name_info2,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    ),
                                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                                        p_name_info,
                                        std::make_shared<ast::Name_expr_node>(
                                            package_types_name_info,
                                            ast::Name_expr_node::Suffixes{
                                                std::make_shared<ast::Call_args_node>(
                                                    ast::Call_args_node::Args{
                                                        std::make_shared<ast::Name_expr_node>(
                                                            y_name_info2,
                                                            ast::Name_expr_node::Suffixes{},
                                                            nullptr
                                                        )
                                                    }
                                                )
                                            },
                                            nullptr
                                        )
                                    )
                                },
                                std::make_shared<ast::Name_expr_node>(
                                    V_name_info2,
                                    ast::Name_expr_node::Suffixes{},
                                    nullptr
                                )
                            )
                        )
                    )
                }
            )
        );

        scopes.add_id_def(
            vyzov_funkcii_name_id,
            symbol_table::Id_kind::Package_category_name,
            symbol_table::Def_for_specific_kind{
                vyzov_funkcii_name_info.pos_,
                package_category_function_call,
                0
            }
        );

        return package_category_function_call;
    }

    std::shared_ptr<ast::Package_category_def_node> build_package_category_indexing(std::shared_ptr<strings::trie::Char_trie>& ids_trie,
                                                                                    symbol_table::Scopes&                      scopes)
    {
        const std::size_t indexing_name_id          = ids_trie->insert(std::u32string{U"индексация"});
        const std::size_t get_value_name_id         = ids_trie->insert(std::u32string{U"получить_значение"});
        const std::size_t get_ref_name_id           = ids_trie->insert(std::u32string{U"получить_ссылку"});
        const std::size_t T_name_id                 = ids_trie->insert(std::u32string{U"T"});
        const std::size_t V_name_id                 = ids_trie->insert(std::u32string{U"V"});
        const std::size_t E_name_id                 = ids_trie->insert(std::u32string{U"E"});
        const std::size_t x_name_id                 = ids_trie->insert(std::u32string{U"x"});
        const std::size_t y_name_id                 = ids_trie->insert(std::u32string{U"y"});
        const std::size_t p_name_id                 = ids_trie->insert(std::u32string{U"p"});
        const std::size_t package_types_name_id     = ids_trie->insert(std::u32string{U"типы_пакета"});

        const ast::Id_info indexing_name_info = {
            .identifier_id_ = indexing_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{276, 34},
                .end_pos_   = iscaner::Position{276, 43}
            }
        };

        const ast::Id_info get_value_name_info = {
            .identifier_id_ = get_value_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 33},
                .end_pos_   = iscaner::Position{278, 49}
            }
        };

        const ast::Id_info get_ref_name_info1 = {
            .identifier_id_ = get_ref_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 26},
                .end_pos_   = iscaner::Position{279, 40}
            }
        };

        const ast::Id_info get_ref_name_info2 = {
            .identifier_id_ = get_ref_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 33},
                .end_pos_   = iscaner::Position{280, 47}
            }
        };

        const ast::Id_info T_name_info1 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{276, 46},
                .end_pos_   = iscaner::Position{276, 46}
            }
        };

        const ast::Id_info T_name_info2 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 55},
                .end_pos_   = iscaner::Position{278, 55}
            }
        };

        const ast::Id_info T_name_info3 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 46},
                .end_pos_   = iscaner::Position{279, 46}
            }
        };

        const ast::Id_info T_name_info4 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 53},
                .end_pos_   = iscaner::Position{280, 53}
            }
        };

        const ast::Id_info V_name_info1 = {
            .identifier_id_ = V_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{276, 49},
                .end_pos_   = iscaner::Position{276, 49}
            }
        };

        const ast::Id_info V_name_info2 = {
            .identifier_id_ = V_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 89},
                .end_pos_   = iscaner::Position{278, 89}
            }
        };

        const ast::Id_info V_name_info3 = {
            .identifier_id_ = V_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 80},
                .end_pos_   = iscaner::Position{279, 80}
            }
        };

        const ast::Id_info V_name_info4 = {
            .identifier_id_ = V_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 100},
                .end_pos_   = iscaner::Position{280, 100}
            }
        };

        const ast::Id_info E_name_info = {
            .identifier_id_ = E_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{276, 71},
                .end_pos_   = iscaner::Position{276, 71}
            }
        };

        const ast::Id_info y_name_info1 = {
            .identifier_id_ = y_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{276, 64},
                .end_pos_   = iscaner::Position{276, 64}
            }
        };

        const ast::Id_info y_name_info2 = {
            .identifier_id_ = y_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 83},
                .end_pos_   = iscaner::Position{278, 83}
            }
        };

        const ast::Id_info y_name_info3 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 74},
                .end_pos_   = iscaner::Position{279, 74}
            }
        };

        const ast::Id_info y_name_info4 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 81},
                .end_pos_   = iscaner::Position{280, 81}
            }
        };

        const ast::Id_info p_name_info1 = {
            .identifier_id_ = p_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 64},
                .end_pos_   = iscaner::Position{278, 64}
            }
        };

        const ast::Id_info p_name_info2 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 55},
                .end_pos_   = iscaner::Position{279, 55}
            }
        };

        const ast::Id_info p_name_info3 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 62},
                .end_pos_   = iscaner::Position{280, 62}
            }
        };

        const ast::Id_info x_name_info1 = {
            .identifier_id_ = x_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 51},
                .end_pos_   = iscaner::Position{278, 51}
            }
        };

        const ast::Id_info x_name_info2 = {
            .identifier_id_ = x_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 42},
                .end_pos_   = iscaner::Position{279, 42}
            }
        };

        const ast::Id_info x_name_info3 = {
            .identifier_id_ = x_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 49},
                .end_pos_   = iscaner::Position{280, 49}
            }
        };

        const ast::Id_info package_types_name_info1 = {
            .identifier_id_ = package_types_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{278, 71},
                .end_pos_   = iscaner::Position{278, 81}
            }
        };

        const ast::Id_info package_types_name_info2 = {
            .identifier_id_ = package_types_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{279, 62},
                .end_pos_   = iscaner::Position{279, 72}
            }
        };

        const ast::Id_info package_types_name_info3 = {
            .identifier_id_ = package_types_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{280, 69},
                .end_pos_   = iscaner::Position{280, 79}
            }
        };

        const auto package_category_indexing = std::make_shared<ast::Package_category_def_node>(
            ast::Exported_kind::Exported,
            indexing_name_info,
            ast::Package_category_def_node::Category_arg_groups{
                std::make_shared<ast::Usual_package_category_arg_group_node>(
                    std::vector<ast::Id_info>{
                        T_name_info1, V_name_info1
                    },
                    std::make_shared<ast::Type_type_expr_node>()
                ),
                std::make_shared<ast::Package_category_args_package_with_maybe_same_parametrical_type_node>(
                    y_name_info1,
                    std::make_shared<ast::Name_expr_node>(
                        E_name_info,
                        ast::Name_expr_node::Suffixes{},
                        nullptr
                    )
                )
            },
            nullptr, // there are no used categories
            std::make_shared<ast::Category_body_node>(
                ast::Category_body_node::Category_body_elems{
                    std::make_shared<ast::Func_header_as_category_body_elem_node>(
                        std::make_shared<ast::Package_func_header_node>(
                            ast::Pure_kind::Pure,
                            get_value_name_info,
                            std::make_shared<ast::Package_func_signature_node>(
                                ast::Package_func_signature_node::Groups_of_args{
                                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                                        std::vector<ast::Id_info>{
                                            x_name_info1
                                        },
                                        std::make_shared<ast::Name_expr_node>(
                                            T_name_info2,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    ),
                                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                                        p_name_info1,
                                        std::make_shared<ast::Name_expr_node>(
                                            package_types_name_info1,
                                            ast::Name_expr_node::Suffixes{
                                                std::make_shared<ast::Call_args_node>(
                                                    ast::Call_args_node::Args{
                                                        std::make_shared<ast::Name_expr_node>(
                                                            y_name_info2,
                                                            ast::Name_expr_node::Suffixes{},
                                                            nullptr
                                                        )
                                                    }
                                                )
                                            },
                                            nullptr
                                        )
                                    )
                                },
                                std::make_shared<ast::Name_expr_node>(
                                    V_name_info2,
                                    ast::Name_expr_node::Suffixes{},
                                    nullptr
                                )
                            )
                        )
                    ),

                    std::make_shared<ast::Func_header_as_category_body_elem_node>(
                        std::make_shared<ast::Package_func_header_node>(
                            ast::Pure_kind::Non_pure,
                            get_ref_name_info1,
                            std::make_shared<ast::Package_func_signature_node>(
                                ast::Package_func_signature_node::Groups_of_args{
                                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                                        std::vector<ast::Id_info>{
                                            x_name_info2
                                        },
                                        std::make_shared<ast::Name_expr_node>(
                                            T_name_info3,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    ),
                                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                                        p_name_info2,
                                        std::make_shared<ast::Name_expr_node>(
                                            package_types_name_info2,
                                            ast::Name_expr_node::Suffixes{
                                                std::make_shared<ast::Call_args_node>(
                                                    ast::Call_args_node::Args{
                                                        std::make_shared<ast::Name_expr_node>(
                                                            y_name_info3,
                                                            ast::Name_expr_node::Suffixes{},
                                                            nullptr
                                                        )
                                                    }
                                                )
                                            },
                                            nullptr
                                        )
                                    )
                                },
                                std::make_shared<ast::Ref_type_expr_node>(
                                    std::make_shared<ast::Name_expr_node>(
                                        V_name_info3,
                                        ast::Name_expr_node::Suffixes{},
                                        nullptr
                                    ),
                                    ast::Ref_type_expr_node::Ref_kind::Non_const_ref
                                )
                            )
                        )
                    ),

                    std::make_shared<ast::Func_header_as_category_body_elem_node>(
                        std::make_shared<ast::Package_func_header_node>(
                            ast::Pure_kind::Pure,
                            get_ref_name_info2,
                            std::make_shared<ast::Package_func_signature_node>(
                                ast::Package_func_signature_node::Groups_of_args{
                                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                                        std::vector<ast::Id_info>{
                                            x_name_info3
                                        },
                                        std::make_shared<ast::Name_expr_node>(
                                            T_name_info4,
                                            ast::Name_expr_node::Suffixes{},
                                            nullptr
                                        )
                                    ),
                                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                                        p_name_info3,
                                        std::make_shared<ast::Name_expr_node>(
                                            package_types_name_info3,
                                            ast::Name_expr_node::Suffixes{
                                                std::make_shared<ast::Call_args_node>(
                                                    ast::Call_args_node::Args{
                                                        std::make_shared<ast::Name_expr_node>(
                                                            y_name_info4,
                                                            ast::Name_expr_node::Suffixes{},
                                                            nullptr
                                                        )
                                                    }
                                                )
                                            },
                                            nullptr
                                        )
                                    )
                                },
                                std::make_shared<ast::Ref_type_expr_node>(
                                    std::make_shared<ast::Name_expr_node>(
                                        V_name_info4,
                                        ast::Name_expr_node::Suffixes{},
                                        nullptr
                                    ),
                                    ast::Ref_type_expr_node::Ref_kind::Const_ref
                                )
                            )
                        )
                    )
                }
            )
        );

        scopes.add_id_def(
            indexing_name_id,
            symbol_table::Id_kind::Package_category_name,
            symbol_table::Def_for_specific_kind{
                indexing_name_info.pos_,
                package_category_indexing,
                0
            }
        );

        return package_category_indexing;
    }

    std::pair<std::shared_ptr<ast::Module_node>, symbol_table::Scopes> build_module_prelude_impl(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        symbol_table::Scopes              scopes;

        std::size_t prelude_name_id = ids_trie->insert(std::u32string{U"Преамбула"});
        const ast::Id_info prelude_name_info = {
            .identifier_id_ = prelude_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 8},
                .end_pos_   = iscaner::Position{1, 16}
            }
        };

        scopes.create_new_scope();

        const auto meta_func_vozmozhno                = build_meta_func_vozmozhno(ids_trie, scopes);
        const auto meta_func_variant                  = build_meta_func_variant(ids_trie, scopes);
        const auto one_arg_funcs                      = build_one_arg_funcs(ids_trie, scopes);
        const auto rounding_funcs                     = build_rounding_funcs(ids_trie, scopes);
        const auto log_funcs                          = build_log_funcs(ids_trie, scopes);
        const auto pi_func                            = build_pi(ids_trie, scopes);
        const auto category_ravenstvo_TE              = build_category_ravenstvo_TE(ids_trie, scopes);
        const auto category_ravenstvo_TT              = build_category_ravenstvo_TT(ids_trie, scopes);
        const auto category_uporyadochenie_TE         = build_category_uporydochenie_TE(ids_trie, scopes);
        const auto category_uporyadochenie_TT         = build_category_uporydochenie_TT(ids_trie, scopes);
        const auto category_preobr_v_stroku           = build_category_preobr_v_stroku_T(ids_trie, scopes);
        const auto category_additivnyj_monoid_T       = build_category_additivnyj_monoid_T(ids_trie, scopes);
        const auto category_multiplicativnyj_monoid_T = build_category_multiplicativnyj_monoid_T(ids_trie, scopes);
        const auto package_func_min                   = build_package_func_min(ids_trie, scopes);
        const auto package_func_max                   = build_package_func_max(ids_trie, scopes);
        const auto package_category_function_call     = build_package_category_function_call(ids_trie, scopes);
        const auto package_category_indexing          = build_package_category_indexing(ids_trie, scopes);

        ast::Block_node::Block_entries last_entries = {
            pi_func,                      category_ravenstvo_TE,              category_ravenstvo_TT,
            category_uporyadochenie_TE,   category_uporyadochenie_TT,         category_preobr_v_stroku,
            category_additivnyj_monoid_T, category_multiplicativnyj_monoid_T, package_func_min,
            package_func_max,             package_category_function_call,     package_category_indexing
        };

        ast::Block_node::Block_entries module_body_entries;

        module_body_entries.push_back(meta_func_vozmozhno);
        module_body_entries.push_back(meta_func_variant);
        module_body_entries.insert(module_body_entries.end(), one_arg_funcs.begin(), one_arg_funcs.end());
        module_body_entries.insert(module_body_entries.end(), rounding_funcs.begin(), rounding_funcs.end());
        module_body_entries.insert(module_body_entries.end(), log_funcs.begin(), log_funcs.end());
        module_body_entries.insert(module_body_entries.end(), last_entries.begin(), last_entries.end());

        const auto module_ast = std::make_shared<ast::Module_node>(
            // module name:
            std::make_shared<ast::Qualified_id_node>(
                std::vector<ast::Id_info>{
                    prelude_name_info
                }
            ),
            nullptr, // there are no used modules
            std::make_shared<ast::Block_node>(module_body_entries)
        );

        return {module_ast, scopes};
    }
}

namespace symbol_table{
    std::shared_ptr<Module> build_module_prelude(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const auto [module_ast, scopes] = build_module_prelude_impl(ids_trie);

        return std::make_shared<Module>(scopes, module_ast);
    }
}