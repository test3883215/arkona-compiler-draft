/*
    File:    scopes.cpp
    Created: 31 August 2023 at 20:44 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/scopes.hpp"

namespace symbol_table{
    void Scopes::create_new_scope()
    {
        scopes_.push_front(Scope{});
    }

    void Scopes::delete_current_scope()
    {
        scopes_.pop_front();
    }

    void Scopes::add_id_def(std::size_t id, Id_kind kind, const Def_for_specific_kind& def)
    {
        auto& current_scope = scopes_.front();
        current_scope.add_id_def(id, kind, def);
    }

    Defs_for_specific_id Scopes::get_id_defs(std::size_t id) const
    {
        for(const auto& s : scopes_)
        {
            const auto d = s.get_id_defs(id);
            if(!d.empty())
            {
                return d;
            }
        }
        return {};
    }
}