/*
    File:    module_system_io_files.cpp
    Created: 15 October 2023 at 16:00 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/module_system_io_files.hpp"

#include <array>
#include "../../arkona-ast-lib/include/all_nodes.hpp"
#include "../../iscanner/include/position.hpp"

namespace{
    ast::Id_info get_system_name_info(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t system_name_id = ids_trie->insert(std::u32string{U"Система"});

        const ast::Id_info system_name_info = {
            .identifier_id_ = system_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 8},
                .end_pos_   = iscaner::Position{1, 14}
            }
        };

        return system_name_info;
    }

    ast::Id_info get_io_name_info(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t io_name_id = ids_trie->insert(std::u32string{U"Ввод_вывод"});

        const ast::Id_info io_name_info = {
            .identifier_id_ = io_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 17},
                .end_pos_   = iscaner::Position{1, 26}
            }
        };

        return io_name_info;
    }

    ast::Id_info get_files_name_info(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t files_name_id = ids_trie->insert(std::u32string{U"Файлы"});

        const ast::Id_info files_name_info = {
            .identifier_id_ = files_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 29},
                .end_pos_   = iscaner::Position{1, 33}
            }
        };

        return files_name_info;
    }

    constexpr std::size_t num_of_names_strings = 5;

    std::array<ast::Id_info, num_of_names_strings> get_strings_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t strings_name_id = ids_trie->insert(std::u32string{U"Строки"});

        const ast::Id_info strings_name_info1 = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{2, 12},
                .end_pos_   = iscaner::Position{2, 17}
            }
        };

        const ast::Id_info strings_name_info2 = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 39},
                .end_pos_   = iscaner::Position{4, 44}
            }
        };

        const ast::Id_info strings_name_info3 = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 39},
                .end_pos_   = iscaner::Position{5, 44}
            }
        };

        const ast::Id_info strings_name_info4 = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 39},
                .end_pos_   = iscaner::Position{6, 44}
            }
        };

        const ast::Id_info strings_name_info5 = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 39},
                .end_pos_   = iscaner::Position{7, 44}
            }
        };

        return {
            strings_name_info1,
            strings_name_info2,
            strings_name_info3,
            strings_name_info4,
            strings_name_info5
        };
    }

    std::array<ast::Id_info, num_of_names_strings> get_encodings_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t encodings_name_id  = ids_trie->insert(std::u32string{U"Кодировки"});

        const ast::Id_info encodings_name_info1 = {
            .identifier_id_ = encodings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{2, 20},
                .end_pos_   = iscaner::Position{2, 28}
            }
        };

        const ast::Id_info encodings_name_info2 = {
            .identifier_id_ = encodings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 47},
                .end_pos_   = iscaner::Position{4, 55}
            }
        };

        const ast::Id_info encodings_name_info3 = {
            .identifier_id_ = encodings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 47},
                .end_pos_   = iscaner::Position{5, 55}
            }
        };

        const ast::Id_info encodings_name_info4 = {
            .identifier_id_ = encodings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 47},
                .end_pos_   = iscaner::Position{6, 55}
            }
        };

        const ast::Id_info encodings_name_info5 = {
            .identifier_id_ = encodings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 47},
                .end_pos_   = iscaner::Position{7, 55}
            }
        };

        return {
            encodings_name_info1,
            encodings_name_info2,
            encodings_name_info3,
            encodings_name_info4,
            encodings_name_info5
        };
    }

    struct Encoding_t_infos{
        std::size_t                 encoding_t_name_id_;
        std::array<ast::Id_info, 4> encoding_t_name_infos_;
    };

    Encoding_t_infos get_encoding_t_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t encoding_t_name_id = ids_trie->insert(std::u32string{U"Кодировка"});

        const ast::Id_info encoding_t_name_info1 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 25},
                .end_pos_   = iscaner::Position{4, 33}
            }
        };

        const ast::Id_info encoding_t_name_info2 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 58},
                .end_pos_   = iscaner::Position{4, 66}
            }
        };

        const ast::Id_info encoding_t_name_info3 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{26, 72},
                .end_pos_   = iscaner::Position{26, 80}
            }
        };

        const ast::Id_info encoding_t_name_info4 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{39, 72},
                .end_pos_   = iscaner::Position{39, 80}
            }
        };

        Encoding_t_infos result = {
            .encoding_t_name_id_ = encoding_t_name_id,
            .encoding_t_name_infos_ = {
                encoding_t_name_info1,
                encoding_t_name_info2,
                encoding_t_name_info3,
                encoding_t_name_info4
            }
        };

        return result;
    }

    struct Encoding8_t_infos{
        std::size_t                 encoding_t_name_id_;
        std::array<ast::Id_info, 4> encoding_t_name_infos_;
    };

    Encoding8_t_infos get_encoding8_t_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t encoding_t_name_id = ids_trie->insert(std::u32string{U"Кодировка8"});

        const ast::Id_info encoding_t_name_info1 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 25},
                .end_pos_   = iscaner::Position{5, 34}
            }
        };

        const ast::Id_info encoding_t_name_info2 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 58},
                .end_pos_   = iscaner::Position{5, 67}
            }
        };

        const ast::Id_info encoding_t_name_info3 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{29, 50},
                .end_pos_   = iscaner::Position{29, 59}
            }
        };

        const ast::Id_info encoding_t_name_info4 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{43, 50},
                .end_pos_   = iscaner::Position{43, 59}
            }
        };

        Encoding8_t_infos result = {
            .encoding_t_name_id_ = encoding_t_name_id,
            .encoding_t_name_infos_ = {
                encoding_t_name_info1,
                encoding_t_name_info2,
                encoding_t_name_info3,
                encoding_t_name_info4
            }
        };

        return result;
    }

    struct Encoding16_t_infos{
        std::size_t                 encoding_t_name_id_;
        std::array<ast::Id_info, 4> encoding_t_name_infos_;
    };

    Encoding16_t_infos get_encoding16_t_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t encoding_t_name_id = ids_trie->insert(std::u32string{U"Кодировка16"});

        const ast::Id_info encoding_t_name_info1 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 25},
                .end_pos_   = {6, 35}
            }
        };

        const ast::Id_info encoding_t_name_info2 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 58},
                .end_pos_   = iscaner::Position{6, 68}
            }
        };

        const ast::Id_info encoding_t_name_info3 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{32, 50},
                .end_pos_   = iscaner::Position{32, 60}
            }
        };

        const ast::Id_info encoding_t_name_info4 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{47, 50},
                .end_pos_   = iscaner::Position{47, 60}
            }
        };

        Encoding16_t_infos result = {
            .encoding_t_name_id_ = encoding_t_name_id,
            .encoding_t_name_infos_ = {
                encoding_t_name_info1,
                encoding_t_name_info2,
                encoding_t_name_info3,
                encoding_t_name_info4
            }
        };

        return result;
    }

    struct Encoding32_t_infos{
        std::size_t                 encoding_t_name_id_;
        std::array<ast::Id_info, 4> encoding_t_name_infos_;
    };

    Encoding32_t_infos get_encoding32_t_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t encoding_t_name_id = ids_trie->insert(std::u32string{U"Кодировка32"});

        const ast::Id_info encoding_t_name_info1 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 25},
                .end_pos_   = iscaner::Position{7, 35}
            }
        };

        const ast::Id_info encoding_t_name_info2 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 58},
                .end_pos_   = iscaner::Position{7, 68}
            }
        };

        const ast::Id_info encoding_t_name_info3 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{35, 50},
                .end_pos_   = iscaner::Position{35, 60}
            }
        };

        const ast::Id_info encoding_t_name_info4 = {
            .identifier_id_ = encoding_t_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{51, 50},
                .end_pos_   = iscaner::Position{51, 60}
            }
        };

        Encoding32_t_infos result = {
            .encoding_t_name_id_ = encoding_t_name_id,
            .encoding_t_name_infos_ = {
                encoding_t_name_info1,
                encoding_t_name_info2,
                encoding_t_name_info3,
                encoding_t_name_info4
            }
        };

        return result;
    }

    struct Mode_name_infos{
        std::size_t                 mode_name_id_;
        std::array<ast::Id_info, 3> mode_name_infos_;
    };

    Mode_name_infos get_mode_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t mode_name_id = ids_trie->insert(std::u32string{U"режим"});

        const ast::Id_info mode_name_info1 = {
            .identifier_id_ = mode_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{9, 20},
                .end_pos_   = iscaner::Position{9, 24}
            }
        };

        const ast::Id_info mode_name_info2 = {
            .identifier_id_ = mode_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{9, 41},
                .end_pos_   = iscaner::Position{9, 45}
            }
        };

        const ast::Id_info mode_name_info3 = {
            .identifier_id_ = mode_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{14, 62},
                .end_pos_   = iscaner::Position{14, 66}
            }
        };

        Mode_name_infos result = {
            .mode_name_id_ = mode_name_id,
            .mode_name_infos_ = {
                mode_name_info1,
                mode_name_info2,
                mode_name_info3
            }
        };

        return result;
    }

    struct Modes_name_infos{
        ast::Id_info read_name_info_;
        ast::Id_info write_name_info_;
        ast::Id_info trunc_name_info_;
        ast::Id_info append_name_info_;
    };

    Modes_name_infos get_modes_name_infos(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t read_name_id   = ids_trie->insert(std::u32string{U"чтение"});
        const std::size_t write_name_id  = ids_trie->insert(std::u32string{U"запись"});
        const std::size_t trunc_name_id  = ids_trie->insert(std::u32string{U"усечение"});
        const std::size_t append_name_id = ids_trie->insert(std::u32string{U"дописывание"});

        const ast::Id_info read_name_info = {
            .identifier_id_ = read_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 32},
                .end_pos_   = iscaner::Position{10, 37}
            }
        };

        const ast::Id_info write_name_info = {
            .identifier_id_ = write_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 40},
                .end_pos_   = iscaner::Position{10, 45}
            }
        };

        const ast::Id_info trunc_name_info = {
            .identifier_id_ = trunc_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 48},
                .end_pos_   = iscaner::Position{10, 55}
            }
        };

        const ast::Id_info append_name_info = {
            .identifier_id_ = append_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 58},
                .end_pos_   = iscaner::Position{10, 68}
            }
        };

        Modes_name_infos result = {
            .read_name_info_   = read_name_info,
            .write_name_info_  = write_name_info,
            .trunc_name_info_  = trunc_name_info,
            .append_name_info_ = append_name_info
        };

        return result;
    }

    struct Info_for_openf_and_sizef{
        std::size_t  openf_name_id_;
        std::size_t  sizef_name_id_;
        ast::Id_info openf_name_info_;
        ast::Id_info sizef_name_info_;
        ast::Id_info fname_name_info1_;
        ast::Id_info fname_name_info2_;
        ast::Id_info access_mode_name_info_;
    };

    Info_for_openf_and_sizef get_info_for_openf_and_sizef(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t openf_name_id       = ids_trie->insert(std::u32string{U"открытьф"});
        const std::size_t sizef_name_id       = ids_trie->insert(std::u32string{U"размерф"});
        const std::size_t fname_name_id       = ids_trie->insert(std::u32string{U"имя_файла"});
        const std::size_t access_mode_name_id = ids_trie->insert(std::u32string{U"режим_доступа"});

        const ast::Id_info openf_name_info = {
            .identifier_id_ = openf_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{13, 23},
                .end_pos_   = iscaner::Position{13, 30}
            }
        };

        const ast::Id_info sizef_name_info = {
            .identifier_id_ = sizef_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{16, 23},
                .end_pos_   = iscaner::Position{16, 29}
            }
        };

        const ast::Id_info fname_name_info1 = {
            .identifier_id_ = fname_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{13, 32},
                .end_pos_   = iscaner::Position{13, 40}
            }
        };

        const ast::Id_info fname_name_info2 = {
            .identifier_id_ = fname_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{16, 31},
                .end_pos_   = iscaner::Position{16, 39}
            }
        };

        const ast::Id_info access_mode_name_info = {
            .identifier_id_ = access_mode_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{14, 32},
                .end_pos_   = iscaner::Position{14, 44}
            }
        };

        Info_for_openf_and_sizef result = {
            .openf_name_id_         = openf_name_id,
            .sizef_name_id_         = sizef_name_id,
            .openf_name_info_       = openf_name_info,
            .sizef_name_info_       = sizef_name_info,
            .fname_name_info1_      = fname_name_info1,
            .fname_name_info2_      = fname_name_info2,
            .access_mode_name_info_ = access_mode_name_info
        };

        return result;
    }

    struct Info_for_closef_readf_writef{
        std::size_t                  closef_name_id_;
        std::size_t                  readf_name_id_;
        std::size_t                  writef_name_id_;
        ast::Id_info                 closef_name_info_;
        ast::Id_info                 readf_name_info_;
        ast::Id_info                 writef_name_info_;
        ast::Id_info                 data_name_info_;
        std::array<ast::Id_info, 11> handle_name_infos_;
    };

    Info_for_closef_readf_writef get_info_for_closef_readf_writef(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t closef_name_id = ids_trie->insert(std::u32string{U"закрытьф"});
        const std::size_t handle_name_id = ids_trie->insert(std::u32string{U"дескриптор"});
        const std::size_t readf_name_id  = ids_trie->insert(std::u32string{U"читатьф"});
        const std::size_t writef_name_id = ids_trie->insert(std::u32string{U"писатьф"});
        const std::size_t data_name_id   = ids_trie->insert(std::u32string{U"данные"});

        const ast::Id_info closef_name_info = {
            .identifier_id_ = closef_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{15, 23},
                .end_pos_   = iscaner::Position{15, 30}
            }
        };

        const ast::Id_info handle_name_info1 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{18, 32},
                .end_pos_   = iscaner::Position{18, 41}
            }
        };

        const ast::Id_info handle_name_info2 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{20, 31},
                .end_pos_   = iscaner::Position{20, 40}
            }
        };

        const ast::Id_info handle_name_info3 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{22, 31},
                .end_pos_   = iscaner::Position{22, 40}
            }
        };

        const ast::Id_info handle_name_info4 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{25, 59},
                .end_pos_   = iscaner::Position{25, 68}
            }
        };

        const ast::Id_info handle_name_info5 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{28, 37},
                .end_pos_   = iscaner::Position{28, 46}
            }
        };

        const ast::Id_info handle_name_info6 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{31, 37},
                .end_pos_   = iscaner::Position{31, 46}
            }
        };

        const ast::Id_info handle_name_info7 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{34, 37},
                .end_pos_   = iscaner::Position{34, 46}
            }
        };

        const ast::Id_info handle_name_info8 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{37, 59},
                .end_pos_   = iscaner::Position{37, 68}
            }
        };

        const ast::Id_info handle_name_info9 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{41, 37},
                .end_pos_   = iscaner::Position{41, 46}
            }
        };

        const ast::Id_info handle_name_info10 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{45, 37},
                .end_pos_   = iscaner::Position{45, 46}
            }
        };

        const ast::Id_info handle_name_info11 = {
            .identifier_id_ = handle_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{49, 37},
                .end_pos_   = iscaner::Position{49, 46}
            }
        };

        const ast::Id_info readf_name_info = {
            .identifier_id_ = readf_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{20, 23},
                .end_pos_   = iscaner::Position{20, 29}
            }
        };

        const ast::Id_info writef_name_info = {
            .identifier_id_ = writef_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{22, 23},
                .end_pos_   = iscaner::Position{22, 29}
            }
        };

        const ast::Id_info data_name_info = {
            .identifier_id_ = data_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{23, 31},
                .end_pos_   = iscaner::Position{23, 36}
            }
        };

        Info_for_closef_readf_writef result = {
            .closef_name_id_    = closef_name_id,
            .readf_name_id_     = readf_name_id,
            .writef_name_id_    = writef_name_id,
            .closef_name_info_  = closef_name_info,
            .readf_name_info_   = readf_name_info,
            .writef_name_info_  = writef_name_info,
            .data_name_info_    = data_name_info,
            .handle_name_infos_ = {
                handle_name_info1,  handle_name_info2, handle_name_info3,
                handle_name_info4,  handle_name_info5, handle_name_info6,
                handle_name_info7,  handle_name_info8, handle_name_info9,
                handle_name_info10, handle_name_info11
            }
        };

        return result;
    }

    struct Info_for_read_and_write_text{
        std::size_t                 read_text_name_id_;
        std::size_t                 write_text_name_id_;
        std::array<ast::Id_info, 4> read_text_name_infos_;
        std::array<ast::Id_info, 4> write_text_name_info_;
        std::array<ast::Id_info, 8> kodirovka_name_infos_;
        std::array<ast::Id_info, 4> variant_name_infos_;
        std::array<ast::Id_info, 4> text_name_infos_;
    };

    Info_for_read_and_write_text get_info_for_read_and_write_text(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const std::size_t read_text_name_id  = ids_trie->insert(std::u32string{U"читать_текст"});
        const std::size_t kodirovka_name_id  = ids_trie->insert(std::u32string{U"кодировка"});
        const std::size_t variant_name_id    = ids_trie->insert(std::u32string{U"вариант"});
        const std::size_t write_text_name_id = ids_trie->insert(std::u32string{U"писать_текст"});
        const std::size_t text_name_id       = ids_trie->insert(std::u32string{U"текст"});

        const ast::Id_info read_text_name_info1 = {
            .identifier_id_ = read_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{25, 46},
                .end_pos_   = iscaner::Position{25, 57}
            }
        };

        const ast::Id_info read_text_name_info2 = {
            .identifier_id_ = read_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{28, 24},
                .end_pos_   = iscaner::Position{28, 35}
            }
        };

        const ast::Id_info read_text_name_info3 = {
            .identifier_id_ = read_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{31, 24},
                .end_pos_   = iscaner::Position{31, 35}
            }
        };

        const ast::Id_info read_text_name_info4 = {
            .identifier_id_ = read_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{34, 24},
                .end_pos_   = iscaner::Position{34, 35}
            }
        };

        const ast::Id_info kodirovka_name_info1 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{26, 59},
                .end_pos_   = iscaner::Position{25, 67}
            }
        };

        const ast::Id_info kodirovka_name_info2 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{29, 37},
                .end_pos_   = iscaner::Position{29, 45}
            }
        };

        const ast::Id_info kodirovka_name_info3 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{32, 37},
                .end_pos_   = iscaner::Position{32, 45}
            }
        };

        const ast::Id_info kodirovka_name_info4 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{35, 37},
                .end_pos_   = iscaner::Position{35, 45}
            }
        };

        const ast::Id_info kodirovka_name_info5 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{39, 59},
                .end_pos_   = iscaner::Position{39, 67}
            }
        };

        const ast::Id_info kodirovka_name_info6 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{43, 37},
                .end_pos_   = iscaner::Position{43, 45}
            }
        };

        const ast::Id_info kodirovka_name_info7 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{47, 37},
                .end_pos_   = iscaner::Position{47, 45}
            }
        };

        const ast::Id_info kodirovka_name_info8 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{51, 37},
                .end_pos_   = iscaner::Position{51, 45}
            }
        };

        const ast::Id_info variant_name_info1 = {
            .identifier_id_ = variant_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{26, 85},
                .end_pos_   = iscaner::Position{26, 91}
            }
        };

        const ast::Id_info variant_name_info2 = {
            .identifier_id_ = variant_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{29, 64},
                .end_pos_   = iscaner::Position{29, 70}
            }
        };

        const ast::Id_info variant_name_info3 = {
            .identifier_id_ = variant_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{32, 65},
                .end_pos_   = iscaner::Position{32, 71}
            }
        };

        const ast::Id_info variant_name_info4 = {
            .identifier_id_ = variant_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{35, 65},
                .end_pos_   = iscaner::Position{35, 71}
            }
        };

        const ast::Id_info write_text_name_info1 = {
            .identifier_id_ = write_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{37, 46},
                .end_pos_   = iscaner::Position{37, 57}
            }
        };

        const ast::Id_info write_text_name_info2 = {
            .identifier_id_ = write_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{41, 24},
                .end_pos_   = iscaner::Position{41, 35}
            }
        };

        const ast::Id_info write_text_name_info3 = {
            .identifier_id_ = write_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{45,24},
                .end_pos_   = iscaner::Position{45, 35}
            }
        };

        const ast::Id_info write_text_name_info4 = {
            .identifier_id_ = write_text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{49, 24},
                .end_pos_   = iscaner::Position{49, 35}
            }
        };

        const ast::Id_info text_name_info1 = {
            .identifier_id_ = text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{38, 59},
                .end_pos_   = iscaner::Position{38, 63}
            }
        };

        const ast::Id_info text_name_info2 = {
            .identifier_id_ = text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{42, 37},
                .end_pos_   = iscaner::Position{42, 41}
            }
        };

        const ast::Id_info text_name_info3 = {
            .identifier_id_ = text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{46, 37},
                .end_pos_   = iscaner::Position{46, 41}
            }
        };

        const ast::Id_info text_name_info4 = {
            .identifier_id_ = text_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{50, 37},
                .end_pos_   = iscaner::Position{50, 41}
            }
        };

        Info_for_read_and_write_text result = {
            .read_text_name_id_    = read_text_name_id,
            .write_text_name_id_   = write_text_name_id,
            .read_text_name_infos_ = {
                read_text_name_info1,
                read_text_name_info2,
                read_text_name_info3,
                read_text_name_info4
            },
            .write_text_name_info_ = {
                write_text_name_info1,
                write_text_name_info2,
                write_text_name_info3,
                write_text_name_info4
            },
            .kodirovka_name_infos_ = {
                kodirovka_name_info1, kodirovka_name_info2, kodirovka_name_info3,
                kodirovka_name_info4, kodirovka_name_info5, kodirovka_name_info6,
                kodirovka_name_info7, kodirovka_name_info8
            },
            .variant_name_infos_ = {
                variant_name_info1, variant_name_info2,
                variant_name_info3, variant_name_info4
            },
            .text_name_infos_ = {
                text_name_info1, text_name_info2, 
                text_name_info3, text_name_info4
            }
        };

        return result;
    }

    std::pair<std::shared_ptr<ast::Module_node>, symbol_table::Scopes> build_module_system_io_files_impl(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        symbol_table::Scopes scopes;

        const auto system_name_info     = get_system_name_info(ids_trie);
        const auto io_name_info         = get_io_name_info(ids_trie);
        const auto files_name_info      = get_files_name_info(ids_trie);
        const auto strings_name_infos   = get_strings_name_infos(ids_trie);
        const auto encodings_name_infos = get_encodings_name_infos(ids_trie);

        const auto [encoding_t_name_id, encoding_t_name_infos]                          = get_encoding_t_name_infos(ids_trie);
        const auto [encoding8_t_name_id, encoding8_t_name_infos]                        = get_encoding8_t_name_infos(ids_trie);
        const auto [encoding16_t_name_id, encoding16_t_name_infos]                      = get_encoding16_t_name_infos(ids_trie);
        const auto [encoding32_t_name_id, encoding32_t_name_infos]                      = get_encoding32_t_name_infos(ids_trie);
        const auto [mode_name_id, mode_name_infos]                                      = get_mode_name_infos(ids_trie);
        const auto [read_name_info, write_name_info, trunc_name_info, append_name_info] = get_modes_name_infos(ids_trie);

        const auto [openf_name_id, 
                    sizef_name_id, 
                    openf_name_info,
                    sizef_name_info,
                    fname_name_info1,
                    fname_name_info2,
                    access_mode_name_info] = get_info_for_openf_and_sizef(ids_trie);

        const auto [closef_name_id,
                    readf_name_id,
                    writef_name_id,
                    closef_name_info,
                    readf_name_info,
                    writef_name_info,
                    data_name_info,
                    handle_name_infos] = get_info_for_closef_readf_writef(ids_trie);

        const auto [read_text_name_id,
                    write_text_name_id,
                    read_text_name_infos,
                    write_text_name_infos,
                    kodirovka_name_infos,
                    variant_name_infos,
                    text_name_infos] = get_info_for_read_and_write_text(ids_trie);

        scopes.create_new_scope();

        const auto alias_for_encoding = std::make_shared<ast::Alias_def_node>(
            ast::Exported_kind::Exported,
            encoding_t_name_infos[0],
            std::make_shared<ast::Name_expr_node>(
                strings_name_infos[1],
                ast::Name_expr_node::Suffixes{
                    std::make_shared<ast::Scoped_id_node>(
                        encodings_name_infos[1]
                    ),
                    std::make_shared<ast::Scoped_id_node>(
                        encoding_t_name_infos[1]
                    )
                },
                nullptr
            )
        );

        scopes.add_id_def(
            encoding_t_name_id,
            symbol_table::Id_kind::Alias_name,
            symbol_table::Def_for_specific_kind{
                encoding_t_name_infos[0].pos_,
                alias_for_encoding,
                0
            }
        );

        const auto alias_for_encoding8 = std::make_shared<ast::Alias_def_node>(
            ast::Exported_kind::Exported,
            encoding8_t_name_infos[0],
            std::make_shared<ast::Name_expr_node>(
                strings_name_infos[2],
                ast::Name_expr_node::Suffixes{
                    std::make_shared<ast::Scoped_id_node>(
                        encodings_name_infos[2]
                    ),
                    std::make_shared<ast::Scoped_id_node>(
                        encoding8_t_name_infos[1]
                    )
                },
                nullptr
            )
        );

        scopes.add_id_def(
            encoding8_t_name_id,
            symbol_table::Id_kind::Alias_name,
            symbol_table::Def_for_specific_kind{
                encoding8_t_name_infos[0].pos_,
                alias_for_encoding8,
                0
            }
        );

        const auto alias_for_encoding16 = std::make_shared<ast::Alias_def_node>(
            ast::Exported_kind::Exported,
            encoding16_t_name_infos[0],
            std::make_shared<ast::Name_expr_node>(
                strings_name_infos[3],
                ast::Name_expr_node::Suffixes{
                    std::make_shared<ast::Scoped_id_node>(
                        encodings_name_infos[3]
                    ),
                    std::make_shared<ast::Scoped_id_node>(
                        encoding16_t_name_infos[1]
                    )
                },
                nullptr
            )
        );

        scopes.add_id_def(
            encoding16_t_name_id,
            symbol_table::Id_kind::Alias_name,
            symbol_table::Def_for_specific_kind{
                encoding16_t_name_infos[0].pos_,
                alias_for_encoding16,
                0
            }
        );

        const auto alias_for_encoding32 = std::make_shared<ast::Alias_def_node>(
            ast::Exported_kind::Exported,
            encoding32_t_name_infos[0],
            std::make_shared<ast::Name_expr_node>(
                strings_name_infos[4],
                ast::Name_expr_node::Suffixes{
                    std::make_shared<ast::Scoped_id_node>(
                        encodings_name_infos[4]
                    ),
                    std::make_shared<ast::Scoped_id_node>(
                        encoding32_t_name_infos[1]
                    )
                },
                nullptr
            )
        );

        scopes.add_id_def(
            encoding32_t_name_id,
            symbol_table::Id_kind::Alias_name,
            symbol_table::Def_for_specific_kind{
                encoding32_t_name_infos[0].pos_,
                alias_for_encoding32,
                0
            }
        );

        const auto mode_enum = std::make_shared<ast::Enum_type_expr_node>(
            mode_name_infos[1],
            ast::Enum_type_expr_node::Elements_of_enum_type{
                read_name_info,
                write_name_info,
                trunc_name_info,
                append_name_info
            }
        );

        scopes.add_id_def(
            mode_name_id,
            symbol_table::Id_kind::Enum_type_name,
            symbol_table::Def_for_specific_kind{
                mode_name_infos[1].pos_,
                mode_enum,
                0
            }
        );

        const auto types = std::make_shared<ast::Type_def_node>(
            ast::Exported_kind::Exported,
            std::vector<ast::Type_def_node::Type_info>{
                ast::Type_def_node::Type_info{
                    mode_name_infos[0],
                    mode_enum
                }
            }
        );

        scopes.add_id_def(
            mode_name_id,
            symbol_table::Id_kind::Type_name,
            symbol_table::Def_for_specific_kind{
                mode_name_infos[0].pos_,
                types,
                0
            }
        );

        const auto openf_func = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Non_pure_and_non_main,
            openf_name_info,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{fname_name_info1},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{access_mode_name_info},
                        std::make_shared<ast::Set_type_expr_node>(
                            std::make_shared<ast::Name_expr_node>(
                                mode_name_infos[2],
                                ast::Name_expr_node::Suffixes{},
                                nullptr
                            )
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            openf_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                openf_name_info.pos_,
                openf_func,
                0
            }
        );

        const auto sizef_func = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Non_pure_and_non_main,
            sizef_name_info,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{fname_name_info2},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Unsigned
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            sizef_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                sizef_name_info.pos_,
                sizef_func,
                0
            }
        );

        const auto closef_func = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Non_pure_and_non_main,
            closef_name_info,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[0]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            closef_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                closef_name_info.pos_,
                closef_func,
                0
            }
        );

        const auto readf_func = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Non_pure_and_non_main,
            readf_name_info,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[1]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    )
                },
                std::make_shared<ast::Array_type_expr_node>(
                    ast::Array_type_expr_node::Indices_of_array_type{},
                    std::make_shared<ast::Fixed_size_base_type_expr_node>(
                        ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::Unsigned8
                    )
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            readf_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                readf_name_info.pos_,
                readf_func,
                0
            }
        );

        const auto writef_func = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Non_pure_and_non_main,
            writef_name_info,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[2]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{data_name_info},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Array_type_expr_node>(
                                ast::Array_type_expr_node::Indices_of_array_type{},
                                std::make_shared<ast::Fixed_size_base_type_expr_node>(
                                    ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::Unsigned8
                                )
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    )
                },
                std::make_shared<ast::Array_type_expr_node>(
                    ast::Array_type_expr_node::Indices_of_array_type{},
                    std::make_shared<ast::Fixed_size_base_type_expr_node>(
                        ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::Unsigned8
                    )
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            writef_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                writef_name_info.pos_,
                writef_func,
                0
            }
        );

        const auto abstract_generic_read_text_func = std::make_shared<ast::Abstract_generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            read_text_name_infos[0],
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[3]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[0]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding_t_name_infos[2],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    variant_name_infos[0],
                    ast::Name_expr_node::Suffixes{
                        std::make_shared<ast::Meta_call_args_node>(
                            ast::Meta_call_args_node::Args{
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                                ),
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                                )
                            }
                        )
                    },
                    nullptr
                )
            )
        );

        scopes.add_id_def(
            read_text_name_id,
            symbol_table::Id_kind::Abstract_generic_func_name,
            symbol_table::Def_for_specific_kind{
                read_text_name_infos[0].pos_,
                abstract_generic_read_text_func,
                0
            }
        );

        const auto generic_read_text_func8 = std::make_shared<ast::Generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            read_text_name_infos[1],
            std::make_shared<ast::Func_signature_node>(
               ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[4]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[1]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding8_t_name_infos[2],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    variant_name_infos[1],
                    ast::Name_expr_node::Suffixes{
                        std::make_shared<ast::Meta_call_args_node>(
                            ast::Meta_call_args_node::Args{
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                                ),
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                                )
                            }
                        )
                    },
                    nullptr
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            read_text_name_id,
            symbol_table::Id_kind::Generic_func_name,
            symbol_table::Def_for_specific_kind{
                read_text_name_infos[1].pos_,
                generic_read_text_func8,
                0
            }
        );

        const auto generic_read_text_func16 = std::make_shared<ast::Generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            read_text_name_infos[2],
            std::make_shared<ast::Func_signature_node>(
               ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[5]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[2]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding16_t_name_infos[2],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    variant_name_infos[2],
                    ast::Name_expr_node::Suffixes{
                        std::make_shared<ast::Meta_call_args_node>(
                            ast::Meta_call_args_node::Args{
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                                ),
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                                )
                            }
                        )
                    },
                    nullptr
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            read_text_name_id,
            symbol_table::Id_kind::Generic_func_name,
            symbol_table::Def_for_specific_kind{
                read_text_name_infos[2].pos_,
                generic_read_text_func16,
                0
            }
        );

        const auto generic_read_text_func32 = std::make_shared<ast::Generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            read_text_name_infos[3],
            std::make_shared<ast::Func_signature_node>(
               ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[6]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[3]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding32_t_name_infos[2],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Name_expr_node>(
                    variant_name_infos[3],
                    ast::Name_expr_node::Suffixes{
                        std::make_shared<ast::Meta_call_args_node>(
                            ast::Meta_call_args_node::Args{
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                                ),
                                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                                )
                            }
                        )
                    },
                    nullptr
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            read_text_name_id,
            symbol_table::Id_kind::Generic_func_name,
            symbol_table::Def_for_specific_kind{
                read_text_name_infos[3].pos_,
                generic_read_text_func32,
                0
            }
        );

        const auto abstract_generic_write_text_func = std::make_shared<ast::Abstract_generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            write_text_name_infos[0],
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[7]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{text_name_infos[0]},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[4]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding_t_name_infos[3],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                )
            )
        );

        scopes.add_id_def(
            write_text_name_id,
            symbol_table::Id_kind::Abstract_generic_func_name,
            symbol_table::Def_for_specific_kind{
                write_text_name_infos[0].pos_,
                abstract_generic_write_text_func,
                0
            }
        );

        const auto generic_write_text_func8 = std::make_shared<ast::Generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            write_text_name_infos[1],
            std::make_shared<ast::Func_signature_node>(
               ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[8]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{text_name_infos[1]},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[5]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding8_t_name_infos[3],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            write_text_name_id,
            symbol_table::Id_kind::Generic_func_name,
            symbol_table::Def_for_specific_kind{
                write_text_name_infos[1].pos_,
                generic_write_text_func8,
                0
            }
        );

        const auto generic_write_text_func16 = std::make_shared<ast::Generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            write_text_name_infos[2],
            std::make_shared<ast::Func_signature_node>(
               ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[9]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{text_name_infos[2]},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[6]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding16_t_name_infos[3],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            write_text_name_id,
            symbol_table::Id_kind::Generic_func_name,
            symbol_table::Def_for_specific_kind{
                write_text_name_infos[2].pos_,
                generic_write_text_func16,
                0
            }
        );

        const auto generic_write_text_func32 = std::make_shared<ast::Generic_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            write_text_name_infos[3],
            std::make_shared<ast::Func_signature_node>(
               ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{handle_name_infos[10]},
                        std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                            std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                            ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{text_name_infos[3]},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_infos[7]},
                        std::make_shared<ast::Name_expr_node>(
                            encoding32_t_name_infos[3],
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::Signed
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            write_text_name_id,
            symbol_table::Id_kind::Generic_func_name,
            symbol_table::Def_for_specific_kind{
                write_text_name_infos[3].pos_,
                generic_write_text_func32,
                0
            }
        );

        const auto module_ast = std::make_shared<ast::Module_node>(
            // module name:
            std::make_shared<ast::Qualified_id_node>(
                std::vector<ast::Id_info>{
                    system_name_info, 
                    io_name_info, 
                    files_name_info
                }
            ),
            // used modules:
            std::make_shared<ast::Used_modules_node>(
                ast::Used_modules_node::Names_of_used_modules{
                    std::make_shared<ast::Qualified_id_node>(
                        std::vector<ast::Id_info>{
                            strings_name_infos[0],
                            encodings_name_infos[0]
                        }
                    )
                }
            ),
            // module body:
            std::make_shared<ast::Block_node>(
                ast::Block_node::Block_entries{
                    alias_for_encoding,
                    alias_for_encoding8,
                    alias_for_encoding16,
                    alias_for_encoding32,
                    types,
                    openf_func,
                    sizef_func,
                    closef_func,
                    readf_func,
                    writef_func,
                    abstract_generic_read_text_func,
                    generic_read_text_func8,
                    generic_read_text_func16,
                    generic_read_text_func32,
                    abstract_generic_write_text_func,
                    generic_write_text_func8,
                    generic_write_text_func16,
                    generic_write_text_func32
                }
            )
        );

        return {module_ast, scopes};
    }
}

namespace symbol_table{
    std::shared_ptr<Module> build_module_system_io_files(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const auto [module_ast, scopes] = build_module_system_io_files_impl(ids_trie);

        return std::make_shared<Module>(scopes, module_ast);
    }
}