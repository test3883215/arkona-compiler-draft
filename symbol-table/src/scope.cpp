/*
    File:    scope.cpp
    Created: 29 August 2023 at 19:39 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/scope.hpp"

namespace symbol_table{
    Defs_for_specific_id Scope::get_id_defs(std::size_t id) const
    {
        auto it = defs_.find(id);
        if(it == defs_.end())
        {
            return {};
        }

        const auto defs_for_specific_id = it->second;
        return defs_for_specific_id;
    }

    void Scope::add_id_def(std::size_t id, Id_kind kind, const Def_for_specific_kind& def)
    {
        auto defs_it = defs_.find(id);
        Defs_for_specific_id defs_for_specific_id;
        if(defs_it != defs_.end())
        {
            defs_for_specific_id = defs_it->second;
        }

        auto defs_for_specific_id_it = defs_for_specific_id.find(kind);
        Defs_for_specific_kind defs_for_specific_kind;
        if(defs_for_specific_id_it != defs_for_specific_id.end())
        {
            defs_for_specific_kind = defs_for_specific_id_it->second;
        }

        defs_for_specific_kind.push_front(def);
        defs_for_specific_id[kind] = defs_for_specific_kind;
        defs_[id] = defs_for_specific_id;
    }
}