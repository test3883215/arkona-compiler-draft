/*
    File:    module_strings_encoding.cpp
    Created: 02 October 2023 at 20:52 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/module_strings_encoding.hpp"
#include "../../arkona-ast-lib/include/all_nodes.hpp"
#include "../../iscanner/include/position.hpp"

namespace{
    std::pair<std::shared_ptr<ast::Module_node>, symbol_table::Scopes> build_module_strings_encoding_impl(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        symbol_table::Scopes              scopes;

        const std::size_t strings_name_id   = ids_trie->insert(std::u32string{U"Строки"});
        const std::size_t encodings_name_id = ids_trie->insert(std::u32string{U"Кодировки"});

        const ast::Id_info strings_name_info = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 8},
                .end_pos_   = iscaner::Position{1, 13}
            }
        };

        const ast::Id_info encodings_name_info = {
            .identifier_id_ = encodings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 16},
                .end_pos_   = iscaner::Position{1, 24}
            }
        };

        const std::size_t encoding32_name_id = ids_trie->insert(std::u32string{U"Кодировка32"});
        const std::size_t encoding16_name_id = ids_trie->insert(std::u32string{U"Кодировка16"});
        const std::size_t encoding8_name_id  = ids_trie->insert(std::u32string{U"Кодировка8"});
        const std::size_t encoding_name_id   = ids_trie->insert(std::u32string{U"Кодировка"});

        const std::size_t ucs4_name_id       = ids_trie->insert(std::u32string{U"UCS4"});
        const std::size_t ucs2_name_id       = ids_trie->insert(std::u32string{U"UCS2"});
        const std::size_t cp866_name_id      = ids_trie->insert(std::u32string{U"CP866"});
        const std::size_t cp1251_name_id     = ids_trie->insert(std::u32string{U"CP1251"});
        const std::size_t koi8r_name_id      = ids_trie->insert(std::u32string{U"KOI8R"});
        const std::size_t utf8_name_id       = ids_trie->insert(std::u32string{U"UTF8"});

        const ast::Id_info encoding_name_info = {
            .identifier_id_ = encoding_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 20},
                .end_pos_   = iscaner::Position{6, 28}
            }
        };

        const ast::Id_info encoding32_name_info1 = {
            .identifier_id_ = encoding32_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 20},
                .end_pos_   = iscaner::Position{3, 30}
            }
        };

        const ast::Id_info encoding32_name_info2 = {
            .identifier_id_ = encoding32_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 47},
                .end_pos_   = iscaner::Position{3, 57}
            }
        };

        const ast::Id_info encoding32_name_info3 = {
            .identifier_id_ = encoding32_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 69},
                .end_pos_   = iscaner::Position{6, 79}
            }
        };

        const ast::Id_info encoding16_name_info1 = {
            .identifier_id_ = encoding16_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 20},
                .end_pos_   = iscaner::Position{4, 30}
            }
        };

        const ast::Id_info encoding16_name_info2 = {
            .identifier_id_ = encoding16_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 47},
                .end_pos_   = iscaner::Position{4, 57}
            }
        };

        const ast::Id_info encoding16_name_info3 = {
            .identifier_id_ = encoding16_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 56},
                .end_pos_   = iscaner::Position{6, 66}
            }
        };

        const ast::Id_info encoding16_name_info4 = {
            .identifier_id_ = encoding16_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{12, 55},
                .end_pos_   = iscaner::Position{12, 65}
            }
        };

        const ast::Id_info encoding16_name_info5 = {
            .identifier_id_ = encoding16_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{18, 54},
                .end_pos_   = iscaner::Position{18, 64}
            }
        };

        const ast::Id_info encoding8_name_info1 = {
            .identifier_id_ = encoding8_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 20},
                .end_pos_   = iscaner::Position{5, 29}
            }
        };

        const ast::Id_info encoding8_name_info2 = {
            .identifier_id_ = encoding8_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 47},
                .end_pos_   = iscaner::Position{5, 56}
            }
        };

        const ast::Id_info encoding8_name_info3 = {
            .identifier_id_ = encoding8_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 44},
                .end_pos_   = iscaner::Position{6, 53}
            }
        };

        const ast::Id_info encoding8_name_info4 = {
            .identifier_id_ = encoding8_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{9, 55},
                .end_pos_   = iscaner::Position{9, 64}
            }
        };

        const ast::Id_info encoding8_name_info5 = {
            .identifier_id_ = encoding8_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{15, 54},
                .end_pos_   = iscaner::Position{15, 63}
            }
        };

        const ast::Id_info ucs4_name_info = {
            .identifier_id_ = ucs4_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 59},
                .end_pos_   = iscaner::Position{3, 62}
            }
        };

        const ast::Id_info ucs2_name_info = {
            .identifier_id_ = ucs2_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 59},
                .end_pos_   = iscaner::Position{4, 62}
            }
        };

        const ast::Id_info cp866_name_info = {
            .identifier_id_ = cp866_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 58},
                .end_pos_   = iscaner::Position{5, 62}
            }
        };

        const ast::Id_info cp1251_name_info = {
            .identifier_id_ = cp1251_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 65},
                .end_pos_   = iscaner::Position{5, 70}
            }
        };

        const ast::Id_info koi8r_name_info = {
            .identifier_id_ = koi8r_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 73},
                .end_pos_   = iscaner::Position{5, 77}
            }
        };

        const ast::Id_info utf8_name_info = {
            .identifier_id_ = utf8_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{5, 80},
                .end_pos_   = iscaner::Position{5, 83}
            }
        };

        scopes.create_new_scope();

        const auto encoding32_enum = std::make_shared<ast::Enum_type_expr_node>(
            encoding32_name_info2,
            ast::Enum_type_expr_node::Elements_of_enum_type{ucs4_name_info}
        );

        scopes.add_id_def(
            encoding32_name_id,
            symbol_table::Id_kind::Enum_type_name,
            symbol_table::Def_for_specific_kind{
                encoding32_name_info2.pos_,
                encoding32_enum,
                0
            }
        );

        const auto encoding16_enum = std::make_shared<ast::Enum_type_expr_node>(
            encoding32_name_info2,
            ast::Enum_type_expr_node::Elements_of_enum_type{ucs2_name_info}
        );

        scopes.add_id_def(
            encoding16_name_id,
            symbol_table::Id_kind::Enum_type_name,
            symbol_table::Def_for_specific_kind{
                encoding16_name_info2.pos_,
                encoding16_enum,
                0
            }
        );

        const auto encoding8_enum = std::make_shared<ast::Enum_type_expr_node>(
            encoding8_name_info2,
            ast::Enum_type_expr_node::Elements_of_enum_type{
                cp866_name_info,
                cp1251_name_info,
                koi8r_name_info,
                utf8_name_info
            }
        );

        scopes.add_id_def(
            encoding8_name_id,
            symbol_table::Id_kind::Enum_type_name,
            symbol_table::Def_for_specific_kind{
                encoding8_name_info2.pos_,
                encoding8_enum,
                0
            }
        );

        const auto encoding_gen = std::make_shared<ast::Gen_type_expr_node>(
            ast::Gen_type_expr_node::Elems_of_gen_type{
                std::make_shared<ast::Name_expr_node>(
                    encoding8_name_info3,
                    ast::Name_expr_node::Suffixes{},
                    nullptr
                ),
                std::make_shared<ast::Name_expr_node>(
                    encoding16_name_info3,
                    ast::Name_expr_node::Suffixes{},
                    nullptr
                ),
                std::make_shared<ast::Name_expr_node>(
                    encoding32_name_info3,
                    ast::Name_expr_node::Suffixes{},
                    nullptr
                )
            }
        );

        const auto types = std::make_shared<ast::Type_def_node>(
            ast::Exported_kind::Exported,
            std::vector<ast::Type_def_node::Type_info>{
                ast::Type_def_node::Type_info{
                    encoding32_name_info1,
                    encoding32_enum
                },
                ast::Type_def_node::Type_info{
                    encoding16_name_info1,
                    encoding16_enum
                },
                ast::Type_def_node::Type_info{
                    encoding8_name_info1,
                    encoding8_enum
                },
                ast::Type_def_node::Type_info{
                    encoding_name_info,
                    encoding_gen
                },
            }
        );

        scopes.add_id_def(
            encoding32_name_id,
            symbol_table::Id_kind::Type_name,
            symbol_table::Def_for_specific_kind{
                encoding32_name_info1.pos_,
                types,
                0
            }
        );

        scopes.add_id_def(
            encoding16_name_id,
            symbol_table::Id_kind::Type_name,
            symbol_table::Def_for_specific_kind{
                encoding16_name_info1.pos_,
                types,
                1
            }
        );

        scopes.add_id_def(
            encoding8_name_id,
            symbol_table::Id_kind::Type_name,
            symbol_table::Def_for_specific_kind{
                encoding8_name_info1.pos_,
                types,
                2
            }
        );

        scopes.add_id_def(
            encoding_name_id,
            symbol_table::Id_kind::Type_name,
            symbol_table::Def_for_specific_kind{
                encoding_name_info.pos_,
                types,
                3
            }
        );

        const std::size_t iz_kodirovki_name_id = ids_trie->insert(std::u32string{U"из_кодировки"});
        const std::size_t vs_name_id           = ids_trie->insert(std::u32string{U"s"});
        const std::size_t kodirovka_name_id    = ids_trie->insert(std::u32string{U"кодировка"});
        const std::size_t v_kodirovku_name_id  = ids_trie->insert(std::u32string{U"в_кодировку"});

        const ast::Id_info v_kodirovku_name_info1 = {
            .identifier_id_ = v_kodirovku_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{14, 30},
                .end_pos_   = iscaner::Position{14, 40}
            }
        };

        const ast::Id_info v_kodirovku_name_info2 = {
            .identifier_id_ = v_kodirovku_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{17, 30},
                .end_pos_   = iscaner::Position{17, 40}
            }
        };

        const ast::Id_info iz_kodirovki_name_info1 = {
            .identifier_id_ = iz_kodirovki_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{8, 30},
                .end_pos_   = iscaner::Position{8, 41}
            }
        };

        const ast::Id_info iz_kodirovki_name_info2 = {
            .identifier_id_ = iz_kodirovki_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{11, 30},
                .end_pos_   = iscaner::Position{11, 41}
            }
        };

        const ast::Id_info vs_name_info1 = {
            .identifier_id_ = vs_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{8, 43},
                .end_pos_   = iscaner::Position{8, 43}
            }
        };

        const ast::Id_info vs_name_info2 = {
            .identifier_id_ = vs_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{11, 43},
                .end_pos_   = iscaner::Position{11, 43}
            }
        };

        const ast::Id_info vs_name_info3 = {
            .identifier_id_ = vs_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{14, 42},
                .end_pos_   = iscaner::Position{14, 42}
            }
        };

        const ast::Id_info vs_name_info4 = {
            .identifier_id_ = vs_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{17, 42},
                .end_pos_   = iscaner::Position{17, 42}
            }
        };

        const ast::Id_info kodirovka_name_info1 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{9, 43},
                .end_pos_   = iscaner::Position{9, 51}
            }
        };

        const ast::Id_info kodirovka_name_info2 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{11, 43},
                .end_pos_   = iscaner::Position{11, 51}
            }
        };

        const ast::Id_info kodirovka_name_info3 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{15, 42},
                .end_pos_   = iscaner::Position{15, 50}
            }
        };

        const ast::Id_info kodirovka_name_info4 = {
            .identifier_id_ = kodirovka_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{18, 42},
                .end_pos_   = iscaner::Position{18, 50}
            }
        };

        const auto from_encoding8 = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Pure,
            iz_kodirovki_name_info1,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{vs_name_info1},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Fixed_size_base_type_expr_node>(
                                ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String8
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_info1},
                        std::make_shared<ast::Name_expr_node>(
                            encoding8_name_info4,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            iz_kodirovki_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                iz_kodirovki_name_info1.pos_,
                from_encoding8,
                0
            }
        );

        const auto from_encoding16 = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Pure,
            iz_kodirovki_name_info2,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{vs_name_info2},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Fixed_size_base_type_expr_node>(
                                ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String16
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_info2},
                        std::make_shared<ast::Name_expr_node>(
                            encoding16_name_info4,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            iz_kodirovki_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                iz_kodirovki_name_info2.pos_,
                from_encoding16,
                0
            }
        );

        const auto to_encoding8 = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Pure,
            v_kodirovku_name_info1,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{vs_name_info3},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_info3},
                        std::make_shared<ast::Name_expr_node>(
                            encoding8_name_info5,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(
                    ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String8
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            v_kodirovku_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                v_kodirovku_name_info1.pos_,
                to_encoding8,
                0
            }
        );

        const auto to_encoding16 = std::make_shared<ast::Usual_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Usual_func_def_node::Func_kind::Pure,
            v_kodirovku_name_info2,
            std::make_shared<ast::Func_signature_node>(
                ast::Func_signature_node::Groups_of_args{
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{vs_name_info4},
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Func_arg_group_node>(
                        std::vector<ast::Id_info>{kodirovka_name_info4},
                        std::make_shared<ast::Name_expr_node>(
                            encoding16_name_info5,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(
                    ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String16
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            v_kodirovku_name_id,
            symbol_table::Id_kind::Func_name,
            symbol_table::Def_for_specific_kind{
                v_kodirovku_name_info2.pos_,
                to_encoding16,
                0
            }
        );

        const auto module_ast = std::make_shared<ast::Module_node>(
            // module name:
            std::make_shared<ast::Qualified_id_node>(
                std::vector<ast::Id_info>{
                    strings_name_info, encodings_name_info
                }
            ),
            nullptr, // there are no used modules
            std::make_shared<ast::Block_node>(
                ast::Block_node::Block_entries{
                    types,
                    from_encoding8,
                    from_encoding16,
                    to_encoding8,
                    to_encoding16
                }
            )
        );

        return {module_ast, scopes};
    }
}

namespace symbol_table{
    std::shared_ptr<Module> build_module_strings_encoding(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const auto [module_ast, scopes] = build_module_strings_encoding_impl(ids_trie);

        return std::make_shared<Module>(scopes, module_ast);
    }
}