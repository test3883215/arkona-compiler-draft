/*
    File:    module_system_io.cpp
    Created: 12 October 2023 at 20:50 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/module_system_io.hpp"

#include "../../arkona-ast-lib/include/all_nodes.hpp"
#include "../../iscanner/include/position.hpp"

namespace{
    std::pair<std::shared_ptr<ast::Module_node>, symbol_table::Scopes> build_module_system_io_impl(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        symbol_table::Scopes              scopes;

        const std::size_t system_name_id = ids_trie->insert(std::u32string{U"Система"});
        const std::size_t io_name_id     = ids_trie->insert(std::u32string{U"Ввод_вывод"});
        const std::size_t print_name_id  = ids_trie->insert(std::u32string{U"печать"});
        const std::size_t input_name_id  = ids_trie->insert(std::u32string{U"ввод"});
        const std::size_t fmt_name_id    = ids_trie->insert(std::u32string{U"fmt"});
        const std::size_t args_name_id   = ids_trie->insert(std::u32string{U"аргументы"});
        const std::size_t T_name_id      = ids_trie->insert(std::u32string{U"T"});

        const ast::Id_info system_name_info = {
            .identifier_id_ = system_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 8},
                .end_pos_   = iscaner::Position{1, 14}
            }
        };

        const ast::Id_info io_name_info = {
            .identifier_id_ = io_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 17},
                .end_pos_   = iscaner::Position{1, 26}
            }
        };

        const ast::Id_info print_name_info = {
            .identifier_id_ = print_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 32},
                .end_pos_   = iscaner::Position{3, 37}
            }
        };

        const ast::Id_info input_name_info = {
            .identifier_id_ = input_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 32},
                .end_pos_   = iscaner::Position{6, 34}
            }
        };

        const ast::Id_info fmt_name_info1 = {
            .identifier_id_ = fmt_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 39},
                .end_pos_   = iscaner::Position{3, 41}
            }
        };

        const ast::Id_info fmt_name_info2 = {
            .identifier_id_ = fmt_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 37},
                .end_pos_   = iscaner::Position{6, 39}
            }
        };

        const ast::Id_info args_name_info1 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 45},
                .end_pos_   = iscaner::Position{4, 53}
            }
        };

        const ast::Id_info args_name_info2 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 43},
                .end_pos_   = iscaner::Position{7, 51}
            }
        };

        const ast::Id_info T_name_info1 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 60},
                .end_pos_   = iscaner::Position{4, 60}
            }
        };

        const ast::Id_info T_name_info2 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 58},
                .end_pos_   = iscaner::Position{7, 58}
            }
        };

        scopes.create_new_scope();

        const auto print_func = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            print_name_info,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                        std::vector<ast::Id_info>{
                            fmt_name_info1
                        },
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info1,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::Void)
            ),
            nullptr
        );

        scopes.add_id_def(
            print_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                print_name_info.pos_,
                print_func,
                0
            }
        );

        const auto input_func = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            input_name_info,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                        std::vector<ast::Id_info>{
                            fmt_name_info2
                        },
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info2,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::Void)
            ),
            nullptr
        );

        scopes.add_id_def(
            input_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                input_name_info.pos_,
                input_func,
                0
            }
        );

        const auto module_ast = std::make_shared<ast::Module_node>(
            // module name:
            std::make_shared<ast::Qualified_id_node>(
                std::vector<ast::Id_info>{
                    system_name_info, io_name_info
                }
            ),
            nullptr, // there are no used modules
            std::make_shared<ast::Block_node>(
                ast::Block_node::Block_entries{
                    print_func,
                    input_func
                }
            )
        );

        return {module_ast, scopes};
    }
}

namespace symbol_table{
    std::shared_ptr<Module> build_module_system_io(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const auto [module_ast, scopes] = build_module_system_io_impl(ids_trie);

        return std::make_shared<Module>(scopes, module_ast);
    }
}