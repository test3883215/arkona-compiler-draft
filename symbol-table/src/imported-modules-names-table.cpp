/*
    File:    imported-modules-names-table.cpp
    Created: 28 August 2023 at 14:58 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/imported-modules-names-table.hpp"

namespace symbol_table{
    bool Table_of_names_of_imported_modules::contains(const std::vector<ast::Id_info>& qid)
    {
        std::size_t idx = qids_trie_->insert(qid);
        
        const auto it = set_of_indices_of_qids_.find(idx);

        return it != set_of_indices_of_qids_.end();
    }

    std::size_t Table_of_names_of_imported_modules::add_qid(const std::vector<ast::Id_info>& qid)
    {
        std::size_t idx = qids_trie_->insert(qid);
        set_of_indices_of_qids_.insert(idx);
        return idx;
    }
}