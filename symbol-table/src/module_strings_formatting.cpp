/*
    File:    module_strings_formatting.cpp
    Created: 14 October 2023 at 15:32 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/module_strings_formatting.hpp"

#include "../../arkona-ast-lib/include/all_nodes.hpp"
#include "../../iscanner/include/position.hpp"

namespace{
    std::pair<std::shared_ptr<ast::Module_node>, symbol_table::Scopes> build_module_strings_formatting_impl(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        symbol_table::Scopes              scopes;

        scopes.create_new_scope();

        const std::size_t strings_name_id    = ids_trie->insert(std::u32string{U"Строки"});
        const std::size_t formatting_name_id = ids_trie->insert(std::u32string{U"Форматирование"});
        const std::size_t format_name_id     = ids_trie->insert(std::u32string{U"формат"});
        const std::size_t fmt_name_id        = ids_trie->insert(std::u32string{U"fmt"});
        const std::size_t args_name_id       = ids_trie->insert(std::u32string{U"аргументы"});
        const std::size_t T_name_id          = ids_trie->insert(std::u32string{U"T"});

        const ast::Id_info strings_name_info = {
            .identifier_id_ = strings_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 8},
                .end_pos_   = iscaner::Position{1, 13}
            }
        };

        const ast::Id_info formatting_name_info = {
            .identifier_id_ = formatting_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{1, 16},
                .end_pos_   = iscaner::Position{1, 29}
            }
        };

        const ast::Id_info format_name_info1 = {
            .identifier_id_ = format_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 32},
                .end_pos_   = iscaner::Position{3, 37}
            }
        };

        const ast::Id_info format_name_info2 = {
            .identifier_id_ = format_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 32},
                .end_pos_   = iscaner::Position{6, 37}
            }
        };

        const ast::Id_info format_name_info3 = {
            .identifier_id_ = format_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{9, 32},
                .end_pos_   = iscaner::Position{9, 37}
            }
        };

        const ast::Id_info fmt_name_info1 = {
            .identifier_id_ = fmt_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{3, 39},
                .end_pos_   = iscaner::Position{3, 41}
            }
        };

        const ast::Id_info fmt_name_info2 = {
            .identifier_id_ = fmt_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{6, 39},
                .end_pos_   = iscaner::Position{6, 41}
            }
        };

        const ast::Id_info fmt_name_info3 = {
            .identifier_id_ = fmt_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{9, 39},
                .end_pos_   = iscaner::Position{9, 41}
            }
        };

        const ast::Id_info args_name_info1 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 45},
                .end_pos_   = iscaner::Position{4, 53}
            }
        };

        const ast::Id_info args_name_info2 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 45},
                .end_pos_   = iscaner::Position{7, 53}
            }
        };

        const ast::Id_info args_name_info3 = {
            .identifier_id_ = args_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 45},
                .end_pos_   = iscaner::Position{10, 53}
            }
        };

        const ast::Id_info T_name_info1 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{4, 60},
                .end_pos_   = iscaner::Position{4, 60}
            }
        };

        const ast::Id_info T_name_info2 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{7, 60},
                .end_pos_   = iscaner::Position{7, 60}
            }
        };

        const ast::Id_info T_name_info3 = {
            .identifier_id_ = T_name_id,
            .pos_           = {
                .begin_pos_ = iscaner::Position{10, 60},
                .end_pos_   = iscaner::Position{10, 60}
            }
        };

        const auto format_func_for_string = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            format_name_info1,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                        std::vector<ast::Id_info>{
                            fmt_name_info1
                        },
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                                std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                                ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info1,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info1,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Non_fixed_size_base_type_expr_node>(
                    std::vector<ast::Non_fixed_size_base_type_expr_node::Modifier>{},
                    ast::Non_fixed_size_base_type_expr_node::Non_fixed_size_base_type_kind::String
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            format_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                format_name_info1.pos_,
                format_func_for_string,
                0
            }
        );

        const auto format_func_for_string8 = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            format_name_info2,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                        std::vector<ast::Id_info>{
                            fmt_name_info2
                        },
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Fixed_size_base_type_expr_node>(
                                ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String8
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info2,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info2,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(
                    ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String8
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            format_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                format_name_info2.pos_,
                format_func_for_string8,
                0
            }
        );

        const auto format_func_for_string16 = std::make_shared<ast::Package_func_def_node>(
            ast::Exported_kind::Exported,
            ast::Pure_kind::Non_pure,
            format_name_info3,
            std::make_shared<ast::Package_func_signature_node>(
                ast::Package_func_signature_node::Groups_of_args{
                    std::make_shared<ast::Usual_package_func_arg_group_node>(
                        std::vector<ast::Id_info>{
                            fmt_name_info3
                        },
                        std::make_shared<ast::Ref_type_expr_node>(
                            std::make_shared<ast::Fixed_size_base_type_expr_node>(
                                ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String16
                            ),
                            ast::Ref_type_expr_node::Ref_kind::Const_ref
                        )
                    ),
                    std::make_shared<ast::Package_func_args_package_with_maybe_same_parametrical_type_node>(
                        args_name_info3,
                        std::make_shared<ast::Name_expr_node>(
                            T_name_info3,
                            ast::Name_expr_node::Suffixes{},
                            nullptr
                        )
                    )
                },
                std::make_shared<ast::Fixed_size_base_type_expr_node>(
                    ast::Fixed_size_base_type_expr_node::Fixed_size_base_type_kind::String16
                )
            ),
            nullptr
        );

        scopes.add_id_def(
            format_name_id,
            symbol_table::Id_kind::Package_func_name,
            symbol_table::Def_for_specific_kind{
                format_name_info3.pos_,
                format_func_for_string16,
                0
            }
        );

        const auto module_ast = std::make_shared<ast::Module_node>(
            // module name:
            std::make_shared<ast::Qualified_id_node>(
                std::vector<ast::Id_info>{
                    strings_name_info, formatting_name_info
                }
            ),
            nullptr, // there are no used modules
            std::make_shared<ast::Block_node>(
                ast::Block_node::Block_entries{
                    format_func_for_string,
                    format_func_for_string8,
                    format_func_for_string16
                }
            )
        );

        return {module_ast, scopes};
    }
}

namespace symbol_table{
    std::shared_ptr<Module> build_module_strings_formatting(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        const auto [module_ast, scopes] = build_module_strings_formatting_impl(ids_trie);

        return std::make_shared<Module>(scopes, module_ast);
    }
}