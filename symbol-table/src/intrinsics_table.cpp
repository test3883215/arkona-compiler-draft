/*
    File:    intrinsics_table.cpp
    Created: 28 October 2023 at 17:43 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include "../include/intrinsics_table.hpp"

#include <utility>

namespace{
    const std::pair<const char32_t*, symbol_table::Intrinsic_kind> data[] = {
        {U"алгебраич_компоненты", symbol_table::Intrinsic_kind::Get_algebraic_components},
        {U"алгебраич_ли",         symbol_table::Intrinsic_kind::Is_algebraic            },
        {U"алгебраич_тип",        symbol_table::Intrinsic_kind::Create_algebraic_type   },
        {U"аргументы_функции",    symbol_table::Intrinsic_kind::Get_func_args           },
        {U"базовый_тип",          symbol_table::Intrinsic_kind::Get_base_type           },
        {U"вещественный_ли",      symbol_table::Intrinsic_kind::Is_float                },
        {U"добавь_компоненты",    symbol_table::Intrinsic_kind::Add_components          },
        {U"индексы",              symbol_table::Intrinsic_kind::Get_indices             },
        {U"кватернионный_ли",     symbol_table::Intrinsic_kind::Is_quaternion           },
        {U"комплексный_ли",       symbol_table::Intrinsic_kind::Is_complex              },
        {U"кортеж_ли",            symbol_table::Intrinsic_kind::Is_tuple                },
        {U"массив_ли",            symbol_table::Intrinsic_kind::Is_array                },
        {U"множество_ли",         symbol_table::Intrinsic_kind::Is_set                  },
        {U"общий_тип",            symbol_table::Intrinsic_kind::Get_common_type         },
        {U"перечисление_ли",      symbol_table::Intrinsic_kind::Is_enum                 },
        {U"порядок_указателя",    symbol_table::Intrinsic_kind::Get_pointer_order       },
        {U"символьный_ли",        symbol_table::Intrinsic_kind::Is_char                 },
        {U"строковый_ли",         symbol_table::Intrinsic_kind::Is_string               },
        {U"структура_ли",         symbol_table::Intrinsic_kind::Is_struct               },
        {U"тип_значения",         symbol_table::Intrinsic_kind::Get_return_type         },
        {U"тип_кортеж",           symbol_table::Intrinsic_kind::Create_tuple_type       },
        {U"тип_массив",           symbol_table::Intrinsic_kind::Create_array_type       },
        {U"тип_множество",        symbol_table::Intrinsic_kind::Create_set_type         },
        {U"тип_перечисл",         symbol_table::Intrinsic_kind::Create_enum_type        },
        {U"тип_структ",           symbol_table::Intrinsic_kind::Create_struct_type      },
        {U"тип_указатель",        symbol_table::Intrinsic_kind::Create_pointer_type     },
        {U"типы_кортежа",         symbol_table::Intrinsic_kind::Get_tuple_types         },
        {U"типы_пакета",          symbol_table::Intrinsic_kind::Get_package_types       },
        {U"указатель_ли",         symbol_table::Intrinsic_kind::Is_pointer              },
        {U"функция_ли",           symbol_table::Intrinsic_kind::Is_func                 },
        {U"целочисленный_ли",     symbol_table::Intrinsic_kind::Is_integer              },
        {U"числовой_ли",          symbol_table::Intrinsic_kind::Is_number               },
    };
}

namespace symbol_table{
    std::map<std::size_t, Intrinsic_kind> build_intrinsics_table(std::shared_ptr<strings::trie::Char_trie>& ids_trie)
    {
        std::map<std::size_t, Intrinsic_kind> result;

        for(const auto& [name, kind] : data)
        {
            const std::size_t id = ids_trie->insert(std::u32string{name});

            result[id] = kind;
        }

        return result;
    }
}