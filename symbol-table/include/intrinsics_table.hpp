/*
    File:    intrinsics_table.hpp
    Created: 28 October 2023 at 16:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <map>

#include "../../strings-utils/include/char_trie.hpp"

namespace symbol_table{
    enum class Intrinsic_kind{
        Is_float,         Is_integer,               Is_complex,        Is_quaternion,
        Is_number,        Is_string,                Is_char,           Is_pointer,
        Is_func,          Is_struct,                Is_enum,           Is_set,
        Is_tuple,         Is_array,                 Is_algebraic,      Get_pointer_order,
        Get_base_type,    Get_indices,              Get_func_args,     Get_return_type,
        Get_tuple_types,  Get_algebraic_components, Create_set_type,   Create_array_type,
        Create_enum_type, Create_struct_type,       Create_tuple_type, Create_algebraic_type,
        Add_components,   Create_pointer_type,      Get_package_types, Get_common_type
    };

    std::map<std::size_t, Intrinsic_kind> build_intrinsics_table(std::shared_ptr<strings::trie::Char_trie>& ids_trie);
}