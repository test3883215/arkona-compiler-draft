/*
    File:    module.hpp
    Created: 03 September 2023 at 15:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory> 

#include "../include/scopes.hpp"
#include "../../arkona-ast-lib/include/module_node.hpp"

namespace symbol_table{
    struct Module final{
        Module()              = default;
        Module(const Module&) = default;
        ~Module()             = default;

        Module(const Scopes&                            scopes,
               const std::shared_ptr<ast::Module_node>& module_ast)
            : scopes_{scopes}
            , module_ast_{module_ast}
        {
        }

        Scopes                            scopes_;
        std::shared_ptr<ast::Module_node> module_ast_;
    };
}