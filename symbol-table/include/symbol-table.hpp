/*
    File:    symbol-table.hpp
    Created: 05 March 2025 at 15:21 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>
#include <set>
#include <string>

#include "../include/scopes.hpp"

namespace symbol_table{
    struct Symbol_table final{
        Symbol_table()                    = default;
        Symbol_table(const Symbol_table&) = default;
        ~Symbol_table()                   = default;

        Symbol_table(const std::shared_ptr<Scopes>& scopes, 
                     const std::string&             current_module_name, 
                     const std::set<std::string>&   used_modules_names)
            : scopes_{scopes}
            , current_module_name_{current_module_name}
            , used_modules_names_{used_modules_names}
        {
        }

        bool is_current_module_name(const std::string& name) const
        {
            return current_module_name_ == name;
        }

        bool is_used_module(const std::string& name) const
        {
            return used_modules_names_.find(name) != used_modules_names_.end();
        }

        std::shared_ptr<Scopes> scopes_;
        std::string             current_module_name_;
        std::set<std::string>   used_modules_names_;  
    };
}