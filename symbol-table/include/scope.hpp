/*
    File:    scope.hpp
    Created: 29 August 2023 at 18:46 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <forward_list>
#include <map>
#include <memory>
#include <utility>

#include "../../arkona-ast-lib/include/ast_node.hpp"
#include "../../iscanner/include/position.hpp"

namespace symbol_table{
    enum class Id_kind{
        Abstract_generic_func_name,                                                Abstract_generic_package_func_name,
        Alias_name,                                                                Category_arg_name,
        Category_name,                                                             Const_name_when_const_is_category_body_elem,
        Const_name_when_const_is_category_impl_elem,                               Const_name,
        Loop_label,                                                                Field_name,
        Forall_loop_var_name,                                                      For_loop_var_name,
        Func_arg_name,                                                             Generic_func_name,
        Indexed_forall_loop_index_var_name,                                        Indexed_forall_loop_elem_var_name,
        Var_name,                                                                  Meta_const_name,
        Meta_forall_loop_var_name,                                                 Meta_for_loop_var_name,
        Meta_func_arg_name,                                                        Meta_func_name,
        Meta_indexed_forall_loop_index_var_name,                                   Meta_indexed_forall_loop_elem_var_name,
        Base_name,                                                                 Elem_of_tuple_of_package_category_args_with_maybe_same_parametrical_type,
        Elem_of_tuple_of_package_category_args_with_same_non_parametrical_type,    Elem_of_tuple_of_package_category_args_with_same_parametrical_type,
        Elem_of_package_category_args_with_maybe_same_parametrical_type,           Elem_of_package_category_args_with_same_non_parametrical_type,
        Elem_of_package_category_args_with_same_parametrical_type,                 Package_category_name,
        Elem_of_tuple_of_package_func_args_with_maybe_same_parametrical_type,      Elem_of_tuple_of_package_func_args_with_same_non_parametrical_type,
        Elem_of_tuple_of_package_func_args_with_same_parametrical_type,            Elem_of_package_func_args_with_maybe_same_parametrical_type,
        Elem_of_package_func_args_with_same_non_parametrical_type,                 Elem_of_package_func_args_with_same_parametrical_type,
        Package_func_name,                                                         Package_generic_func_name,
        Elem_of_tuple_of_package_meta_func_args_with_maybe_same_parametrical_type, Elem_of_tuple_of_package_meta_func_args_with_same_non_parametrical_type,
        Elem_of_tuple_of_package_meta_func_args_with_same_parametrical_type,       Elem_of_package_meta_func_args_with_maybe_same_parametrical_type,
        Elem_of_package_meta_func_args_with_same_non_parametrical_type,            Elem_of_package_meta_func_args_with_same_parametrical_type,
        Package_meta_func_name,                                                    Scoped_id,
        Struct_type_name,                                                          Enum_type_name,
        Elem_of_tuple_of_vars,                                                     Type_name,
        Elem_of_enum_type,                                                         Func_name,
        Elem_of_group_of_package_category_args,                                    Elem_of_group_of_package_func_args,
        Elem_of_group_of_package_meta_func_args
    };

    struct Def_for_specific_kind{
        iscaner::Position_range    pos_;
        std::shared_ptr<ast::Node> def_node_;
        std::size_t                additional_idx_;
    };

    using Defs_for_specific_kind = std::forward_list<Def_for_specific_kind>;

    using Defs_for_specific_id   = std::map<Id_kind, Defs_for_specific_kind>;

    class Scope final{
    public:
        Scope()             = default;
        Scope(const Scope&) = default;
        ~Scope()            = default;

        Defs_for_specific_id get_id_defs(std::size_t id) const;

        void add_id_def(std::size_t id, Id_kind kind, const Def_for_specific_kind& def);

    private:
        std::map<std::size_t, Defs_for_specific_id> defs_;
    };
}