/*
    File:    scopes.hpp
    Created: 31 August 2023 at 20:36 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <forward_list>
#include <map>
#include <memory>
#include <utility>

#include "../include/scope.hpp"

namespace symbol_table{
    class Scopes final{
    public:
        Scopes()              = default;
        Scopes(const Scopes&) = default;
        ~Scopes()             = default;

        void create_new_scope();

        void delete_current_scope();

        void add_id_def(std::size_t id, Id_kind kind, const Def_for_specific_kind& def);

        Defs_for_specific_id get_id_defs(std::size_t id) const;

    private:
        std::forward_list<Scope> scopes_;
    };
}