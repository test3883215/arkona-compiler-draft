/*
    File:    imported-modules-names-table.hpp
    Created: 28 August 2023 at 14:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <cstddef>
#include <memory>
#include <set>
#include <vector>

#include "../../strings-utils/include/size_t_trie.hpp"
#include "../../arkona-ast-lib/include/id_info.hpp"

namespace symbol_table{
    class Table_of_names_of_imported_modules final{
    public:
        Table_of_names_of_imported_modules()                                          = default;
        Table_of_names_of_imported_modules(const Table_of_names_of_imported_modules&) = default;
        ~Table_of_names_of_imported_modules()                                         = default;

        explicit Table_of_names_of_imported_modules(const std::shared_ptr<strings::trie::Size_t_trie>& qids_trie)
            : qids_trie_{qids_trie}
            , set_of_indices_of_qids_{}
        {
        }

        bool contains(const std::vector<ast::Id_info>& qid);

        std::size_t add_qid(const std::vector<ast::Id_info>& qid);

    private:
        std::shared_ptr<strings::trie::Size_t_trie> qids_trie_;
        std::set<std::size_t>                       set_of_indices_of_qids_;
    };
}