/*
    File:    module_strings_encoding.hpp
    Created: 03 September 2023 at 16:37 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>

#include "../include/module.hpp"
#include "../../strings-utils/include/char_trie.hpp"

namespace symbol_table{
    std::shared_ptr<Module> build_module_strings_encoding(std::shared_ptr<strings::trie::Char_trie>& ids_trie);
}