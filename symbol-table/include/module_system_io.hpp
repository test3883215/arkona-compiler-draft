/*
    File:    module_system_io.hpp
    Created: 03 September 2023 at 16:39 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>

#include "../include/module.hpp"
#include "../../strings-utils/include/char_trie.hpp"

namespace symbol_table{
    std::shared_ptr<Module> build_module_system_io(std::shared_ptr<strings::trie::Char_trie>& ids_trie);
}