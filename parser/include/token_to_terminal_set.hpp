/*
    File:    token_to_terminal_set.hpp
    Created: 29 September 2024 at 13:38 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include "../../scanner/include/lexeme.hpp"
#include "../../scanner/include/scanner.hpp"
#include "../../bitset/include/static_bitset.hpp"
#include "../include/terminal_enum.hpp"
namespace parser{
    using Token        = scanner::Arkona_token;
    using Terminal_set = pascal_set::set<Terminal_category,
                                         Terminal_category::End_of_text,
                                         Terminal_category::Unknown>;

    Terminal_set token_to_terminal_set(const Token& token);
}