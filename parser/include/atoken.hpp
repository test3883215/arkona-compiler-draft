/*
    File:    atoken.hpp
    Created: 23 November 2023 at 20:13 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include "../../scanner/include/scanner.hpp"

namespace parser{
    using Token = scanner::Arkona_token;
}