/*
    File:    parser.hpp
    Created: 04 November 2023 at 19:57 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <memory>

#include "../../iscanner/include/errors_and_tries.hpp"
#include "../../scanner/include/scanner.hpp"
#include "../../symbol-table/include/symbol-table.hpp"
#include "../../symbol-table/include/module.hpp"

namespace parser{
    class Parser final{
    public:
        Parser();
        Parser(const Parser&) = default;

        ~Parser();

        Parser(const std::shared_ptr<scanner::Scanner>&           scanner,
               const Errors_and_tries&                            et,
               const std::shared_ptr<symbol_table::Symbol_table>& table);

        std::shared_ptr<symbol_table::Module> parse();
    private:
        struct Parser_impl;
        std::shared_ptr<Parser_impl> impl_;
    };
}