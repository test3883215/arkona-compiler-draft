/*
    File:    parser.cpp
    Created: 04 November 2023 at 20:03 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <cstddef>
#include <cstdio>
#include <string>
#include "../include/parser.hpp"
#include "../../symbol-table/include/imported-modules-names-table.hpp"
#include "../../symbol-table/include/intrinsics_table.hpp"
#include "../include/token_to_terminal_set.hpp"
#include "../../strings-utils/include/join.hpp"
#include "../../strings-utils/include/idx_to_string.hpp"

namespace parser{
    template<typename T>
    using proc_result_t = std::pair<std::shared_ptr<T>, bool>;

    struct Parser::Parser_impl final{
    public:
        Parser_impl()                   = default;
        Parser_impl(const Parser_impl&) = default;
        ~Parser_impl()                  = default;

        Parser_impl(const std::shared_ptr<scanner::Scanner>&           scanner,
                    const Errors_and_tries&                            et,
                    const std::shared_ptr<symbol_table::Symbol_table>& table)
            : scanner_{scanner}
            , et_{et}
            // , scopes_{scopes}
            , table_{table}
        {
        }

        std::shared_ptr<symbol_table::Module> parse();
    private:
        std::shared_ptr<scanner::Scanner>           scanner_;
        Errors_and_tries                            et_;
        // std::shared_ptr<symbol_table::Scopes>       scopes_;
        std::shared_ptr<symbol_table::Symbol_table> table_;

        std::shared_ptr<strings::trie::Size_t_trie>                       qids_trie_;
        std::shared_ptr<symbol_table::Table_of_names_of_imported_modules> imported_modules_;
        std::map<std::size_t, symbol_table::Intrinsic_kind>               intrinsics_table_;

        // std::string current_module_name_;

        Terminal_set get_current(Token& token);

        proc_result_t<ast::Module_node>       module_node_proc();
        proc_result_t<ast::Qualified_id_node> qualified_id_proc();
        proc_result_t<ast::Used_modules_node> used_modules_proc();
        proc_result_t<ast::Used_modules_node> uses_stmt_proc();
        proc_result_t<ast::Block_node>        block_node_proc();
        proc_result_t<ast::Block_entry_node>  block_entry_node_proc();

        proc_result_t<ast::Block_entry_node>  nonmarked_stmt_proc();

        std::string qualified_id_to_string(const std::shared_ptr<ast::Qualified_id_node>& components) const;
    };

    std::shared_ptr<symbol_table::Module> Parser::Parser_impl::parse()
    {
        qids_trie_        = std::make_shared<strings::trie::Size_t_trie>();
        imported_modules_ = std::make_shared<symbol_table::Table_of_names_of_imported_modules>(qids_trie_);
        intrinsics_table_ = symbol_table::build_intrinsics_table(et_.ids_trie_);

        auto [module_node_ptr, flag] = module_node_proc();

        auto result = std::make_shared<symbol_table::Module>(*(table_->scopes_), module_node_ptr);

        return result;
    }
    
    Parser::~Parser() = default;

    Parser::Parser() : impl_{std::make_shared<Parser_impl>()} {}


    Parser::Parser(const std::shared_ptr<scanner::Scanner>&           scanner,
                   const Errors_and_tries&                            et,
                   const std::shared_ptr<symbol_table::Symbol_table>& table)
    : impl_(std::make_shared<Parser_impl>(scanner, et, table))
    {
    }

    std::shared_ptr<symbol_table::Module> Parser::parse()
    {
        return impl_->parse();
    }

    Terminal_set Parser::Parser_impl::get_current(Token& token)
    {
        token = scanner_->current_lexeme();
        return token_to_terminal_set(token);
    }

    std::string Parser::Parser_impl::qualified_id_to_string(const std::shared_ptr<ast::Qualified_id_node>& q_id) const
    {
        std::string result;

        const auto components = q_id->components_;

        result = strings::join::join([this](const auto& component)
                                     {
                                         return idx_to_string(et_.ids_trie_, component.identifier_id_);
                                     },
                                     std::begin(components),
                                     std::end(components),
                                     std::string{"::"});

        return result;
    }

    proc_result_t<ast::Module_node> Parser::Parser_impl::module_node_proc()
    {
        /**
         * This function handles the rule
         *      modulo -> модуль module_name used_modules? block
         * Using the notation
         *      m       модуль
         *      n       module_name
         *      u       used_modules
         *      b       block
         * we can rewrite this rule in the form
         *      module -> m n u? b
         * Minimal DFA for the regexp
         *      m n u? b
         * has the following transition table:
         *
         * |-------|---|---|---|---|------------------|
         * | State | m | n | u | b | Remark           |
         * |-------|---|---|---|---|------------------|
         * |   A   | B |   |   |   | Start state      |
         * |-------|---|---|---|---|------------------|
         * |   B   |   | C |   |   |                  |
         * |-------|---|---|---|---|------------------|
         * |   C   |   |   | D | E |                  |
         * |-------|---|---|---|---|------------------|
         * |   D   |   |   |   | E |                  |
         * |-------|---|---|---|---|------------------|
         * |   E   |   |   |   |   | Final state      |
         * |-------|---|---|---|---|------------------|
         */

        proc_result_t<ast::Module_node> result{std::make_shared<ast::Module_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C, D, E
        };

        State state = State::A;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state)
            {
                case State::A:
                    if(set_of_terminals[Terminal_category::Modulo]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword модуль\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        if(set_of_terminals[Terminal_category::Id]){
                            // When we got Id, we can suppose that it is the start part of the name of the current module.
                            // Hence, we need to move into the state B
                            state = State::B;
                        } else if(set_of_terminals[Terminal_category::Used_modules] || set_of_terminals[Terminal_category::Block]){
                            // When we got the keyword использует, then it is the start of the used modules list.
                            // Hence, we need to move into the state C
                            state = State::C;
                        }else{
                            // Otherwise, we cannot guess next state:
                            result.second = false;
                            return result;
                        }
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Id]){
                        scanner_->back();
                        auto [q_id_ptr, flag]         = qualified_id_proc();
                        result.first->name_           = q_id_ptr;
                        table_->current_module_name_  = qualified_id_to_string(q_id_ptr);

                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::C;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected (qualified) identifier.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::C:
                    if(set_of_terminals[Terminal_category::Used_modules]){
                        scanner_->back();
                        auto [used_modules_ptr, flag] = used_modules_proc();
                        result.first->used_modules_   = used_modules_ptr;
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::D;
                    }else if(set_of_terminals[Terminal_category::Block]){
                        scanner_->back();
                        auto [block_ptr, flag] = block_node_proc();
                        result.first->body_    = block_ptr;
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::E;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected lists of used modules or the start of a block.\n",
                               pos.line_no_,
                               pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::D:
                    if(set_of_terminals[Terminal_category::Block]){
                        scanner_->back();
                        auto [block_ptr, flag] = block_node_proc();
                        result.first->body_    = block_ptr;
                        if(!flag)
                        {
                            result.second = false;
                            return result;
                        }
                        state = State::E;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected the start of a block.\n",
                               pos.line_no_,
                               pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::E:
                    scanner_->back();
                    return result;
                    break;
                default:
                    ;
            }
        }

        if(state != State::E){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Qualified_id_node> Parser::Parser_impl::qualified_id_proc()
    {
        /**
         * The method handles the rule
         *     qualified_id -> ид(::ид)*
         * Using the notation
         *     i    ид
         *     s    ::
         * we can rewrite this rule in the form
         *     qualified_id -> i(si)*
         * Mininal DFA for the regexp
         *     i(si)*
         * has the following transition table:
         *
         * |-------|---|---|------------------|
         * | State | i | s | Remark           |
         * |-------|---|---|------------------|
         * |   A   | B |   | Start state      |
         * |-------|---|---|------------------|
         * |   B   |   | A | Final state      |
         * |-------|---|---|------------------|
         */


        proc_result_t<ast::Qualified_id_node> result{std::make_shared<ast::Qualified_id_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B
        };

        State state = State::A;

        // std::vector<Token> components;
        std::vector<ast::Id_info> components;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Id]){
                        // components.push_back(current_token);
                        components.push_back(ast::Id_info{current_token.lexeme_.id_index_, current_token.range_});
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected identifier\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        result.second             = false;
                        result.first->components_ = components;
                        scanner_->back();
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Scope_resolution]){
                        state = State::A;
                    }else{
                        result.first->components_ = components;
                        scanner_->back();
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::B){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Used_modules_node> Parser::Parser_impl::used_modules_proc()
    {
        /**
         * This method handles the rule
         *      used_modules -> used_stmt(; used_stmt)*
         * Using the notation
         *      u   uses_stmt
         *      s   ;
         * we can rewrite this rule in the form
         *      used_modules -> u(su)*
         * Minimal DFA for the regexp
         *      u(su)*
         * has the following transition table:
         *
         * |-------|---|---|------------------|
         * | State | u | s | Remark           |
         * |-------|---|---|------------------|
         * |   A   | B |   | Start state      |
         * |-------|---|---|------------------|
         * |   B   |   | A | Final state      |
         * |-------|---|---|------------------|
         */

        proc_result_t<ast::Used_modules_node> result{std::make_shared<ast::Used_modules_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B
        };

        State state = State::A;

        std::vector<std::shared_ptr<ast::Qualified_id_node>> names;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Used_stmt]){
                        scanner_->back();
                        auto [uses_stmt_ptr, flag] = uses_stmt_proc();

                        names.insert(names.end(), std::begin(uses_stmt_ptr->names_), std::end(uses_stmt_ptr->names_));

                        if(!flag){
                            result.first->names_ = names;
                            result.second        = false;
                            return result;
                        }

                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword использует.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Semicolon]){
                        state = State::A;
                    }else if(set_of_terminals[Terminal_category::Used_stmt]){
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: missing semicolon.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        state = State::A;
                    }else{
                        scanner_->back();
                        result.first->names_ = names;
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::B){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }


        return result;
    }

    proc_result_t<ast::Used_modules_node> Parser::Parser_impl::uses_stmt_proc()
    {
        /**
         * This method handles the rule
         *      uses_stmt -> использует qualified_id(, qualified_id)*
         * Using the notation
         *      u   использует
         *      q   qualified_id
         *      c   ,
         * we can rewrite this rule in the form
         *      uses_stmt -> uq(cq)*
         * Minimal DFA for the regexp
         *      uq(cq)*
         * has the the following transition table:
         *
         * |-------|---|---|---|------------------|
         * | State | u | q | c | Remark           |
         * |-------|---|---|---|------------------|
         * |   A   | B |   |   | Start state      |
         * |-------|---|---|---|------------------|
         * |   B   |   | C |   |                  |
         * |-------|---|---|---|------------------|
         * |   C   |   |   | B | Final state      |
         * |-------|---|---|---|------------------|
         */

        proc_result_t<ast::Used_modules_node> result{std::make_shared<ast::Used_modules_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, C
        };

        State state = State::A;

        std::vector<std::shared_ptr<ast::Qualified_id_node>> names;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state){
                case State::A:
                    if(set_of_terminals[Terminal_category::Kw_ispolzuet]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected keyword использует.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Id]){
                        scanner_->back();
                        auto [q_id_ptr, flag] = qualified_id_proc();
                        names.push_back(q_id_ptr);

                        auto current_used_module_name = qualified_id_to_string(q_id_ptr);

                        if(table_->is_current_module_name(current_used_module_name)){
                            const auto& pos = current_token.range_.begin_pos_;
                            printf("Error at %zu:%zu: imported module name %s is the current compiled module name.\n",
                                   pos.line_no_,
                                   pos.line_pos_,
                                   current_used_module_name.c_str());
                            et_.ec_->increment_number_of_errors();
                        } else if(table_->is_used_module(current_used_module_name)){
                            const auto& pos = current_token.range_.begin_pos_;
                            printf("Error at %zu:%zu: imported module name %s is already defined.\n",
                                   pos.line_no_,
                                   pos.line_pos_,
                                   current_used_module_name.c_str());
                            et_.ec_->increment_number_of_errors();
                        } else{
                            names.push_back(q_id_ptr);
                            table_->used_modules_names_.insert(current_used_module_name);
                            // table_->add_used_module_name(current_used_module_name);
                        }

                        if(!flag)
                        {
                            result.first->names_  = names;
                            result.second         = false;
                            return result;
                        }
                        state = State::C;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected (qualified) identifier which is used module name.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::C:
                    if(set_of_terminals[Terminal_category::Comma]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected comma.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                default:
                    ;
            }
        }

        if(state != State::C){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Block_node> Parser::Parser_impl::block_node_proc()
    {
        /**
         * This method handles the rule
         *      block -> {block_entry*}
         * Using the notation
         *      b  block_entry
         * we can rewrite this rule in the form
         *      block -> {b*}
         * Minimal DFA for the regexp
         *      {b*}
         * has the the following transition table:
         *
         * |-------|---|---|---|------------------|
         * | State | { | b | } | Remark           |
         * |-------|---|---|---|------------------|
         * |   A   | B |   |   | Start state      |
         * |-------|---|---|---|------------------|
         * |   B   |   | B | D |                  |
         * |-------|---|---|---|------------------|
         * |   D   |   |   |   | Final state      |
         * |-------|---|---|---|------------------|
         */
        proc_result_t<ast::Block_node> result{std::make_shared<ast::Block_node>(), true};

        Token        current_token;
        Terminal_set set_of_terminals;

        enum class State{
            A, B, D
        };

        State state = State::A;

        ast::Block_node::Block_entries block_entries;

        while(!((set_of_terminals = get_current(current_token))[Terminal_category::End_of_text]))
        {
            switch(state)
            {
                case State::A:
                    if(set_of_terminals[Terminal_category::Figure_bracket_opened]){
                        state = State::B;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected {.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::B:
                    if(set_of_terminals[Terminal_category::Block_entry]){
                        scanner_->back();
                        auto [entry_ptr, flag] = block_entry_node_proc();
                        block_entries.push_back(entry_ptr);

                        if(!flag)
                        {
                            result.first->entries_ = block_entries;
                            result.second          = false;
                            return result;
                        }
                        state = State::B;
                    }else if(set_of_terminals[Terminal_category::Figure_bracket_closed]){
                        state = State::D;
                        result.first->entries_ = block_entries;
                        return result;
                    }else{
                        const auto& pos = current_token.range_.begin_pos_;
                        printf("Error at %zu:%zu: expected block entry or }.\n", pos.line_no_, pos.line_pos_);
                        et_.ec_->increment_number_of_errors();
                        scanner_->back();
                        result.second = false;
                        return result;
                    }
                    break;
                case State::D:
                    break;
                default:
                    ;
            }
        }

        if(state != State::D){
            const auto& pos = current_token.range_.begin_pos_;
            printf("Error at %zu:%zu: unexpected end of text.\n", pos.line_no_, pos.line_pos_);
            et_.ec_->increment_number_of_errors();
            scanner_->back();
            result.second = false;
        }

        return result;
    }

    proc_result_t<ast::Block_entry_node> Parser::Parser_impl::nonmarked_stmt_proc()
    {

        proc_result_t<ast::Block_entry_node> result;
        result.second = true;
        return result;
    }

    proc_result_t<ast::Block_entry_node> Parser::Parser_impl::block_entry_node_proc()
    {

        proc_result_t<ast::Block_entry_node> result;
        return result;
    }
}