/*
    File:    terminal_enum.cpp
    Created: 08 January 2024 at 18:47 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#include "../include/terminal_enum.hpp"

namespace{
    const char* terminal_category_strs[] = {
        "End_of_text",                   "Add_like",               "Address",                "Address_like",             
        "Alloc_expr",                    "Array_expr",             "Array_literal_begin",    "Array_literal_end",        
        "Array_type",                    "Assignment",             "Base_type",              "Bit_and",                  
        "Bit_or",                        "Bitscale_literal_begin", "Bitscale_literal_end",   "Bitwise_not",              
        "Block_entry",                   "Cast_expr",              "Category_arg_group",     "Category_body_elem",       
        "Category_impl_elem",            "Colon",                  "Comma",                  "Common_template_type",     
        "Common_type",                   "Cond_op",                "Constant_size_type",     "Constructor",              
        "Data_address",                  "Deref_op",               "Destructor",             "Dot",                      
        "Enum_type",                     "Exported_stmt",          "Expr",                   "Expr0",                    
        "Expr1",                         "Expr10",                 "Expr11",                 "Expr12",                   
        "Expr13",                        "Expr14",                 "Expr15",                 "Expr16",                   
        "Expr17",                        "Expr18",                 "Expr19",                 "Expr2",                    
        "Expr20",                        "Expr21",                 "Expr22",                 "Expr3",                    
        "Expr4",                         "Expr5",                  "Expr6",                  "Expr7",                    
        "Expr8",                         "Expr9",                  "Field_values_list",      "Fields_group_def",         
        "Figure_bracket_closed",         "Figure_bracket_opened",  "Free_expr",              "Func_arg_group",           
        "Func_header",                   "Func_signature",         "Func_value",             "Gen_type",                 
        "Get_elem_type",                 "Get_expr_type",          "Id",                     "Kw_abstraktnaja",          
        "Kw_bajt",                       "Kw_bezzn",               "Kw_bezzn128",            "Kw_bezzn16",               
        "Kw_bezzn32",                    "Kw_bezzn64",             "Kw_bezzn8",              "Kw_bolshe",                
        "Kw_bolshoe",                    "Kw_chistaja",            "Kw_diapazon",            "Kw_dlya",                  
        "Kw_eksport",                    "Kw_ekviv",               "Kw_element",             "Kw_esli",                  
        "Kw_funktsija",                  "Kw_golovnaya",           "Kw_inache",              "Kw_ines",                  
        "Kw_ispolzuet",                  "Kw_istina",              "Kw_iz",                  "Kw_kak",                   
        "Kw_kategorija",                 "Kw_kompl",               "Kw_kompl128",            "Kw_kompl32",               
        "Kw_kompl64",                    "Kw_kompl80",             "Kw_konst",               "Kw_konstanty",             
        "Kw_kvat",                       "Kw_kvat128",             "Kw_kvat32",              "Kw_kvat64",                
        "Kw_kvat80",                     "Kw_ljambda",             "Kw_log",                 "Kw_log16",                 
        "Kw_log32",                      "Kw_log64",               "Kw_log8",                "Kw_lozh",                  
        "Kw_malenkoe",                   "Kw_massiv",              "Kw_menshe",              "Kw_meta",                  
        "Kw_modul",                      "Kw_nastrojka",           "Kw_nichto",              "Kw_obobshchenie",          
        "Kw_obobshchjonnaja",            "Kw_operatsija",          "Kw_osvobodi",            "Kw_paket",                 
        "Kw_paketnaja",                  "Kw_pauk",                "Kw_perechisl_mnozh",     "Kw_perechislenie",         
        "Kw_perem",                      "Kw_poka",                "Kw_pokuda",              "Kw_porjadok",              
        "Kw_porjadok16",                 "Kw_porjadok32",          "Kw_porjadok64",          "Kw_porjadok8",             
        "Kw_postfiksnaja",               "Kw_povtorjaj",           "Kw_prefiksnaja",         "Kw_preobrazuj",            
        "Kw_prodolzhi",                  "Kw_psevdonim",           "Kw_pusto",               "Kw_rassmatrivaj",          
        "Kw_ravno",                      "Kw_razbor",              "Kw_realizatsija",        "Kw_realizuj",              
        "Kw_samo",                       "Kw_sbros",               "Kw_shkala",              "Kw_shkalu",                
        "Kw_simv",                       "Kw_simv16",              "Kw_simv32",              "Kw_simv8",                 
        "Kw_sravni_s",                   "Kw_ssylka",              "Kw_stroka",              "Kw_stroka16",              
        "Kw_stroka32",                   "Kw_stroka8",             "Kw_struktura",           "Kw_tip",                   
        "Kw_tipy",                       "Kw_to",                  "Kw_tsel",                "Kw_tsel128",               
        "Kw_tsel16",                     "Kw_tsel32",              "Kw_tsel64",              "Kw_tsel8",                 
        "Kw_v",                          "Kw_vechno",              "Kw_veshch",              "Kw_veshch128",             
        "Kw_veshch32",                   "Kw_veshch64",            "Kw_veshch80",            "Kw_vozvrat",               
        "Kw_vyberi",                     "Kw_vydeli",              "Kw_vyjdi",               "Label_prefix",             
        "Labeled_loop",                  "Left_arrow",             "Literal",                "Log_and",                  
        "Log_or",                        "Logical_not",            "Match_branch",           "Meta_bracket_closed",      
        "Meta_bracket_opened",           "Meta_stmt",              "Meta_var_def",           "Metafunc_arg_group",       
        "Metafunc_signature",            "Mmatch_branch",          "Modifier",               "Module_name",              
        "Modulo",                        "Mselect_branch",         "Mspider_branch",         "Mul_like",                 
        "Nonbracket_maybe_pure_op_name", "Nonmarked_stmt",         "Operation_header",       "Other_type",               
        "Package_category_arg_group",    "Package_func_arg_group", "Package_func_signature", "Package_metafunc_arg_group",
        "Package_metafunc_signature",    "Pattern",                "Postfix_inc",            "Pow_like",                 
        "Prefix_inc",                    "Ptr_def",                "Qualified_id",           "Range",                    
        "Range_excluded_end",            "Range_type",             "Ref_type",               "Rel_op",                   
        "Right_arrow",                   "Round_bracket_closed",   "Round_bracket_opened",   "Scale_type",               
        "Scope_resolution",              "Select_branch",          "Semicolon",              "Set_expr",                 
        "Set_literal_begin",             "Set_literal_end",        "Set_type",               "Sizeof_like",              
        "Spider_branch",                 "Square_bracket_closed",  "Square_bracket_opened",  "Stmt",                     
        "Struct_fields_def",             "Struct_type",            "Template_type",          "Tuple_begin",              
        "Tuple_end",                     "Tuple_expr",             "Type_value",             "Unary_plus",               
        "Used_categories",               "Var_def",                "Without_size_modifier",  "Used_stmt",
        "Used_modules",                  "Category_impl_body",     "Category_body",          "Block", 
        "Scale_expr",                    "Unknown"      
    };
}

namespace parser{
    std::string to_string(Terminal_category t)
    {
        return terminal_category_strs[static_cast<std::uint16_t>(t)];
    }
}