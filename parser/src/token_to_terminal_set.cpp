/*
    File:    token_to_terminal_set.cpp
    Created: 29 September 2024 at 13:45 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#include <map>
#include "../include/token_to_terminal_set.hpp"

namespace{
    using Lexem_kind     = scanner::Lexem_kind;
    using Keyword_kind   = scanner::Keyword_kind;
    using Delimiter_kind = scanner::Delimiter_kind;
    using Lexeme         = scanner::LexemeInfo;

    using namespace parser;

    const Terminal_set default_set = {Terminal_category::End_of_text};

    const Terminal_set set_for_literal = {
        Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,        
        Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                 
        Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                 
        Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                        
        Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,        
        Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                 
        Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                  
        Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                         
        Terminal_category::Expr9,         Terminal_category::Literal,         Terminal_category::Match_branch,
        Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,
        Terminal_category::Select_branch, Terminal_category::Spider_branch,
    };
    
    const Terminal_set set_for_id = {
        Terminal_category::Block_entry,                Terminal_category::Category_arg_group,     Terminal_category::Expr,                    
        Terminal_category::Expr0,                      Terminal_category::Expr1,                  Terminal_category::Expr10,                  
        Terminal_category::Expr11,                     Terminal_category::Expr12,                 Terminal_category::Expr13,                  
        Terminal_category::Expr14,                     Terminal_category::Expr15,                 Terminal_category::Expr16,                  
        Terminal_category::Expr17,                     Terminal_category::Expr18,                 Terminal_category::Expr19,                  
        Terminal_category::Expr2,                      Terminal_category::Expr20,                 Terminal_category::Expr21,                 
        Terminal_category::Expr3,                      Terminal_category::Expr4,                  Terminal_category::Expr5,                   
        Terminal_category::Expr6,                      Terminal_category::Expr7,                  Terminal_category::Expr8,                
        Terminal_category::Expr9,                      Terminal_category::Field_values_list,      Terminal_category::Fields_group_def,        
        Terminal_category::Func_arg_group,             Terminal_category::Id,                     Terminal_category::Match_branch,            
        Terminal_category::Meta_var_def,               Terminal_category::Metafunc_arg_group,     Terminal_category::Mmatch_branch,           
        Terminal_category::Module_name,                Terminal_category::Mselect_branch,         Terminal_category::Mspider_branch,         
        Terminal_category::Package_category_arg_group, Terminal_category::Package_func_arg_group, Terminal_category::Package_metafunc_arg_group,        
        Terminal_category::Qualified_id,               Terminal_category::Select_branch,          Terminal_category::Spider_branch,        
        Terminal_category::Struct_fields_def,          Terminal_category::Used_categories,        Terminal_category::Var_def,
    };
    
    const Terminal_set sets_for_keywords[] = {
        // абстрактная
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_abstraktnaja
        },
        // байт
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_bajt,        Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // беззн        
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_bezzn,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // беззн8        
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_bezzn8,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // беззн16      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_bezzn16,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // беззн32       
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_bezzn32,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // беззн64      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_bezzn64,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // беззн128      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_bezzn128,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // больше
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,        
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,                 
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,                 
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,                        
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,        
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,                 
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,                  
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,                         
            Terminal_category::Expr9,          Terminal_category::Kw_bolshe,     Terminal_category::Literal,         
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,
        },
        // большое    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,          Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,         Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,         Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,         Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,         Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,          Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,          Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,          Terminal_category::Kw_bolshoe,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,  Terminal_category::Modifier,         
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,         
            Terminal_category::Spider_branch,  Terminal_category::Type_value,     
        },
        // в
        {
            Terminal_category::Kw_v
        },
        // вечно      
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_vechno
        },
        // вещ   
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_veshch,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // вещ32    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_veshch32,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // вещ64   
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_veshch64,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // вещ80    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_veshch80,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // вещ128    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_veshch128,   Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // возврат     
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_vozvrat
        },
        // выбери    
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_vyberi
        },
        // выдели     
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr15,        Terminal_category::Expr16,         Terminal_category::Expr17,         
            Terminal_category::Expr18,        Terminal_category::Expr19,         Terminal_category::Expr2,          
            Terminal_category::Expr20,        Terminal_category::Expr21,         Terminal_category::Expr22,         
            Terminal_category::Expr3,         Terminal_category::Expr4,          Terminal_category::Expr5,          
            Terminal_category::Expr6,         Terminal_category::Expr7,          Terminal_category::Expr8,          
            Terminal_category::Expr9,         Terminal_category::Kw_vydeli,      Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Select_branch, Terminal_category::Spider_branch,  Terminal_category::Alloc_expr,
        },
        // выйди
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_vyjdi
        },
        // головная   
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_golovnaya
        },
        // диапазон  
        {
            Terminal_category::Block_entry,      Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,            Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,           Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr15,           Terminal_category::Expr16,         Terminal_category::Expr17,         
            Terminal_category::Expr18,           Terminal_category::Expr19,         Terminal_category::Expr2,          
            Terminal_category::Expr20,           Terminal_category::Expr21,         Terminal_category::Expr22,         
            Terminal_category::Expr3,            Terminal_category::Expr4,          Terminal_category::Expr5,          
            Terminal_category::Expr6,            Terminal_category::Expr7,          Terminal_category::Expr8,          
            Terminal_category::Expr9,            Terminal_category::Kw_diapazon,    Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch,    Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Range_type,       Terminal_category::Select_branch,  Terminal_category::Spider_branch,  
            Terminal_category::Type_value,
        },
        // для     
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_dlya
        },
        // если    
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_esli
        },
        // из    
        {
            Terminal_category::Kw_iz
        },
        // иначе 
        {
            Terminal_category::Kw_inache
        },
        // инес    
        {
            Terminal_category::Kw_ines
        },
        // использует
        {
            Terminal_category::Kw_ispolzuet, Terminal_category::Used_modules, Terminal_category::Used_stmt,
        },
        // истина      
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,        
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,                 
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,                 
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,                        
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,        
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,                 
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,                  
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,                         
            Terminal_category::Expr9,          Terminal_category::Kw_istina,     Terminal_category::Literal,         
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,
        },
        // как
        {
            Terminal_category::Kw_kak
        },
        // категория   
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_kategorija
        },
        // кват       
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_kvat,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // кват32      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kvat32,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // кват64     
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kvat64,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // кват80      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kvat80,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // кват128    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kvat128,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // компл       
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_kompl,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // компл32    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kompl32,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // компл64     
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kompl64,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // компл80    
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kompl80,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // компл128  
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_kompl128,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // конст     
        {
            Terminal_category::Block_entry,    Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem,            
            Terminal_category::Expr,           Terminal_category::Expr0,              Terminal_category::Expr1,                 
            Terminal_category::Expr10,         Terminal_category::Expr11,             Terminal_category::Expr12,                
            Terminal_category::Expr13,         Terminal_category::Expr14,             Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,             Terminal_category::Expr18,                
            Terminal_category::Expr19,         Terminal_category::Expr2,              Terminal_category::Expr20,                
            Terminal_category::Expr21,         Terminal_category::Expr22,             Terminal_category::Expr3,                 
            Terminal_category::Expr4,          Terminal_category::Expr5,              Terminal_category::Expr6,                 
            Terminal_category::Expr7,          Terminal_category::Expr8,              Terminal_category::Expr9,                 
            Terminal_category::Kw_konst,       Terminal_category::Match_branch,       Terminal_category::Mmatch_branch,         
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch,     Terminal_category::Ref_type,
            Terminal_category::Select_branch,  Terminal_category::Spider_branch,      Terminal_category::Type_value, 
        },
        // константы 
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_konstanty
        },
        // лог       
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_log,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // лог8      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_log8,        Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // лог16      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_log16,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // лог32      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_log32,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // лог64      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_log64,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // ложь       
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,        
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,                 
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,                 
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,                        
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,        
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,                 
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,                  
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,                         
            Terminal_category::Expr9,          Terminal_category::Kw_lozh,       Terminal_category::Literal,         
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,
        },
        // лямбда     
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,          
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,         
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,         
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,         
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,          
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,         
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,          
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,          
            Terminal_category::Expr9,          Terminal_category::Func_value,    Terminal_category::Kw_ljambda,     
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,  
        },
        // маленькое  
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,          Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,         Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,         Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,         Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,         Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,          Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,          Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,          Terminal_category::Kw_malenkoe,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,  Terminal_category::Modifier,         
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,         
            Terminal_category::Spider_branch,  Terminal_category::Type_value,     
        },
        // массив     
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,         Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,         Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,         Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,          Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,          Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_massiv,      Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch,             
            Terminal_category::Array_type,    Terminal_category::Select_branch,  Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // меньше     
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,        
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,                 
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,                 
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,                        
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,        
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,                 
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,                  
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,                         
            Terminal_category::Expr9,          Terminal_category::Kw_menshe,     Terminal_category::Literal,         
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,
        },
        // мета
        {
            Terminal_category::Block_entry, Terminal_category::Kw_meta,
            Terminal_category::Meta_stmt,   Terminal_category::Stmt,
        },
        // модуль
        {
            Terminal_category::Modulo, Terminal_category::Kw_modul
        },
        // настройка  
        {
            Terminal_category::Block_entry,    Terminal_category::Constructor,   Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,         Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,        Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,        Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,        Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,        Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,         Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,         Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,         Terminal_category::Kw_nastrojka,      
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,              
        },
        // ничто      
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,        
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,                 
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,                 
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,                        
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,        
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,                 
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,                  
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,                         
            Terminal_category::Expr9,          Terminal_category::Kw_nichto,     Terminal_category::Literal,         
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,
        },
        // обобщение  
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_obobshchenie, Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,             
            Terminal_category::Gen_type,      Terminal_category::Select_branch,   Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // обобщённая  
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_obobshchjonnaja
        },
        // операция   
        {
            Terminal_category::Block_entry,    Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem, 
            Terminal_category::Kw_operatsija,  Terminal_category::Nonmarked_stmt,     Terminal_category::Operation_header,   
            Terminal_category::Stmt,
        },
        // освободи   
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr15,        Terminal_category::Expr16,         Terminal_category::Expr17,         
            Terminal_category::Expr18,        Terminal_category::Expr19,         Terminal_category::Expr2,          
            Terminal_category::Expr20,        Terminal_category::Expr21,         Terminal_category::Expr22,         
            Terminal_category::Expr3,         Terminal_category::Expr4,          Terminal_category::Expr5,          
            Terminal_category::Expr6,         Terminal_category::Expr7,          Terminal_category::Expr8,          
            Terminal_category::Expr9,         Terminal_category::Kw_osvobodi,    Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Select_branch, Terminal_category::Spider_branch,  Terminal_category::Free_expr,
        },
        // пакет     
        {
            Terminal_category::Kw_paket,               Terminal_category::Package_category_arg_group,    
            Terminal_category::Package_func_arg_group, Terminal_category::Package_metafunc_arg_group,
        },
        // пакетная   
        {
            Terminal_category::Block_entry, Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem,
            Terminal_category::Func_header, Terminal_category::Kw_paketnaja,       Terminal_category::Nonmarked_stmt,
            Terminal_category::Stmt,
        },
        // паук       
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_pauk
        },
        // перем      
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_perem
        },
        // перечисл_множ  
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,               Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,             Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,             Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,             Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,             Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,             Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,              Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,              Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_perechisl_mnozh, Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,     Terminal_category::Mspider_branch,             
            Terminal_category::Set_type,      Terminal_category::Select_branch,      Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // перечисление 
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,             Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,           Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,           Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,           Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,           Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,           Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,            Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,            Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_perechislenie, Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,   Terminal_category::Mspider_branch,             
            Terminal_category::Enum_type,     Terminal_category::Select_branch,    Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // повторяй      
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_povtorjaj
        },
        // пока           
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_poka
        },
        // покуда        
        {
            Terminal_category::Kw_pokuda
        },
        // порядок        
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_porjadok,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // порядок8      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_porjadok8,   Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // порядок16      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_porjadok16,  Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // порядок32     
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_porjadok32,  Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // порядок64      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_porjadok64,  Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // постфиксная   
        {
            Terminal_category::Block_entry,     Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem,
            Terminal_category::Kw_postfiksnaja, Terminal_category::Nonmarked_stmt,     Terminal_category::Operation_header,
            Terminal_category::Stmt,
        },
        // преобразуй     
        {
            Terminal_category::Block_entry,    Terminal_category::Cast_expr,     Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,         Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,        Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,        Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,        Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,        Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,         Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,         Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,         Terminal_category::Kw_preobrazuj,  
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,  
        },
        // префиксная    
        {
            Terminal_category::Block_entry,    Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem,
            Terminal_category::Kw_prefiksnaja, Terminal_category::Nonmarked_stmt,     Terminal_category::Operation_header,
            Terminal_category::Stmt,
        },
        // продолжи       
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_prodolzhi
        },
        // псевдоним     
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_psevdonim
        },
        // пусто          
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_pusto,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // равно         
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,          Terminal_category::Expr0,        
            Terminal_category::Expr1,          Terminal_category::Expr10,        Terminal_category::Expr11,                 
            Terminal_category::Expr12,         Terminal_category::Expr13,        Terminal_category::Expr14,                 
            Terminal_category::Expr15,         Terminal_category::Expr16,        Terminal_category::Expr17,                        
            Terminal_category::Expr18,         Terminal_category::Expr19,        Terminal_category::Expr2,        
            Terminal_category::Expr20,         Terminal_category::Expr21,        Terminal_category::Expr22,                 
            Terminal_category::Expr3,          Terminal_category::Expr4,         Terminal_category::Expr5,                  
            Terminal_category::Expr6,          Terminal_category::Expr7,         Terminal_category::Expr8,                         
            Terminal_category::Expr9,          Terminal_category::Kw_ravno,      Terminal_category::Literal,         
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,
        },
        // разбор         
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_razbor
        },
        // рассматривай 
        {
            Terminal_category::Block_entry,    Terminal_category::Cast_expr,     Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,         Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,        Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,        Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,        Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,        Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,         Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,         Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,         Terminal_category::Kw_rassmatrivaj,  
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,  
        },
        // реализация     
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_realizatsija
        },
        // реализуй      
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_realizuj
        },
        // само           
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_samo,         Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,             
            Terminal_category::Other_type,    Terminal_category::Select_branch,   Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // сброс         
        {
            Terminal_category::Block_entry,    Terminal_category::Destructor,    Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,         Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,        Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,        Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,        Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,        Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,         Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,         Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,         Terminal_category::Kw_sbros,      
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,              
        },
        // симв           
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_simv,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // симв8         
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_simv8,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // симв16         
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_simv16,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // симв32        
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_simv32,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // сравни_с      
        {
            Terminal_category::Kw_sravni_s, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ссылка        
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_ssylka,       Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,             
            Terminal_category::Ref_type,      Terminal_category::Select_branch,   Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // строка         
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_stroka,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // строка8       
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_stroka8,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // строка16       
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_stroka16,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // строка32      
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_stroka32,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // структура      
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_struktura,    Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,             
            Terminal_category::Struct_type,   Terminal_category::Select_branch,   Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // тип           
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_tip,          Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,             
            Terminal_category::Other_type,    Terminal_category::Select_branch,   Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // типы           
        {
            Terminal_category::Nonmarked_stmt, Terminal_category::Stmt, 
            Terminal_category::Block_entry,    Terminal_category::Kw_tipy
        },
        // то            
        {
            Terminal_category::Kw_to
        },
        // функция
        {
            Terminal_category::Block_entry, Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem,           
            Terminal_category::Func_header, Terminal_category::Kw_funktsija,       Terminal_category::Nonmarked_stmt,
            Terminal_category::Stmt,
        },
        // чистая     
        {
            Terminal_category::Block_entry,      Terminal_category::Category_body_elem, Terminal_category::Category_impl_elem,
            Terminal_category::Func_header,      Terminal_category::Kw_chistaja,        Terminal_category::Nonmarked_stmt,
            Terminal_category::Operation_header, Terminal_category::Stmt,
        },
        // цел           
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,           Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,                 Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,                Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,                Terminal_category::Expr16,         
            Terminal_category::Expr17,         Terminal_category::Expr18,                Terminal_category::Expr19,         
            Terminal_category::Expr2,          Terminal_category::Expr20,                Terminal_category::Expr21,         
            Terminal_category::Expr22,         Terminal_category::Expr3,                 Terminal_category::Expr4,          
            Terminal_category::Expr5,          Terminal_category::Expr6,                 Terminal_category::Expr7,          
            Terminal_category::Expr8,          Terminal_category::Expr9,                 Terminal_category::Kw_tsel,        
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,         Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,         Terminal_category::Spider_branch,  
            Terminal_category::Type_value,     Terminal_category::Without_size_modifier,         
        },
        // цел8          
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_tsel8,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // цел16         
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_tsel16,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // цел32         
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_tsel32,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // цел64         
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_tsel64,      Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // цел128        
        {
            Terminal_category::Base_type,      Terminal_category::Block_entry,    Terminal_category::Constant_size_type,            
            Terminal_category::Expr,           Terminal_category::Expr0,          Terminal_category::Expr1,                  
            Terminal_category::Expr10,         Terminal_category::Expr11,         Terminal_category::Expr12,                 
            Terminal_category::Expr13,         Terminal_category::Expr14,         Terminal_category::Expr15,                
            Terminal_category::Expr16,         Terminal_category::Expr17,         Terminal_category::Expr18,                 
            Terminal_category::Expr19,         Terminal_category::Expr2,          Terminal_category::Expr20,                 
            Terminal_category::Expr21,         Terminal_category::Expr22,         Terminal_category::Expr3,                      
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,                      
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Kw_tsel128,     Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,            
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Type_value,
        },
        // шкала         
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,            Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,          Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,          Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,          Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,          Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,          Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,           Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,           Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Kw_shkala,       Terminal_category::Match_branch,               
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch,  Terminal_category::Mspider_branch,             
            Terminal_category::Scale_type,    Terminal_category::Select_branch,   Terminal_category::Spider_branch,              
            Terminal_category::Type_value,
        },
        // шкалу         
        {
            Terminal_category::Kw_shkalu
        },
        // эквив
        {
            Terminal_category::Kw_ekviv, Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Rel_op,
        },
        // экспорт  
        {
            Terminal_category::Block_entry, Terminal_category::Exported_stmt,
            Terminal_category::Kw_eksport,  Terminal_category::Stmt,
        },
        // элемент       
        {
            Terminal_category::Kw_element, Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Rel_op,
        },
    };
    
    Terminal_set keyword_to_set(const Lexeme& l)
    {
        Terminal_set result;
        
        const auto lc  = l.code_;
        const auto lsk = lc.subkind_;
        
        return sets_for_keywords[static_cast<unsigned>(lsk)];
    }
    
    const Terminal_set sets_for_delimiters[] = {
        // !      Logical_not     
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,           Terminal_category::Expr0, 
            Terminal_category::Expr1,          Terminal_category::Expr2,          Terminal_category::Expr3, 
            Terminal_category::Logical_not,    Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Spider_branch,
        },
        // !&&    Logical_and_not 
        {
            Terminal_category::Log_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // !&&.   Logical_and_not_full        
        {
            Terminal_category::Log_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // !&&.=  Logical_and_not_full_assign
        {
            Terminal_category::Assignment
        },
        // !&&=   Logical_and_not_assign      
        {
            Terminal_category::Assignment
        },
        // !=     NEQ  
        {
            Terminal_category::Rel_op, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // !||    Logical_or_not  
        {
            Terminal_category::Log_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // !||.   Logical_or_not_full        
        {
            Terminal_category::Log_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // !||.=  Logical_or_not_full_assign  
        {
            Terminal_category::Assignment
        },
        // !||=   Logical_or_not_assign      
        {
            Terminal_category::Assignment
        },
        // #      Sharp
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,           Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,         Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,         Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr16,         Terminal_category::Expr2,
            Terminal_category::Expr3,          Terminal_category::Expr4,          Terminal_category::Expr5,
            Terminal_category::Expr6,          Terminal_category::Expr7,          Terminal_category::Expr8,
            Terminal_category::Expr9,          Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Sizeof_like,    Terminal_category::Spider_branch,
        },
        // ##     Data_size                  
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,           Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,         Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,         Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr16,         Terminal_category::Expr2,
            Terminal_category::Expr3,          Terminal_category::Expr4,          Terminal_category::Expr5,
            Terminal_category::Expr6,          Terminal_category::Expr7,          Terminal_category::Expr8,
            Terminal_category::Expr9,          Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Sizeof_like,    Terminal_category::Spider_branch,
        },
        // %      Remainder                   
        {
            Terminal_category::Mul_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // %.     Float_remainder            
        {
            Terminal_category::Mul_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // %.=    Float_remainder_assign      
        {
            Terminal_category::Assignment
        },
        // %=     Remainder_assign           
        {
            Terminal_category::Assignment
        },
        // &      Bitwise_and                 
        {
            Terminal_category::Bit_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // &&     Logical_and                
        {
            Terminal_category::Log_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // &&.    Logical_and_full            
        {
            Terminal_category::Log_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // &&.=   Logical_and_full_assign    
        {
            Terminal_category::Assignment
        },
        // &&=    Logical_and_assign          
        {
            Terminal_category::Assignment
        },
        // &=     Bitwise_and_assign         
        {
            Terminal_category::Assignment
        },
        // (      Round_bracket_opened  
        {
            Terminal_category::Block_entry,          Terminal_category::Expr,           Terminal_category::Expr0,
            Terminal_category::Expr1,                Terminal_category::Expr10,         Terminal_category::Expr11,
            Terminal_category::Expr12,               Terminal_category::Expr13,         Terminal_category::Expr14,
            Terminal_category::Expr15,               Terminal_category::Expr16,         Terminal_category::Expr17,
            Terminal_category::Expr18,               Terminal_category::Expr19,         Terminal_category::Expr2,
            Terminal_category::Expr20,               Terminal_category::Expr21,         Terminal_category::Expr3,
            Terminal_category::Expr4,                Terminal_category::Expr5,          Terminal_category::Expr6,
            Terminal_category::Expr7,                Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Func_signature,       Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch,       Terminal_category::Mspider_branch, Terminal_category::Package_func_signature,
            Terminal_category::Round_bracket_opened, Terminal_category::Select_branch,  Terminal_category::Spider_branch,
        },
        // (:     Tuple_begin                
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,                      
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,                     
            Terminal_category::Expr15,        Terminal_category::Expr16,         Terminal_category::Expr17,                     
            Terminal_category::Expr18,        Terminal_category::Expr19,         Terminal_category::Expr2,                      
            Terminal_category::Expr20,        Terminal_category::Expr21,         Terminal_category::Expr22,                     
            Terminal_category::Expr3,         Terminal_category::Expr4,          Terminal_category::Expr5,                      
            Terminal_category::Expr6,         Terminal_category::Expr7,          Terminal_category::Expr8,                      
            Terminal_category::Expr9,         Terminal_category::Match_branch,   Terminal_category::Meta_var_def,
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch,             
            Terminal_category::Select_branch, Terminal_category::Spider_branch,  Terminal_category::Tuple_begin,
            Terminal_category::Tuple_expr,    Terminal_category::Var_def,
        },
        // )      Round_bracket_closed     
        {
            Terminal_category::Round_bracket_closed
        },
        // *      Mul                        
        {
            Terminal_category::Mul_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // **     Power    
        {
            Terminal_category::Pow_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // **.    Float_power                
        {
            Terminal_category::Pow_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // **.=   Float_power_assign          
        {
            Terminal_category::Assignment
        },
        // **=    Power_assign               
        {
            Terminal_category::Assignment
        },
        // */     Comment_end  
        {},
        // *=     Mul_assign                 
        {
            Terminal_category::Assignment
        },
        // +      Plus   
        {
            Terminal_category::Add_like,       Terminal_category::Block_entry,    Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,          Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,         Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,         Terminal_category::Expr2,                      
            Terminal_category::Expr3,          Terminal_category::Expr4,          Terminal_category::Expr5,                      
            Terminal_category::Expr6,          Terminal_category::Expr7,          Terminal_category::Expr8,                      
            Terminal_category::Expr9,          Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
        // ++     Inc        
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr2,         Terminal_category::Expr3,          Terminal_category::Expr4,          
            Terminal_category::Expr5,         Terminal_category::Expr6,          Terminal_category::Expr7,          
            Terminal_category::Expr8,         Terminal_category::Expr9,          Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Postfix_inc,   Terminal_category::Prefix_inc,     Terminal_category::Select_branch,                 
            Terminal_category::Spider_branch,              
        },
        // ++<    Inc_with_wrap               
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr2,         Terminal_category::Expr3,          Terminal_category::Expr4,          
            Terminal_category::Expr5,         Terminal_category::Expr6,          Terminal_category::Expr7,          
            Terminal_category::Expr8,         Terminal_category::Expr9,          Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Postfix_inc,   Terminal_category::Prefix_inc,     Terminal_category::Select_branch,                 
            Terminal_category::Spider_branch,              
        },
        // +=     Plus_assign                
        {
            Terminal_category::Assignment
        },
        // ,      Comma  
        {
            Terminal_category::Comma
        },
        // -      Minus                      
        {
            Terminal_category::Add_like,       Terminal_category::Block_entry,    Terminal_category::Expr,           
            Terminal_category::Expr0,          Terminal_category::Expr1,          Terminal_category::Expr10,         
            Terminal_category::Expr11,         Terminal_category::Expr12,         Terminal_category::Expr13,         
            Terminal_category::Expr14,         Terminal_category::Expr15,         Terminal_category::Expr2,                      
            Terminal_category::Expr3,          Terminal_category::Expr4,          Terminal_category::Expr5,                      
            Terminal_category::Expr6,          Terminal_category::Expr7,          Terminal_category::Expr8,                      
            Terminal_category::Expr9,          Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
        // --     Dec                         
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr2,         Terminal_category::Expr3,          Terminal_category::Expr4,          
            Terminal_category::Expr5,         Terminal_category::Expr6,          Terminal_category::Expr7,          
            Terminal_category::Expr8,         Terminal_category::Expr9,          Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Postfix_inc,   Terminal_category::Prefix_inc,     Terminal_category::Select_branch,                 
            Terminal_category::Spider_branch,              
        },
        // --<    Dec_with_wrap              
        {
            Terminal_category::Block_entry,   Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,         Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,        Terminal_category::Expr13,         Terminal_category::Expr14,         
            Terminal_category::Expr2,         Terminal_category::Expr3,          Terminal_category::Expr4,          
            Terminal_category::Expr5,         Terminal_category::Expr6,          Terminal_category::Expr7,          
            Terminal_category::Expr8,         Terminal_category::Expr9,          Terminal_category::Match_branch,   
            Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, 
            Terminal_category::Postfix_inc,   Terminal_category::Prefix_inc,     Terminal_category::Select_branch,                 
            Terminal_category::Spider_branch,              
        },
        // -=     Minus_assign                
        {
            Terminal_category::Assignment
        },
        // ->     Right_arrow   
        {
            Terminal_category::Right_arrow
        },
        // .      Dot       
        {
            Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Dot
        },
        // ..     Range 
        {
            Terminal_category::Range
        },
        // ...    Range_excluded_end  
        {
            Terminal_category::Range_excluded_end
        },
        // .|.    Algebraic_separator    
        {
            Terminal_category::Add_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // .|.=   Algebraic_separator_assign  
        {
            Terminal_category::Assignment
        },
        // /      Div                        
        {
            Terminal_category::Mul_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // /*     Comment_begin   
        {},
        // /=     Div_assign                 
        {
            Terminal_category::Assignment
        },
        // /\     Symmetric_difference        
        {
            Terminal_category::Mul_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // /\=    Symmetric_difference_assign
        {
            Terminal_category::Assignment
        },
        // :      Colon          
        {
            Terminal_category::Colon
        },
        // :)     Tuple_end     
        {
            Terminal_category::Tuple_end
        },
        // ::     Scope_resolution     
        {
            Terminal_category::Scope_resolution
        },
        // :=     Copy                       
        {
            Terminal_category::Assignment
        },
        // :>     Meta_bracket_closed  
        {
            Terminal_category::Meta_bracket_closed
        },
        // :]     Array_literal_end    
        {
            Terminal_category::Array_literal_end
        },
        // :}     Set_literal_end     
        {
            Terminal_category::Set_literal_end
        },
        // ;      Semicolon         
        {
            Terminal_category::Block_entry, Terminal_category::Nonmarked_stmt,
            Terminal_category::Semicolon,   Terminal_category::Stmt,
        },
        // <      LT                          
        {
            Terminal_category::Rel_op, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // <!!>   Common_template_type
        {
            Terminal_category::Common_template_type
        },
        // <!>    Template_type   
        {
            Terminal_category::Template_type
        },
        // <&&>   Data_address        
        {
            Terminal_category::Address_like,   Terminal_category::Block_entry,   Terminal_category::Data_address,
            Terminal_category::Expr,           Terminal_category::Expr0,         Terminal_category::Expr1,         
            Terminal_category::Expr10,         Terminal_category::Expr11,        Terminal_category::Expr12,        
            Terminal_category::Expr13,         Terminal_category::Expr14,        Terminal_category::Expr15,
            Terminal_category::Expr16,         Terminal_category::Expr17,        Terminal_category::Expr18,
            Terminal_category::Expr19,         Terminal_category::Expr2,         Terminal_category::Expr3,          
            Terminal_category::Expr4,          Terminal_category::Expr5,         Terminal_category::Expr6,          
            Terminal_category::Expr7,          Terminal_category::Expr8,         Terminal_category::Expr9,          
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,    
        },
        // <&>    Address                     
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,           Terminal_category::Expr0,          
            Terminal_category::Expr1,          Terminal_category::Expr10,         Terminal_category::Expr11,         
            Terminal_category::Expr12,         Terminal_category::Expr13,         Terminal_category::Expr14,   
            Terminal_category::Expr15,         Terminal_category::Expr16,         Terminal_category::Expr17,
            Terminal_category::Expr18,         Terminal_category::Expr19,         Terminal_category::Expr2,         
            Terminal_category::Expr3,          Terminal_category::Expr4,          Terminal_category::Expr5,         
            Terminal_category::Expr6,          Terminal_category::Expr7,          Terminal_category::Expr8,         
            Terminal_category::Expr9,          Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, 
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Select_branch,                            
            Terminal_category::Spider_branch,  Terminal_category::Address_like,   Terminal_category::Address,
        },
        // <+>    Type_add 
        {
            Terminal_category::Add_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // <+>=   Type_add_assign             
        {
            Terminal_category::Assignment
        },
        // <-     Left_arrow      
        {
            Terminal_category::Left_arrow
        },
        // <:     Meta_bracket_opened     
        {
            Terminal_category::Meta_bracket_opened, Terminal_category::Package_metafunc_signature,
            Terminal_category::Metafunc_signature,
        },
        // <:>    Common_type  
        {
            Terminal_category::Common_type            
        },
        // <<     Left_shift                  
        {
            Terminal_category::Bit_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // <<<    Cyclic_left_shift          
        {
            Terminal_category::Bit_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // <<<=   Cyclic_left_shift_assign    
        {
            Terminal_category::Assignment
        },
        // <<=    Left_shift_assign          
        {
            Terminal_category::Assignment
        },
        // <=     LEQ                         
        {
            Terminal_category::Rel_op, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // <?>    Get_expr_type    
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,           Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,         Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,         Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr2,          Terminal_category::Expr3,
            Terminal_category::Expr4,          Terminal_category::Expr5,          Terminal_category::Expr6,
            Terminal_category::Expr7,          Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Get_expr_type,  Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
        // <??>   Get_elem_type     
        {
            Terminal_category::Address_like,   Terminal_category::Block_entry,   Terminal_category::Get_elem_type,
            Terminal_category::Expr,           Terminal_category::Expr0,         Terminal_category::Expr1,         
            Terminal_category::Expr10,         Terminal_category::Expr11,        Terminal_category::Expr12,        
            Terminal_category::Expr13,         Terminal_category::Expr14,        Terminal_category::Expr15,
            Terminal_category::Expr16,         Terminal_category::Expr17,        Terminal_category::Expr18,
            Terminal_category::Expr19,         Terminal_category::Expr2,         Terminal_category::Expr3,          
            Terminal_category::Expr4,          Terminal_category::Expr5,         Terminal_category::Expr6,          
            Terminal_category::Expr7,          Terminal_category::Expr8,         Terminal_category::Expr9,          
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch, Terminal_category::Mselect_branch, 
            Terminal_category::Mspider_branch, Terminal_category::Select_branch, Terminal_category::Spider_branch,    
            
        },
        // <|     Label_prefix   
        {
            Terminal_category::Labeled_loop, Terminal_category::Stmt,
            Terminal_category::Block_entry,  Terminal_category::Label_prefix,
        },
        // =      Assign                      
        {
            Terminal_category::Assignment
        },
        // ==     EQ                         
        {
            Terminal_category::Rel_op, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // >      GT                          
        {
            Terminal_category::Rel_op, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // >=     GEQ                        
        {
            Terminal_category::Rel_op, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // >>     Right_shift                 
        {
            Terminal_category::Bit_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // >>=    Right_shift_assign         
        {
            Terminal_category::Assignment
        },
        // >>>    Cyclic_right_shift          
        {
            Terminal_category::Bit_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // >>>=   Cyclic_right_shift_assign  
        {
            Terminal_category::Assignment
        },
        // ?      Cond_op   
        {
            Terminal_category::Cond_op
        },
        // ?.     Cond_op_full               
        {
            Terminal_category::Cond_op
        },
        // @      At     
        {
            Terminal_category::Ptr_def,                       Terminal_category::Deref_op,       Terminal_category::Expr0,
            Terminal_category::Expr1,                         Terminal_category::Expr2,          Terminal_category::Expr3,
            Terminal_category::Expr4,                         Terminal_category::Expr5,          Terminal_category::Expr6,
            Terminal_category::Expr7,                         Terminal_category::Expr8,          Terminal_category::Expr9,
            Terminal_category::Expr10,                        Terminal_category::Expr11,         Terminal_category::Expr12,
            Terminal_category::Expr13,                        Terminal_category::Expr14,         Terminal_category::Expr15,
            Terminal_category::Expr16,                        Terminal_category::Expr17,         Terminal_category::Expr18,
            Terminal_category::Expr19,                        Terminal_category::Expr20,         Terminal_category::Expr,
            Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Mspider_branch, Terminal_category::Spider_branch,
            Terminal_category::Mmatch_branch,                 Terminal_category::Match_branch,   Terminal_category::Mselect_branch,
            Terminal_category::Select_branch,                 Terminal_category::Block_entry,
        },
        // [      Square_bracket_opened   
        {
            Terminal_category::Square_bracket_opened
        },
        // [:     Array_literal_begin      
        {
            Terminal_category::Array_expr,      Terminal_category::Array_literal_begin, Terminal_category::Block_entry,
            Terminal_category::Expr,            Terminal_category::Expr0,               Terminal_category::Expr1,                         
            Terminal_category::Expr10,          Terminal_category::Expr11,              Terminal_category::Expr12,
            Terminal_category::Expr13,          Terminal_category::Expr14,              Terminal_category::Expr15,
            Terminal_category::Expr16,          Terminal_category::Expr17,              Terminal_category::Expr18,
            Terminal_category::Expr19,          Terminal_category::Expr2,               Terminal_category::Expr20,         
            Terminal_category::Expr21,          Terminal_category::Expr22,              Terminal_category::Expr3,
            Terminal_category::Expr4,           Terminal_category::Expr5,               Terminal_category::Expr6,
            Terminal_category::Expr7,           Terminal_category::Expr8,               Terminal_category::Expr9,
            Terminal_category::Match_branch,    Terminal_category::Mmatch_branch,       Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch,  Terminal_category::Select_branch,       Terminal_category::Spider_branch,
        },
        // \      Set_difference             
        {
            Terminal_category::Mul_like, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // \=     Set_difference_assign       
        {
            Terminal_category::Assignment
        },
        // ]      Square_bracket_closed   
        {
            Terminal_category::Square_bracket_closed
        },
        // ^      Bitwise_xor    
        {
            Terminal_category::Bit_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ^=     Bitwise_xor_assign         
        {
            Terminal_category::Assignment
        },
        // ^^     Logical_xor                 
        {
            Terminal_category::Log_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ^^=    Logical_xor_assign         
        {
            Terminal_category::Assignment
        },
        // {      Figure_bracket_opened    
        {
            Terminal_category::Block,              Terminal_category::Block_entry,            Terminal_category::Category_body,
            Terminal_category::Category_impl_body, Terminal_category::Figure_bracket_opened,  Terminal_category::Nonmarked_stmt,
            Terminal_category::Stmt,
        },
        // {..}   Pattern     
        {
            Terminal_category::Pattern
        },
        // {:     Set_literal_begin
        {
            Terminal_category::Set_expr,       Terminal_category::Set_literal_begin, Terminal_category::Block_entry,
            Terminal_category::Expr,           Terminal_category::Expr0,             Terminal_category::Expr1,                         
            Terminal_category::Expr10,         Terminal_category::Expr11,            Terminal_category::Expr12,
            Terminal_category::Expr13,         Terminal_category::Expr14,            Terminal_category::Expr15,
            Terminal_category::Expr16,         Terminal_category::Expr17,            Terminal_category::Expr18,
            Terminal_category::Expr19,         Terminal_category::Expr2,             Terminal_category::Expr20,         
            Terminal_category::Expr21,         Terminal_category::Expr22,            Terminal_category::Expr3,
            Terminal_category::Expr4,          Terminal_category::Expr5,             Terminal_category::Expr6,
            Terminal_category::Expr7,          Terminal_category::Expr8,             Terminal_category::Expr9,
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,     Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch, Terminal_category::Select_branch,     Terminal_category::Spider_branch,
        },
        // |      Bitwise_or                 
        {
            Terminal_category::Bit_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // |#|    Cardinality                 
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,           Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,         Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,         Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr16,         Terminal_category::Expr2,
            Terminal_category::Expr3,          Terminal_category::Expr4,          Terminal_category::Expr5,
            Terminal_category::Expr6,          Terminal_category::Expr7,          Terminal_category::Expr8,
            Terminal_category::Expr9,          Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,
            Terminal_category::Mselect_branch, Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name,
            Terminal_category::Select_branch,  Terminal_category::Sizeof_like,    Terminal_category::Spider_branch,
        },
        // |=     Bitwise_or_assign          
        {
            Terminal_category::Assignment
        },
        // ||     Logical_or                  
        {
            Terminal_category::Log_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ||.    Logical_or_full            
        {
            Terminal_category::Log_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ||.=   Logical_or_full_assign      
        {
            Terminal_category::Assignment
        },
        // ||=    Logical_or_assign          
        {
            Terminal_category::Assignment
        },
        // }      Figure_bracket_closed    
        {
            Terminal_category::Figure_bracket_closed
        },
        // ~      Bitwise_not   
        {
            Terminal_category::Bitwise_not
        },
        // ~&     Bitwise_and_not             
        {
            Terminal_category::Bit_and, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ~&=    Bitwise_and_not_assign     
        {
            Terminal_category::Assignment
        },
        // ~|     Bitwise_or_not              
        {
            Terminal_category::Bit_or, Terminal_category::Nonbracket_maybe_pure_op_name
        },
        // ~|=    Bitwise_or_not_assign      
        {
            Terminal_category::Assignment
        },
        // |:     Bitscale_literal_begin  
        {
            Terminal_category::Scale_expr,      Terminal_category::Bitscale_literal_begin, Terminal_category::Block_entry,
            Terminal_category::Expr,            Terminal_category::Expr0,                  Terminal_category::Expr1,                         
            Terminal_category::Expr10,          Terminal_category::Expr11,                 Terminal_category::Expr12,
            Terminal_category::Expr13,          Terminal_category::Expr14,                 Terminal_category::Expr15,
            Terminal_category::Expr16,          Terminal_category::Expr17,                 Terminal_category::Expr18,
            Terminal_category::Expr19,          Terminal_category::Expr2,                  Terminal_category::Expr20,         
            Terminal_category::Expr21,          Terminal_category::Expr22,                 Terminal_category::Expr3,
            Terminal_category::Expr4,           Terminal_category::Expr5,                  Terminal_category::Expr6,
            Terminal_category::Expr7,           Terminal_category::Expr8,                  Terminal_category::Expr9,
            Terminal_category::Match_branch,    Terminal_category::Mmatch_branch,          Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch,  Terminal_category::Select_branch,          Terminal_category::Spider_branch,            
        },
        // :|     Bitscale_literal_end     
        {
            Terminal_category::Bitscale_literal_end
        },
        // <#     Minimal_value_in_collection 
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,                          Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,                        Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,                        Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr2,                         Terminal_category::Expr3,
            Terminal_category::Expr4,          Terminal_category::Expr5,                         Terminal_category::Expr6,
            Terminal_category::Expr7,          Terminal_category::Expr8,                         Terminal_category::Expr9,
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,                 Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
        // #>     Maximal_value_in_collection
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,                          Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,                        Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,                        Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr2,                         Terminal_category::Expr3,
            Terminal_category::Expr4,          Terminal_category::Expr5,                         Terminal_category::Expr6,
            Terminal_category::Expr7,          Terminal_category::Expr8,                         Terminal_category::Expr9,
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,                 Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
        // <<#    Minimal_value_of_type       
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,                          Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,                        Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,                        Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr2,                         Terminal_category::Expr3,
            Terminal_category::Expr4,          Terminal_category::Expr5,                         Terminal_category::Expr6,
            Terminal_category::Expr7,          Terminal_category::Expr8,                         Terminal_category::Expr9,
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,                 Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
        // #>>    Maximal_value_of_type              
        {
            Terminal_category::Block_entry,    Terminal_category::Expr,                          Terminal_category::Expr0,
            Terminal_category::Expr1,          Terminal_category::Expr10,                        Terminal_category::Expr11,
            Terminal_category::Expr12,         Terminal_category::Expr13,                        Terminal_category::Expr14,
            Terminal_category::Expr15,         Terminal_category::Expr2,                         Terminal_category::Expr3,
            Terminal_category::Expr4,          Terminal_category::Expr5,                         Terminal_category::Expr6,
            Terminal_category::Expr7,          Terminal_category::Expr8,                         Terminal_category::Expr9,
            Terminal_category::Match_branch,   Terminal_category::Mmatch_branch,                 Terminal_category::Mselect_branch,
            Terminal_category::Mspider_branch, Terminal_category::Nonbracket_maybe_pure_op_name, Terminal_category::Select_branch,
            Terminal_category::Spider_branch,  Terminal_category::Unary_plus,
        },
    };

    Terminal_set delimiter_to_set(const Lexeme& l)
    {
        const auto lc  = l.code_;
        const auto lsk = lc.subkind_;
        
        return sets_for_delimiters[static_cast<unsigned>(lsk)];
    }
}

namespace parser{
    Terminal_set token_to_terminal_set(const Token& token)
    {
        Terminal_set result = default_set;
        
        const auto l   = token.lexeme_;
        const auto lc  = l.code_;
        const auto lk  = lc.kind_;
        
        switch(lk){
            case Lexem_kind::Keyword:
                return keyword_to_set(l);
                break;
            case Lexem_kind::Id:      
                return set_for_id;
                break;
            case Lexem_kind::Delimiter:
                return delimiter_to_set(l);
                break;
            case Lexem_kind::Char:         case Lexem_kind::String: case Lexem_kind::Integer: 
            case Lexem_kind::Float:        case Lexem_kind::Complex: case Lexem_kind::Quat:    
            case Lexem_kind::Encoded_char:
                return set_for_literal;
                break;
            default:
                ;
        }

        return result;
    }
}