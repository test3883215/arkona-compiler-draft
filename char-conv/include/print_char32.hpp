/*
    File:    print_char32.hpp
    Created: 20 March 2021 at 11:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
    License: GPLv3
*/

#pragma once

#include <string>

std::string show_char32(const char32_t c);
void print_char32(const char32_t c);