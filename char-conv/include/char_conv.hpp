/*
    File:    char_conv.hpp
    Created: 20 March 2021 at 11:40 MSK
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
    License: GPLv3
*/

#pragma once

#include <string>

/**
\param [in] utf8str --- UTF-8 string

\return value of the type std::u32string, representing the same string,
but in the encoding UTF-32
*/
std::u32string utf8_to_u32string(const std::string& utf8str);

/**
\param [in] u32str --- string in the encoding UTF-32

\return value of the type std::string, representing the same string,
but in the encoding UTF-8
*/
std::string u32string_to_utf8(const std::u32string& u32str);

/**
\param [in] c --- character in the encoding UTF-32

\return value of the type std::string, consisting of bytes, representing
the same character, but in the encoding UTF-8.
*/
std::string char32_to_utf8(const char32_t c);

/**
\param [in] s --- string in the locale "C"

\return string s in which each char is converted to upper case
*/
std::string to_upper(const std::string& s);

/**
\param [in] s --- string in the locale "C"

\return string s in which each char is converted to lower case
*/
std::string to_lower(const std::string& s);